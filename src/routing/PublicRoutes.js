import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, useLocation } from 'react-router-dom';

const PublicRoutes = ({ children, ...rest }) => {
    let location = useLocation();
    const userRole = useSelector(state => state.user.role);

    if (!userRole) return children;
    return (
        <Redirect
            to={{
                pathname: '/admin/cabinet',
                state: { from: location },
            }}
        />
    );
};

export default PublicRoutes;
