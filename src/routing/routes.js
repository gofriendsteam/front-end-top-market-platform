import React, { Suspense, lazy, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchAllMarketplaces } from '../store/actions/user';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { privateRoutes, publicRoutes } from './routingMap';
import PrivateRoute from './PrivateRoute';
import Loader from '../components/Shared/Loader/Loader';
import withTransition from '../hocs/withTransition';

const AdministratorSide = lazy(() => import('../components/index'));
const UserSide = lazy(() => import('../LandingPage/UserSide'));


const Routes = () => {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(fetchAllMarketplaces());
	}, []);

	return (
		<Suspense fallback={<Loader />}>
			<Router >
				<div className="App">
					<Switch>
						{publicRoutes.map((route, index) => (
							<Route
								key={index}
								exact={route.exact}
								path={route.path}
								render={props => (
									<UserSide>
										<route.component {...props} />{' '}
									</UserSide>
								)}
							/>
						))}

						<PrivateRoute>
							<AdministratorSide>
								<Suspense fallback={<Loader />}>
									<Switch>
										{privateRoutes.map((route, index) => (
											<Route
												key={index}
												exact={route.exact}
												path={route.path}
												component={withTransition(route.component)}
											/>
										))}
									</Switch>
								</Suspense>
							</AdministratorSide>
						</PrivateRoute>
					</Switch>
				</div>
			</Router>
		</Suspense>
	);
};

export default Routes;
