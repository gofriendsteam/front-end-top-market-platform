export default {
	contractorMenu: [
		{
			label: 'Мой кабинет',
			leftIcon: 'icon ion-ios-person',
			key: '/admin/cabinet',
		},
		{
			label: 'Товары',
			leftIcon: 'icon ion-ios-cart',
			key: '/admin/products',
			children: [
				{
					label: 'Все товары',
					leftIcon: 'icon ion-ios-cart',
					key: '/admin/products',
				},
				{
					label: 'История загрузок',
					leftIcon: 'icon ion-ios-cart',
					key: '/admin/products/download_history',
				},
			]
		},
		{
			label: 'Мои заказы',
			leftIcon: 'icon ion-ios-clipboard',
			key: '/admin/contractor_orders',
		},
		{
			label: 'Финансы',
			leftIcon: 'icon ion-ios-pie',
			key: '/admin/finance',
		},
		{
			label: 'База знаний',
			leftIcon: 'icon ion-md-globe',
			key: '/admin/knowledge_base',
		},
		// {
		// 	title: 'Доп. услуги',
		// 	icon: services,
		// 	activeIcon: servicesActive,
		// 	href: 'additional_services',
		// 	developing: true,
		// },
		// {
		// 	title: 'Обуч. модуль',
		// 	icon: 'read',
		// 	// activeIcon: studyActive,
		// 	href: 'learning',
		// 	developing: true,
		// },
		// {
		// 	title: 'Контактная форма',
		// 	icon: сontact,
		// 	activeIcon: сontactActive,
		// 	href: 'contacts-form',
		// },
	],

	partnerMenu: [
		{
			label: 'Мой кабинет',
			leftIcon: 'icon ion-ios-person',
			key: '/admin/cabinet',
		},
		{
			label: 'Товары',
			leftIcon: 'icon ion-ios-cart',
			key: '/admin/categories',
			children: [
				{
					label: 'Все товары',
					leftIcon: 'icon ion-ios-cart',
					key: '/admin/categories',
				},
				{
					label: 'Мои товары',
					leftIcon: 'icon ion-md-basket',
					key: '/admin/my_products',
				},
				{
					label: 'История загрузок',
					leftIcon: 'icon ion-md-alarm',
					key: '/admin/my_products/download_history',
				},
			],
		},
		// {
		// 	label: 'Мои товары',
		// 	leftIcon: 'icon ion-md-basket',
		// 	key: '/admin/my_products',
		//
		// },
		{
			label: 'Мой магазин',
			leftIcon: 'icon ion-md-globe',
			key: '/admin/store',
		},
		{
			label: 'Мои заказы',
			leftIcon: 'icon ion-md-appstore',
			key: '/admin/orders',
		},
		{
			label: 'Финансы',
			leftIcon: 'icon ion-ios-pie',
			key: '/admin/finance',
		},
		{
			label: 'База знаний',
			leftIcon: 'icon ion-md-globe',
			key: '/admin/knowledge_base',
		},
		{
			label: 'Поставщики',
			leftIcon: 'icon ion-md-contacts',
			key: '/admin/suppliers',
		},
		// {
		// 	title: 'Доп. услуги',
		// 	icon: services,
		// 	activeIcon: servicesActive,
		// 	href: 'additional_services',
		// 	developing: false,
		// },
		// {
		// 	title: 'Обуч. модуль',
		// 	icon: 'read',
		// 	// activeIcon: studyActive,
		// 	href: 'learning',
		// 	developing: false,
		// },
		// {
		//   title: 'Контактная форма',
		//   icon: сontact,
		//   activeIcon: сontactActive,
		//   href: 'contacts-form'
		// }
	],
};
