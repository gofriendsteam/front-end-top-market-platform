import React, { lazy } from 'react';

export const privateRoutes = [
	{
		component: lazy(() => import('../components/Pages/Cabinet/Cabinet')),
		path: '/admin/cabinet',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/SendSupplier/SendSupplier'),
		),
		path: '/admin/sendsupplier',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/KnowledgeBase/KnowledgeBase'),
		),
		path: '/admin/knowledge_base',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/ProfileSettings/ProfileSettings'),
		),
		path: '/admin/profile_settings',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/CompanySettings/CompanySettings'),
		),
		path: '/admin/company_settings',
		exact: false,
	},
	{
		component: lazy(() => import('../components/Pages/Cart/Cart')),
		path: '/admin/cart',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/AdditionalServices/AdditionalServices'),
		),
		path: '/admin/additional_services',
		exact: false,
	},
	{
		component: lazy(() => import('../components/Pages/Employees/Employees')),
		path: '/admin/employees',
		exact: false,
	},
	{
		component: lazy(() => import('../components/Pages/Finance/Finance')),
		path: '/admin/finance',
		exact: false,
	},

	{
		component: lazy(() => import('../components/Pages/Store/Store')),
		path: '/admin/store',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Categories/NewCategories'),
		),
		path: '/admin/categories',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/SubcategoryPages/SubcategoryPages'),
		),
		path: '/admin/categories/:id',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/LearningModule/LearningModule'),
		),
		path: '/admin/learning',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Instruction/Instruction'),
		),
		path: '/admin/instruction',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Instruction/InstructionsForSellers'),
		),
		path: '/admin/instruction_sellers',
		exact: false,
	},
	{
		component: lazy(() => import('../components/Pages/Orders/Orders')),
		path: '/admin/orders',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/ContractorOrders/ContractorOrders'),
		),
		path: '/admin/contractor_orders',
		exact: false,
	},
	{
		component: lazy(() => import('../LandingPage/ContactsForm2/ContactsForm')),
		path: '/admin/contacts-form',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/ContractorProducts/NewContractorProducts'),
		),
		path: '/admin/products',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/ContractorProducts/DownloadHistory'),
		),
		path: '/admin/products/download_history',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/SubcategoryPages/SubcategoryPages'),
		),
		path: '/admin/products/:id',
		exact: false,
	},

	{
		component: lazy(() =>
			import(
				'../components/Pages/MyProducts/components/DownloadHistoryPartner'
			),
		),
		path: '/admin/my_products/download_history',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/MyProducts/NewMyProducts'),
		),
		path: '/admin/my_products',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/SubcategoryPages/SubcategoryPages'),
		),
		path: '/admin/my_products/:id',
		exact: true,
	},

	{
		component: lazy(() =>
			import('../components/Pages/MyContractors/MyContractors'),
		),
		path: '/admin/suppliers',
		exact: true,
	},
];

export const publicRoutes = [
	{
		component: lazy(() => import('../components/Pages/Auth/Login/Login')),
		path: '/',
		exact: true,
	},
	{
		component: lazy(() => import('../components/Pages/Auth/Login/Login')),
		path: '/login',
		exact: true,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Auth/Registration/Registration'),
		),
		path: '/registration',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Auth/ResetPassword/ResetPassword'),
		),
		path: '/reset_password',
		exact: false,
	},
	{
		component: lazy(() =>
			import('../components/Pages/Auth/Registration/ConfirmRegistration'),
		),
		path: '/confirm_email',
		exact: false,
	},
];
