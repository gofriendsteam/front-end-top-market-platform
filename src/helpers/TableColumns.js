import { Tooltip, Icon } from 'antd';
import React from 'react';
import defaultImg from '../img/defaultimage.png';

const colors = {
	green: {
		color: 'green',
		fontSize: 20,
	},
	red: {
		color: 'red',
		fontSize: 20,
	},
};

export const getPrice = price => (price ? price : '0.00');
export const getCount = count => (count ? count + ' шт.' : '0 шт.');
export const getBrand = brand =>
	brand ? brand : <span className="table-error-value">Отсутствует</span>;
export const getVendor = vendorCode =>
	vendorCode.length > 7 ? (
		<Tooltip placement="bottom" title={vendorCode}>
			{vendorCode.slice(0, 7) + '...'}
		</Tooltip>
	) : !vendorCode ? (
		<span className="table-error-value">Отсутствует</span>
	) : (
		vendorCode
	);
export const getDescription = (name, item) => (
	<div className="product-avatar">
		<div className="product-avatar-block">
			<img
				src={
					item.avatarUrl ||
					(item.coverImages.length > 0
						? item.coverImages[0].imageDecoded
						: item.imageUrls.length > 0
						? item.imageUrls[0].url
						: defaultImg)
				}
				alt=""
			/>
		</div>
		<div>
			{name.length > 50 ? (
				<Tooltip placement="bottom" title={name}>
					{name.slice(0, 50) + '...'}
				</Tooltip>
			) : (
				name
			)}
		</div>
	</div>
);
export const getCategory = category => (
	<span>
		{category ? (
			category.name.length > 25 ? (
				<Tooltip placement="bottom" title={category.name}>
					{category.name.slice(0, 25) + '...'}
				</Tooltip>
			) : (
				category.name
			)
		) : (
			<span className="table-error-value">Отсутствует</span>
		)}
	</span>
);
export const getPriceWithDiscount = (price, item) =>
	price
		? (+price * ((100 + +item.partnerPercent) / 100)).toFixed(2)
		: (
				+item.contractorPriceForPartner *
				((100 + +item.partnerPercent) / 100)
		  ).toFixed(2);
export const getDiscount = (partnerPercent, item) =>
	item.contractorPriceForPartner
		? `${((+item.contractorPriceForPartner * +partnerPercent) / 100).toFixed(
				2,
		  )} \n (${partnerPercent}%)`
		: `${((+item.price * +partnerPercent) / 100).toFixed(
				2,
		  )} \n (${partnerPercent}%)`;

export const isInYml = isYml =>
	isYml ? (
		<Icon style={colors.green} type="check" />
	) : (
		<Icon style={colors.red} type="minus-circle" />
	);
export const getName = name =>
	name ? name : <span className="table-error-value">Отсутствует</span>;
