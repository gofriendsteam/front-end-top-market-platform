export const getBase64 = (file, cb) => {
	let reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function() {
		cb(reader.result);
	};
	reader.onerror = function(error) {
		console.log('Error: ', error);
	};
};
const renaming = item =>
	!item.subcategories.length // функция для ренейминга входящих данных в вид, подходящий для ant.design Cascader
		? {
				label: item.name,
				value: item.id,
		  }
		: {
				label: item.name,
				value: item.id,
				children: item.subcategories.map(renaming),
		  };

export const renameCategories = categories => categories.map(renaming);

export const shouldDeleteError = (error, field, callback) => {
	if (error && error[field]) {
		const newError = { ...error };
		delete newError[field];

		callback(newError);
	}
};
