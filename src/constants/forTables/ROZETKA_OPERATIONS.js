export const ROZETKA_OPERATIONS = [
  '',
  'Резервирование суммы по сделанному заказу',
  'Комиссия за продажу',
  'Снятие резерва за невыполненный заказ',
  'Пополнение счета',
  'Списание абонплаты',
  'Корретировка баланса',
  'Корректировка заказа',
  'Изменение количества в заказе',
  'Возврат резерва за отменённый товар',
  'Резервирование суммы по добавленному товару',
  'Корретировка счета абонплаты (проведена вручную)',
  'Пополнение счета за доступ к платформе',
  'Автокорректировка заказа',
  'Возврат заказа',
  'Корректировка роялти по заказу',
  'Корректировка баланса роялти'
];
