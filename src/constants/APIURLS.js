const SERVERS = {
	PRODUCT: {
		// XHR: "https://api.smartlead.top/api/v1/",
		XHR: 'https://api.topmarket.ua/api/v1/',
	},
	DEV: {
		XHR: 'https://api.topmarket.ua/api/v1/',
		// XHR: 'http://192.168.1.172:8000/api/v1/',
	},
};

export const BASE_URL = /\bdev\b|\blocalhost\b/.test(document.location.hostname)
	? // ? SERVERS.DEV.XHR /* <=== set here server what needs for developing -  */
	  SERVERS.DEV.XHR /* <=== set here server what needs for developing -  */
	: SERVERS.PRODUCT.XHR;

/****************************/

// USER
export const LOGIN = 'login/';
export const REGISTRATION = 'register/';
export const CONFIRM_EMAIL = 'activate/';
export const PROFILE = 'profile/';
export const PASSWORD = 'password_change/';
export const RESET_PASSWORD = 'password_reset/';
export const CONTACT_FORM = 'marketplace/base/contact_us/';
export const GET_POCKETS = 'marketplace/pockets/';
export const ALL_MARKET_PLACES = 'marketplaces/';

//RozetkaPRom Acccaunt
export const ROZETKA_ACCOUNT = 'cabinet/rozetka/';

// FINANCE BALANCE
export const BALANCE = 'profile/balance';

// COMPANY
export const COMPANY_PROFILE = 'company/detail/';
export const UPDATE_COMPANY_PROFILE = 'company/update/';
export const GET_COMPANY_TYPE = 'company/company_type/';
export const GET_ACTIVITY_AREAS = 'company/activity_areas/';
export const GET_SERVICE_INDUSTRY = 'company/service_industry/';
export const COMPANY_DOCUMENTS = 'company/documents/';
export const COMPANY_PITCH = 'company/pitch/';

// PRODUCTS
export const CONTRACTOR_PRODUCTS = 'catalog/contractor_products/';
export const PARTNER_PRODUCTS = 'catalog/partner_products/';
export const ALL_PRODUCTS = 'catalog/partner_products/products_by_contractors/';
export const CONTRACTOR_CATEGORIES =
	'catalog/contractor_products/contractor_categories/';
export const UPLOAD_PRODUCTS = 'catalog/products_upload/';
export const REMOVE_CONTRACTOR_PRODUCTS =
	'catalog/contractor_products/delete_list_of_products/';
export const REMOVE_PARTNER_PRODUCTS =
	'catalog/partner_products/delete_list_of_products/';
export const NEW_PRODUCTS = 'catalog/contractor_products/';
export const DOWNLOADS_STATUS = 'catalog/contractor_products/upload_history/';
export const DOWNLOADS_STATUS_PARTNER =
	'catalog/partner_products/upload_history/';
export const COPY_TO_MY_PRODUCTS =
	'catalog/partner_products/copy_to_my_products/';
export const ADD_MY_STORE = 'catalog/partner_products/add_to_mysite/';
export const UPDATE_ALL_SELECTED_PRODUCTS = 'catalog/multiply_change_product/';
// YML
export const YML = 'catalog/yml-handler/';
export const DELETE_PRODUCTS_FROM_YML =
	'catalog/yml-handler/delete_products_from_yml/';

// CATEGORIES
export const ALL_CATEGORIES = 'catalog/categories/';
export const FIRST_LEVEL_CATEGORIES = 'catalog/categories/first_level/';

// OPTIONS
export const OPTIONS_BY_CATEGORY = 'catalog/options_by_category/';
export const OPTIONS_BY_CATEGORY_WITH_TEXT_AREA =
	'catalog/options_by_category_with_text_area/';

// ORDERS
export const ORDERS = 'orders/';
export const PASS_TO_CONTRACTOR = id => `orders/${id}/pass_to_contractor/`;
export const CONTRACTOR_ORDERS = 'orders_contractor/';

// STORE
export const STORE = 'my_store/create/';
export const UPDATE_STORE = 'my_store/';

// FINANCE GET MONEY
export const PACK_INVOICE = 'payments/buy_pocket_by_invoice/'; // Продавец.  Для покупки пакетов.    Cчёт-фактура. ИСПОЛЬЗУЕТСЯ
export const PACK_LIQPAY = 'payments/pocket-pay/'; // Продавец.  Для покупки пакетов.    LiqPay.       ИСПОЛЬЗУЕТСЯ
export const UNBALANCES = 'payments/application_to_unbalances/'; // Продавец.  Для вывода средств.     Карта.         ИСПОЛЬЗУЕТСЯ
export const RECHARGE_INVOICE = 'payments/recharge_balance_by_invoice/'; // Поставщик. Для пополнения баланса. Cчёт-фактура. ИСПОЛЬЗУЕТСЯ
export const RECHARGE_LIQPAY = 'payments/recharge-balance-pay/'; // Поставщик. Для пополнения баланса. LiqPay.       ИСПОЛЬЗУЕТСЯ

// FINANCE HISTORY
export const TRANSACTIONS_FULL = 'payments/transactions/full/';
export const TRANSACTIONS_RECHARGE_BY_LIQPAY =
	'payments/transactions/recharge_by_liqpay/';
export const RECHARGE_BY_INVOICE = 'payments/recharge_by_invoice/';
// FINANCE HISTORY. ROZETKA
export const ROZETKA_TRANSACTIONS_HISTORY =
	'payments/transactions/rozetka/trans_history/';
export const ROZETKA_INVOICES = 'payments/transactions/rozetka/invoices/';
export const ROZETKA_SALES_REPORTS =
	'payments/transactions/rozetka/reports_files/';

export const ORDERS_HISTORY = 'payments/transactions/order/';

// ABC
export const ABC_LIST = 'abc/content/list/';
export const ABC_ITEM = 'abc/item/detail/';

// NOVA POSHTA
export const NOVA_POSHTA = 'company/nova_poshta/';

//MY_CONTRACTORS
export const MY_CONTRACTORS = 'contractors/my_contractors/';

//CHAT
export const MESSAGE_CREATE = "/create_message/"
export const CHAT = 'chat/partner/';
