export const colors = {
    '1': '#cbbe1d',
    '2': '#770b85',
    '5': '#fb3f4c',
    '6': '#12bd0d',
    '11': '#fb3f4c',
    '12': '#999',
    '18': '#fb3f4c',
    '26': '#3860f6',
    '28': '#fb3f4c',
    '29': '#999',
};