import moment from 'moment';
import { statusList } from '../orderStatus';
import styles from '../../components/Pages/Orders/Orders.module.css';
import { colors } from '../orderColors';
import { Icon, Popover, Timeline } from 'antd';
import React from 'react';
import TimelineBlock from '../../components/Pages/Orders/components/TimelineBlock';
import StatusBlock from '../../components/Pages/Orders/components/StatusBlock';

export const orderRozetkaColumn = update => [
	{
		title: '№ заказа',
		dataIndex: 'rozetkaId',
		key: 'rozetkaId',
		render: (a, { rozetkaId, id }) => (
			<span>№ {rozetkaId ? rozetkaId : id}</span>
		),
		width: 150,
	},
	{
		title: 'Дата заказа',
		dataIndex: 'created',
		key: 'created',
		width: 250,
		render: (date, order) => (
			<span>
				{moment(date).format('DD-MM-YYYY HH:mm')}
				<img
					style={{ width: '30px', margin: '0 0 0 30px' }}
					src={order.items.length > 0 ? order.items[0].imageUrl : ''}
					alt=""
				/>
			</span>
		),
	},
	{
		title: 'К-во',
		dataIndex: 'quantity',
		key: 'quantity',
		width: 100,
		render: (item, order) => (
			<span className="product-avatar">
				{order.items.length > 0 ? order.items[0].quantity : ''} шт.
			</span>
		),
	},
	{
		title: 'Сумма',
		dataIndex: 'amount',
		key: 'amount',
		width: 150,
	},
	{
		title: 'Статус заказа',
		dataIndex: 'status',
		key: 'status',
		render: (status, order) => {
			let selectedStatus = statusList.find(item => item.id === status);
			let dates = [];
			let newHistory = [];

			const sortedArr = order.statusHistory.sort(function(a, b) {
				return new Date(b.created) - new Date(a.created);
			});

			for (let i = 0; i < order.statusHistory.length; i++) {
				if (sortedArr.length > 0) {
					if (sortedArr[i + 1]) {
						if (
							moment(sortedArr[i].created).format('YYYY-MM-DD') ===
							moment(sortedArr[i + 1].created).format('YYYY-MM-DD')
						) {
							dates.push(sortedArr[i]);
						} else {
							dates.push(sortedArr[i]);

							newHistory.push({
								title: moment(sortedArr[i].created).format('YYYY-MM-DD'),
								date: dates,
							});

							dates = [];
						}
					}
				}
			}

			return (
				<span className={styles.orderStatusInTable}>
					<span style={{ color: colors[status], fontWeight: 'bold' }}>
						{selectedStatus.title}
						{order.statusChildren.length > 0 && (
							<Popover
								trigger="click"
								// onVisibleChange={val => console.log(val)}
								content={
									<StatusBlock
										update={update}
										statusChildren={order.statusChildren}
										id={order.id}
									/>
								}
							>
								<i className={`${styles.editBtn} icon ion-md-create`} />
							</Popover>
						)}
					</span>

					<Popover content={<TimelineBlock newHistory={newHistory} />}>
						<Icon type="clock-circle" style={{ color: '#4A90E2' }} />
					</Popover>
				</span>
			);
		},
	},
];
export const contractorOrdersColumns = [
	{
		title: '№ заказа',
		dataIndex: 'id',
		key: 'orderNumber',
		render: (date, item) => (
			<span>
				{item.baseOrder.rozetkaId
					? item.baseOrder.rozetkaId
					: item.baseOrder.id}
			</span>
		),
	},
	{
		title: 'Дата заказа',
		dataIndex: 'dateOrder',
		key: 'dateOrder',
		render: (date, item) => (
			<span>{moment(item.baseOrder.created).format('DD-MM-YYYY HH:mm')}</span>
		),
	},
	{
		title: 'ID Продавца',
		dataIndex: 'partnerId',
		key: 'partnerId',
	},
	{
		title: 'Сумма',
		dataIndex: 'amount',
		key: 'amount',
		render: (date, item) => <span>{item.baseOrder.amount}</span>,
	},
	{
		title: 'Статус заказа',
		dataIndex: 'status',
		key: 'status',
		render: (status, order) => {
			let selectedStatus = statusList.find(
				item => item.id === order.baseOrder.status,
			);
			let dates = [];
			let newHistory = [];

			const sortedArr = order.baseOrder.statusHistory.sort(function(a, b) {
				return new Date(b.created) - new Date(a.created);
			});

			for (let i = 0; i < order.baseOrder.statusHistory.length; i++) {
				if (sortedArr.length > 0) {
					if (sortedArr[i + 1]) {
						if (
							moment(sortedArr[i].created).format('YYYY-MM-DD') ===
							moment(sortedArr[i + 1].created).format('YYYY-MM-DD')
						) {
							dates.push(sortedArr[i]);
						} else {
							dates.push(sortedArr[i]);

							newHistory.push({
								title: moment(sortedArr[i].created).format('YYYY-MM-DD'),
								date: dates,
							});

							dates = [];
						}
					}
				}
			}

			return (
				<span className={styles.orderStatusInTable}>
					<span style={{ color: status === 5 ? '#02850e' : '#cbbe1d' }}>
						{selectedStatus.title}
						{order.baseOrder.statusChildren.length > 0 && (
							<Popover
								trigger="click"
								content={
									<StatusBlock
										// update={update}
										statusChildren={order.baseOrder.statusChildren.map(
											child => ({
												label: child.name,
												value: child.id,
											}),
										)}
										id={order.baseOrder.id}
									/>
								}
							>
								<i className={`${styles.editBtn} icon ion-md-create`} />
							</Popover>
						)}
					</span>
					<Popover content={<TimelineBlock newHistory={newHistory} />}>
						<Icon type="clock-circle" style={{ color: '#4A90E2' }} />
					</Popover>
				</span>
			);
		},
	},
];
