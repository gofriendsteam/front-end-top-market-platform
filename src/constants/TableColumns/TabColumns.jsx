import React, { Fragment } from 'react';
import { Icon, Tooltip } from 'antd';
import financeStyles from '../../components/Pages/Finance/Finance.module.css';
import {
	getPrice,
	getCount,
	getBrand,
	getVendor,
	getDescription,
	getCategory,
	getPriceWithDiscount,
	getDiscount,
	isInYml,
	getName,
} from '../../helpers/TableColumns';
import { Link } from 'react-router-dom';
import { ROZETKA_OPERATIONS } from '../forTables/ROZETKA_OPERATIONS';
import CustomButton from '../../components/Shared/Button/Button';

const months = [
	'январь',
	'февраль',
	'март',
	'апрель',
	'май',
	'июнь',
	'июль',
	'август',
	'сентябрь',
	'октябрь',
	'ноябрь',
	'декабрь',
];
const convertTimestamp = {
	toPeroidFormat: timestamp => {
		const date = new Date(timestamp);
		return `${months[date.getMonth()]} ${date.getFullYear()} г.`;
	},
	toDateFormat: timestamp => {
		const date = new Date(timestamp);
		return `${(date.getDate() + '').padStart(2, 0)}.${(
			date.getMonth() +
			1 +
			''
		).padStart(2, 0)}.${date.getFullYear()}`;
	},
	toTimeFormat: timestamp => {
		const date = new Date(timestamp);
		return `${(date.getDate() + '').padStart(2, 0)}.${(
			date.getMonth() +
			1 +
			''
		).padStart(2, 0)}.${date.getFullYear()}
    ${(date.getHours() + '').padStart(2, 0)}:${(
			date.getMinutes() + ''
		).padStart(2, 0)}`;
	},
};

const TRANSACTION_SOURCES = [
	'',
	'Пополнение баланса',
	'Покупка пакета',
	'Вывод средств',
	'Успешный заказ',
	'Возврат товара',
	'Покупка модуля',
];
const DOWNLOAD_HISTORY = {
	yml: 'Rozetka YML',
	prom_xml: 'Prom YML',
	inner: 'XLS',
	// rozetka: 'Rozetka',
};

export const COLUMNS = {
	financeColumns: {
		contractor: {
			invoicesForPayment: [
				{
					title: 'Дата',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
					// sorter: STRING('date'),
					// width: 200,
				},
				{
					title: 'Тип',
					dataIndex: 'liqpayOrderId',
					key: 'liqpayOrderId',
					render: liqpayOrderId => (liqpayOrderId ? 'LiqPay' : 'Счет-фактура'),
					// filters: FINANCE_FILTERS.contractor.invoice.type,
					// onFilter: onFilter.indexOf('type'),
					// width: 200,
				},
				{
					title: 'ID оплаты',
					dataIndex: 'id',
					key: 'id',
					// sorter: STRING('accountNumber'),
					// width: 200,
				},
				{
					title: 'Сумма',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => (
						<div style={{ textAlign: 'end' }}>{(+amount).toFixed(2)}</div>
					),
					// sorter: STRING('cost'),
					width: 100,
				},
				{
					title: 'Статус',
					dataIndex: 'isApproved',
					key: 'isApproved',
					render: isApproved =>
						isApproved ? (
							<span style={{ color: 'green' }}>APPROVED</span>
						) : (
							<span style={{ color: 'red' }}>NOT APPROVED</span>
						),
					// filters: FINANCE_FILTERS.contractor.invoice.status,
					// onFilter: onFilter.indexOf('status'),
					// width: 150,
				},
				{
					dataIndex: 'invoiceFile',
					key: 'invoiceFile',
					render: invoiceFile => (
						<span className={financeStyles.icons}>
							<Tooltip title="Скачать файл">
								<a href={invoiceFile} download>
									<Icon
										type="download"
										className={financeStyles.icons_download}
									/>
								</a>
							</Tooltip>
							{/* <Tooltip title={<span style={{textAlign: "center"}}><div>Загрузить файл</div>подтверждения оплаты</span>}>
                <Icon type="upload" className={financeStyles.icons_upload}/>
              </Tooltip> */}
						</span>
					),
					// width: 100,
				},
			],
			transactionsHistory: [
				{
					title: 'Тип операции',
					dataIndex: 'source',
					key: 'source',
					// filters: FINANCE_FILTERS.contractor.transactionsHistory.type,
					// onFilter: onFilter.indexOf('type'),
					// width: 350,
				},
				{
					title: 'ID операции',
					dataIndex: 'id',
					key: 'id',
					// sorter: NUMBER('orderNumber'),
					// width: 150,
				},
				{
					title: 'Дата операции',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
					// sorter: STRING('date'),
					// width: 300,
				},
				{
					title: 'Сумма',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => amount.split('.')[0],
					// sorter: NUMBER('change'),
				},
			],
			mutualSettlements: [
				{
					title: 'Дата',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
				},
				{
					title: 'ID продавца',
					dataIndex: 'partner',
					key: 'partner',
				},
				{
					title: 'ID заказа',
					dataIndex: 'systemOrder',
					key: 'systemOrder',
				},
				{
					title: 'Сумма',
					dataIndex: 'totalSum',
					key: 'totalSum',
					render: totalSum => (+totalSum).toFixed(2),
					// }, {
					//   title: 'Коммисия Rozetka',
					//   dataIndex: 'id',
					//   key: 'id',
				},
				{
					title: 'Отчисления продавцу',
					dataIndex: 'partnerAndMarketplacePart',
					key: 'partnerAndMarketplacePart',
					render: sum => (+sum).toFixed(2),
				},
				{
					title: 'Остаток',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => (+amount).toFixed(2),
				},
				{
					title: 'Баланс',
					dataIndex: 'zero',
					key: 'zero',
					// }, {
					//   title: 'Дополнительные расходы',
					//   dataIndex: 'zero',
					//   key: 'zero',
				},
				{
					title: 'Статус',
					dataIndex: 'source',
					key: 'source',
					render: code => TRANSACTION_SOURCES[code],
				},
			],
		},
		partner: {
			transactionsHistory: [
				{
					title: 'Тип операции',
					dataIndex: 'source',
					key: 'source',
					// filters: FINANCE_FILTERS.partner.transactionsHistory.source,
					// onFilter: onFilter.indexOf('source'),
					// width: 350,
				},
				{
					title: 'ID операции',
					dataIndex: 'id',
					key: 'id',
					// sorter: NUMBER('orderNumber'),
					// width: 300,
				},
				{
					title: 'Дата операции',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
					// sorter: STRING('date'),
					// width: 300,
				},
				{
					title: 'Сумма',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => (+amount).toFixed(2),
					// sorter: NUMBER('change'),
				},
			],
			withdrawalRequests: [
				{
					title: 'Дата',
					dataIndex: 'date',
					key: 'date',
					// sorter: STRING('date'),
					// width: 200,
				},
				{
					title: 'Тип',
					dataIndex: 'type',
					key: 'type',
					// filters: FINANCE_FILTERS.partner.withdrawalRequests.type,
					// onFilter: onFilter.indexOf('type'),
					// width: 200,
				},
				{
					title: 'Номер заявки',
					dataIndex: 'requestNumber',
					key: 'requestNumber',
					// sorter: STRING('requestNumber'),
					// width: 200,
				},
				{
					title: 'Сумма',
					dataIndex: 'cost',
					key: 'cost',
					// sorter: STRING('cost'),
					// width: 150,
				},
				{
					title: 'Статус',
					dataIndex: 'status',
					key: 'status',
					// width: 150,
					// filters: FINANCE_FILTERS.partner.withdrawalRequests.status,
					// onFilter: onFilter.indexOf('status'),
				},
			],
			rozetkaTransactionsHistory: [
				{
					title: 'Номер операции',
					dataIndex: 'logId',
					key: 'log_id',
					// width: 200,
				},
				{
					title: 'Дата операции',
					dataIndex: 'transactionTs',
					key: 'transaction_ts',
					render: transactionTs => convertTimestamp.toTimeFormat(transactionTs),
					// width: 200,
				},
				{
					title: 'Тип операции',
					dataIndex: 'operationType',
					key: 'operation_type',
					render: operationType => (
						<span style={{ color: '#999' }}>
							{ROZETKA_OPERATIONS[operationType] || operationType}
						</span>
					),
					// filters: FINANCE_FILTERS.partner.rozetkaTransactionsHistory.operationType,
					// width: 150,
				},
				{
					title: 'ID заказа',
					dataIndex: 'orderId',
					key: 'order_id',
					render: (cell, row) => {
						if (row.operationType === 4) return null;
						if (row.operationType === 5) return null;
						return cell;
					},
					// width: 150,
				},
				{
					title: 'ID товара',
					dataIndex: 'productId',
					key: 'productId',
					render: (cell, row) => {
						if (row.operationType === 4) return null;
						if (row.operationType === 5) return null;
						return cell;
					},
					// width: 150,
				},
				{
					title: 'Цена',
					dataIndex: 'price',
					key: 'price',
					render: (cell, row) => {
						if (row.operationType === 4) return null;
						if (row.operationType === 5) return null;
						return (+cell).toFixed(2);
					},
					// width: 150,
				},
				{
					title: 'Количество',
					dataIndex: 'quantity',
					key: 'quantity',
					render: (cell, row) => {
						if (row.operationType === 4) return null;
						if (row.operationType === 5) return null;
						if (row.operationType === 9) return null;
						return cell;
					},
					// width: 150,
				},
				{
					title: 'Общая стоимость',
					dataIndex: 'cost',
					key: 'cost',
					render: (cell, row) => {
						if (row.operationType === 4) return null;
						if (row.operationType === 5) return null;
						if (row.operationType === 9) return null;
						return (+cell).toFixed(2);
					},
					// width: 150,
				},
				{
					title: 'Начисление',
					dataIndex: 'balanceChanged',
					key: 'balanceChanged',
					render: (cell, row) => {
						switch (row.operationType) {
							case 1:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);
							case 2:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#999' }}
									>
										{(+cell * -1).toFixed(2)}
									</span>
								);
							case 3:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#999' }}
									>
										{(+row.sumInGray).toFixed(2)}
									</span>
								);
							case 4:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										{(+row.balanceChanged).toFixed(2)}
									</span>
								);
							case 5:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);
							case 9:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#999' }}
									>
										{(+row.sumInGrayChanged * -1).toFixed(2)}
									</span>
								);
							case 14:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										{(+cell).toFixed(2)}
									</span>
								);
							default:
								return null;
							// break;
						}
					},

					// width: 100,
				},
				{
					title: 'Списание',
					dataIndex: 'sumInGrayChanged',
					key: 'sumInGrayChanged',
					render: (cell, row) => {
						switch (row.operationType) {
							case 1:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#999' }}
									>
										{(+cell).toFixed(2)}
									</span>
								);
							case 2:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										{(+cell * -1).toFixed(2)}
									</span>
								);
							case 3:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);
							case 4:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);
							case 5:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										{(+row.balanceChanged * -1).toFixed(2)}
									</span>
								);
							case 9:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);
							case 14:
								return (
									<span
										style={{ fontSize: 13, fontWeight: 400, color: '#000' }}
									>
										–
									</span>
								);

							default:
								return null;
							// break;
						}
					},
					// width: 100,
				},
				{
					title: 'Баланс Rozetka',
					dataIndex: 'currentBalance',
					key: 'currentBalance',
					render: (cell, row) => {
						if (row.operationType === 5) return null;
						return (
							<Fragment>
								<span style={{ fontSize: 15, fontWeight: 700 }}>
									{(+cell + +row.sumInGray).toFixed(2)}
								</span>
								<br />
								<span style={{ fontSize: 13, fontWeight: 400, color: '#999' }}>
									(-{(+row.sumInGray).toFixed(2)})
								</span>
							</Fragment>
						);
					},
					// width: 100,
				},
			],
			rozetkaInvoices: [
				{
					title: 'Дата',
					dataIndex: 'dateOfInvoice',
					key: 'dateOfInvoice',
					render: dateOfInvoice => convertTimestamp.toTimeFormat(dateOfInvoice),
					// width: 150,
				},
				{
					title: 'Тип',
					dataIndex: 'type',
					key: 'type',
					// width: 150,
				},
				{
					title: 'Номер счета',
					dataIndex: 'number',
					key: 'number',
					// width: 200,
				},
				{
					title: 'Сумма счета',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => (+amount).toFixed(2),
					// width: 150,
				},
				{
					title: 'Статус оплаты',
					dataIndex: 'status',
					key: 'status',
				},
				{
					title: '',
					dataIndex: 'fileInvoice',
					key: 'fileInvoice',
					render: fileInvoice => (
						<span className={financeStyles.icons}>
							<Tooltip placement="top" title="Скачать файл">
								<a href={fileInvoice} style={{ width: 'inherit' }}>
									<Icon
										type="download"
										className={financeStyles.icons_download}
									/>
								</a>
							</Tooltip>
						</span>
					),
					// width: 70,
				},
			],
			rozetkaSalesReports: [
				{
					title: 'ID',
					dataIndex: 'id',
					key: 'id',
					// width: 150,
				},
				{
					title: 'Rozetka ID',
					dataIndex: 'rozetkaId',
					key: 'rozetkaId',
					// width: 150,
				},
				{
					title: 'Период отсчета',
					dataIndex: 'reportPeriod',
					key: 'reportPeriod',
					render: reportPeriod => convertTimestamp.toPeroidFormat(reportPeriod),
					// width: 150,
				},
				{
					title: 'Дата создания',
					dataIndex: 'createdAt',
					key: 'createdAt',
					render: createdAt => convertTimestamp.toTimeFormat(createdAt),
					// width: 150,
				},
				{
					title: 'Статус',
					dataIndex: 'status',
					key: 'status',
				},
				{
					title: '',
					dataIndex: 'reportFile',
					key: 'reportFile',
					render: reportFile => (
						<span className={financeStyles.icons}>
							<Tooltip title="Скачать файл">
								<a href={reportFile} style={{ width: 'inherit' }}>
									<Icon
										type="download"
										className={financeStyles.icons_download}
									/>
								</a>
							</Tooltip>
						</span>
					),
					// width: 118,
				},
			],
			mutualSettlements: [
				{
					title: 'Дата',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
				},
				{
					title: 'ID поставщика',
					dataIndex: 'contractor',
					key: 'contractor',
				},
				{
					title: 'ID заказа',
					dataIndex: 'systemOrder',
					key: 'systemOrder',
				},
				{
					title: 'Сумма',
					dataIndex: 'totalSum',
					key: 'totalSum',
					render: totalSum => (+totalSum).toFixed(2),
					// }, {
					//   title: 'Коммисия Rozetka',
					//   dataIndex: 'id',
					//   key: 'id',
				},
				{
					title: 'Отчисления поставщику',
					dataIndex: 'contractorAndMarketplacePart',
					key: 'contractorAndMarketplacePart',
					render: sum => (+sum).toFixed(2),
				},
				{
					title: 'Остаток',
					dataIndex: 'amount',
					key: 'amount',
					render: amount => (+amount).toFixed(2),
				},
				{
					title: 'Баланс',
					dataIndex: 'zero',
					key: 'zero',
					// }, {
					//   title: 'Дополнительные расходы',
					//   dataIndex: 'zero',
					//   key: 'zero',
				},
				{
					title: 'Статус',
					dataIndex: 'source',
					key: 'source',
					render: code => TRANSACTION_SOURCES[code],
				},
			],
		},
	},
	contractorProducts: {
		contractor: {
			uploadsHistory: [
				{
					title: 'Дата',
					dataIndex: 'created',
					key: 'created',
					render: created => convertTimestamp.toTimeFormat(created),
					width: 200,
				},
				{
					title: 'Тип файла',
					dataIndex: 'fileType',
					key: 'file_type',
					className: 'column-middle',
					render: file_type => DOWNLOAD_HISTORY[file_type],
					width: 150,
				},
				{
					title: 'Количество товаров в документе',
					dataIndex: 'totalProductsCount',
					key: 'total_products_count',
					className: 'column-middle',
				},
				{
					title: 'Количество товаров загруженных на платформу',
					dataIndex: 'importedProductsCount',
					key: 'imported_products_count',
					className: 'column-middle',
				},
				{
					title: 'Статус загрузки',
					dataIndex: 'isUploaded',
					key: 'is_uploaded',
					className: 'column-middle',
					render: is_uploaded =>
						is_uploaded ? (
							<Icon type="check" style={{ fontSize: '24px', color: 'green' }} />
						) : (
							<Icon
								type="loading"
								style={{ fontSize: '24px', color: '#4A90E2' }}
							/>
						),
				},
				{
					title: 'Ошибки',
					dataIndex: 'errors',
					key: 'errors',
					className: 'column-middle',
					render: errors =>
						!errors || errors === 'No errors' ? (
							<Icon type="check" style={{ fontSize: '24px', color: 'green' }} />
						) : (
							<Tooltip title={errors}>
								<Icon
									type="warning"
									style={{ fontSize: '24px', color: '#ffae42' }}
								/>
							</Tooltip>
						),
				},
				{
					title: 'Файл',
					dataIndex: 'xlsFile',
					key: 'xls_file',
					className: 'column-middle',
					render: xls_file => (
						<a href={xls_file} download>
							<Icon
								type="file"
								style={{ fontSize: '24px', color: '#4A90E2' }}
							/>
						</a>
					),
				},
			],
		},
	},
};

export const MyContractorsColumn = openChat => [
	{
		title: 'ID Поставщика',
		key: 'id',
		dataIndex: 'id',
		// render: getDescription,
		width: 150,
		render: id => (
			<Link
				to={{
					pathname: '/admin/my_products',
					state: {
						contractorId: id,
					},
				}}
			>
				#{id}
			</Link>
		),
	},
	{
		title: 'Контакты',
		dataIndex: 'Contacts',
		// render: getDescription,
		children: [
			{
				title: 'ФИО',
				dataIndex: 'name',
				key: 'name',
				width: 150,
				render: getName,
				// sorter: (a, b) => a.age - b.age,
			},
			{
				title: 'Телефон',
				dataIndex: 'phone',
				key: 'phone',
				width: 150,
				render: getName,
				// sorter: (a, b) => a.age - b.age,
			},
			{
				title: 'E-mail',
				dataIndex: 'email',
				key: 'email',
				width: 150,
				render: getName,
				// sorter: (a, b) => a.age - b.age,
			},
		],
		// width: 300,
	},
	{
		title: 'Сообщения',
		key: 'messages',
		dataIndex: 'messages',
		render: (a, {chatId, id}) => (
			<CustomButton
				onClick={() => openChat(chatId, id)}
				title="Открыть чат"
				type="primary"
				siz="small"
			/>
		),
		// render: getDescription,
		// width: 300,
	},
];

export const MyProductColumn = handleOpenWindow => [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: getDescription,
		width: 300,
	},
	{
		title: 'ID поставщика',
		dataIndex: 'contractorId',
		ellipsis: true,
		width: 120,
		render: id => id || <span className="status-success">Мой товар</span>,
	},
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		ellipsis: true,
		render: getVendor,
	},

	{
		title: 'Бренд',
		dataIndex: 'brand',
		ellipsis: true,
		render: getBrand,
	},
	{
		title: 'Категория',
		dataIndex: 'category',
		ellipsis: true,
		render: getCategory,
	},
	{
		title: 'Кол-во',
		dataIndex: 'count',
		render: getCount,
	},
	{
		title: 'Цена поставщика',
		dataIndex: 'contractorPriceForPartner',
		ellipsis: true,
		render: (price, item) => (item.contractorId ? price : item.price),
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		ellipsis: true,
		// render: (price, item) => (item.contractorId ? price : '-'),
		render: getPrice,
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: getPriceWithDiscount,
	},
	{
		title: 'Наценка',
		dataIndex: 'partnerPercent',
		render: getDiscount,
	},

	{
		title: 'YML',
		width: 120,
		dataIndex: 'isAddedToYmlRozetka',
		render: isInYml,
	},
	{
		title: '',
		dataIndex: 'actions',
		fixed: 'right',
		width: 100,
		render: (actions, item) => (
			// <div>
			// 	<i className="icon ion-md-create" style={{fontSize: 28}}/>
			// </div>
			<CustomButton
				title="Редактировать"
				type="primary"
				size="small"
				className="edit-btn"
				onClick={() => handleOpenWindow(item)}
			/>
		),
	},
];
