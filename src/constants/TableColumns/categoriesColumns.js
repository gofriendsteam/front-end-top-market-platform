
import {
	getPrice,
	getCount,
	getBrand,
	getVendor,
	getDescription,
	getCategory,
} from '../../helpers/TableColumns';
import { Tooltip } from 'antd';
import React from 'react';
import CustomButton from '../../components/Shared/Button/Button';


export const categoriesColumn = [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: getDescription,
		width: 300,
	},
	{
		title: 'ID поставщика',
		dataIndex: 'contractorId',
		ellipsis: true,
	},
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		ellipsis: true,
		render: getVendor,
	},
	{
		title: 'Бренд',
		dataIndex: 'brand',
		ellipsis: true,
		render: getBrand,
	},
	{
		title: 'Категория',
		dataIndex: 'category',
		ellipsis: true,
		render: getCategory,
	},
	{
		title: 'Количество',
		dataIndex: 'count',
		render: getCount,
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: getPrice,
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		ellipsis: true,
		render: getPrice,
		// width: 250
	},
];
export const sendContractorColumn = handleSendToContractor => [
					{
						title: 'Название товара',
						dataIndex: 'name',
						render: getDescription,
						width: 350,
					},
					{
						title: 'ID поставщика',
						dataIndex: 'contractorId',
						ellipsis: true,
					},
					{
						title: 'Артикул',
						dataIndex: 'vendorCode',
						ellipsis: true,
						render: getVendor
					},
					{
						title: 'Бренд',
						dataIndex: 'brand',
						ellipsis: true,
						render: getBrand,
					},
					{
						title: 'Категория',
						dataIndex: 'category',
						ellipsis: true,
						render: getCategory,
					},
					{
						title: 'Кол-во',
						dataIndex: 'count',
						render: getCount,
					},
					{
						title: 'Цена',
						dataIndex: 'price',
						render: getPrice,
					},
					{
						title: 'Рекомендуемая розничная цена',
						dataIndex: 'recommendedPrice',
						ellipsis: true,
						render: getPrice,
						width: 250,
					},
					{
						title: '',
						dataIndex: 'action',
						fixed: 'right', //sendToSupplier()
						render: (a, rec) => console.log(rec) || (
							<CustomButton
								className="edit-btn"
								onClick={() =>
									handleSendToContractor(rec.id)
								}
								title="Отправить"
								type="primary"
							/>
						),
					},
				];

