import React from 'react';
import CustomButton from '../../components/Shared/Button/Button';
import {
	getPrice,
	getCount,
	getBrand,
	getVendor,
	getDescription,
	getCategory,
} from '../../helpers/TableColumns';
export const contractorProductsColumn = openProduct => [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: getDescription,
		width: 300,
	},
	// {
	// 	title: 'ID поставщика',
	// 	dataIndex: 'contractorId',
	// 	ellipsis: true,
	// 	width: 120,
	// },
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		ellipsis: true,
		render: getVendor,
	},
	{
		title: 'Бренд',
		dataIndex: 'brand',
		ellipsis: true,
		render: getBrand,
	},
	{
		title: 'Категория',
		dataIndex: 'category',
		ellipsis: true,
		render: getCategory,
	},
	{
		title: 'Кол-во',
		dataIndex: 'count',
		render: getCount,
		width: 150,
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: getPrice,
		width: 150,
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		// ellipsis: true,
		render: getPrice,
	},
	{
		title: '',
		dataIndex: 'actions',
		fixed: 'right',
		render: (e, product) => (
			<CustomButton
				title="Редактировать"
				type="primary"
				className="edit-btn"
				onClick={() => openProduct(product)}
			/>
		),
	},
];
