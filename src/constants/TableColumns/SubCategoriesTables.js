import { Tooltip } from 'antd';
import React from 'react';
import CustomButton from '../../components/Shared/Button/Button';

export const columns_contractors = openProduct => [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: (name, item) => (
			<div className="product-avatar" style={{ minWidth: 230 }}>
				<div className="product-avatar-block">
					<img
						src={
							item.avatarUrl ||
							(item.coverImages.length > 0
								? item.coverImages[0].imageDecoded
								: item.imageUrls.length > 0
								? item.imageUrls[0].url
								: '')
						}
						alt=""
					/>
				</div>
				<span>
					{name.length > 50 ? (
						<Tooltip placement="bottom" title={name}>
							{name.slice(0, 50) + '...'}
						</Tooltip>
					) : (
						name
					)}
				</span>
			</div>
		),
		width: 350,
	},
	{
		title: 'ID поставщика',
		dataIndex: 'contractorId',
	},
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		render: vendorCode =>
			vendorCode.length > 7 ? (
				<Tooltip placement="bottom" title={vendorCode}>
					{vendorCode.slice(0, 7) + '...'}
				</Tooltip>
			) : (
				vendorCode
			),
	},
	{
		title: 'Бренд',
		dataIndex: 'brand',
		width: '10%',
	},
	{
		title: 'Категория',
		dataIndex: 'category',
		render: category => (
			<span>
				{category ? (
					category.name.length > 25 ? (
						<Tooltip placement="bottom" title={category.name}>
							{category.name.slice(0, 25) + '...'}
						</Tooltip>
					) : (
						category.name
					)
				) : (
					''
				)}
			</span>
		),
	},
	{
		title: 'Количество',
		dataIndex: 'count',
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: price => price && price.toFixed(2),
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		render: price => price && price.toFixed(2),
	},

	{
		title: '',
		dataIndex: 'actions',
		render: (e, product) => (
			<CustomButton
				title="Редактировать"
				type="primary"
				className="edit-btn"
				onClick={() => openProduct(product)}
			/>
		),
	},
];
export const columns_all = [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: (name, item) => (
			<div className="product-avatar" style={{ minWidth: 230 }}>
				<div className="product-avatar-block">
					<img
						src={
							item.avatarUrl ||
							(item.coverImages.length > 0
								? item.coverImages[0].imageDecoded
								: item.imageUrls.length > 0
								? item.imageUrls[0].url
								: '')
						}
						alt=""
					/>
				</div>
				<span>
					{name.length > 50 ? (
						<Tooltip placement="bottom" title={name}>
							{name.slice(0, 50) + '...'}
						</Tooltip>
					) : (
						name
					)}
				</span>
			</div>
		),
		width: 350,
	},
	{
		title: 'ID поставщика',
		dataIndex: 'contractorId',
	},
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		render: vendorCode =>
			vendorCode.length > 7 ? (
				<Tooltip placement="bottom" title={vendorCode}>
					{vendorCode.slice(0, 7) + '...'}
				</Tooltip>
			) : (
				vendorCode
			),
	},
	{
		title: 'Бренд',
		dataIndex: 'brand',
		width: '10%',
	},
	{
		title: 'Категория',
		dataIndex: 'category',
		render: category => (
			<span>
				{category ? (
					category.name.length > 25 ? (
						<Tooltip placement="bottom" title={category.name}>
							{category.name.slice(0, 25) + '...'}
						</Tooltip>
					) : (
						category.name
					)
				) : (
					''
				)}
			</span>
		),
	},
	{
		title: 'Количество',
		dataIndex: 'count',
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: price => price && price.toFixed(2),
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		render: price => price && price.toFixed(2),
	},
];
export const columns_my = openProduct => [
	{
		title: 'Название товара',
		dataIndex: 'name',
		render: (name, item) => (
			<div className="product-avatar" style={{ minWidth: 230 }}>
				<div className="product-avatar-block">
					<img
						src={
							item.avatarUrl ||
							(item.coverImages.length > 0
								? item.coverImages[0].imageDecoded
								: item.imageUrls.length > 0
								? item.imageUrls[0].url
								: '')
						}
						alt=""
					/>
				</div>
				<span>
					{name.length > 50 ? (
						<Tooltip placement="bottom" title={name}>
							{name.slice(0, 50) + '...'}
						</Tooltip>
					) : (
						name
					)}
				</span>
			</div>
		),
		width: '350px',
	},
	{
		title: 'ID поставщика',
		dataIndex: 'contractorId',
		render: id => id || 'Мой товар',
	},
	{
		title: 'Артикул',
		dataIndex: 'vendorCode',
		render: vendorCode =>
			vendorCode.length > 7 ? (
				<Tooltip placement="bottom" title={vendorCode}>
					{vendorCode.slice(0, 7) + '...'}
				</Tooltip>
			) : (
				vendorCode
			),
	},
	{
		title: 'Бренд',
		dataIndex: 'brand',
		width: '10%',
	},
	{
		title: 'Количество',
		dataIndex: 'count',
	},
	{
		title: 'Цена поставщика',
		dataIndex: 'contractorPriceForPartner',
		render: (price, item) =>
			item.contractorId ? price.toFixed(2) : item.price.toFixed(2),
	},
	{
		title: 'Рекомендуемая розничная цена',
		dataIndex: 'recommendedPrice',
		render: (price, item) => (item.contractorId ? price : '-'),
	},
	{
		title: 'Цена',
		dataIndex: 'price',
		render: (price, item) =>
			price
				? (+price * ((100 + +item.partnerPercent) / 100)).toFixed(2)
				: (
						+item.contractorPriceForPartner *
						((100 + +item.partnerPercent) / 100)
				  ).toFixed(2),
	},
	{
		title: 'Наценка',
		dataIndex: 'partnerPercent',
		render: (partnerPercent, item) =>
			item.contractorPriceForPartner
				? `${(
						(+item.contractorPriceForPartner * +partnerPercent) /
						100
				  ).toFixed(2)} \n (${partnerPercent}%)`
				: `${((+item.price * +partnerPercent) / 100).toFixed(
						2,
				  )} \n (${partnerPercent}%)`,
	},
	{
		title: 'YML',
		dataIndex: 'isAddedToYmlRozetka',
		render: isYml => (isYml ? '+' : '-'),
	},
	{
		title: '',
		dataIndex: 'actions',
		fixed: 'right',
		render: (actions, item) => (
			<CustomButton
				title="Редактировать"
				type="primary"
				className="edit-btn"
				onClick={() => openProduct(item)}
			/>
		),
	},
];
