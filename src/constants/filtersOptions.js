const filters = {
	categories: {
		category_id: '',
		name: '',
		brand: '',
		in_stock: '',
		vendor_code: '',
		min_price: '',
		max_price: '',
		independent_products: '',
		user_id: ""
	},
	orders: {
		id: '',
		min_date: '',
		max_date: '',
		status: '',
		user_fio: '',
		user_phone: '',
	}
};

export default filters;
