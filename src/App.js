import React from 'react';
import Routes from './routing/routes';
import { ThemeProvider } from 'styled-components';
import './App.css';
import 'antd/dist/antd.css';
import 'axios-progress-bar/dist/nprogress.css';
import configureStore from './store';
import { PersistGate } from 'redux-persist/integration/react';
import theme from './theme';
import { Provider } from 'react-redux';
const App = () => (
	<Provider store={configureStore().store}>
		<PersistGate
			loading={<div>LOADING</div>}
			persistor={configureStore().persistor}
		>
			<ThemeProvider theme={theme}>
				<Routes />
			</ThemeProvider>
		</PersistGate>
	</Provider>
);

export default App;
