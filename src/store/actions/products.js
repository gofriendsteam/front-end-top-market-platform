import { getAllCategories } from '../../utils/api/productsActions';
import {
	GET_ALL_CATEGORIES,
	GET_ALL_MODIFIED_CATEGORIES,
} from '../ActionTypes';
import { renameCategories } from '../../helpers/functions';

const putAllCategories = data => ({ type: GET_ALL_CATEGORIES, data });
const putModifiedCategories = data => ({
	type: GET_ALL_MODIFIED_CATEGORIES,
	data,
});

export const fetchAllCategories = () => async dispatch => {
	try {
		const res = await getAllCategories();

		dispatch(putAllCategories(res));
		const modifiedCategories = renameCategories(res);
		dispatch(putModifiedCategories(modifiedCategories));

	} catch (err) {
		console.log(err);
	}
};
