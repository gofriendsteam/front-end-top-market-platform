import { PUT_MARKET_PLACES } from '../ActionTypes';
import { getAllMarketplaces } from '../../utils/api/userActions';

const putMarketplaces = data => ({ type: PUT_MARKET_PLACES, data });

export const fetchAllMarketplaces = () => async dispatch => {
	try {
		const { results } = await getAllMarketplaces();
		dispatch(
			putMarketplaces(
				results.map(el => ({
					label: el.title,
					value: el.id,
				})),
			),
		);
		// console.log(res);
	} catch (e) {
		console.log(e);
	}
};
