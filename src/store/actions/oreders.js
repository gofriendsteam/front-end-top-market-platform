import { SEND_ORDER_TO_SUPPLIER } from '../ActionTypes';

export const sendingOrderToSupplier = data => ({ type: SEND_ORDER_TO_SUPPLIER, data });
