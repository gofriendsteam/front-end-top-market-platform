import { getView } from '../../helpers/getView';

export const toggleAll = (width, height) => {
	const view = getView(width);
	const collapsed = view !== 'DesktopView';
	return {
		type: 'TOGGLE_ALL',
		collapsed,
		view,
		height,
	};
};
export const toggleCollapsed = () => ({
	type: 'COLLAPSE_CHANGE',
});
export const toggleOpenDrawer = () => ({
	type: 'COLLAPSE_OPEN_DRAWER',
});
export const changeOpenKeys = openKeys => ({
	type: 'CHANGE_OPEN_KEYS',
	openKeys,
});
export const changeCurrent = current => ({
	type: 'CHANGE_CURRENT',
	current,
});
export const clearMenu = () => ({
	type: 'CLEAR_MENU',
});
