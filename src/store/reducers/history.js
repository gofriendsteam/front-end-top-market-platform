const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'PUT_TO_HISTORY':
      return action.history;
    default:
      return state;
  }
};
