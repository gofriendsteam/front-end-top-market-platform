import { PUT_MARKET_PLACES } from '../ActionTypes';

const marketplaceReducer = (state = [], { type, data }) => {
	switch (type) {
		case PUT_MARKET_PLACES:
			return [...data];
		default:
			return state;
	}
};
export default marketplaceReducer;
