import { combineReducers } from 'redux';
import user from './user';
import history from './history';
import categories from './categories';
import modal from "./modal";
import App from "./App"
import marketplaceReducer from "./marketplaces"
import orders from "./orders";
import modifiedCategories from './modifiedCattegories';
import chat from "./chat"

export default combineReducers({
	user,
	chat,
	categories,
	modifiedCategories,
	orders,
	history,
	modal,
	App,
	marketplaces: marketplaceReducer
});
