import {UPDATE_PROFILE, CHANGE_CATEGORY} from "../ActionTypes"

const initialUserState = {
	selectedCategory: '',
};

export default function userState(state = initialUserState, { type, payload }) {
	switch (type) {
		case UPDATE_PROFILE:
			return {
				...state,
				...payload,
			};

		case CHANGE_CATEGORY:
			return {
				...state,
				...payload,
			};

		default:
			return state;
	}
}
