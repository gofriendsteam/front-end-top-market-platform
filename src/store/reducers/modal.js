import { TOOGLE_MODAL } from '../ActionTypes';

const initialState = {
	isOpen: false,
	type: '',
	component: null,
	data: null,
};
console.log(initialState)
export default (state = initialState, { type, data }) => {
	switch (type) {
        case TOOGLE_MODAL:
			return {...data};
		default:
			return state;
	}
};
