import { SEND_ORDER_TO_SUPPLIER  } from '../ActionTypes';


const initialState = null;

export default (state = initialState, { type, data }) => {
	switch (type) {
		case SEND_ORDER_TO_SUPPLIER:
			return {...data};
		default:
			return state;
	}
};
