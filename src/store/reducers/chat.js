import { TOOGLE_CHAT } from '../ActionTypes';

const initialState = {
	isOpen: false,
	data: null,
};

export default (state = initialState, { type, data }) => {
	switch (type) {
		case TOOGLE_CHAT:
			return { ...state, data, isOpen: !state.isOpen };
		default:
			return state;
	}
};
