import { GET_ALL_CATEGORIES } from '../ActionTypes';

const initialState = [];

export default (state = initialState, { type, data }) => {
	switch (type) {
		case GET_ALL_CATEGORIES:
			return [...data];
		default:
			return state;
	}
};
