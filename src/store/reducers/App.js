import { getView } from '../../helpers/getView';
import { getDefaultPath } from '../../helpers/urlSync';

const isServer = typeof window === 'undefined';
const preKeys = getDefaultPath();

const initState = {
	collapsed: !isServer && window.innerWidth > 1320 ? false : true,
	view: !isServer && getView(window.innerWidth),
	height: !isServer && window.innerHeight,
	openDrawer: false,
	openKeys: preKeys,
	current: preKeys,
};

export default function appReducer(state = initState, action) {
	switch (action.type) {
		case 'COLLAPSE_CHANGE':
			return {
				...state,
				collapsed: !state.collapsed,
			};
		case 'COLLAPSE_OPEN_DRAWER':
			return {
				...state,
				openDrawer: !state.openDrawer,
			};
		case 'TOGGLE_ALL':
			if (state.view !== action.view || action.height !== state.height) {
				const height = action.height ? action.height : state.height;
				return {
					...state,
					collapsed: action.collapsed,
					view: action.view,
					height,
				};
			}
			break;
		case 'CHANGE_OPEN_KEYS':
			return {
				...state,
				openKeys: action.openKeys,
			};
		case 'CHANGE_CURRENT':
			return {
				...state,
				current: action.current,
			};
		case 'CLEAR_MENU':
			return {
				...state,
				openKeys: [],
				current: [],
			};
		default:
			return state;
	}
	return state;
}
