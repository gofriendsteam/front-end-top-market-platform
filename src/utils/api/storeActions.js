import api from './request';

import { STORE, UPDATE_STORE } from '../../constants/APIURLS';

export const createMyStore = store => {
	// console.log("login_request this.props", this.props);
	return api('post', STORE, store);
};

export const updateStore = store => {
	// console.log("login_request this.props", this.props);
	return api('patch', UPDATE_STORE, store);
};

export const getMyStore = () => {
	return api('get', 'my_store/');
};
