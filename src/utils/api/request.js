import axios from 'axios';
import { notification } from 'antd';
import { loadProgressBar } from 'axios-progress-bar';
import jwtDecode from 'jwt-decode';
import { BASE_URL } from '../../constants/APIURLS';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
loadProgressBar();

const http = async (method, url, data, type) => {
	let token = localStorage.getItem('token');

	if (token) {
		const parseToken = jwtDecode(token);
		const dateNow = Date.now();
		const difference = parseToken.exp * 1000 - dateNow;
		if (difference <= 0) {
			const refresh_token = localStorage.getItem('refreshtoken');
			const { data } = await axios.post(`${BASE_URL}token_refresh/`, {
				refresh: refresh_token,
			});
			localStorage.setItem('token', data.access);
			localStorage.setItem('refreshtoken', data.refresh);
			token = data.access;
		}
	}
	return new Promise((resolve, reject) => {
		axios({
			method: method,
			url: `${BASE_URL}${url}`,
			data: data,

			headers: token
				? {
						'Content-Type': type || 'application/json',
						authorization: `Bearer ${token}`,
				  }
				: {
						'Content-Type': type || 'application/json',
				  },
		})
			.then(result => {
				resolve(result.data);
			})
			.catch(error => {
				if (error.response != null) {
					if (typeof error.response.data === 'object') {
						for (let key in error.response.data) {
							if (key !== 'messages') {
								if (typeof error.response.data[key] === 'string') {
									console.log('Error', error.response);
									if (error.response.data[key] === 'token_not_valid') {
										window.location.pathname = '/login';
										localStorage.removeItem('persist:root');
										localStorage.clear();
										notification.error({
											// message: key,
											// description: error.response.data[key][0],
											message:
												'Извините за неудобства, перезайдите пожалуйста в систему.',
										});
									}
									notification.error({
										// message: key,
										// description: error.response.data[key][0],
										message: error.response.data[key],
									});
								} else {
									if (typeof error.response.data[key][0] !== 'object') {
										notification.error({
											// message: key,
											// description: error.response.data[key][0],
											message: error.response.data[key][0],
										});
									}
								}
							}
						}
					} else {
						notification.error({
							message: 'Сервер не отвечает',
						});
					}
					reject(error);
				}
			});
	});
};

export default http;
