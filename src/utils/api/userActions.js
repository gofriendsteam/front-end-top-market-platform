import api from './request';

import {
	LOGIN,
	REGISTRATION,
	CONFIRM_EMAIL,
	PROFILE,
	PASSWORD,
	RESET_PASSWORD,
	CONTACT_FORM,
	PACK_INVOICE,
	PACK_LIQPAY,
	RECHARGE_INVOICE,
	RECHARGE_LIQPAY,
	UNBALANCES,
	TRANSACTIONS_FULL,
	TRANSACTIONS_RECHARGE_BY_LIQPAY,
	RECHARGE_BY_INVOICE,
	ROZETKA_TRANSACTIONS_HISTORY,
	ROZETKA_INVOICES,
	ROZETKA_SALES_REPORTS,
	ORDERS_HISTORY,
	ORDERS,
	ABC_LIST,
	ABC_ITEM,
	BALANCE,
	ALL_MARKET_PLACES,
	ROZETKA_ACCOUNT,
	CHAT_MESSAGE,
	CHAT,
	MESSAGE_CREATE
} from '../../constants/APIURLS';
import { changeCurrent } from '../../store/actions/AsideBar';

export const login = user => dispatch => {
	return api('post', LOGIN, user).then(res => {
		localStorage.setItem('token', res.access);
		localStorage.setItem('refreshtoken', res.refresh);
		getProfile().then(res => {
			dispatch({
				type: 'UPDATE_PROFILE',
				payload: res,
			});
			dispatch(changeCurrent(['/admin/cabinet']));
		});
	});
};

export const registration = user => {
	return api('post', REGISTRATION, user);
};
export const getAllMarketplaces = () => api('get', ALL_MARKET_PLACES);

export const confirmEmail = token => {
	return api('get', CONFIRM_EMAIL + token);
};

export const getProfile = () => {
	return api('get', PROFILE);
};

export const updateProfile = user => dispatch => {
	return api('patch', PROFILE, user).then(res => {
		dispatch({
			type: 'UPDATE_PROFILE',
			payload: res,
		});
	});
};

export const selectedCategory = category => dispatch => {
	return dispatch({
		type: 'CHANGE_CATEGORY',
		payload: category,
	});
};

export const changePassword = pass => {
	return api('put', PASSWORD, pass);
};

export const resetPassword = email => {
	return api('post', RESET_PASSWORD, email);
};

export const sendContactForm = form => {
	return api('post', CONTACT_FORM, form);
};

// FINANCE GET MONEY | POST
// Продавец. Для покупки пакетов. Счёт-фактура. ИСПОЛЬЗУЕТСЯ
export const sendInvoice = pocket => {
	return api('post', PACK_INVOICE, pocket);
};
// Продавец. Для покупки пакетов. LiqPay. ИСПОЛЬЗУЕТСЯ
export const buyPackByLiqPay = pocketId => {
	return api('post', PACK_LIQPAY, pocketId);
};
// Продавец. Для вывода средств. LiqPay. ИСПОЛЬЗУЕТСЯ
export const unbalances = amount => {
	return api('post', UNBALANCES, amount);
};
// Поставщик. Для попонения баланса. Cчёт-фактура. ИСПОЛЬЗУЕТСЯ
export const rechargeInvoice = pocket => {
	return api('post', RECHARGE_INVOICE, pocket);
};
// Поставщик. Для попонения баланса. LiqPay. ИСПОЛЬЗУЕТСЯ
export const rechargeByLiqPay = amount => {
	return api('post', RECHARGE_LIQPAY, amount);
};

// FINANCE HISTORY
export const transactionsHistory = (query = '') => {
	return api('get', TRANSACTIONS_FULL + query);
};
export const transactionsRechargeByLiqpay = (query = '') => {
	return api('get', TRANSACTIONS_RECHARGE_BY_LIQPAY + query);
};
export const rechargeByInvoice = (query = '') => {
	return api('get', RECHARGE_BY_INVOICE + query);
};
export const rozetkaTransactionsHistory = (query = '') => {
	return api('get', ROZETKA_TRANSACTIONS_HISTORY + query);
};
export const rozetkaInvoices = (query = '') => {
	return api('get', ROZETKA_INVOICES + query);
};
export const rozetkaSalesReports = (query = '') => {
	return api('get', ROZETKA_SALES_REPORTS + query);
};
export const ordersHistory = (query = '') => {
	return api('get', ORDERS_HISTORY + query);
};

// ABC
export const abcList = () => api('get', ABC_LIST);
export const abcItem = id => api('get', ABC_ITEM + id);
// BALANCE
export const balance = () => api('get', BALANCE);

//Rozetka Prom ACCOUNT
export const getRozetkaData = () => api('get', ROZETKA_ACCOUNT);
export const updateRozetkaData = data => api('patch', ROZETKA_ACCOUNT, data);

//Orders
export const updatePartnerStatus = (data, url) => api('patch', url, data);

//chat
export const fetchAllMessages = (chatId) => api('get', CHAT + `${chatId}/`);
export const sendMessage = (chatId, message) => api('post', CHAT + chatId + MESSAGE_CREATE, message);
export const createChat = (data) => api('post', CHAT, data);
