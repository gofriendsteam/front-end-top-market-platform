import api from './request';

import {
	ALL_CATEGORIES,
	FIRST_LEVEL_CATEGORIES,
	OPTIONS_BY_CATEGORY,
	OPTIONS_BY_CATEGORY_WITH_TEXT_AREA,
	CONTRACTOR_PRODUCTS,
	CONTRACTOR_CATEGORIES,
	UPLOAD_PRODUCTS,
	DOWNLOADS_STATUS,
	DOWNLOADS_STATUS_PARTNER,
	NEW_PRODUCTS,
	ALL_PRODUCTS,
	REMOVE_CONTRACTOR_PRODUCTS,
	REMOVE_PARTNER_PRODUCTS,
	COPY_TO_MY_PRODUCTS,
	PARTNER_PRODUCTS,
	YML,
	UPDATE_ALL_SELECTED_PRODUCTS,
	DELETE_PRODUCTS_FROM_YML,
	ADD_MY_STORE,
} from '../../constants/APIURLS';

//CATEGORIES
export const getAllCategories = () => {
	return api('get', `${ALL_CATEGORIES}`);
};
export const getFirstLevelCategories = () => {
	return api('get', `${FIRST_LEVEL_CATEGORIES}`);
};
export const getCategoriesById = id => {
	return api('get', `${ALL_CATEGORIES}${id}/children`);
};

//OPTIONS
export const getOptionsByCategory = categoryId => {
	return api('get', `${OPTIONS_BY_CATEGORY}${categoryId}`);
};
export const getOptionsByCategoryWithTextArea = categoryId => {
	return api('get', `${OPTIONS_BY_CATEGORY_WITH_TEXT_AREA}${categoryId}`);
};


//CONTRACTOR
export const getContractorProducts = url => {
	return api('get', CONTRACTOR_PRODUCTS + url);
};
export const getContractorCategories = () => {
	return api('get', CONTRACTOR_CATEGORIES);
};
export const uploadXls = file => {
	return api('post', UPLOAD_PRODUCTS, file);
};
export const getDownloadsStatus = () => {
	return api('get', DOWNLOADS_STATUS);
};
export const createNewProduct = product => {
	return api('post', NEW_PRODUCTS, product);
};
export const updateProduct = product => {
	// console.log('!!! updateProduct !!!', product);
	return api('patch', `${NEW_PRODUCTS}${product.id}/`, product).then(data =>
		console.log('!!! updateProduct RESPONSE !!!', data),
	);
};
export const removeContractorProduct = products => {
	return api('post', `${REMOVE_CONTRACTOR_PRODUCTS}`, products);
};

//PARTNER
export const getAllProducts = (url = '') => {
	return api('get', `${ALL_PRODUCTS + url}`);
};
export const copyProducts = products => {
	return api('post', `${COPY_TO_MY_PRODUCTS}`, products);
};
export const getPartnerProducts = (url = '') => {
	return api('get', `${PARTNER_PRODUCTS + url}`);
};
export const updatePartnerProduct = product => {
	return api('patch', `${PARTNER_PRODUCTS}${product.id}/`, product);
};
export const removePartnerProduct = products => {
	return api('post', `${REMOVE_PARTNER_PRODUCTS}`, products);
};
export const getPartnerDownloadsStatus = () => {
	return api('get', DOWNLOADS_STATUS_PARTNER);
};
export const updateAllSelectedProducts = (data) => api('post', UPDATE_ALL_SELECTED_PRODUCTS,data)
//Filtered for subcategories
export const getFilteredProducts = id => {
	return api('get', `catalog/products_by_category/${id}`);
};

// YML
export const generateYml = () => {
	return api('get', `${YML}`);
};
export const addProductsToYml = (products, type) => {
	return api('post', `${YML}`, products);
};
export const deleteProductsFromYml = (products, type) => {
	return api('post', `${DELETE_PRODUCTS_FROM_YML}`, products);
};

export const getYmlProducts = (type, url) => {
	return api('get', YML + `${type}/${url ? url : ''}`);
};
export const getYmlProducts2 = (url) => {
	return api('get', YML + `${url ? url : ''}`);
}

export const addProductToMyStore = data => {
	return api('post', ADD_MY_STORE, data);
};
