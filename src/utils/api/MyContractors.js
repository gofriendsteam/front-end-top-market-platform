import api from './request';
import { MY_CONTRACTORS } from '../../constants/APIURLS';

export const getMyContractors = () => api('get', MY_CONTRACTORS);
