export const putHistory = (history) => {
  return {
    type: 'PUT_TO_HISTORY',
    history,
  };
};
