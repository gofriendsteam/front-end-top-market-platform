import api from './request';

import {
  ORDERS,
  CONTRACTOR_ORDERS,
  PASS_TO_CONTRACTOR
} from '../../constants/APIURLS';

// PARNER
export const getOrders = url => api('get', ORDERS + url);
export const editOrder = (id, data) => api('patch', ORDERS + `${id}/`, data);
export const passToContractor = id => api('get', PASS_TO_CONTRACTOR(id));
export const createOrder = (data) => api('post' , ORDERS, data);

// CONTRACTOR
export const getContractorOrders = url => api('get', CONTRACTOR_ORDERS + url);
export const editContractorOrder = (id, data) =>
  api('patch', CONTRACTOR_ORDERS + `${id}/`, data);
export const sendToNewContractor = (orderId, data) => api('post',ORDERS + `${orderId}/set_new_contractor/`, data)
