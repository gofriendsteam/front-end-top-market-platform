export const router = router => {
  return {
    type: 'PUT_ROUTER',
    router
  };
};
