import axios from 'axios';
// import { notification } from 'antd';

const BASE_URL = 'https://api.novaposhta.ua/v2.0/json/';
const API_KEY = '6e04382bfb9a58fc8101600f54ee4dc3';

export const getSettlements = (apiKey, value) => {
  const data = {
    modelName: 'AddressGeneral',
    calledMethod: 'getSettlements',
    methodProperties: {
      FindByString: value,
      Warehouse: '1'
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};
export const getСities = ( value) => {
  const data = {
    modelName: 'AddressGeneral',
    calledMethod: 'getCities',
    methodProperties: {},
    API_KEY
  };
  return axios.post(BASE_URL, data);
};
export const getWarehouses = (apiKey, value) => {
  const data = {
    modelName: 'AddressGeneral',
    calledMethod: 'getWarehouses',
    methodProperties: {
      SettlementRef: value
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const getTypes = (apiKey) => {
  const data = {
    modelName: 'Common',
    calledMethod: 'getServiceTypes',

    apiKey
  }
  return axios.post(BASE_URL, data);
}
export const getWarehouseByCity = (value) => {
  const data = {
    modelName: 'AddressGeneral',
    calledMethod: 'getWarehouses',
    methodProperties: {
      CityRef: value
    },
    API_KEY
  };
  return axios.post(BASE_URL, data);
};
export const getCounterparties = apiKey => {
  const data = {
    modelName: 'Counterparty',
    calledMethod: 'getCounterparties',
    methodProperties: {
      CounterpartyProperty: 'Sender',
      Page: '1'
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const getCounterpartyAddresses = (apiKey, Ref) => {
  const data = {
    modelName: 'Counterparty',
    calledMethod: 'getCounterpartyAddresses',
    methodProperties: {
      Ref,
      CounterpartyProperty: 'Sender'
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const getCounterpartyContactPersons = (apiKey, Ref) => {
  const data = {
    modelName: 'Counterparty',
    calledMethod: 'getCounterpartyContactPersons',
    methodProperties: {
      Ref,
      Page: '1'
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const createContactPerson = (apiKey, methodProperties) => {
  const data = {
    modelName: 'ContactPerson',
    calledMethod: 'save',
    methodProperties,
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const deleteContactPerson = (apiKey, Ref) => {
  const data = {
    modelName: 'ContactPerson',
    calledMethod: 'delete',
    methodProperties: {
      Ref
    },
    apiKey
  };
  return axios.post(BASE_URL, data);
};

export const createNovaPoshtaTTN = data => {
  return axios.post(BASE_URL, data);
};
