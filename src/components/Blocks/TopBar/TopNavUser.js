import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Menu, Popover } from 'antd';
import defaultUser from "../../../img/navIcons/user.svg"
// import IntlMessages from '@iso/components/utility/intlMessages';
// import userpic from '@iso/assets/images/user1.png';
// import authAction from '@iso/redux/auth/actions';
import TopbarDropdownWrapper from '../../../styles/TopNavDropDownWrapper.styles';

// const { logout } = authAction;

export default function TopbarUser() {
	const [visible, setVisibility] = React.useState(false);
	const dispatch = useDispatch();
	const { avatarImage, role } = useSelector(state => state.user);
	function handleVisibleChange() {
		setVisibility(visible => !visible);
	}
	const handleLogout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('persist:root');
		window.location.href = `${window.location.origin}/`;
		localStorage.clear();
	};

	const content = (
		<TopbarDropdownWrapper className="isoUserDropdown">
			<Link className="isoDropdownLink" to="/admin/profile_settings">
				Настройки профиля
			</Link>

			<Link className="isoDropdownLink" to="/admin/company_settings">
				Настройки компании
			</Link>

			<div className="isoDropdownLink" onClick={handleLogout}>
				Выход
			</div>
		</TopbarDropdownWrapper>
	);

	return (
		<Popover
			content={content}
			trigger="click"
			visible={visible}
			onVisibleChange={handleVisibleChange}
			arrowPointAtCenter={true}
			placement="bottomLeft"
		>
			<div className="isoImgWrapper">
				<img alt="user" src={avatarImage || defaultUser} />
				<span className="userActivity online" />

			</div>
			<p>{role === 'PARTNER' ? 'Продавець' : 'Поставщик'}</p>
		</Popover>
	);
}
