import React from 'react';
import { Popover } from 'antd';
import { useSelector } from 'react-redux';
// import IntlMessages from '@iso/components/utility/intlMessages';
import TopbarDropdownWrapper from './TopbarNotification.styles';

const demoNotifications = [
	{
		id: 1,
		name: 'David Doe',
		notification:
			'A National Book Award Finalist An Edgar Award Finalist A California Book Award Gold Medal Winner',
	},
	{
		id: 2,
		name: 'Navis Doe',
		notification:
			'A National Book Award Finalist An Edgar Award Finalist A California Book Award Gold Medal Winner',
	},
];

export default function TopbarNotification() {
	const [visible, setVisiblity] = React.useState(false);
	// const customizedTheme = useSelector(state => state.ThemeSwitcher.topbarTheme);

	function handleVisibleChange() {
		setVisiblity(visible => !visible);
	}

	const content = (
		<TopbarDropdownWrapper className="topbarNotification">
			{/*<div>asdadasdas</div>*/}
			<div className="isoDropdownHeader">
				<h3>
					Мои оповещания
					{/*<IntlMessages id="sidebar.notification" />*/}
				</h3>
			</div>
			<div className="isoDropdownBody">
				{/*{demoNotifications.map(notification => (*/}
				{/*	<a className="isoDropdownListItem" key={notification.id} href="# ">*/}
				{/*		<h5>{notification.name}</h5>*/}
				{/*		<p>{notification.notification}</p>*/}
				{/*	</a>*/}
				{/*))}*/}
			</div>
			<a className="isoViewAllBtn" href="#">
				Все уведомления
			</a>
		</TopbarDropdownWrapper>
	);
	return (
		<Popover
			content={content}
			trigger="click"
			visible={visible}
			onVisibleChange={handleVisibleChange}
			placement="bottomLeft"
		>
			<div className="isoIconWrapper">
				<i
					className="icon ion-md-notifications"
					// style={{ color: customizedTheme.textColor }}
				/>
			</div>
		</Popover>
	);
}
