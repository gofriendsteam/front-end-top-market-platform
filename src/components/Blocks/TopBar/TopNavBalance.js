import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import styles from '../Header/Header.module.css';
import dollar from '../../../img/dollar.svg';

const TopNavBalance = () => {
	const balance = useSelector(state => state.user.userBalance);
	return (
		<div className="balance">
			<p>Баланс:</p>

			<div className="balance-numbers">
				<div className="dollarIcon">
					<img src={dollar} alt="" />
				</div>
				<div>
					<span>{balance.toFixed(2)}</span> грн
				</div>
			</div>
		</div>
	);
};

export default TopNavBalance;
