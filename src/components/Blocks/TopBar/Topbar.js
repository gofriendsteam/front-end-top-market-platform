import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Layout } from 'antd';
import { toggleCollapsed } from '../../../store/actions/AsideBar';
import TopbarUser from './TopNavUser';
import TopbarWrapper from './Topbar.styles';
import TopNavBalance from "./TopNavBalance";
import TopbarNotification from "./TopbarNotification";
import TopbarMessage from "./TopbarMessage";

const { Header } = Layout;

export default function Topbar() {
	const [selectedItem, setSelectedItem] = React.useState('');
	const customizedTheme = {
		themeName: 'defaultTheme',
		buttonColor: '#ffffff',
		textColor: '#323332',
	};
	const { collapsed, openDrawer } = useSelector(state => state.App);
	const dispatch = useDispatch();
	const handleToggle = React.useCallback(() => dispatch(toggleCollapsed()), [
		dispatch,
	]);
	const isCollapsed = collapsed && !openDrawer;
	const styling = {
		background: customizedTheme.backgroundColor,
		position: 'fixed',
		width: '100%',
		height: 70,
	};
	return (
		<TopbarWrapper>
			<Header
				style={styling}
				className={
					isCollapsed ? 'isomorphicTopbar collapsed' : 'isomorphicTopbar'
				}
			>
				<div className="isoLeft">
					<i
						className={'icon ion-md-menu'}
						style={{ color: customizedTheme.textColor, fontSize: 26 }}
						onClick={handleToggle}
					/>
					<TopNavBalance/>
				</div>

				<ul className="isoRight">

					<li
						onClick={() => setSelectedItem('notification')}
						className={selectedItem ? 'isoNotify active' : 'isoNotify'}
					>
						<TopbarNotification />
					</li>

					<li onClick={() => setSelectedItem('message')} className="isoMsg">
						<TopbarMessage />
					</li>
					<li onClick={() => setSelectedItem('user')} className="isoUser">
						<TopbarUser />
					</li>
				</ul>
			</Header>
		</TopbarWrapper>
	);
}
