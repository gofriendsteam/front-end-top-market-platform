import React from 'react';
import styles from './ProductPictureGallery.module.css';
import ProductPictureGaleryPictures from './ProductPictureGaleryPictures';
const ProductPictureGalleryContainer = ({
  imageList,
  showModal,
  setAvatarUrl,
  showDeleteConfirm
}) => {
  return (
    <ul className={styles.galleryList}>
      <ProductPictureGaleryPictures
        imageList={imageList}
        showModal={showModal}
        setAvatarUrl={setAvatarUrl}
        showDeleteConfirm={showDeleteConfirm}
      />
    </ul>
  );
};

export default ProductPictureGalleryContainer;
