import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';
import styles from './ProductPictureGallery.module.css';
import ProductPictureGalleryContainer from './ProductPictureGalleryContainer';

class ProductPictureGallery extends Component {
	state = {
		currentImageIndex: 0,
		urlImageList: this.props.urlImageList,
		coverImageList: this.props.coverImageList,
		imageList: [...this.props.urlImageList, ...this.props.coverImageList],
		visible: false,
	};

	shouldComponentUpdate(prevProps, state) {
		if (prevProps.urlImageList.length !== this.props.urlImageList.length) {
			this.setState(prev => ({
				...prev,
				urlImageList: prevProps.urlImageList,
				imageList: [...prevProps.urlImageList, ...prevProps.coverImageList],
			}));
		}
		if (
			state.imageList.length !==
			prevProps.coverImageList.length + prevProps.urlImageList.length
		) {
			this.setState(prev => ({
				...prev,
				coverImageList: prevProps.coverImageList,
				imageList: [...prevProps.urlImageList, ...prevProps.coverImageList],
			}));
		}
		console.log(prevProps.coverImageList, this.props.coverImageList, state);

		return true;
	}
	showModal = e => {
		console.log('asd', this.state.imageList);
		const newCurentImageIndex = this.state.imageList.indexOf(
			this.state.imageList.find(
				el => el.url === e.target.src || el.imageDecoded === e.target.src,
			),
		);

		this.setState({
			currentImageIndex: newCurentImageIndex,
			visible: true,
		});
	};

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			visible: false,
		});
	};

	// Вызывает callback для смены картинки для аватара карточки товара
	setAvatarUrl = idx => {
		const currentImage = this.state.imageList[
			(typeof idx === 'number' && idx) || this.state.currentImageIndex
		];
		console.log(this.state.currentImageIndex, idx);
		const avatarImage = currentImage.url
			? currentImage.url
			: currentImage.imageDecoded
			? currentImage.imageDecoded
			: null;

		this.props.changeAvatarUrl(avatarImage);
	};

	// Модальное окно подтверждения удаления изображения
	showDeleteConfirm = idx => {
		Modal.confirm({
			title: 'Вы уверены, что хотите удалить изображение?',
			centered: true,
			okText: 'Да',
			okType: 'danger',
			cancelText: 'Нет',
			onOk: () => {
				const {
					imageList,
					urlImageList,
					coverImageList,
					currentImageIndex,
				} = this.state;

				const newImageList = imageList.filter(
					el =>
						el.url !== imageList[idx || currentImageIndex].url ||
						el.imageDecoded !==
							imageList[idx || currentImageIndex].imageDecoded,
				);
				const newUrlImageList = urlImageList.filter(
					el => el.url !== imageList[idx || currentImageIndex].url,
				);
				const newCoverImageList = coverImageList.filter(
					el =>
						el.imageDecoded !==
						imageList[idx || currentImageIndex].imageDecoded,
				);

				if (currentImageIndex === imageList.length - 1) {
					this.showPrev();
				}

				this.setState(
					{
						imageList: [...newImageList],
						urlImageList: [...newUrlImageList],
						coverImageList: [...newCoverImageList],
					},
					() => {
						!this.state.imageList.length && this.handleCancel();
						this.props.deleteImage(newCoverImageList, newUrlImageList);
					},
				);
			},
			onCancel: () => {
				console.log('-= ProductPictureGallery =-\nCancel');
			},
		});
	};

	showPrev = () => {
		const { imageList, currentImageIndex } = this.state;

		const newCurentImageIndex = () => {
			if (currentImageIndex === 0) {
				return imageList.length - 1;
			}
			return currentImageIndex - 1;
		};

		this.setState({
			currentImageIndex: newCurentImageIndex(),
		});
	};

	showNext = () => {
		const newCurentImageIndex = () => {
			if (this.state.currentImageIndex === this.state.imageList.length - 1) {
				return 0;
			}
			return this.state.currentImageIndex + 1;
		};

		this.setState({
			currentImageIndex: newCurentImageIndex(),
		});
	};

	render() {
		const { visible, imageList, currentImageIndex } = this.state;
		console.log(
			'-= ProductPictureGallery =-\n STATE at render() moment',
			this.state,
		);

		return (
			<Fragment>
				<ProductPictureGalleryContainer
					imageList={imageList}
					showModal={this.showModal}
					setAvatarUrl={this.setAvatarUrl}
					showDeleteConfirm={this.showDeleteConfirm}
				/>

				<Modal
					width={1000}
					centered
					style={{ userSelect: 'none' }}
					visible={visible}
					onOk={this.handleOk}
					onCancel={this.handleCancel}
					footer={[
						<Button key="avatar" onClick={this.setAvatarUrl}>
							Установить эту картинку на аватар
						</Button>,
						<Button key="back" onClick={() => this.showDeleteConfirm(null)}>
							Удалить изображение
						</Button>,
					]}
				>
					<p>
						{currentImageIndex + 1} of {imageList.length}
					</p>
					<div className={styles.overViewBlock}>
						{imageList[1] && <p onClick={this.showPrev}>{'<'}</p>}

						{imageList.length && (
							<img
								src={
									imageList[currentImageIndex].url ||
									imageList[currentImageIndex].imageDecoded ||
									''
								}
								alt=""
								className={styles.overViewBlock__img}
							/>
						)}

						{imageList[1] && (
							<p onClick={this.showNext} className={styles.overViewBlock__next}>
								>
							</p>
						)}
					</div>
				</Modal>
			</Fragment>
		);
	}
}

ProductPictureGallery.propTypes = {
	coverImageList: PropTypes.array.isRequired,
	urlImageList: PropTypes.array.isRequired,
};

export default ProductPictureGallery;
