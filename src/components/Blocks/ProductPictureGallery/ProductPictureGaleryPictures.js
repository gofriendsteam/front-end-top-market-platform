import React from 'react';
import styles from './ProductPictureGallery.module.css';

const ProductPictureGaleryPictures = ({
	imageList,
	showModal,
	setAvatarUrl,
	showDeleteConfirm,
}) => {
	console.log('HERE', imageList);
	return(
		imageList.map((el, i) => (
			<li
				className={styles.galleryItem}
				key={el.imageDecoded || el.url + i}
				index={i}
			>
				<a onClick={() => showDeleteConfirm(i)} className={styles.closeButton}>
					&#10060;
				</a>
				<a onClick={() => setAvatarUrl(i)} className={styles.addMainImg}>
					сделать главным
				</a>
				{console.log(el)}
				<img onClick={showModal} src={el.imageDecoded || el.url} alt="" />
			</li>
		))
	);
};

export default ProductPictureGaleryPictures;
