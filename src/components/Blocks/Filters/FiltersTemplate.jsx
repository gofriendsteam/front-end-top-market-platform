import React, { useState } from 'react';
import { Cascader, Input } from 'antd';
import styles from '../../Pages/Categories/Categories.module.css';
import { FiltersWrapper } from '../../Shared/Styles/MainWrapper';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

const FiltersTemplate = ({ values, onChange, handleChangeFilters }) => {
	const { pathname } = useLocation();

	const options = useSelector(state => state.modifiedCategories);
	const RenderSearchInput = () => {
		switch (pathname) {
			case '/admin/categories':
				return (
					<Input
						className="filter-input"
						type="number"
						min={0}
						name="independent_products"
						value={values.independent_products}
						placeholder="Поиск по ID"
						onChange={onChange}
					/>
				);
			case '/admin/my_products':
				return (
					<Input
						className="filter-input"
						type="number"
						min={0}
						name="user_id"
						value={values.user_id}
						placeholder="Поиск по ID"
						onChange={onChange}
					/>
				);
			default:
				return null;
		}
	};
	return (
		<FiltersWrapper>
			<Cascader
				expandTrigger="hover"
				changeOnSelect
				className="categories-cascader"
				onChange={onChange}
				options={options}
				value={values.category_id}
				placeholder="Категория"
			/>
			<Input
				className="filter-input"
				type="text"
				name="name"
				value={values.name}
				placeholder="Название товара"
				onChange={onChange}
			/>

			{RenderSearchInput()}
			{/*<Input*/}
			{/*	className="filter-input"*/}
			{/*	type="number"*/}
			{/*	min={0}*/}
			{/*	name="user_id"*/}
			{/*	value={values.user_id}*/}
			{/*	placeholder="Поиск по ID"*/}
			{/*	onChange={onChange}*/}
			{/*/>*/}
			<Input
				className="filter-input"
				type="text"
				name="vendor_code"
				value={values.vendor_code}
				placeholder="Артикул"
				onChange={onChange}
			/>
			<Input
				className="filter-input"
				type="text"
				name="brand"
				value={values.brand}
				placeholder="Бренд"
				onChange={onChange}
			/>

			<select
				selected
				onChange={({ target: { value } }) =>
					handleChangeFilters({
						target: {
							name: 'in_stock',
							value: value,
						},
					})
				}
			>
				<option
					value="placeholder"
					disabled
					selected
					style={{ display: 'none' }}
				>
					Количество
				</option>
				<option value="">Все</option>
				<option value={true}>В наличии</option>
				<option value={false}>Нет в наличии</option>
			</select>
			<Input
				className={styles.inputPrice}
				type="number"
				placeholder="Цена от"
				name="min_price"
				value={values.min_price}
				min="0"
				onChange={onChange}
			/>
			<Input
				className={styles.inputPrice}
				type="number"
				placeholder="Цена до"
				name="max_price"
				value={values.max_price}
				min={values.min_price}
				onChange={onChange}
			/>
		</FiltersWrapper>
	);
};

export default FiltersTemplate;
