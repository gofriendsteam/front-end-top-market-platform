import React from 'react';
// import styles from '../../Pages/Orders/Orders.module.css';
import { statusList } from '../../../constants/orderStatus';
import { Input } from 'antd';
import { FiltersWrapper } from '../../Shared/Styles/MainWrapper';

const OrderFilters = ({ values, onChange }) => {
	return (
		<FiltersWrapper className="order-filters">
			<div className="input-item">
				{/*<label>Номер заказа</label>*/}
				<Input
					type="number"
					name="id"
					min={0}
					placeholder="Номер заказа"
					value={values.id}
					onChange={onChange}
				/>
			</div>

			{/*<div className="input-item">*/}
			{/*	/!*<label>Название товара /Код товара</label>*!/*/}
			{/*	<Input*/}
			{/*		type="text"*/}
			{/*		name="id"*/}
			{/*		placeholder="Название товара /Код товара"*/}
			{/*		value={values.id}*/}
			{/*		onChange={handleChangeInput}*/}
			{/*	/>*/}
			{/*</div>*/}

			<div className="input-item">
				{/*<label>Дата заказа</label>*/}
				<Input
					type="date"
					placeholder="Дата заказа от:"
					name="min_date"
					value={values.min_date}
					onChange={onChange}
				/>
			</div>
			<div className="input-item">
				<Input
					type="date"
					placeholder="Дата заказа до:"
					name="max_date"
					value={values.max_date}
					onChange={onChange}
				/>
			</div>

			<div className="input-item select">
				{/*<label>Статус заказа</label>*/}
				<select
					placeholder="Статус заказа"
					name="status"
					value={values.status}
					onChange={onChange}
				>
					{statusList.map(item => (
						<option value={item.id} key={item.id}>
							{item.title}
						</option>
					))}
				</select>
			</div>
			<div className="input-item">
				{/*<label>ФИО покупателя</label>*/}
				<Input
					type="text"
					placeholder="ФИО покупателя"
					name="user_fio"
					value={values.user_fio}
					onChange={onChange}
				/>
			</div>
			<div className="input-item">
				{/*<label>ФИО покупателя</label>*/}
				<Input
					type="text"
					placeholder="Телефон получателя"
					name="user_phone"
					value={values.user_phone}
					onChange={onChange}
				/>
			</div>
		</FiltersWrapper>
	);
};

export default OrderFilters;
