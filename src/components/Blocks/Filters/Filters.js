import React, { useState, Fragment } from 'react';
import { Modal } from 'antd';
import FiltersTemplate from './FiltersTemplate';
import OrderFilters from "./OrderFilters";


const Filters = props => {
	const [modaState, setModalState] = useState(false);
	const windowWidth = window.innerWidth;
	const openModal = () => setModalState(true);
	const closeModal = () => setModalState(false);
	console.log('Props', props);
	return windowWidth > 768 ? (
		props.type && props.type === "orders" ? <OrderFilters {...props}/> : <FiltersTemplate {...props} />
	) : (
		<Fragment>
			<i className="icon ion-md-settings" onClick={() => openModal()}/>
			<Modal visible={modaState} title="Фильтры" onCancel={() => closeModal()} footer={false}>
				{props.type && props.type === "orders" ? <OrderFilters {...props}/> : <FiltersTemplate {...props} />}
			</Modal>
		</Fragment>
	);
};

export default Filters;
