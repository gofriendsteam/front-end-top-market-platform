import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Cascader, Button, Input } from 'antd';
import s from './Options.module.css';

const filter = (inputValue, path) =>
	path.some(
		option => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1,
	);

const Options = ({
	selectedOptionsArr = [],
	filledOptionByTextAreas = [],
	manualOptions = [],
	optionsArr = [],
	optionByTextAreasArr = [],
	onChangeOptionsCascader = fn => fn,
	addManualOptions = fn => fn,
	changeManualOptions = fn => fn,
	removeManualOptions = fn => fn,
}) => {
	// console.log('-= Options =-\nprops.optionsArr:', optionsArr)
	// console.log('-= Options =-\nprops.selectedOptionsArr:', selectedOptionsArr)
	// console.log('-= Options =-\nprops.optionByTextAreasArr:', optionByTextAreasArr)

	return (
		<Fragment>
			{optionsArr.length ? ( // проверяем длинну массива и отрисовываем разметку, если есть
				<fieldset>
					<legend>Селективные параметры</legend>
					<ul className={s.Group}>
						{optionsArr.map(el => (
							<li className={s.GroupItem} key={el.id}>
								<p className={s.Text}>{el.group}</p>
								<Cascader
									className={s.Parameters}
									defaultValue={
										selectedOptionsArr.length &&
										selectedOptionsArr.find(item => item.group === el.id)
											? [
													selectedOptionsArr.find(item => item.group === el.id)
														.value,
											  ]
											: []
									}
									placeholder="Please select"
									expandTrigger="hover"
									options={el.values.map(item => ({
										// Переименовывааем ключи свойств
										label: item.value,
										value: item.id,
										isLeaf: !item.isHaveChildren,
									}))}
									onChange={(value, selectedOptions) =>
										onChangeOptionsCascader(value, selectedOptions, el.id)
									}
									showSearch={{ filter }}
								/>
							</li>
						))}
					</ul>
				</fieldset>
			) : null}

			{optionByTextAreasArr.length ? ( // проверяем длинну массива и отрисовываем разметку, если есть
				<fieldset>
					<legend>Параметры с заполнением значения вручную</legend>
					<ul className={s.Group}>
						{optionByTextAreasArr.map(el => (
							<li className={s.GroupItem} key={el.id}>
								<p className={s.Text}>{el.group}</p>
								<Input
									type="text"
									className={s.Text}
									value={
										(filledOptionByTextAreas && // проверяем если ли вообще массив
										filledOptionByTextAreas.find(
											item => +item.group === el.id,
										) && // проверка на наличие искомого элемента
											filledOptionByTextAreas.find(
												item => +item.group === el.id,
											).value) || // берём значение в найденном элементе
										''
									} // Подставляем пустую строку если не удалось найти значение
									placeholder="Введите значение параметра"
									data-fieldset="semimanual"
									data-type="value"
									data-id={el.id}
									onChange={changeManualOptions}
								/>
							</li>
						))}
					</ul>
				</fieldset>
			) : null}

			{manualOptions.length ? ( // проверяем длинну массива и отрисовываем разметку, если есть
				<fieldset>
					<legend>
						Дополнительные параметры с заполнением вручную{' '}
						<span style={{ color: 'red' }}>*</span>
					</legend>
					<ul className={s.Group}>
						{manualOptions.map((el, i) => (
							<li className={s.GroupItem} key={i}>
								<Input
									type="text"
									className={s.Text}
									value={el.group || ''}
									placeholder="Введите название параметра"
									data-fieldset="manual"
									data-type="group"
									data-id={i}
									onChange={changeManualOptions}
								/>
								<Input
									type="text"
									className={s.Text}
									value={el.value || ''}
									placeholder="Введите значение параметра"
									data-fieldset="manual"
									data-type="value"
									data-id={i}
									onChange={changeManualOptions}
								/>
								<Button
									shape="circle"
									size="small"
									type="danger"
									icon="close"
									onClick={removeManualOptions}
									data-id={i}
								/>
							</li>
						))}
					</ul>
				</fieldset>
			) : null}
			<Button onClick={addManualOptions}>
				Добавить дополнительный параметр
			</Button>
		</Fragment>
	);
};

Options.propTypes = {
	// Массивы заполненных параметров, которые хранятся в карточке товара
	selectedOptionsArr: PropTypes.array.isRequired,
	filledOptionByTextAreas: PropTypes.array.isRequired,
	manualOptions: PropTypes.array.isRequired,
	// Массивы возможных параметров исходя из категории товара
	optionsArr: PropTypes.array.isRequired,
	optionByTextAreasArr: PropTypes.array.isRequired,
	// CALLBACKS
	onChangeOptionsCascader: PropTypes.func.isRequired,
	changeManualOptions: PropTypes.func.isRequired,
	addManualOptions: PropTypes.func.isRequired,
	removeManualOptions: PropTypes.func.isRequired,
};

export default Options;
