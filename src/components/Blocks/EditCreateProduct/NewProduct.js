import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import { Modal, Cascader, Tabs, notification, Input, Checkbox } from 'antd';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import CustomButton from '../../Shared/Button/Button';
import 'antd/dist/antd.css';
import styles from '../../Pages/MyProducts/MyProducts.module.css';
import s from './Options.module.css';
import {
	createNewProduct,
	getAllCategories,
	getOptionsByCategory,
	getOptionsByCategoryWithTextArea,
	updateProduct,
	updatePartnerProduct,
} from '../../../utils/api/productsActions';
import ProductPictureGallery from '../ProductPictureGallery/ProductPictureGallery';
import Options from './Options';
import Loader from '../../Shared/Loader/Loader';

const Tab = Tabs.TabPane;

const initialState = {
	// Tabs
	// Tab 1
	name: '',
	brand: '',
	vendorCode: '',
	count: '',
	weight: '',
	packing: {
		length: '',
		width: '',
		height: '',
	},
	description: '',
	// Tab 2
	category: '',
	// Tab 3
	// price: '',
	recommendedPrice: '',
	delivery: true,
	oldPrice: '',
	// Tab 4
	avatarUrl: '',
	imageUrl: '',
	imageUrls: [],
	coverImages: [],
	uploadImage: false,
	// Tab 5
	options: [],
	optionByTextAreas: [],
	manualOptions: [],
	// все возможные опции для категории товара
	optionsArr: [],
	optionByTextAreasArr: [],
	// Выбранные опции для категории товара
	selectedOption: [],
	selectedOptionsArr: [],

	// hide modal
	visible: false,
	// set default active Tab
	activeTabKey: '1',
	// reset default value for category selector
	selectedCategories: [],
	// reset others values
	id: '',
	loading: false,
	contractorProduct: '',
};

class NewProduct extends Component {
	state = {
		...initialState,
	};

	getOptionsByCategoryId = async () => {
		if (this.state.category) {
			const CATEGORY = this.state.category.id || this.state.category;

			await getOptionsByCategory(CATEGORY) // Получаем массив параметров с селективным заполнением для категории товара
				.then(data => {
					this.setState({
						optionsArr: data,
					});
				});

			await getOptionsByCategoryWithTextArea(CATEGORY) // Получаем массив параметров с полуручным заполнением для категории товара
				.then(data => {
					this.setState({
						optionByTextAreasArr: data,
					});
				});
		} else {
			this.setState({
				optionsArr: [],
				optionByTextAreasArr: [],
			});
		}
	};

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	handleOk = e => {
		this.setState({
			visible: false,
		});
	};

	handleCancel = e => {
		this.setState({
			...initialState,
		});

		this.props.onUpdateProduct();
	};

	onDrop = async files => {
		await files.forEach(file => {
			this.getBase64(file, result => {
				this.setState(
					prev => {
						const newArr = prev.coverImages;
						newArr.push({
							imageDecoded: result,
						});
						return {
							coverImages: newArr,
							uploadImage: true,
						};
					},
					() => {
						notification.success({
							message: 'Изображение загружено, не забудьте сохранить изменения',
						});

					},
				);
			});
		});
	};
	toogleloader = () => this.setState({ loading: !this.state.loading });
	getBase64(file, cb) {
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function() {
			cb(reader.result);
		};
		reader.onerror = function(error) {
			console.log('Error: ', error);
		};
	}

	handleInput = ({ target: { value, name, checked } }) => {
		name === 'length' || name === 'width' || name === 'height'
			? this.setState({
					packing: { ...this.state.packing, [name]: +value < 0 ? 0 : value },
			  })
			: this.setState({ [name]: name === 'delivery' ? checked : value });
	};

	checkProductBeforeSave = () => {
		const {
			category,
			count,
			description,
			manualOptions,
			name,
			price,
			vendorCode,
			oldPrice,
			delivery,
		} = this.state;
		let errors = 0;

		!name &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Поле "Название товара" не заполнено',
			});
		!vendorCode &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Поле "Артикул" не заполнено',
			});
		!count &&
			count !== 0 &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Поле "Количество" не заполнено',
			});
		!description &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Поле "Описание" не заполнено',
			});
		!category &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Выберите категорию для вашего товара',
			});
		!price &&
			this.props.user.role === 'CONTRACTOR' &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Укажите цену для вашего товара',
			});
		manualOptions.find(el => el.group === '' || el.value === '') &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message:
					'Все строки дополнительных параметров должны быть заполненны перед сохранением!',
			});

		return errors;
	};

	handleCreateProduct = async e => {
		e.preventDefault();

		if (this.checkProductBeforeSave()) return;
		this.toogleloader();
		try {
			let newProduct = { ...this.state };
			await createNewProduct(newProduct);
			this.props.onUpdate();
			this.handleCancel();
			this.toogleloader();
		} catch (e) {
			this.toogleloader();
		}
	};

	handleCopyProduct = async e => {
		e.preventDefault();

		if (this.checkProductBeforeSave()) return;

		if (this.props.user.role === 'PARTNER') {
			typeof this.state.category === 'object'
				? await createNewProduct({
						...this.state,
						category: this.state.category.id,
						categories: [],
				  })
				: await createNewProduct({
						...this.state,
						categories: [],
				  });
		}

		if (this.props.user.role === 'CONTRACTOR') {
			console.log(this.state.category.id);
			typeof this.state.category === 'object'
				? await createNewProduct({
						...this.state,
						category: this.state.category.id,
						categories: [],
				  })
				: await createNewProduct({
						...this.state,
						categories: [],
				  });
		}

		this.props.onUpdate();
		this.handleCancel();
	};

	updateProduct = async () => {
		let { avatarUrl, coverImages, imageUrls } = this.state;
		if (avatarUrl) {
			const isCoverContainAvatar =
				coverImages.length &&
				coverImages.find(
					el => el.imageDecoded.split('?')[0] === avatarUrl.split('?')[0],
				);
			const isUrlContainAvatar =
				imageUrls.length &&
				imageUrls.find(el => el.url.split('?')[0] === avatarUrl.split('?')[0]);

			// Проверка. Если в процессе редактирования было удалено изображение и оно было назначено на аватар, то нужно удалить аватар
			if (!isCoverContainAvatar && !isUrlContainAvatar) {
				console.log('We delete image that was avatar');
				avatarUrl = '';
			}
		}

		if (this.checkProductBeforeSave()) return;
		const newState = this.state;

		try {
			if (this.props.user.role === 'PARTNER') {
				this.toogleloader();
				typeof this.state.category === 'object'
					? await updatePartnerProduct({
							...newState,
							category: newState.category.id,
							categories: [],
							avatarUrl,
					  })
					: await updatePartnerProduct({
							...newState,
							categories: [],
							avatarUrl,
					  });
				this.toogleloader();
			}
		} catch (e) {
			this.toogleloader();
		}

		try {
			if (this.props.user.role === 'CONTRACTOR') {
				this.toogleloader();
				typeof this.state.category === 'object'
					? await updateProduct({
							...newState,
							category: newState.category.id,
							categories: [],
							avatarUrl,
					  })
					: await updateProduct({
							...newState,
							categories: [],
							avatarUrl,
					  });
				this.toogleloader();
			}
		} catch (e) {
			this.toogleloader();
		}

		this.props.onUpdate();
		this.handleCancel();
	};

	onChangeCascader = value => {
		this.setState({
			options: [],
			optionByTextAreas: [],
			// manualOptions: [], //закоментировано так как теперь не нужно чтобы при смене категории обнулялись параметры введённые вручную. Если опять передумают - просто раскомментиру это ;)
			selectedCategories: value,
			category: value[value.length - 1],
		});
	};

	// START CALLBACKS for Options
	// сохраняет выбранные параметры в state
	onChangeOptionsCascader = (value, selectedOptions, groupId) => {
		[value] = value;

		if (!value) {
			const options = [
				...this.state.options.filter(el => el.group !== groupId),
			];
			this.setState({
				options,
			});
			return;
		}

		const productOptionsArr = this.state.optionsArr;
		const selectedOption = (optionId => {
			const findGroupId = productOptionsArr.find(el =>
				el.values.find(el => el.id === optionId),
			).id;
			return { group: findGroupId, value };
		})(value);
		const options = [
			...this.state.options.filter(el => el.group !== selectedOption.group),
			selectedOption,
		];

		this.setState(
			{
				options,
			},
			// ()=>console.log('!!! NEW STATE', this.state)
		);
	};
	// записывает значение инпута в state
	changeManualOptions = e => {
		let { value } = e.target;
		const FIELDSET = e.target.dataset.fieldset;
		const TYPE = e.target.dataset.type;
		const ID = +e.target.dataset.id;
		console.log('TYPE', TYPE);
		if (FIELDSET === 'manual') {
			const updated = [
				...this.state.manualOptions.map((el, i) =>
					i === ID ? { ...el, [TYPE]: value } : el,
				),
			];
			this.setState(
				{
					manualOptions: updated,
				},

			);
		}
		if (FIELDSET === 'semimanual') {
			console.log('changeManualOptions\nSEMIMANUAL\n');
			const updated = value
				? [
						...this.state.optionByTextAreas.filter(el => el.group !== ID),
						{ group: ID, value: value },
				  ]
				: [...this.state.optionByTextAreas.filter(el => el.group !== ID)];
			this.setState(
				{
					optionByTextAreas: updated,
				},
				() =>
					console.log('changeManualOptions\nSTATE after setState', this.state),
			);
		}
	};
	// добавляет поле для полностью ручных параметров
	addManualOptions = () => {
		const { manualOptions } = this.state;

		manualOptions.length === 0
			? this.setState((state, props) => ({
					manualOptions: [{ group: '', value: '' }],
			  }))
			: manualOptions[manualOptions.length - 1].group === '' ||
			  manualOptions[manualOptions.length - 1].value === ''
			? notification.error({
					message:
						'Прежде чем добавить новый параметр, заполните поля предыдущего параметра',
			  })
			: this.setState((state, props) => ({
					manualOptions: [...state.manualOptions, { group: '', value: '' }],
			  }));
	};
	// удаляет поле для полностью ручных параметров
	removeManualOptions = e => {
		const ID = +e.target.dataset.id;
		this.setState((state, props) => ({
			manualOptions: state.manualOptions.filter((el, i) => i !== ID),
		}));
	};
	// END CALLBACKS for Options

	handleAddImageUrl = () => {
		this.state.imageUrl &&
			this.setState(
				{
					imageUrl: '',
					imageUrls: [
						...this.state.imageUrls,
						{
							url: this.state.imageUrl,
						},
					],
				},
				// () => console.log('ADDed URL IMG:', this.state)
			);
	};

	// ========================= callbacks for PPG  =========================
	// =============================== START ===============================
	// ================== (change avatar  of product card) ==================
	changeAvatarUrl = avatarUrl =>
		this.setState({ avatarUrl }, () =>
			notification.success({
				message:
					'Аватар изменён, не забудьте нажать кнопку "Сохранить" чтобы изменения вступили в силу',
			}),
		);

	// ====================== (used to delete images) ======================
	handleUpdateImages = (newCovers, newUrls) => {
		this.setState({
			coverImages: [...newCovers],
			imageUrls: [...newUrls],
		});
	};
	// ================================ END ================================

	descriptionEditor = data => {
		// console.log(data);
		this.setState({ description: data });
	};

	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++
	async componentDidMount() {
		const allCategoriesList = await getAllCategories(); // получаем полный список категорий
		console.log(
			'-= NewProduct =-\n Полный список категорий в том виде, в каком они приходят с бэка',
			allCategoriesList,
		);
		if (this.state.category) this.getOptionsByCategoryId();
		const renaming = item =>
			!item.subcategories.length // функция для ренейминга входящих данных в вид, подходящий для ant.design Cascader
				? {
						label: item.name,
						value: item.id,
				  }
				: {
						label: item.name,
						value: item.id,
						children: item.subcategories.map(renaming),
				  };

		const renamedAllCategoriessList = allCategoriesList.map(renaming); // конвертируем данные в вид, подходящий для ant.Design Cascader
		// console.log('-= NewProduct =-\n Полный список категорий в изменённом виде', renamedAllCategoriessList);
		// и записываем их в state
		this.setState({
			categories: renamedAllCategoriessList,
		});
		// P.S. По моему сугубо личному мнению это создаёт неоправданную лишнюю нагрузку.
		// Вместо того чтобы каждый раз при вызове редактирования карточки товара спрашивать у бэка список категорий, лучше сделать это единожды при логинизации пользователя.
		// И хранить эти данные в store Redux-a.
		// #мысли #оптимизация #оптимизацияприложения
	}
	componentDidUpdate(prevProps, prevState) {
		if (this.state.category && prevState.category !== this.state.category) {
			this.getOptionsByCategoryId();
		}
	}
	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.update) {
			this.setState({
				...nextProps.product,
				visible: true,
			});
		}
	}
	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++

	render() {
		const {
			id,
			name,
			brand,
			vendorCode,
			count,
			description,
			price,
			recommendedPrice,
			imageUrls,
			imageUrl,
			categories,
			selectedCategories,
			activeTabKey,
			coverImages,
			uploadImage,
			options,
			optionByTextAreas,
			manualOptions,
			optionsArr,
			optionByTextAreasArr,
			partnerPercent,
			contractorPriceForPartner,
			weight,
			packing: { length, width, height },
			visible,
			loading,
			delivery,
			oldPrice,
		} = this.state;
		const {
			handleCancel,
			handleOk,
			handleInput,
			handleUpdateImages,
			handleAddImageUrl,
			changeAvatarUrl,
			updateProduct,
			handleCreateProduct,
			onChangeCascader,
			showModal,
			descriptionEditor,
		} = this;

		// console.log('-= NewProducts =-\nSTATE after render()', this.state);

		return (
			<Fragment>
				{this.props.user.role === 'CONTRACTOR' && (
					<CustomButton
						title="Добавить товар"
						type="primary"
						onClick={showModal}
					/>
				)}
				{/* <Modal>Content</Modal> */}
				<Modal
					width={990}
					title={id ? name : 'Новый товар'}
					visible={visible}
					className="create-edit-modal"
					onOk={handleOk}
					onCancel={handleCancel}
					footer={false}
				>
					<div className="modal">
						{loading && <Loader />}
						<Tabs
							type="line"
							animated={false}
							activeKey={activeTabKey}
							onChange={key => this.setState({ activeTabKey: key })}
						>
							<Tab tab="Основная информация" key="1">
								<form className={styles.mainInfo}>
									<div className={styles.inputsGroup}>
										<div>
											<label>
												Название товара <span style={{ color: 'red' }}>*</span>
											</label>
											<Input
												type="text"
												name="name"
												value={name}
												onChange={handleInput}
												// style={{ width: 896 }}
											/>
										</div>
										<div>
											<label>Бренд</label>
											<Input
												type="text"
												name="brand"
												value={brand || ''}
												onChange={handleInput}
											/>
										</div>
										<div>
											<label>
												Артикул <span style={{ color: 'red' }}>*</span>
											</label>
											<Input
												type="text"
												name="vendorCode"
												value={vendorCode}
												onChange={handleInput}
											/>
										</div>
										<div>
											<label>
												Количество <span style={{ color: 'red' }}>*</span>
											</label>
											<Input
												type="number"
												name="count"
												value={count}
												onChange={handleInput}
											/>
										</div>
										<div>
											<label>Вес (кг) </label>
											<Input
												type="number"
												step="0.1"
												name="weight"
												value={weight}
												onChange={handleInput}
												// style={{ width: 210 }}
											/>
										</div>

										<div className={styles.volumes}>
											<h3>Габариты упаковки (см)</h3>
											<div>
												<label>Длина (см) </label>
												<Input
													type="number"
													placeholder="Длина"
													name="length"
													value={length}
													onChange={handleInput}
												/>
											</div>
											<div>
												<label>Ширина (см) </label>
												<Input
													type="number"
													placeholder="Ширина"
													name="width"
													value={width}
													onChange={handleInput}
												/>
											</div>
											<div>
												<label>Высота (см) </label>
												<Input
													type="number"
													placeholder="Высота"
													name="height"
													value={height}
													onChange={handleInput}
												/>
											</div>
										</div>
									</div>
									<div style={{ display: 'flex', alignItems: 'center' }}>
										<label>Доставка:</label>
										<Checkbox
											style={{ marginLeft: 20 }}
											checked={delivery}
											name="delivery"
											onChange={handleInput}
										/>
									</div>
									<div>
										<label>
											Описание <span style={{ color: 'red' }}>*</span>
										</label>
										<CKEditor
											editor={ClassicEditor}
											data={description}
											config={{
												toolbar: [
													// 'Heading',
													'|',
													'Bold',
													'Italic',
													'Alignment',
													'|',
													'BulletedList',
													'NumberedList',
													'|',
													// 'Link',
													// 'BlockQuote',
													'Undo',
													'Redo',
												],
												removePlugins: [
													'Image',
													'ImageCaption',
													'ImageStyle',
													'ImageToolbar',
													'ImageUpload',
												],
											}}
											onInit={editor => {
												// You can store the "editor" and use when it is needed.
												console.log('Editor is ready to use!', editor);
											}}
											onChange={(event, editor) => {
												const data = editor.getData();
												// console.log({ event, editor, data });
												descriptionEditor(data);
											}}
											onBlur={(event, editor) => {
												console.log('Blur.', editor);
											}}
											onFocus={(event, editor) => {
												console.log('Focus.', editor);
											}}
										/>
										<br />
									</div>

									<div className={styles.buttomcontrols}>
										<span>
											<span style={{ color: 'red' }}>*</span> - поле обязательно
											к заполнению
										</span>
										<CustomButton
											type="primary"
											title="Копировать товар"
											disabled={true}
											onClick={this.handleCopyProduct}
										/>
										{id ? (
											<CustomButton
												type="primary"
												title="Сохранить"
												onClick={updateProduct}
											/>
										) : (
											<CustomButton
												title="Далее"
												type="primary"
												onClick={() => this.setState({ activeTabKey: '2' })}
											/>
										)}
									</div>
								</form>
							</Tab>
							<Tab tab="Категории" key="2">
								<form className={styles.selectCategory}>
									<div>
										<label>
											Выберите категорию для вашего товара{' '}
											<span style={{ color: 'red' }}>*</span>
										</label>
										<Cascader
											defaultValue={selectedCategories}
											value={selectedCategories}
											options={categories}
											onChange={onChangeCascader}
											expandTrigger="hover"
											changeOnSelect
											placeholder="Please select"
											// popupPlacement="bottomRight"
										/>
									</div>
									<div className={styles.buttomcontrols}>
										<span>
											<span style={{ color: 'red' }}>*</span> - поле обязательно
											к заполнению
										</span>
										{id ? (
											<CustomButton
												title="Сохранить"
												type="primary"
												// className={styles.save}
												onClick={updateProduct}
											/>
										) : (
											<CustomButton
												title="Далее"
												type="primary"
												// className={styles.save}
												onClick={() => this.setState({ activeTabKey: '3' })}
											/>
										)}
									</div>
								</form>
							</Tab>

							<Tab tab="Цена" key="3">
								<form className={styles.selectPrice}>
									{this.props.user.role === 'PARTNER' ? (
										this.props.product.contractorId ? (
											<div>
												<p>
													Цена товара от поставщика:{' '}
													<b>{contractorPriceForPartner} ₴</b>
												</p>
												<label>
													Укажите свой процент наценки для товара (%)
												</label>
												<Input
													type="number"
													name="partnerPercent"
													value={partnerPercent}
													onChange={e => e.target.value >= 0 && handleInput(e)}
												/>
												<label>
													Укажите старую цену без скидки для товара, ₴
												</label>
												<Input
													type="number"
													name="oldPrice"
													value={oldPrice}
													onChange={handleInput}
												/>
												<p>
													Цена товара после наценки:{' '}
													<b>
														{parseInt(
															(+contractorPriceForPartner *
																(100 + +partnerPercent)) /
																100,
														)}{' '}
														₴
													</b>
												</p>
											</div>
										) : (
											<div>
												<label>
													Укажите цену за которую Вы приобрели Ваш товар (грн)
													<span style={{ color: 'red' }}> *</span>
												</label>
												<Input
													type="number"
													step="0.01"
													name="price"
													value={price}
													onChange={handleInput}
												/>
												<label>
													Укажите старую цену без скидки для товара, ₴
												</label>
												<Input
													type="number"
													name="oldPrice"
													value={oldPrice}
													onChange={handleInput}
												/>
												<label>
													Укажите свой процент наценки для товара (%)
												</label>
												<Input
													type="number"
													name="partnerPercent"
													value={partnerPercent}
													onChange={handleInput}
												/>

												<p>
													Цена товара после наценки:{' '}
													<b>
														{((+price * (100 + +partnerPercent)) / 100).toFixed(
															2,
														)}{' '}
														₴
													</b>
												</p>
											</div>
										)
									) : (
										<div>
											<label>
												Укажите цену для вашего товара (грн)
												<span style={{ color: 'red' }}> *</span>
											</label>
											<Input
												type="number"
												name="price"
												value={price}
												onChange={handleInput}
											/>
											<label>
												Укажите старую цену без скидки для товара, ₴
											</label>
											<Input
												type="number"
												name="oldPrice"
												value={oldPrice}
												onChange={handleInput}
											/>

											<label>
												Укажите рекомендуемую розничную цену для вашего товара
												(грн)
												<span style={{ color: 'red' }}> *</span>
											</label>
											<Input
												type="text"
												name="recommendedPrice"
												value={recommendedPrice}
												onChange={handleInput}
											/>
										</div>
									)}
									<div className={styles.buttomcontrols}>
										{this.props.user.role === 'CONTRACTOR' && (
											<span>
												<span style={{ color: 'red' }}>*</span> - поле
												обязательно к заполнению
											</span>
										)}
										{id ? (
											<CustomButton
												title="Сохранить"
												type="primary"
												// className={styles.save}
												onClick={updateProduct}
											/>
										) : (
											<CustomButton
												title="Далее"
												type="primary"
												onClick={() => this.setState({ activeTabKey: '4' })}
											/>
										)}
									</div>
								</form>
							</Tab>

							<Tab tab="Изображение" key="4">
								<div className={styles.addPicture}>
									{coverImages.length > 0 || imageUrls.length > 0 ? (
										<ProductPictureGallery
											coverImageList={coverImages}
											urlImageList={imageUrls}
											deleteImage={handleUpdateImages}
											changeAvatarUrl={changeAvatarUrl}
											key={id}
										/>
									) : (
										<div className={styles.defaultPicture}>
											<img
												height="120"
												width="120"
												src={'https://kare.ee/images/no-image.jpg'}
											/>
										</div>
									)}

									<div className={styles.upload}>
										<div>
											{/* <input type="file" name="uploadfile" id="addImg" /> */}
											<Dropzone
												onDrop={this.onDrop}
												onRe
												accept=".png, .svg, .jpg, .jpeg"
											>
												{({ getRootProps, getInputProps }) => (
													<div {...getRootProps({ className: 'dropzone' })}>
														<input {...getInputProps()} />
														<CustomButton
															type="primary"
															title="Добавить картинку"
														/>
													</div>
												)}
											</Dropzone>
										</div>

										<div className={styles.addUrl}>
											<Input
												type="text"
												name="imageUrl"
												value={imageUrl}
												placeholder="Вставьте сюда ссылку на картинку"
												onChange={handleInput}
											/>

											<CustomButton
												title="Добавить URL"
												type="primary"
												// className={styles.addUrlBtn}
												onClick={handleAddImageUrl}
											/>
										</div>
									</div>

									<div className={styles.buttomcontrols}>
										{uploadImage ? (
											<span>Изображение загружено</span>
										) : (
											<span>
												Фотография должна быть не меньше 150х150 пикселей, и не
												болше чем 1000х1000 пикселей.
											</span>
										)}

										{id ? (
											<CustomButton
												title="Сохранить"
												type="primary"
												onClick={updateProduct}
											/>
										) : (
											<CustomButton
												type="primary"
												title="Далее"
												onClick={() => this.setState({ activeTabKey: '5' })}
											/>
										)}
									</div>
								</div>
							</Tab>

							{/* ================== ПАРАМЕТРЫ =================== */}
							<Tab tab="Параметры" key="5">
								<ul className={s.Group}>
									<Options
										// Передаём списки заполненных параметров, которые хранятся в карточке товара
										selectedOptionsArr={options}
										filledOptionByTextAreas={optionByTextAreas}
										manualOptions={manualOptions}
										// Передаём массивы возможных параметров исходя из категории товара
										optionsArr={optionsArr}
										optionByTextAreasArr={optionByTextAreasArr}
										// CALLBACKS
										onChangeOptionsCascader={this.onChangeOptionsCascader}
										changeManualOptions={this.changeManualOptions}
										addManualOptions={this.addManualOptions}
										removeManualOptions={this.removeManualOptions}
									/>
								</ul>

								<div className={styles.buttomcontrols}>
									<span>
										<span style={{ color: 'red' }}>*</span> - поле обязательно к
										заполнению
									</span>
									{id ? (
										<CustomButton
											type="primary"
											title="Сохранить"
											// className={styles.save}
											onClick={updateProduct}
										/>
									) : (
										<CustomButton
											title="Сохранить"
											type="primary"
											onClick={handleCreateProduct}
										/>
									)}
								</div>
							</Tab>
							{/* ================== ПАРАМЕТРЫ =================== */}
						</Tabs>
					</div>
				</Modal>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

export default connect(mapStateToProps)(NewProduct);
