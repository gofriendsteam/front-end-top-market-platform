import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Modal, Cascader, Tabs, notification, Input } from 'antd';
import CustomButton from '../../Shared/Button/Button';
import 'antd/dist/antd.css';
import styles from '../../Pages/MyProducts/MyProducts.module.css';
import s from './Options.module.css';
import {
	getAllCategories,
	getOptionsByCategory,
	getOptionsByCategoryWithTextArea,
	// getFirstLevelCategories,
	updateProduct,
	updatePartnerProduct,
	updateAllSelectedProducts,
} from '../../../utils/api/productsActions';

import Options from './Options';

const Tab = Tabs.TabPane;

const initialState = {
	// Tabs
	// Tab 1

	brand: '',

	count: '',

	// Tab 2
	category: '',
	// Tab 3
	// price: '',
	// recommendedPrice: '',
	// Tab 4

	// Tab 5
	options: [],
	optionByTextAreas: [],
	manualOptions: [],
	// все возможные опции для категории товара
	optionsArr: [],
	optionByTextAreasArr: [],
	// Выбранные опции для категории товара
	selectedOption: [],
	selectedOptionsArr: [],

	// hide modal
	visible: false,
	// set default active Tab
	activeTabKey: '1',
	// reset default value for category selector
	// reset others values
};

class NewProduct extends Component {
	state = {
		...initialState,
	};

	getOptionsByCategoryId = async () => {
		if (this.state.category) {
			const CATEGORY = this.state.category.id || this.state.category;

			await getOptionsByCategory(CATEGORY) // Получаем массив параметров с селективным заполнением для категории товара
				.then(data => {
					this.setState({
						optionsArr: data,
					});
				});

			await getOptionsByCategoryWithTextArea(CATEGORY) // Получаем массив параметров с полуручным заполнением для категории товара
				.then(data => {
					this.setState({
						optionByTextAreasArr: data,
					});
				});
		} else {
			this.setState({
				optionsArr: [],
				optionByTextAreasArr: [],
			});
		}
	};
	handleOpen = () => {
		this.setState({

			visible: true,
		});
	}
	handleCancel = e => {
		this.setState({
			...initialState,
			visible: false,
		});
		// this.props.closeModal(false);
		//
		// this.props.onUpdateProduct();
	};

	handleInput = ({ target: { value, name } }) => {
		name === 'length' || name === 'width' || name === 'height'
			? this.setState({ packing: { ...this.state.packing, [name]: value } })
			: this.setState({ [name]: value });
	};

	updateProduct = async () => {
		const { onUpdate } = this.props;
		const data = {
			productListIds: this.props.selectedRowKeys,
			brand: this.state.brand,
			count: this.state.count,
			category: this.state.category,
			options: this.state.options,
			manualOptions: this.state.manualOptions,
			optionByTextAreas: this.state.optionByTextAreas,
			price: this.state.price,
			recommendedPrice: this.state.recommendedPrice,
			partnerPercent: this.state.partnerPercent,
		};
		for (let key in data) {
			if (!data[key] || data[key].length === 0) {
				delete data[key];
			}
		}
		try {
			const res = await updateAllSelectedProducts(data);
			onUpdate();
			this.handleCancel();
		} catch (e) {
			console.log(e);
		}

		// this.props.onUpdate();
		// this.handleCancel();
	};

	onChangeCascader = value => {
		this.setState({
			options: [],
			optionByTextAreas: [],
			// manualOptions: [], //закоментировано так как теперь не нужно чтобы при смене категории обнулялись параметры введённые вручную. Если опять передумают - просто раскомментиру это ;)
			selectedCategories: value,
			category: value[value.length - 1],
		});
	};

	// START CALLBACKS for Options
	// сохраняет выбранные параметры в state
	onChangeOptionsCascader = (value, selectedOptions, groupId) => {
		[value] = value;

		if (!value) {
			const options = [
				...this.state.options.filter(el => el.group !== groupId),
			];
			this.setState({
				options,
			});
			return;
		}

		const productOptionsArr = this.state.optionsArr;
		const selectedOption = (optionId => {
			const findGroupId = productOptionsArr.find(el =>
				el.values.find(el => el.id === optionId),
			).id;
			return { group: findGroupId, value };
		})(value);
		const options = [
			...this.state.options.filter(el => el.group !== selectedOption.group),
			selectedOption,
		];

		this.setState(
			{
				options,
			},
			// ()=>console.log('!!! NEW STATE', this.state)
		);
	};
	// записывает значение инпута в state
	changeManualOptions = e => {
		let { value } = e.target;
		const FIELDSET = e.target.dataset.fieldset;
		const TYPE = e.target.dataset.type;
		const ID = +e.target.dataset.id;

		if (FIELDSET === 'manual') {
			const updated = [
				...this.state.manualOptions.map((el, i) =>
					i === ID ? { ...el, [TYPE]: value } : el,
				),
			];
			this.setState(
				{
					manualOptions: updated,
				},
				() =>
					console.log('changeManualOptions\nSTATE after setState', this.state),
			);
		}
		if (FIELDSET === 'semimanual') {
			console.log('changeManualOptions\nSEMIMANUAL\n');
			const updated = value
				? [
						...this.state.optionByTextAreas.filter(el => el.group !== ID),
						{ group: ID, value: value },
				  ]
				: [...this.state.optionByTextAreas.filter(el => el.group !== ID)];
			this.setState(
				{
					optionByTextAreas: updated,
				});
		}
	};
	// добавляет поле для полностью ручных параметров
	addManualOptions = () => {
		const { manualOptions } = this.state;

		manualOptions.length === 0
			? this.setState((state, props) => ({
					manualOptions: [{ group: '', value: '' }],
			  }))
			: manualOptions[manualOptions.length - 1].group === '' ||
			  manualOptions[manualOptions.length - 1].value === ''
			? notification.error({
					message:
						'Прежде чем добавить новый параметр, заполните поля предыдущего параметра',
			  })
			: this.setState((state, props) => ({
					manualOptions: [...state.manualOptions, { group: '', value: '' }],
			  }));
	};
	// удаляет поле для полностью ручных параметров
	removeManualOptions = e => {
		const ID = +e.target.dataset.id;
		this.setState((state, props) => ({
			manualOptions: state.manualOptions.filter((el, i) => i !== ID),
		}));
	};
	// END CALLBACKS for Options

	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++
	async componentDidMount() {
		const allCategoriesList = await getAllCategories(); // получаем полный список категорий

		if (this.state.category) this.getOptionsByCategoryId();
		const renaming = item =>
			!item.subcategories.length // функция для ренейминга входящих данных в вид, подходящий для ant.design Cascader
				? {
						label: item.name,
						value: item.id,
				  }
				: {
						label: item.name,
						value: item.id,
						children: item.subcategories.map(renaming),
				  };

		const renamedAllCategoriessList = allCategoriesList.map(renaming); // конвертируем данные в вид, подходящий для ant.Design Cascader
		// console.log('-= NewProduct =-\n Полный список категорий в изменённом виде', renamedAllCategoriessList);
		// и записываем их в state
		this.setState({
			categories: renamedAllCategoriessList,
		});
		// P.S. По моему сугубо личному мнению это создаёт неоправданную лишнюю нагрузку.
		// Вместо того чтобы каждый раз при вызове редактирования карточки товара спрашивать у бэка список категорий, лучше сделать это единожды при логинизации пользователя.
		// И хранить эти данные в store Redux-a.
		// #мысли #оптимизация #оптимизацияприложения
	}
	componentDidUpdate(prevProps, prevState) {
		if (this.state.category && prevState.category !== this.state.category) {
			this.getOptionsByCategoryId();
		}
	}
	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.update) {
			this.setState({
				visible: true,
			});
		}
	}
	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++

	render() {
		const {
			id,
			name,
			brand,
			vendorCode,
			count,
			description,
			price,
			recommendedPrice,
			imageUrls,
			imageUrl,
			categories,
			selectedCategories,
			activeTabKey,
			coverImages,
			uploadImage,
			options,
			optionByTextAreas,
			manualOptions,
			optionsArr,
			optionByTextAreasArr,
			partnerPercent,
			contractorPriceForPartner,
			weight,
			visible,
		} = this.state;
		const { handleInput, onChangeCascader } = this;
		const priceWithPercent = ((+price * (100 + +partnerPercent)) / 100).toFixed(
			2,
		);
		// console.log(priceWithPercent, isNaN(priceWithPercent))
		return (
			<Fragment>
				<CustomButton
					title="Редактировать все"
					type="primary"
					disabled={this.props.selectedRowKeys.length < 2}
					onClick={this.handleOpen}
				/>
				<Modal
					width={990}
					title={'Редактироваие выбраных товаров'}
					visible={visible}
					className="create-edit-modal"
					// onOk={this.handleOk}
					onCancel={this.handleCancel}
					footer={false}
				>
					<Tabs
						type="line"
						animated={false}
						activeKey={activeTabKey}
						onChange={key => this.setState({ activeTabKey: key })}
					>
						<Tab tab="Основная информация" key="1">
							<form className={styles.mainInfo}>
								<div className={styles.inputsMassGroup}>
									<div>
										<label>Бренд</label>
										<Input
											type="text"
											name="brand"
											value={brand || ''}
											onChange={handleInput}
										/>
									</div>
									<div>
										<label>
											Количество <span style={{ color: 'red' }}>*</span>
										</label>
										<Input
											type="number"
											name="count"
											value={count}
											onChange={handleInput}
										/>
									</div>
								</div>

								<div className={styles.buttomcontrols}>
								<span>
									<span style={{ color: 'red' }}>*</span> - при изменении полей,
									они обновятся во всех выбранных товарах.
								</span>
									{/*<CustomButton*/}
									{/*	type="primary"*/}
									{/*	title="Копировать товар"*/}
									{/*	onClick={this.handleCopyProduct}*/}
									{/*/>*/}

									<CustomButton
										type="primary"
										title="Сохранить"
										onClick={this.updateProduct}
									/>
								</div>
							</form>
						</Tab>
						<Tab tab="Категории" key="2">
							<form className={styles.selectCategory}>
								<div>
									<label>
										Выберите категорию для ваших товаров{' '}
										<span style={{ color: 'red' }}>*</span>
									</label>
									<Cascader
										defaultValue={selectedCategories}
										value={selectedCategories}
										options={categories}
										onChange={onChangeCascader}
										expandTrigger="hover"
										changeOnSelect
										placeholder="Please select"
										// popupPlacement="bottomRight"
									/>
								</div>
								<div className={styles.buttomcontrols}>
								<span>
									<span style={{ color: 'red' }}>*</span> - при изменении полей,
									они обновятся во всех выбранных товарах.
								</span>
									<CustomButton
										title="Сохранить"
										type="primary"
										// className={styles.save}
										onClick={this.updateProduct}
									/>
								</div>
							</form>
						</Tab>

						<Tab tab="Цена" key="3">
							<form className={styles.selectPrice}>
								{this.props.user.role === 'PARTNER' ? (
									<div>
										<label>
											Укажите цену за которую Вы приобрели Ваш товар (грн)
											<span style={{ color: 'red' }}> *</span>
										</label>
										<Input
											type="number"
											step="0.01"
											name="price"
											value={price}
											className={styles.priceInput}
											onChange={handleInput}
										/>
										<label>Укажите свой процент наценки для товара (%)</label>
										<Input
											className={styles.priceInput}
											type="number"
											name="partnerPercent"
											value={partnerPercent}
											onChange={handleInput}
										/>

										<p>
											Цена товара после наценки:{' '}
											<b>{isNaN(priceWithPercent) ? 0.0 : priceWithPercent} ₴</b>
										</p>
									</div>
								) : (
									<div>
										<label>
											Укажите цену для вашего товара (грн)
											<span style={{ color: 'red' }}> *</span>
										</label>
										<Input
											type="number"
											name="price"
											value={price}
											onChange={handleInput}
										/>
										<label>
											Укажите рекомендуемую розничную цену для выбраных товаров
											(грн)
											<span style={{ color: 'red' }}> *</span>
										</label>
										<Input
											type="text"
											name="recommendedPrice"
											value={recommendedPrice}
											onChange={handleInput}
										/>
									</div>
								)}
								<div className={styles.buttomcontrols}>
								<span>
									<span style={{ color: 'red' }}>*</span> - при изменении полей,
									они обновятся во всех выбранных товарах.
								</span>
									<CustomButton
										title="Сохранить"
										type="primary"
										// className={styles.save}
										onClick={this.updateProduct}
									/>
								</div>
							</form>
						</Tab>
						{/* ================== ПАРАМЕТРЫ =================== */}
						<Tab tab="Параметры" key="4">
							<ul className={s.Group}>
								<Options
									// Передаём списки заполненных параметров, которые хранятся в карточке товара
									selectedOptionsArr={options}
									filledOptionByTextAreas={optionByTextAreas}
									manualOptions={manualOptions}
									// Передаём массивы возможных параметров исходя из категории товара
									optionsArr={optionsArr}
									optionByTextAreasArr={optionByTextAreasArr}
									// CALLBACKS
									onChangeOptionsCascader={this.onChangeOptionsCascader}
									changeManualOptions={this.changeManualOptions}
									addManualOptions={this.addManualOptions}
									removeManualOptions={this.removeManualOptions}
								/>
							</ul>

							<div className={styles.buttomcontrols}>
							<span>
								<span style={{ color: 'red' }}>*</span> - при изменении полей,
								они обновятся во всех выбранных товарах.
							</span>
								<CustomButton
									type="primary"
									title="Сохранить"
									// className={styles.save}
									onClick={this.updateProduct}
								/>
							</div>
						</Tab>
						{/* ================== ПАРАМЕТРЫ =================== */}
					</Tabs>
				</Modal>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);
