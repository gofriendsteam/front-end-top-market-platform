import React, { useState, useEffect } from 'react';
import TopbarDropdownWrapper from '../TopBar/TopbarNotification.styles';
import { notification, Popover } from 'antd';
import CustomButton from '../../Shared/Button/Button';
import styles from '../../Pages/MyProducts/MyProducts.module.css';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { generateYml } from '../../../utils/api/productsActions';

const ExportYmlProducts = ({ productslength }) => {
	const [visible, setVisiblity] = useState(false);
	const [ymlUrls, setUrl] = useState({ rozetkaUrl: '', promUrl: '' });
	const handleGenerateYml = async () => {
		const { results } = await generateYml();

		const rozetkaUrl = results.find(el => el.ymlType === 'rozetka')
			? results.find(el => el.ymlType === 'rozetka').template
			: null;
		const promUrl = results.find(el => el.ymlType === 'prom')
			? results.find(el => el.ymlType === 'prom').template
			: null;
		setUrl({ rozetkaUrl, promUrl });
	};
	// const customizedTheme = useSelector(state => state.ThemeSwitcher.topbarTheme);
	const [copied, setCopy] = useState(false);
	useEffect(() => {
		handleGenerateYml();
	}, []);
	const handleCopied = () => {
		setCopy(true);
		notification.success({
			message: 'Скопировано',
		});
	};
	function handleVisibleChange() {
		setVisiblity(visible => !visible);
	}
	console.log(ymlUrls);
	const content = (
		<TopbarDropdownWrapper className="topbarNotification export">
			{/*<div>asdadasdas</div>*/}
			<div className="isoDropdownHeader">
				<h3>
					Мои YML файлы
					{/*<IntlMessages id="sidebar.notification" />*/}
				</h3>
			</div>
			<div className="isoDropdownBody">
				{Object.keys(ymlUrls).map(
					type =>
						ymlUrls[type] && (
							<div className="isoDropdownListItem" key={ymlUrls[type]}>
								<h4 className="export-list-head">
									Ссылка на{' '}
									{type === 'rozetkaUrl'
										? 'Rozetka.ua Yml файл'
										: 'Prom.ua Yml файл'}
								</h4>
								<CopyToClipboard
									key={type}
									text={ymlUrls[type]}
									onCopy={() => handleCopied()}
								>
									<div className="export-list-wrapper">
										<span>{ymlUrls[type]}</span>
										<i className="icon ion-md-copy" />
									</div>
								</CopyToClipboard>
							</div>
						),
				)}
			</div>
		</TopbarDropdownWrapper>
	);
	return (
		<Popover
			content={content}
			trigger="click"
			visible={visible}
			onVisibleChange={handleVisibleChange}
			placement="bottom"
		>
			<div className="isoIconWrapper" style={{ display: 'inline-block' }}>
				<CustomButton
					title="Мои YML файлы"
					type="primary"
					disabled={productslength === 0}
					style={{ margin: 0 }}
				/>
			</div>
		</Popover>
	);
};

export default ExportYmlProducts;
