import React, { useState, useEffect } from 'react';
import { ImportExportWrapper } from './ie.styles';
import { options } from './options';
import { Radio, Icon, notification } from 'antd';
import { useDropzone } from 'react-dropzone';
import { useHistory } from 'react-router-dom';
import { uploadXls } from '../../../utils/api/productsActions';
import { useSelector } from 'react-redux';
import Loader from '../../Shared/Loader/Loader';

const ImortYmlProducts = ({ updateProducts, closeModal }) => {
	const [currentRadioValue, setValue] = useState('yml');
	const [isSuccess, setSuccessState] = useState(false);
	const [loading, setLoading] = useState(false);
	const history = useHistory();
	const role = useSelector(state => state.user.role);

	const fetchFiles = async file => {
		const formData = new FormData();
		formData.append('xls_file', file[0]);
		formData.append('file_type', currentRadioValue);
		setLoading(true);
		try {
			await uploadXls(formData);

			notification.success({
				message: 'Файл успешно импортирован.',
			});
			setLoading(false);
			setSuccessState(true);
			updateProducts();
			closeModal({ isOpen: false });
			const redirectUrl =
				role === 'PARTNER'
					? '/admin/my_products/download_history'
					: '/admin/products/download_history';
			history.push(redirectUrl);
		} catch (e) {
			setLoading(false);
		}
	};

	const {
		getRootProps,
		getInputProps,
		isDragActive,
		rejectedFiles,
		acceptedFiles,
	} = useDropzone({
		accept: '.xlsx, .xls, .csv, .xml, .yml',
	});
	const handleChangeType = ({ target: { value } }) => {
		if (isSuccess) setSuccessState(false);
		setValue(value);
	};
	useEffect(() => {
		if (rejectedFiles.length > 0) return;
		if (acceptedFiles.length === 1) {
			fetchFiles(acceptedFiles);
		}
	}, [rejectedFiles, acceptedFiles]);

	return (
		<ImportExportWrapper>
			{loading && <Loader />}
			<div className="type-choose">
				<h5>Выберите YML для импорта</h5>
				<Radio.Group onChange={handleChangeType} value={currentRadioValue}>
					{options.map(({ title, type, disabled }) => (
						<Radio className="radio" value={type} disabled={disabled}>
							{title}
						</Radio>
					))}
				</Radio.Group>
			</div>
			<div className="drop-zone" {...getRootProps()}>
				<input {...getInputProps()} />
				{isDragActive && <div className="overlay" />}
				{isSuccess ? (
					<div className="notify">
						<Icon type="check" />
						<p>Файл {acceptedFiles[0].name} был успешно загружен.</p>
						{/*<p>Для дальнейшей загрузки нажмите кнопку 'Загрузить'</p>*/}
					</div>
				) : rejectedFiles.length > 0 ? (
					<div className="notify">
						<Icon type="close" />
						<p>Неверный тип файла, попробуйте другой!</p>
					</div>
				) : (
					<div className="notify">
						<Icon type="cloud-upload" />
						<p>Для загрузки файла нажмите сюда или перетяните файл!</p>
					</div>
				)}
			</div>
		</ImportExportWrapper>
	);
};

export default ImortYmlProducts;
