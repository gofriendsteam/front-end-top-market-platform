import styled from 'styled-components';
// import { palette } from 'styled-theme';

export const ImportExportWrapper = styled.div`
	min-height: 350px;
	display: flex;
	//flex-wrap: wrap;
	//background: red;
	@media only screen and (max-width: 768px) {
	  flex-wrap: wrap;
	}
	h5 {
		//flex-basis: 100%;
		font-size: 16px;
		margin-bottom: 15px;
	}
	.type-choose {
		width: 41%;
		margin-right: 10px;
		display: flex;
		flex-direction: column;
		button {
			margin: auto 0 0 0;
		}
		@media only screen and (max-width: 768px) {
          margin-bottom: 20px;
	}
	}
	.overlay {
		position: absolute;
		left: 0;
		bottom: 0;
		top: 0;
		right: 0;
		transition: 0.2s ease-in-out;
		background-color: rgba(255, 255, 255, 0.8);
	}

	.drop-zone {
		width: 100%;
		position: relative;
		min-height: 320px;
		display: flex;
		flex-direction: column;
		cursor: pointer;
		flex-wrap: wrap;
		-webkit-box-align: center;
		align-items: center;
		-webkit-box-pack: center;
		justify-content: center;
		background-color: rgb(255, 255, 255);
		text-align: center;
		padding: 35px 0px;
		border-width: 1px;
		border-style: dashed;
		border-color: rgb(233, 233, 233);
		border-image: initial;
		overflow: hidden;
		border-radius: 0;
		p {
			color: rgb(121, 121, 121);
			font-weight: 500;
		}
		i {
			font-size: 53.4px;
			line-height: 32px;
			color: rgb(68, 130, 255);
		}
	}
`;
