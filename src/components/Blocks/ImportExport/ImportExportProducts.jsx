import React, { useState, Fragment } from 'react';
import styles from '../../Pages/MyProducts/MyProducts.module.css';
// import prom_img from '../../../img/ymlExamples/promyml.png';
// import rozetka_img from '../../../img/ymlExamples/rozetkayml.png';
import {
	uploadXls,

	// getMySiteProducts,
} from '../../../utils/api/productsActions';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Menu, Tooltip, Upload, Icon, notification, Radio, Modal } from 'antd';
import CustomButton from "../../Shared/Button/Button";
const { Dragger } = Upload;

const ImportExportProducts = ({
	handleUpdate,
	handleGenerateYml,
	// showExportModal,
	importCondition,
	history,
	// importOnChangeRadio,
	sendToMyStore,
	// handleCopied,
	btnClassName,
	rozetkaUrl,
	promUrl,
	selectedRowKeys,
	products,
}) => {
	const [modalExportVisibility, setExportState] = useState(false);
	const [modalImportVisibility, setImportState] = useState(false);
	const [importValue, setImportValue] = useState(2);
	const [exportValue, setExportValue] = useState(2);
	const [copied, setCopy] = useState(false);

	const importOnChangeRadio = e => {
		setImportValue(e.target.value);
	};
	const exportOnChangeRadio = e => {
		setExportValue(e.target.value);
	};
	const handleCopied = () => {
		setCopy(true);
		notification.success({
			message: 'Скопировано',
		});
	};
	const handleOk = e => {
		console.log(e);
		setExportState(false);
		setImportState(false);
	};
	const handleCancel = e => {
		setExportState(false);
		setImportState(false);
		handleUpdate && handleUpdate();
	};
	const showExportModal = () => {
		setExportState(true);
	};

	const showImportModal = () => {
		setImportState(true);
	};

	const radioStyle = {
		display: 'block',
		height: '28px',
		lineHeight: '28px',
		fontWeight: 'normal',
	};

	const menu_export = (
		<Menu>
			<div>
				<div className={styles.addYml}>
					<div className={styles.radioExportYML}>
						<div className={styles.top}>
							<h5>Выберите формат для экспорта</h5>
						</div>
						<div className={styles.modal_export_view}>
							<Radio.Group onChange={exportOnChangeRadio} value={exportValue}>
								{/* <Radio style={radioStyle} disabled value={1}>
									CSV
								</Radio> */}
								<Radio style={radioStyle} value={2}>
									YML Rozetka
								</Radio>
								<Radio style={radioStyle} value={3}>
									YML Prom
								</Radio>
								{/* <Radio style={radioStyle} disabled value={4}>
									YML Яндекс
								</Radio>
								<Radio style={radioStyle} disabled value={5}>
									YML Яндекс СОКРАЩЕННЫЙ
								</Radio>
								<Radio style={radioStyle} disabled value={6}>
									Другой формат
								</Radio> */}
								{/* <Radio
									style={radioStyle}
									disabled={selectedRowKeys.length ? false : true}
									value={7}
								>
									Отправить товары в магазин
								</Radio> */}
							</Radio.Group>
						</div>
					</div>
					<div className={styles.rowSecondExportBlock}>
						{exportValue < 7 && (
							<Fragment>
								<div className={styles.YMLbtns}>
									<h5>Ссылка на YML файл</h5>
									<input
										type="text"
										className={styles.inputhref}
										value={
											(exportValue === 2 && rozetkaUrl) ||
											(exportValue === 3 && promUrl) ||
											'Не найденно'
										}
										disabled
									/>
									<CopyToClipboard
										text={
											(exportValue === 2 && rozetkaUrl) ||
											(exportValue === 3 && promUrl) ||
											'Не найденно'
										}
										onCopy={() => handleCopied()}
									>
										<button className={styles.copy}>Копировать</button>
									</CopyToClipboard>
								</div>
								{/* <img
									src={
										(exportValue === 2 && rozetka_img) ||
										(exportValue === 3 && prom_img)
									}
									alt=""
								/> */}
							</Fragment>
						)}

						{exportValue === 7 &&
							selectedRowKeys.length &&
							selectedRowKeys.map((row, i) => (
								<div key={i} className={styles.addInMyShopTable}>
									<img
										height="60"
										width="60"
										src={
											products[row].avatarUrl
												? products[row].avatarUrl
												: products[row].imageUrls.length > 0
												? products[row].imageUrls[0].url
												: products[row].coverImages.length
												? products[row].coverImages[0].imageDecoded
												: null
										}
									/>
									<p>{products[row].name}</p>
								</div>
							))}
					</div>
				</div>
				{exportValue >= 7 && (
					<button className={`btn ${styles.addInShop}`} onClick={sendToMyStore}>
						Отправить товары в магазин
					</button>
				)}
			</div>
		</Menu>
	);

	const draggerProps = {
		name: 'file',
		accept: '.xlsx, .xls, .csv, .xml, .yml',
		multiple: false,
		// action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
		onChange: async info => {
			const { status } = info.file;

			if (status !== 'uploading') {
			}

			if (status === 'done') {
				const formData = new FormData();
				formData.append('xls_file', info.file.originFileObj);
				formData.append('file_type', importValue === 2 ? 'yml' : 'prom_xml');

				await uploadXls(formData);

				// this.handleUploadFile(info.file.originFileObj, 'yml');

				notification.success({
					message: 'Успешно импортирован.',
					description: `${info.file.name}`,
				});
				handleCancel();
			} else if (status === 'error') {
				notification.error({
					message: 'Ошибка при импорте yml.',
					description: `${info.file.name}`,
				});
			}
		},
	};

	const menu_import = (
		<Menu>
			<div className={styles.addYml}>
				<div className={styles.top}>
					<h5>Выберите YML для импорта</h5>
					<div className={styles.modal_export_view}>
						<Radio.Group onChange={importOnChangeRadio} value={importValue}>
							{/* <Radio style={radioStyle} disabled value={1}>
								CSV
							</Radio> */}
							<Radio style={radioStyle} value={2}>
								YML Rozetka
							</Radio>
							<Radio style={radioStyle} value={3}>
								YML Prom
							</Radio>
							{/* <Radio style={radioStyle} disabled value={4}>
								YML Яндекс
							</Radio>
							<Radio style={radioStyle} disabled value={5}>
								YML Яндекс СОКРАЩЕННЫЙ
							</Radio>
							<Radio style={radioStyle} disabled value={6}>
								Другой формат
							</Radio> */}
						</Radio.Group>
						<button
							className={`btn ${styles.downloadHistoryBtn}`}
							onClick={() =>
								history.push('/admin/my_products/download_history')
							}
						>
							История загрузок
						</button>
					</div>
				</div>

				<div className={styles.importModal}>
					{/*<Dragger*/}
					{/*	{...draggerProps}*/}
					{/*	className={styles.dragNDropWindow}*/}
					{/*	// style={{}}*/}
					{/*>*/}
					{/*	<p className={styles.importDragNDropYMLType}>*/}
					{/*		Импорт{' '}*/}
					{/*		{(importValue === 2 && 'Rozetka') ||*/}
					{/*			(importValue === 3 && 'Prom')}*/}
					{/*	</p>*/}
					{/*	<p className="ant-upload-drag-icon">*/}
					{/*		<Icon type="inbox" />*/}
					{/*	</p>*/}
					{/*	<p className="ant-upload-text">*/}
					{/*		Кликните или перетащите файл для импорта*/}
					{/*	</p>*/}
					{/*	<p className="ant-upload-hint">*/}
					{/*		Поддерживается загрузка одного YML файла за раз. Предварительно*/}
					{/*		выберите тип ипортируемого YML.*/}
					{/*	</p>*/}
					{/*</Dragger>*/}
				</div>
			</div>
		</Menu>
	);
	return (
		<div>
			{importCondition ? (
				<CustomButton
					onClick={() => {
						showExportModal();
						handleGenerateYml();
					}}
					title='Мой YML файл'
					type='primary'

				/>
			) : (
				<CustomButton
					title='Импортировать YML'
					type='primary'
					onClick={showImportModal}

				/>
			)}

			{/* </Dropdown>} */}
			<Modal
				title="Экспорт товаров"
				visible={modalExportVisibility}
				onOk={handleOk}
				onCancel={handleCancel}
				footer={null}
				className={styles.export__modal}
				width={1000}
				centered
			>
				{menu_export}
			</Modal>

			<Modal
				title="Импорт товаров"
				visible={modalImportVisibility}
				onOk={handleOk}
				onCancel={handleCancel}
				footer={null}
				className={styles.export__modal}
				width={1000}
				centered
			>
				{menu_import}
			</Modal>
		</div>
	);
};

export default ImportExportProducts;
