import React from 'react';
import styles from '../../Pages/ProfileSettings/ProfileSettings.module.css';
import { Form, Input } from 'antd';
import { changePassword } from '../../../utils/api/userActions';
import CustomButton from '../../Shared/Button/Button';

const ChangePasswordForm = props => {
	const { getFieldDecorator } = props.form;
	const handleUpdatePassword = async e => {
		e.preventDefault();

		props.form.validateFields((err, user) => {
			if (!err) {
				changePassword(user).then(() => {
					props.closeModal({ isOpen: false });
					props.form.resetFields();
				});
			}
		});
	};
	return (
		<Form onSubmit={handleUpdatePassword} className={styles.Form}>
			<Form.Item>
				{getFieldDecorator('oldPassword', {
					rules: [
						{
							required: true,
							message: 'Пожалуйста введите Ваш старый пароль!',
						},
					],
				})(<Input placeholder="Старый пароль" />)}
			</Form.Item>

			<Form.Item>
				{getFieldDecorator('newPassword', {
					rules: [
						{ required: true, message: 'Пожалуйста введите Ваш пароль!' },
					],
				})(<Input type="password" placeholder="Новый пароль" />)}
			</Form.Item>

			<Form.Item>
				{getFieldDecorator('confirmPassword', {
					rules: [
						{ required: true, message: 'Пожалуйста введите Ваш пароль!' },
					],
				})(<Input type="password" placeholder="Подтвердите пароль" />)}
			</Form.Item>

			<CustomButton
				type="primary"
				htmlType="submit"
				style={{width: 300}}
				title="Сохранить"
			/>
		</Form>
	);
};

const WrappedNormalProfileForm = Form.create()(ChangePasswordForm);

export default WrappedNormalProfileForm;
