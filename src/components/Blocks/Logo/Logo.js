import React from 'react';
import { Link } from 'react-router-dom';
// import siteConfig from '@iso/config/site.config';

export default ({ collapsed }) => {
	return (
		<div className="isoLogoWrapper">
			{collapsed ? (
				<div>
					<h3>
						<Link to="/admin/cabinet">
							<span style={{color: "#fff"}}>TM</span>
						</Link>
					</h3>
				</div>
			) : (
				<h3>
					<Link to="/admin/cabinet">
						{' '}
						<span style={{color: "#fff"}}>TopMarket</span>
					</Link>
				</h3>
			)}
		</div>
	);
};
