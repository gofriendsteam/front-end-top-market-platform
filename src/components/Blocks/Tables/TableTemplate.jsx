import React, { Component } from 'react';
// import styles from './Finance.module.css';
import { Table } from 'antd';
import { COLUMNS } from '../../../constants/TableColumns/TabColumns';
// import { CONFIG } from '../../../helpers/TabConfig';
import * as userActions from '../../../utils/api/userActions';
import * as productsActions from '../../../utils/api/productsActions';

const baseQuery = '?page=1&page_size=10';

class TableTemplate extends Component {
	state = {
		columns: [],
		count: 0,
		dataSource: [],
		loading: true,
	};


	handleChangeTable = async (pagination, filters, sorter, extra) => {

		this.setState({ loading: true });
		const { action } = this.props;
		const { current, pageSize } = pagination;

		const filtersToString = Object.entries(filters)
			.map(fil =>
				fil
					.map((el, i, arr) =>
						i % 2 ? el.map(a => `&${arr[i - 1]}=${a}`).join('') : '',
					)
					.join(''),
			)
			.join('');
		// console.log('filtersToString', filtersToString);

		const query = `?page=${current}&page_size=${pageSize}${this.props.query}${filtersToString}`;
		// console.log(query);

		const { count = 0, results = [] } = await userActions[action](query);
		const [dataSource] = [results];
		// console.log(count);

		this.setState({
			count,
			dataSource,
			loading: false,
		});
	};

	// LIFECYCLE METHODS
	componentWillMount() {
		const { moduleName, tableName, user } = this.props;
		const columns = COLUMNS[moduleName][user.role.toLowerCase()][tableName];
		this.setState({
			columns,
		});
	}

	async componentDidMount() {
		const { actions = '', action = '', query = '' } = this.props;
		switch (actions) {
			case '':
				const { count = 0, results = [] } = await userActions[action](
					baseQuery + query,
				); // делаем запрос данных
				const [dataSource] = [results];
				this.setState({
					count,
					dataSource,
					loading: false,
				});
				break;
			case 'productsActions':
				const data = await productsActions[action]();
				// console.table(data);
				data.sort((a, b) => Date.parse(b.created) - Date.parse(a.created));
				// console.table(data);
				this.setState({
					count: data.length,
					dataSource: data,
					loading: false,
				});
				break;
			default:
				break;
		}
	}

	render() {
		const { units, willHaveFullData = false } = this.props;
		const total = this.state.count;
		// console.log('STATE', this.state);
		// console.log('PROPS', this.props);

		const CONFIG = {
			pagination: {
				defaultCurrent: 1,
				pageSizeOptions: ['10', '20', '50', '100'],
				showSizeChanger: true,
				showTotal: (total, range) =>
					`${range[0]}-${range[1]} из ${total} ${units}`,
				total,
			},
		};

		return (
			<Table
				{...CONFIG}
				{...this.state}
				onChange={willHaveFullData ? null : this.handleChangeTable}
				scroll={{ x: 768 }}
			/>
		);
	}
}

export default TableTemplate;
