import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Layout, Menu } from 'antd';
import options from '../../../routing/menuList';
// import Scrollbars from '../../Blocks/ScrollBar/CustomScrollBar';
// import IntlMessages from '@iso/components/utility/intlMessages';
// import appActions from '@iso/redux/app/actions';
import Logo from '../Logo/Logo';
import SidebarWrapper from '../../../styles/Sidebar.styles';
import {
	toggleOpenDrawer,
	changeOpenKeys,
	changeCurrent,
	toggleCollapsed,
} from '../../../store/actions/AsideBar';
import SidebarMenu from './SidebarMenu';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const { Sider } = Layout;

export default function NavBar() {
	const dispatch = useDispatch();
	const {
		view,
		openKeys,
		collapsed,
		openDrawer,
		current,
		height,
	} = useSelector(state => state.App);
	const role = useSelector(state => state.user.role);

	// console.log(openKeys, current)
	const customizedTheme = {
		themeName: 'theme3',
		buttonColor: '#4482FF',
		backgroundColor: '#4482FF',
		textColor: '#ffffff',
	};

	function handleClick(e) {
		dispatch(changeCurrent([e.key]));
		if (view === 'MobileView') {
			setTimeout(() => {
				dispatch(toggleCollapsed());
				// dispatch(toggleOpenDrawer());
			}, 100);

			// clearTimeout(timer);
		}
	}
	function onOpenChange(newOpenKeys) {
		const latestOpenKey = newOpenKeys.find(
			key => !(openKeys.indexOf(key) > -1),
		);
		const latestCloseKey = openKeys.find(
			key => !(newOpenKeys.indexOf(key) > -1),
		);
		let nextOpenKeys = [];
		if (latestOpenKey) {
			nextOpenKeys = getAncestorKeys(latestOpenKey).concat(latestOpenKey);
		}
		if (latestCloseKey) {
			nextOpenKeys = getAncestorKeys(latestCloseKey);
		}
		dispatch(changeOpenKeys(nextOpenKeys));
	}
	const getAncestorKeys = key => {
		const map = {
			sub3: ['sub2'],
		};
		return map[key] || [];
	};

	const isCollapsed = collapsed && !openDrawer;
	const mode = isCollapsed === true ? 'vertical' : 'inline';
	const onMouseEnter = event => {
		if (collapsed && openDrawer === false) {
			dispatch(toggleOpenDrawer());
		}
		return;
	};
	const onMouseLeave = () => {
		if (collapsed && openDrawer === true) {
			dispatch(toggleOpenDrawer());
		}
		return;
	};
	const styling = {
		backgroundColor: customizedTheme.backgroundColor,
	};
	const submenuStyle = {
		backgroundColor: 'rgba(0,0,0,0.3)',
		color: customizedTheme.textColor,
	};
	const submenuColor = {
		color: customizedTheme.textColor,
	};
	const optionsMenu = role === "PARTNER" ? options.partnerMenu : options.contractorMenu;
	return (
		<SidebarWrapper>
			<Sider
				trigger={null}
				collapsible={true}
				collapsed={isCollapsed}
				width={240}
				className="isomorphicSidebar"
				onMouseEnter={onMouseEnter}
				onMouseLeave={onMouseLeave}
				style={styling}
			>
				<Logo collapsed={isCollapsed} />

				{/*<Scrollbars>*/}
					<Menu
						style={{ height: '100vh'}}
						onClick={handleClick}
						theme="dark"
						className="isoDashboardMenu"
						mode={mode}
						defaultOpenKeys='/admin/cabinet'
						openKeys={isCollapsed ? [] : openKeys}
						selectedKeys={current[0]}
						onOpenChange={onOpenChange}
					>
						{optionsMenu.map(singleOption => (
							<SidebarMenu
								key={singleOption.key}
								submenuStyle={submenuStyle}
								submenuColor={submenuColor}
								singleOption={singleOption}
							/>
						))}
					</Menu>
				{/*</Scrollbars>*/}
			</Sider>
		</SidebarWrapper>
	);
}
