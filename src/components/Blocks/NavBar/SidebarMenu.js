import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';
// import IntlMessages from '@iso/components/utility/intlMessages';
const SubMenu = Menu.SubMenu;

export default React.memo(function SidebarMenu({
	singleOption,
	submenuStyle,
	submenuColor,
	...rest
}) {
	const { key, label, leftIcon, children } = singleOption;

	if (children) {
		return (
			<SubMenu
				key={key}
				title={
					<span className="isoMenuHolder" style={submenuColor}>
						<i className={leftIcon} />
						<span className="nav-text">
							<div>{label}</div>
							{/*<IntlMessages id={label} />*/}
						</span>
					</span>
				}
				{...rest}
			>
				{children.map((child, idx) => {
					return (
						// <Menu.Item style={submenuStyle} key={child.key}>
						<Menu.Item style={submenuStyle} key={idx}>
							<Link style={submenuColor} to={child.key}>
								{child.label}
							</Link>
						</Menu.Item>
					);
				})}
			</SubMenu>
		);
	}

	return (
		<Menu.Item key={key} {...rest}>
			<Link to={`${key}`}>
				<span className="isoMenuHolder" style={submenuColor}>
					<i className={leftIcon} />
					<span className="nav-text">{label}</span>
				</span>
			</Link>
		</Menu.Item>
	);
});
