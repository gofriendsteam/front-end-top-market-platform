import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import moment from "moment"

const Message = ({ message, date, sender: { name, id, email } }) => {
	const userId = useSelector(state => state.user.id);
	const classNames = userId === id ? 'message me' : 'message other';
	return (
		<div className={classNames}>
			<div className="name">{name}</div>
			<div className="text">{message}</div>
			<div className="date">{moment(date).calendar()}</div>
		</div>
	);
};

export default Message;
