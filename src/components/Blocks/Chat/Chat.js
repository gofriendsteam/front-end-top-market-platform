import React from 'react';
import { Drawer } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { toogleChat } from '../../../store/actions/chat';


import ChatContent from './ChatContent';

const Chat = () => {
	const {isOpen, data} = useSelector(state => state.chat);
	const dispatch = useDispatch();
	const onClose = () => {
		dispatch(toogleChat());
	};
	return (
		<Drawer
			title="Мой чат"
			placement="right"
			width="500"
			wrapClassName="chat-drawer"
			closable={true}
			onClose={onClose}
			visible={isOpen}
		>
			<ChatContent data={data}/>
		</Drawer>
	);
};

export default Chat;
