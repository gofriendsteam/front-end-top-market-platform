import React, { useEffect, useRef, useState } from 'react';
import { Input } from 'antd';
import { ChatWrapper } from './ChatWrapper.style';
import CustomButton from '../../Shared/Button/Button';
import Message from './Message';
import Loader from '../../Shared/Loader/Loader';

import { fetchAllMessages, sendMessage } from '../../../utils/api/userActions';

const me = [
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 2,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
	{
		message: 'asdadasdadasdasdasdaaaaaaaaaaaaaaa',
		date: new Date().toDateString(),
		sender: {
			id: 1,
			email: 'mesage@ukr.net',
			name: 'Miktor',
		},
	},
];
const { TextArea } = Input;
const ChatContent = ({ data }) => {
	const ref = useRef(null);
	const [MInput, setMInput] = useState('');
	const [chatMessages, setChatmessage] = useState([]);
	const [loading, setLoading] = useState(false);

	const handleUserMessage = ({ target: { value } }) => setMInput(value);
	const fetchMessages = chatid => {
		fetchAllMessages(chatid).then(res => setChatmessage(res));
	};
	const handleSendMessages = async () => {
		if (MInput.trim().length === 0) return;
		const res = await sendMessage(data.chatId, { message: MInput });
		console.log('', res);
		// setChatmessage(res);
		// console.log('', res);
		// console.log('', res);
		// setLoading(true);
		// setTimeout(() => {
		// 	setLoading(false);
		// }, 2000);
		// sendMessage()
	};
	useEffect(() => {
		if (data) {
			fetchMessages(data.chatId);
		}
		ref.current.scrollTop = ref.current.scrollHeight;
		// console.log('', ref.current.scrollHeight, ref.current.clientHeight);
	}, []);

	return (
		<ChatWrapper>
			<div className="messages-wrapper" ref={ref}>
				{loading && <Loader />}
				{chatMessages.map(message => (
					<Message {...message} />
				))}
			</div>
			<div className="footer">
				<TextArea
					value={MInput}
					onChange={handleUserMessage}
					placeholder="Введите свое сообщение"
					className="text-area"
					autoSize={{ minRows: 3, maxRows: 5 }}
				/>
				<CustomButton
					disabled={MInput.length === 0}
					title="Отправить"
					type="primary"
					onClick={handleSendMessages}
				/>
			</div>
		</ChatWrapper>
	);
};

export default ChatContent;
