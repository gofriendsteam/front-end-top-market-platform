import styled from 'styled-components';

export const ChatWrapper = styled.div`
	display: flex;
	flex-direction: column;
	height: 92vh;
	.messages-wrapper {
		flex-grow: 1;
		padding: 24px;
		background: rgb(245, 242, 242);
		display: flex;
		flex-direction: column;
		overflow-y: scroll;
	}
	.message {
		border-radius: 5px;
		display: inline-block;
		min-width: 190px;
		padding: 10px;
		position: relative;
		vertical-align: middle;
		word-wrap: break-word;
		margin: 15px 0;
		.date {
			font-size: 11px;
			text-align: right;
		}
		.name {
			font-weight: 500;
			font-size: 15px;
			margin: 0 0 5px;
		}
		.text {
			color: #595959;
			margin-bottom: 15px;
		}

		&.me {
			margin-right: auto;
			background-color: #dfe1f0;
			&:before {
				border: 5px solid transparent;
				border-right-color: #dfe1f0;
				content: '';
				left: -9px;
				position: absolute;
				top: 10px;
			}
		}
		&.other {
			background-color: #fff;
			margin-left: auto;
			&:before {
				border: 5px solid transparent;
				border-right-color: #fff;
				content: '';
				right: -10px;
				transform: rotate(180deg);
				position: absolute;
				top: 10px;
			}
		}
	}
	.footer {
		padding: 20px 20px 0 20px;
		.text-area {
			margin-bottom: 15px;
		}
	}
`;
