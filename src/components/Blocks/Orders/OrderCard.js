import React, { useState, Fragment } from 'react';
import moment from 'moment';
// import { Col } from 'antd';

const OrderCard = ({ record }) => {
	return (
		<Fragment>
			{record.items.map(({ productId, imageUrl, name, quantity, price }) => (
				<div className="product-card" key={productId}>
					<div className="product-image">
						<img src={imageUrl} alt={productId} />
					</div>
					<div className="product-content">
						<h3 className="product-name">{name}</h3>
						<p className="product-price">
							<div>Цена:</div>
							<div>{price}</div>
						</p>
						<p className="product-quantity">
							<div>Количество:</div>
							<div>{quantity} шт.</div>
						</p>
					</div>
				</div>
			))}
			<div className="info">
				<div className="comments">
					<div className="title">Комментарий:</div>
					<div className="comment-list">
						{record.sellerComments.length ? (
							record.sellerComments.map(({ created, comment }) => (
								<div className="text">
									<div className="date">{moment(created).format('HH:mm')}</div>

									<div className="comment-text">- {comment}</div>
								</div>
							))
						) : (
							<div className="comment-text">Отсутсвует.</div>
						)}
					</div>
				</div>
				<div className="delivery">
					<div className="title">Способ доставки:</div>

					{record.delivery ? (
						<div className="delivery-block">
							<div>{record.deliverydeliveryServiceName}</div>
							<div className="delivery-item">
								<div>Отделение:</div>{' '}
								<div>
									{record.delivery.placeNumber
										? `№  ${record.delivery.placeNumber}`
										: 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Имя получателя:</div>{' '}
								<div>
									{record.delivery.recipientTitle
										? record.delivery.recipientTitle
										: 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Город:</div>{' '}
								<div>
									{record.delivery.city ? record.delivery.city : 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Цена доставки:</div>{' '}
								<div>
									{record.delivery.cost ? record.delivery.cost : 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Адрес:</div>{' '}
								<div>
									{record.delivery.placeStreet
										? record.delivery.placeStreet
										: 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Дом:</div>{' '}
								<div>
									{record.delivery.placeHouse
										? record.delivery.placeHouse
										: 'Отсутствует.'}
								</div>
							</div>
							<div className="delivery-item">
								<div>Квартира:</div>{' '}
								<div>
									{record.delivery.placeFlat
										? record.delivery.placeFlat
										: 'Отсутствует.'}
								</div>
							</div>
						</div>
					) : (
						<div className="comment-text">Отсутсвует.</div>
					)}
				</div>
				<div className="last-block">
					<div className="ttn">
						<div className="title">TTN:</div>
						<div>{record.ttn ? record.ttn : 'Отсутствует.'}</div>
					</div>
					<div className="total">
						<div className="total-title">Сумма заказа:</div>
						<div>{record.amount} грн.</div>
					</div>
				</div>
			</div>
		</Fragment>
	);
};

export default OrderCard;
