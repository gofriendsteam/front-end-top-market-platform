import React from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import { toogleModal } from '../../../store/actions/modal';

const MyModal = ({
	toogleModal,
	modal: { isOpen = false, type = '', component: Component, data = null },
}) => {
	const closeModal = () => {
		toogleModal({ isOpen: false, type, component: null, data: null });
	};
	return (
		<Modal
			width={data ? data.width : 540}
			title={data ? data.title : false}
			centered
			visible={isOpen}
			onCancel={closeModal}
			footer={false}
		>
			{Component && (
				<Component {...data} type={type} closeModal={toogleModal} />
			)}
		</Modal>
	);
};

const mapStateToProps = state => ({
	modal: state.modal,
});
const mapDispatchToProps = dispatch => ({
	toogleModal: data => dispatch(toogleModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyModal);
