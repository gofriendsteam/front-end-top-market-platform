import React, { useEffect, useState } from 'react';
import { Menu, Dropdown, Icon } from 'antd';
import { Link } from 'react-router-dom';
import styles from './Header.module.css';
import avatar from '../../../img/avatar.png';
import dollar from '../../../img/dollar.svg';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';

const Header = ({ userBalance, user }) => {
	const [state, setState] = useState({ firstName: 'User', userBalance: 0 });
	// state = {
	// 	firstName: 'User',
	// 	userBalance: 0,
	// };
	useEffect(() => {
		setState({ ...state, userBalance });
	}, []);
	// componentDidMount() {
	// 	this.setState({
	// 		userBalance: this.props.userBalance,
	// 	});
	// }

	const handleLogout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('persist:root');
		window.location.href = `${window.location.origin}/`;
		localStorage.clear();
	};

	const menu = (
		<Menu>
			<Menu.Item>
				<Link to="/admin/profile_settings">Настройки профиля</Link>
			</Menu.Item>
			<Menu.Item>
				<Link to="/admin/company_settings">Настройки компании</Link>
			</Menu.Item>

			<Menu.Item onClick={handleLogout}>Выход</Menu.Item>
		</Menu>
	);
	return (
		<div className="container">
			<header>
				<div className={styles.logo}>
					{/* <img src={logo} alt="logo" /> */}
					TOPMARKET
				</div>
				<div className={styles.balanceBlock}>
					<div>
						<p>Баланс:</p>
					</div>

					<div className={styles.balanceBox}>
						<div className={styles.dollarIcon}>
							<img src={dollar} alt="" />
						</div>
						<div>
							<span>{state.userBalance.toFixed(2)}</span> грн
						</div>
					</div>
				</div>

				<div className={styles.userBlock}>
					<div className={styles.avatar}>
						<img src={user.avatarImage || avatar} alt="" />
					</div>
					<Dropdown overlay={menu}>
						<div className="ant-dropdown-link">
							<div className={styles.nameBlock}>
								<span className={styles.name}>{user.firstName || 'User'}</span>
								<span>
									{user.role === 'PARTNER' ? 'Продавец' : 'Поставщик'}
								</span>
							</div>
							<Icon type="down" />
						</div>
					</Dropdown>
				</div>
			</header>
		</div>
	);
};

const mapStateToProps = state => ({
	user: state.user,
	userBalance: state.user.userBalance,
});

export default connect(mapStateToProps)(Header);
