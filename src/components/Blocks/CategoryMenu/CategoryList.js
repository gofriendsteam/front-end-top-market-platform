import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { connect, useDispatch } from 'react-redux';
import s from './CategoryList.module.css';
import { selectedCategory } from '../../../utils/api/userActions';
import FirstLevelCategories from './FirstLevelCategories';
import CustomButton from '../../Shared/Button/Button';
import { putHistory } from '../../../utils/api/history';
import { useHistory } from 'react-router-dom';

const CategoryList = ({ onSelectCategory, categories = [] }) => {
	const [visible, setVisible] = useState(false);
	const [categoryToShow, setCategoryToShow] = useState(0);
	const dispatch = useDispatch();
	const history = useHistory();
	const toogleVisible = () => {
		setVisible(!visible);
	};
	const changeCategoryToShow = index => {
		setCategoryToShow(index);
	};
	const handleSelectCategory = categoryId => {
		onSelectCategory(categoryId);
		toogleVisible();
	};
	useEffect(() => {
		dispatch(putHistory(history));
	}, []);
	return (
		<div className={s.wrapper}>
			<div
				className={
					visible ? `${s.mainBackground} ${s.visible}` : s.mainBackground
				}
				onClick={toogleVisible}
			/>

			<CustomButton
				type="primary"
				className="category-btn"
				title="Список категорий"
				onClick={toogleVisible}
			/>
			{categories.length > 0 && (
				<FirstLevelCategories
					categories={categories}
					visible={visible}
					categoryToShow={categoryToShow}
					changeCategoryToShow={changeCategoryToShow}
					handleSelectCategory={handleSelectCategory}
				/>
			)}
		</div>
	);
};

const mapStateToProps = state => ({ categories: state.categories });

const mapDispatchToProps = dispatch => ({
	onSelectCategory: id => dispatch(selectedCategory(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryList);
