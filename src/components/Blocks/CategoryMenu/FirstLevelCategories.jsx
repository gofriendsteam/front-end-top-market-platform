import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import s from './FirstLevelCategories.module.css';

import arrow from '../../../img/svg/next.svg';
import icon_1 from '../../../img/2/1.svg';
import icon_2 from '../../../img/2/2.svg';
import icon_3 from '../../../img/2/3.svg';
import icon_4 from '../../../img/2/4.svg';
import icon_5 from '../../../img/2/5.svg';
import icon_6 from '../../../img/2/6.svg';
import icon_7 from '../../../img/2/7.svg';
import icon_8 from '../../../img/2/8.svg';
import icon_9 from '../../../img/2/9.svg';
import icon_10 from '../../../img/2/10.svg';
import icon_11 from '../../../img/2/11.svg';
import icon_12 from '../../../img/2/12.svg';
import icon_13 from '../../../img/2/13.svg';
import icon_14 from '../../../img/2/14.svg';
import icon_15 from '../../../img/2/15.svg';
import icon_16 from '../../../img/2/16.svg';
import icon_17 from '../../../img/2/17.svg';
import icon_18 from '../../../img/2/18.svg';
import icon_19 from '../../../img/2/19.svg';

const icons = {
	'1': icon_1,
	'2': icon_2,
	'3': icon_12,
	'4': icon_4,
	'5': icon_5,
	'6': icon_6,
	'7': icon_7,
	'8': icon_8,
	'9': icon_9,
	'10': icon_16,
	'11': icon_11,
	'12': icon_3,
	'13': icon_13,
	'14': icon_14,
	'15': icon_15,
	'16': icon_10,
	'17': icon_17,
	'18': icon_18,
	'19': icon_19,
};

const FirstLevelCategories = ({
	categories,
	visible,
	categoryToShow,
	changeCategoryToShow,
	handleSelectCategory,
	history,
}) => {
	const [currentPath, setCurrentPath] = useState(null);
	const [currentId, setCurrentId] = useState(null);
	const [idsMap, setIdsMap] = useState(
		categories.length && categories[0].id.toString(),
	);
	console.log('asdsds', idsMap, currentPath, history);
	useEffect(() => {
		history.location &&
			setCurrentPath(
				history.location.pathname.replace(/(?=.\d{1,}).{1,50}/g, ''),
			);

		history.location &&
			setCurrentId(
				history.location.pathname.substring(
					history.location.pathname.lastIndexOf('/') + 1,
				),
			);
	}, [history]);

	const cleanMatch = () => {
		setIdsMap(idsMap.toString().replace(/([/].+)/g, ''));
	};

	useEffect(() => {}, [idsMap, setIdsMap]);

	// console.log(categories);

	return (
		currentPath && (
			<div className={s.menuWrap}>
				<div
					className={visible ? `${s.firstLevel} ${s.visible}` : s.firstLevel}
				>
					{categories.map((category, index) => (
						<span
							key={category.id}
							className={s.firstLevel_item}
							onMouseEnter={() => {
								changeCategoryToShow(index);
								setIdsMap(category.id.toString());
							}}
							onClick={() => handleSelectCategory(category.id)}
						>
							<img
								style={{ width: '15px', margin: '0 10px 0 5px' }}
								src={icons[index + 1]}
								alt=""
							/>
							<img className={s.arrow} src={arrow} alt="" />
							<Link
								className={s.link}
								to={{
									pathname: `${currentPath}/${category.id}`,
									// state: { category: category }
								}}
							>
								<span>{category.name}</span>
							</Link>
						</span>
					))}
					<div
						className={
							visible ? `${s.secondLevel} ${s.visible}` : s.secondLevel
						}
					>
						{categories &&
							categories[categoryToShow].subcategories.map(sub => (
								<div className={s.secondLevel_item} key={sub.id}>
									<span
										onMouseEnter={() =>
											setIdsMap(`${idsMap.replace(/([/].+)/g, '')}/${sub.id}`)
										}
										onClick={() => handleSelectCategory(sub.id)}
									>
										<Link
											className={s.submenuLinks}
											to={{
												pathname: `${currentPath}/${idsMap}`,
												// state: { category: sub }
											}}
										>
											{sub.name}
										</Link>
									</span>
									{sub.subcategories.length > 0
										? sub.subcategories.map(sub2 => (
												<span
													onClick={() => handleSelectCategory(sub2.id)}
													onMouseEnter={() =>
														setIdsMap(
															`${idsMap.replace(/([/].+)/g, '')}/${sub.id}/${
																sub2.id
															}`,
														)
													}
													className={
														sub2.subcategories.length
															? `${s.thirdLevel_item} ${s.blue}`
															: s.thirdLevel_item
													}
													key={sub2.id}
												>
													<Link
														className={
															sub2.subcategories.length
																? `${s.thirdLevel_item} ${s.blue}`
																: s.thirdLevel_item
														}
														to={{
															pathname: `${currentPath}/${idsMap}`,
															// state: { category: sub2 }
														}}
													>
														{sub2.name}
													</Link>
												</span>
										  ))
										: null}
								</div>
							))}
					</div>
				</div>
			</div>
		)
	);
};

const mapStateToProps = state => ({
	history: state.history,
});

export default connect(mapStateToProps)(FirstLevelCategories);
