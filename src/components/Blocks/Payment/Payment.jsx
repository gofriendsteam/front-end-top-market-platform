import React, { useState, useEffect } from 'react';
import styles from './payment.module.css';
import { notification } from 'antd';
import { buyPackByLiqPay, sendInvoice } from '../../../utils/api/userActions';
import CustomButton from '../../Shared/Button/Button';

const Payment = ({ id, toogleModal }) => {
	const [state, setState] = useState({ signature: '', data: '' });

	const getLiqPaySignature = () => {
		buyPackByLiqPay({ pocketId: id }).then(data => setState(data));
	};

	const handleSendInvoice = async () => {
		await sendInvoice({
			pocketId: this.state.packId,
		});
		notification.success({
			message: 'Счет фактура отправлена вам на e-mail!',
		});
		this.setState({
			visible: false,
		});
	};
	useEffect(() => {
		getLiqPaySignature();
	}, [id]);
	console.log(state);
	return (
		<div>
			<div className={styles.modalContent}>
				<p>
					Для того чтобы приобрести пакет доступа на Вашем аккаунте, Вы можете
					воспользоваться двумя способами оплаты : Оплата с помощью сервиса
					LiqPay или же оплатить счёт-фактуру.
				</p>
			</div>
			<div className={styles.payActions}>
				<form
					target="_blank"
					method="POST"
					action="https://www.liqpay.ua/api/3/checkout"
					acceptCharset="utf-8"
				>
					<input type="hidden" name="data" value={state.data} />
					<input type="hidden" name="signature" value={state.signature} />
					<CustomButton
						htmlType='unset'
						type="primary"
						title="Оплатить через LiqPay"
						className={styles.payBtn}
					/>
				</form>

				<CustomButton
					title="Отправить счет-фактуру на e-mail"
					type="primary"
					className={styles.payBtn}
					// onClick={this.handleSendInvoice}
					disabled
				/>
			</div>
		</div>
	);
};

export default Payment;
