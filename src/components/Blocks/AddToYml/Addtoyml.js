import React, { useState } from 'react';
import CustomButton from '../../Shared/Button/Button';
import { Wrapper } from './add.styles';
import { addProductsToYml } from '../../../utils/api/productsActions';
import Loader from '../../Shared/Loader/Loader';

const Addtoyml = ({ selectedIds, closeModal, update }) => {
	const [loading, setLoading] = useState(false);
	const handleAddToYml = async type => {
		setLoading(true);
		try {
			await addProductsToYml({
				ymlType: type,
				productIds: selectedIds,
			});
			setLoading(false);
			update();
			closeModal();
		} catch (e) {
			setLoading(false);
		}
	};

	return (
		<Wrapper>
			{loading && <Loader />}
			<CustomButton
				title="Rozetka YML"
				type="primary"
				onClick={() => handleAddToYml('rozetka')}
				// disabled={!selectedRowKeys.length}
			/>
			<CustomButton
				title="Prom YML"
				type="primary"
				disabled
				onClick={() => handleAddToYml('prom')}
				// disabled={!selectedRowKeys.length}
			/>
		</Wrapper>
	);
};

export default Addtoyml;
