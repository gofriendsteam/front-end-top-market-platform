import React, { useCallback, useEffect, useState } from 'react';
import Preloading from '../Shared/Loader/TablePreloader';


export const useApiCall = (apiCallback, query = '') => {
	const [products, setProducts] = useState([]);
	const [count, setCount] = useState(0);
	const [loading, setLoading] = useState(false);
	const fetchData = useCallback(() => {
		setLoading(true);
		apiCallback(query)
			.then(({ results, count }) => {
				setProducts(results);
				setCount(count);
				setLoading(false);
			})
			.catch(e => setLoading(false));
	} , [query, apiCallback]);

	useEffect(() => {
		fetchData();

	}, [query]);

	return [
		products,
		count,
		fetchData,
		{
			spinning: loading,
			indicator: <Preloading />,
		},
	];
};
