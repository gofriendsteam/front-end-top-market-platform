import { useEffect, useState } from 'react';

export const useOrderPCard = selectedProduct => {
	const [values, setValues] = useState([]);

	useEffect(() => {
		if (selectedProduct) {
			setValues([
				...values,
				{ ...selectedProduct, mycount: selectedProduct.count > 0 ? 1 : 0 },
			]);
		}
	}, [selectedProduct]);
	const setQuantity = (id, quantity) => {
		const setNewValue = values.map(val =>
			val.id === id ? { ...val, mycount: quantity } : val,
		);
		setValues(setNewValue);
	};
	const deleteCart = id => {
		const newValues = values.filter(val => val.id !== id);
		setValues(newValues);
	};
	const resetProductsCart = () => setValues([]);
	return [values, setQuantity, deleteCart,resetProductsCart];
};
