import { useState } from 'react';

export const useFilters = initialState => {
	const [values, setValues] = useState(initialState);
	console.log('input', values);
	// console.log(values)
	const handleChangeInputs = e => {

		if (!e) return;
		if (e instanceof Array) {
			setValues({
				...values,
				category_id: e,
			});
		} else {
			setValues({
				...values,
				[e.target.name]: e.target.value,
			});
		}
	};

	return [values, handleChangeInputs];
};
