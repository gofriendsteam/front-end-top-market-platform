import React, { useEffect, useState } from 'react';
// import { useApiCall } from './useApiCall';
import _ from 'lodash';
import Preloading from '../Shared/Loader/TablePreloader';

let debouncer = _.throttle(
	f => f(),
	700,
	{ leading: true }, // debounce one on leading and one on trailing
);

export const useTable = (apiCall, values, ymlType, additionOption) => {
	const [currentPage, setCurrentPage] = useState(1);
	const [pageSize, setPageSize] = useState(10);
	const [loading, setLoading] = useState(false);
	const [productData, setProductData] = useState({
		count: 0,
		products: [],
	});
	const [selectedRowKeys, setRowKeys] = useState([]);

	const transformToUrl = values =>
		Object.entries(values)
			.map(value =>
				value[1]
					? `&${value[0]}=${
							value[1] instanceof Array && value[1].length
								? value[1].slice(-1)
								: value[1]
					  }`
					: '',
			)
			.join('');

	const fetchData = () => {
		setLoading(true);
		const query = transformToUrl(values);
		const additionQuery = additionOption ? additionOption : '';
		apiCall(
			`${
				ymlType ? `${ymlType}/` : ''
			}?page_size=${pageSize}&page=${currentPage + query + additionQuery}`,
		)
			.then(({ results, count }) => {
				setProductData({
					products: results,
					count: count,
				});
				setLoading(false);
			})
			.catch(e => setLoading(false));
	};
	useEffect(
		() =>
			debouncer(() => {
				fetchData();
			}),
		[pageSize, currentPage, values, additionOption],
	);

	const resetRowKeys = () => setRowKeys([]);
	const onSelectChange = selectedRowKeys =>
		console.log(selectedRowKeys) || setRowKeys(selectedRowKeys);

	const handleChangeTable = ({ current, pageSize }, type) => {
		setCurrentPage(current);
		setPageSize(pageSize);
	};
	const count = productData.count;
	const products = productData.products;
	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
	};
	const config = {
		pagination: {
			pageSize,
			pageSizeOptions: [10, 20, 50, 200],
			showSizeChanger: true,
			total: count,
			current: currentPage,
		},
	};
	console.log('inner TAble');

	return [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		{
			spinning: loading,
			indicator: <Preloading />,
		},
		handleChangeTable,
		fetchData,
		resetRowKeys,
	];
};
