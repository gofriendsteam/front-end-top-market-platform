import styled from 'styled-components';
import { palette } from 'styled-theme';

export const MainLayout = styled.div`
	.category-btn {
		margin: 0 0 0 20px;
	}
	.table-error-value {
		color: #ffabab;
	}
	.how-to-link {
		font-size: 20px;
		@media only screen and (max-width: 600px) {
			font-size: 16px;
		}
		&:hover {
			text-decoration: underline;
		}
	}
	.top-nav {
		display: flex;
		align-items: center;
		flex-wrap: wrap;
		justify-content: space-between;
		padding: 24px 24px 24px 0;
		& button {
			@media only screen and (max-width: 600px) {
				margin: 10px 0 10px 0;
			}
		}
	}
	.actions {
		display: flex;
		align-items: center;
		flex-wrap: wrap;
		margin-bottom: 10px;
		//justify-content: space-between;
		& > button {
			margin: 0 15px 0 0;
			@media only screen and (max-width: 650px) {
				margin: 0 15px 15px 0;
			}
			@media only screen and (max-width: 480px) {
				width: 100% !important;
			}
		}

		&.partner {
			button {
				margin-right: 15px;
				width: 180px;
			}
		}
	}

	.product-avatar {
		display: flex;
		align-items: center;
		min-width: 200px;
	}
	.product-avatar-block {
		margin-right: 10px;
		height: 50px;
		width: 50px;
		display: flex;
		& img {
			width: 100%;
			display: block;
			object-fit: cover;
		}
	}
	.bottom-total-price {
		@media only screen and (max-width: 768px) {
			display: none;
		}
	}
	.my-product-header {
		padding: 24px 0 24px;
	}
	.category-btn {
		margin-left: 0;
	}
	.total {
		font-weight: 500;
		font-size: 16px;
		//margin-bottom: 15px;
		margin-left: auto;
	}
	.controls {
		margin-bottom: 0;
		display: flex;
		align-items: center;
		flex-wrap: wrap;
		button {
			margin-bottom: 15px;
		}
		@media only screen and (max-width: 560px) {
			justify-content: space-between;
		}
		div {
			@media only screen and (max-width: 560px) {
				width: 100%;
			}
		}

		& > button,
		div button {
			margin-right: 10px;
			@media only screen and (max-width: 560px) {
				width: 100%;
				margin-right: 0;
			}
		}
	}

	.breadcrumb {
		padding: 24px 0;
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		&:before {
			content: '';
			width: 5px;
			height: 40px;
			background-color: rgb(228, 230, 233);
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			margin: 0 15px 0 0;
		}
		.breadcrumb-link {
			font-size: 18px;
			color: rgb(120, 129, 149);
		}
		.ant-breadcrumb-separator {
			font-weight: 500;
			margin: 0 9px 0 9px;
		}
	}
	.subs-subcategories-list {
		display: flex;
		flex-wrap: wrap;
		margin-bottom: 24px;
		.subs-links {
			display: inline-block;
			line-height: 28px;
			height: 30px;
			font-size: 14px;
			color: rgb(255, 255, 255);
			opacity: 1;
			margin: 4px 8px 4px 0;
			cursor: pointer;
			white-space: nowrap;
			padding: 0 10px;
			border-radius: 4px;
			border-width: 1px;
			border-style: solid;
			border-color: rgb(233, 233, 233);
			border-image: initial;
			background: rgb(16, 142, 233);
			transition: all 0.3s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
			&:hover {
				opacity: 0.85;
			}
		}
	}

	.ymlrozetka-head {
		display: flex;
		align-items: center;
		padding: 10px 0 20px 0;
		button {
			margin: 0 0 0 20px;
		}
	}
`;

export const TableWrapper = styled.div`
	table > thead tr > th {
		background: rgb(241, 243, 246) !important;
		color: ${palette('text', 1)};
		font-size: 13px;
	}
	.status-success {
		color: rgb(126, 211, 33);
	}
	.edit-btn {
		margin: 0 !important;
	}

	table > tbody tr > td {
		color: ${palette('text', 1)};
		font-size: 12px;
	}
`;

export const FiltersWrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin-bottom: 20px;
	&.order-filters {
		//flex-wrap: nowrap;
	}

	.categories-cascader {
		margin: 0 19px 20px 0;
	}
	select {
		height: 32px;
		margin: 0 10px 20px 0;
		padding: 4px 11px;

		background: #fff;
		border: 1px solid #d9d9d9;
		border-radius: 4px;
	}
	.filter-input {
		width: 200px;
		margin: 0 10px 20px 0;
		@media only screen and (max-width: 768px) {
			width: 100%;
			margin: 0 0 20px 0;
		}
	}
	.input-item {
		margin: 0 10px 10px 0;
		width: 200px;
		//width: 25%;
		select {
			width: 100%;
		}
	}
`;
