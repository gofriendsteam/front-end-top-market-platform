import styled from 'styled-components';

export const Title = styled.h2`
	color: #788195;
	font-family: 'Roboto', sans-serif;
	font-weight: 500;
	padding: ${props => (props.pd ? props.pd : '0')};
	margin: ${props => (props.mr ? props.mr : '0 0 20px 0')};
	font-size: 19px;

	color: rgb(120, 129, 149);
	width: 100%;
	display: flex;
	-webkit-box-align: center;
	align-items: center;
	white-space: nowrap;
	position: relative;
	&:before {
		content: '';
		width: 5px;
		height: 40px;
		background-color: rgb(228, 230, 233);
		display: flex;
		margin: 0px 15px 0px 0px;
	}
	&:after {
		content: '';
		width: 100%;
		height: 1px;
		background-color: rgb(228, 230, 233);
		display: flex;
		margin: 0px 0px 0px 15px;
	}
`;
export const Header = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: ${props => (props.padding ? props.padding : '24px')};
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;
