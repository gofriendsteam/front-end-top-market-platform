import styled from 'styled-components';
// import { palette } from 'styled-theme';

export const Card = styled.div`
	background-color: rgb(255, 255, 255);
	padding: 30px 15px;
	border-width: 1px;
	border-style: solid;
	border-color: rgb(235, 235, 235);
	border-image: initial;
	.instruction-image {
		display: flex;
		justify-content: center;
		& > img {
			max-width: 700px;
			width: 100%;
		}
	}
	.sub-categories-btn-nav {
	display: flex;
	align-items: center;
	justify-content: space-between;
	 button {
	 margin: 0 20px 0 0;
	 }
	}
`;
