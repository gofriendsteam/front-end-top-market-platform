import React from 'react';
import styles from './wrapper.module.css';

const ContentWrapper = ({ className = '', children, ...rest }) => {
	return (
		<div {...rest} className={`${styles.pageWrapper} ${className}`}>
			{children}
		</div>
	);
};

export default ContentWrapper;
