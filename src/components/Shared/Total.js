import React from 'react';
import styled from 'styled-components';
import { palette } from 'styled-theme';

const TotalWrapper = styled.div`
	color: ${palette('text', 1)};
	font-weight: 500;
	margin-left: auto;
	display: flex;
	justify-content: space-between;
	span {
		font-size: 15px;
		color: ${palette('primary', 14)};
	}
`;

const Total = ({ count }) => {
	return (
		<TotalWrapper>
			Товаров: <span>{count} шт.</span>
		</TotalWrapper>
	);
};

export default Total;
