import React from 'react';
import styles from './button.module.css';
import { Button } from 'antd';
import { useHistory } from 'react-router-dom';

const CustomButton = ({ title = '', href, className = '', ...rest }) => {
	let history = useHistory();
	const classNames = className ? `${styles.btn} ${className}` : styles.btn;
	return (
		<Button
			onClick={() => href && history.push(href)}
			{...rest}
			className={classNames}
		>
			{title}
		</Button>
	);
};

export default CustomButton;
