import {Icon} from "antd";
import React from "react";

export default props => (
	<Icon {...props} type="loading" style={{ fontSize: 48 }} spin />
);
