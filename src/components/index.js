import React, { useEffect } from 'react';
import { Layout } from 'antd';
import { fetchAllCategories } from '../store/actions/products';
import { useDispatch } from 'react-redux';
import {
	DashboardContainer,
	DashboardGlobalStyles,
} from '../styles/Dashboard.styles';
import Topbar from './Blocks/TopBar/Topbar';
import NavBar from './Blocks/NavBar/NavBar';
import MyModal from './Blocks/Modal/Modal';
import Chat from './Blocks/Chat/Chat';

const { Content } = Layout;

const styles = {
	layout: { flexDirection: 'row', overflow: 'hidden' },
	content: {
		padding: '70px 15px 20px 15px',
		flexShrink: '0',
		background: 'rgb(245, 242, 242)',
		position: 'relative',
	},
	height: { height: '100vh' },
};

const AdministratorSide = ({ children, ...rest }) => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchAllCategories());
	}, []);

	return (
		<DashboardContainer>
			<MyModal />
			<Chat />
			<DashboardGlobalStyles />
			<Layout style={styles.height}>
				<Topbar />
				<Layout style={styles.layout}>
					<NavBar />
					<Layout className="isoContentMainLayout" style={styles.height}>
						<Content className="isomorphicContent" style={styles.content}>
							{children}
						</Content>
					</Layout>
				</Layout>
			</Layout>
		</DashboardContainer>
	);
};

export default AdministratorSide;
