import styled from 'styled-components';
// import { palette } from 'styled-theme';

export const FinanceWrapper = styled.div`
	padding: 24px 0;
	.cards-wrapper {
		width: 100%;
	}
	.recharge-block button {
		width: 200px;
		font-size: 16px;
		margin-top: 20px;
	}
	.icon-wrapper {
		width: 30%;
		color: #fff;
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 30px;
		background-color: rgba(0, 0, 0, 0.1);
		i {
		font-size: 40px;
		}
	}

	.content {
		position: relative;
		width: 70%;
		padding: 20px 15px 20px 20px;
	}
	.question-icon {
		font-size: 17px;
		position: absolute;
		right: 10px;
		top: 10px;
	}
	.card {
		width: 100%;
		height: 100%;
		//height: inherit;
		//margin-right: 20px;
		display: flex;
		//flex-direction: column;
	}
	.card-title {
		font-size: 16px;
		font-weight: 400;
		line-height: 1.2;
		color: #fff;
	}
	.balance-numbers {
		font-size: 20px;
		font-weight: 500;
		line-height: 1.2;
		color: #fff;
	}
	.b-color {
		background-color: rgb(66, 165, 246);
	}
	.a-color {
		background-color: rgb(126, 211, 32);
	}
	.g-color {
		background-color: rgb(247, 93, 129);
	}
`;
export const Wrapper = styled.div`
	margin: 0 0 25px 0;
	//display: flex;
	//flex-wrap: wrap;
	//align-items: stretch;
	/* background: #fff; */
	h5 {
		color: #4a4a4a;

		font-size: 14px;
		font-weight: 500;
		line-height: 21px;
		margin-bottom: 8px;
		margin-right: 10px;
	}
	/* > div {
    min-width: 145px;
    padding: 0;
    padding-right: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
  > div:not(:first-of-type) {
    padding-left: 20px;
    border-left: 1px solid #e2e2e2;
  } */
`;
