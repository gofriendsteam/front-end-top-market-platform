import React from 'react';
import { Wrapper } from '../finance.styles';
import PartnerBalance from './PartnerBalance';
import ContractorBalance from './ContractorBalance';
import { balance } from '../../../../utils/api/userActions';

class BalanceBar extends React.Component {
	state = { userBalance: 0, frozenBalance: 0, loading: true };
	componentDidMount() {
		this.refreshBalance();
	}
	refreshBalance = async () => {
		this.setState({ loading: true });
		const data = await balance();
		this.setState({ ...data, loading: false });
	};
	render() {
		return (
			<Wrapper>
				{this.props.user.role === 'CONTRACTOR' ? (
					<ContractorBalance
						{...this.state}
						refreshBalance={this.refreshBalance}
					/>
				) : (
					<PartnerBalance
						{...this.state}
						refreshBalance={this.refreshBalance}
					/>
				)}
			</Wrapper>
		);
	}
}

export default BalanceBar;
