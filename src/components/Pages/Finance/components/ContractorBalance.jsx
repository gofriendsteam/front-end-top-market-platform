import React, { Fragment } from 'react';
import { Col, Icon, Row } from 'antd';
import styles from '../Finance.module.css';
import Recharge from '../../../Blocks/Recharge/Recharge';
import styled from 'styled-components';

const BalanceIncome = styled.span`
	color: #4a90e2;
`;
const BalanceOutgo = styled.span`
	color: #f19300;
`;
const BalanceAvailable = styled.span`
	color: #019d45;
`;

const ContractorBalance = ({ userBalance, loading, refreshBalance }) => {
	return (
		<Row
			type="flex"
			align="stretch"
			justify="space-between"
			gutter={[16, 16]}
			className="cards-wrapper"
		>
			<Col span={24} sm={12} lg={8}>
				<div className="card  b-color">
					<div className="icon-wrapper">
						<i className="icon ion-md-card" />
					</div>
					<div className="content">
						<h5 className="card-title">Доход</h5>
						<div className="balance-numbers ">0.00 грн</div>
					</div>
				</div>
			</Col>
			<Col span={24} sm={12} lg={8}>
				<div className="card  g-color">
					<div className="icon-wrapper">
						<i className="icon ion-md-wallet" />
					</div>
					<div className="content">
						<h5 className="card-title">Расход</h5>
						<div className="balance-numbers ">0.00 грн</div>
					</div>
				</div>
			</Col>
			<Col span={24} sm={12} lg={8}>
				<div className="card  a-color">
					<div className="icon-wrapper">
						<i className="icon ion-md-cash" />
					</div>
					<div className="content">
						<h5 className="card-title">На счету</h5>
						<div className="balance-numbers ">{userBalance.toFixed(2)} грн</div>
					</div>
				</div>
			</Col>

			{/*<Icon*/}
			{/*	type="sync"*/}
			{/*	spin={loading}*/}
			{/*	onClick={refreshBalance}*/}
			{/*	style={{*/}
			{/*		fontSize: 40,*/}
			{/*		alignSelf: 'center',*/}
			{/*		color: loading ? '#4A90E2' : 'grey',*/}
			{/*	}}*/}
			{/*/>*/}
			<div className="recharge-block">
				<Recharge title="Пополнение баланса" buttonName="Пополнить" />
			</div>
		</Row>
	);
};

export default ContractorBalance;
