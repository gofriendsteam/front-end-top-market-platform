import React, {Component} from 'react';
import styles from '../Finance.module.css';
import { Table } from 'antd';
import { COLUMNS } from '../../../../constants/TableColumns/TabColumns';
import { CONFIG } from '../../../../helpers/TabConfig';
import * as userActions from '../../../../utils/api/userActions';

class TableTemplate extends Component {
  state = {
    columns: [],
    count: 0,
    dataSource: [],
    loading: true,
  }


  // LIFECYCLE METHODS
  componentWillMount () {
    const {moduleName, tableName, user} = this.props;
    const columns = COLUMNS[moduleName][user.role.toLowerCase()][tableName];
    this.setState({
      columns,
    });
  }

  async componentDidMount () {
    const {actions} = this.props; // в компонент может м
      console.log(actions);
    const getData = action => userActions[action]();
    const [...data] = await Promise.all(actions.map(getData));
      console.log(data);
    const dataSource = data.reduce((acc, el) => [...acc, ...el.results], []);
    // const count = data.reduce((acc, el) => acc+el.count, 0);
    const count = dataSource.length;
      console.log(count);
      console.log(dataSource);
    // // const {count, results} = await actions[action](); //делаем запрос данных
    this.setState({
      count,
      dataSource,
      loading: false,
    });
  }

  render() {
    const { count } = this.state;
    const { units } = this.props
    console.log('STATE', this.state);
    console.log('PROPS', this.props);
    // console.log(results);

    return  (
      <div className={styles.invoiceForPayment}>
          <Table {...this.state}
            {...CONFIG(units, count)}
            // onChange={this.handleChangeTable}
          />
      </div>
    )
  }
};

export default TableTemplate;
