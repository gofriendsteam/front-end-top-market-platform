import React, { Fragment } from 'react';
import {  Icon, Tooltip, Row, Col } from 'antd';
import Recharge from '../../../Blocks/Recharge/Recharge';
import { GREYZONE, AVAILABLE_FOR_PAYOUT } from '../../../../constants/TOOLTIPS';
import CustomButton from "../../../Shared/Button/Button";

const PartnerBalance = ({
	frozenBalance,
	userBalance,
	loading,
	refreshBalance,
}) => {
	return (
		<Fragment>
			<Row
				type="flex"
				align="stretch"
				justify="space-between"
				gutter={[16, 16]}
				className="cards-wrapper"
			>
				<Col span={24} sm={12} lg={8}>
					<div className="card  b-color">
						<div className="icon-wrapper">
							<i className="icon ion-md-card" />
						</div>
						<div className="content">
							<h5 className="card-title">Баланс</h5>
							<div className="balance-numbers ">
								{(+frozenBalance + +userBalance).toFixed(2)}грн
							</div>
						</div>
					</div>
				</Col>
				<Col span={24} sm={12} lg={8}>
					<div className="card  g-color">
						<div className="icon-wrapper">
							<i className="icon ion-md-wallet" />
						</div>
						<div className="content">
							<h5 className="card-title">Серая зона</h5>
							<Tooltip placement="right" title={GREYZONE}>
								<Icon
									type="question-circle"
									theme="twoTone"
									className="question-icon"
								/>
							</Tooltip>

							<div className="balance-numbers">
								{(+frozenBalance).toFixed(2)}грн
							</div>
						</div>
					</div>
				</Col>
				<Col span={24} sm={12} lg={8}>
					<div className="card  a-color">
						<div className="icon-wrapper">
							<i className="icon ion-md-cash" />
						</div>
						<div className="content">
							<h5 className="card-title">Сумма доступная к выводу</h5>
							<div className="balance-numbers">
								{(+userBalance).toFixed(2)}грн
							</div>
						</div>
					</div>
				</Col>
			</Row>
			{/*<Icon*/}
			{/*	type="sync"*/}
			{/*	spin={loading}*/}
			{/*	onClick={refreshBalance}*/}
			{/*	style={{*/}
			{/*		fontSize: 32,*/}
			{/*		alignSelf: 'center',*/}
			{/*		color: loading ? '#4A90E2' : 'grey',*/}
			{/*	}}*/}
			{/*/>*/}
			<div className='recharge-block'>
				{+userBalance >= 1000 ? (
					<Recharge title="Вывести средства" buttonName="Вывести" />
				) : (
					<Tooltip placement="right" title={AVAILABLE_FOR_PAYOUT}>
						<CustomButton
							title='Вывести'
							type='primary'
							disabled
						/>
					</Tooltip>
				)}
			</div>
		</Fragment>
	);
};

export default PartnerBalance;
