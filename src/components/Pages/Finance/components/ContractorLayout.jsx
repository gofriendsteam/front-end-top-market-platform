import React from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
import TableTemplate from '../../../Blocks/Tables/TableTemplate';

const TabPane = Tabs.TabPane;
function callback(key) {
	console.log(key);
}

const ContractorLayout = ({ user }) => {
	return (
		<Tabs
			onChange={callback}
			type="line"
			animated={false}
			style={{ margin: 25 }}
		>
			<TabPane tab="История транзакций" key="1">
				<TableTemplate
					action="transactionsHistory"
					moduleName="financeColumns"
					tableName="transactionsHistory"
					units="транзакций"
					user={user}
				/>
			</TabPane>
			<TabPane tab="Счета на оплату" key="2">
				<TableTemplate
					action="transactionsHistory"
					query="&sourse=1"
					moduleName="financeColumns"
					tableName="invoicesForPayment"
					units="счетов"
					user={user}
				/>
			</TabPane>
			<TabPane tab="Взаиморасчёты c продавцами" key="3">
				<TableTemplate
					action="ordersHistory"
					moduleName="financeColumns"
					tableName="mutualSettlements"
					units="записей"
					user={user}
				/>
			</TabPane>
		</Tabs>
	);
};

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ContractorLayout);
