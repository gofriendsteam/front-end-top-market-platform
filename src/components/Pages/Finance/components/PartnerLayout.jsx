import React from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
import TableTemplate from '../../../Blocks/Tables/TableTemplate';

const TabPane = Tabs.TabPane;
function callback(key) {
	console.log(key);
}

const PartnerLayout = props => {
	const { user } = props;
	console.log(props);

	return (
		<Tabs
			onChange={callback}
			type="line"
			animated={false}
			style={{ margin: 25 }}
		>
			<TabPane tab="История транзакций" key="1">
				<TableTemplate
					action="transactionsHistory"
					moduleName="financeColumns"
					tableName="transactionsHistory"
					units="транзакций"
					user={user}
				/>
			</TabPane>
			<TabPane tab="Финансовые отчёты платформ" key="2">
				<Tabs onChange={callback} type="card">
					<TabPane tab="Отчёты Rozetka" key="Rozetka">
						<Tabs onChange={callback} type="card">
							<TabPane
								tab="История транзакций"
								key="rozetkaTransactionsHistory"
							>
								<TableTemplate
									action="rozetkaTransactionsHistory"
									moduleName="financeColumns"
									tableName="rozetkaTransactionsHistory"
									units="транзакций"
									user={user}
								/>
							</TabPane>
							<TabPane tab="Счета на оплату" key="rozetkaInvoices">
								<TableTemplate
									action="rozetkaInvoices"
									moduleName="financeColumns"
									tableName="rozetkaInvoices"
									units="счетов"
									user={user}
								/>
							</TabPane>
							<TabPane
								tab="Отчёты о проданых товарах "
								key="rozetkaSalesReports"
							>
								<TableTemplate
									action="rozetkaSalesReports"
									moduleName="financeColumns"
									tableName="rozetkaSalesReports"
									units="отчетов"
									user={user}
								/>
							</TabPane>
						</Tabs>
					</TabPane>
					<TabPane tab="Отчёты Prom" key="Prom" style={{ display: 'flex' }}>
						<img
							src="https://static1.squarespace.com/static/55118238e4b0ef57ce737dfb/57b8522d197aea4fdb59ede0/57c094c859cc68adae97f4d7/1472239382345/coming-soon.jpg?format=2500w"
							alt="coming soon"
							width="200"
							style={{ margin: 'auto' }}
						/>
					</TabPane>
				</Tabs>
			</TabPane>
			<TabPane tab="Взаиморасчёты c поставщиками" key="3">
				<TableTemplate
					action="ordersHistory"
					moduleName="financeColumns"
					tableName="mutualSettlements"
					units="записей"
					user={user}
				/>
			</TabPane>
		</Tabs>
	);
};

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PartnerLayout);
