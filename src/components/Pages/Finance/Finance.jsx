import React from 'react';
import { useSelector } from 'react-redux';

import ContractorLayout from './components/ContractorLayout';
import PartnerLayout from './components/PartnerLayout';
import BalanceBar from './components/BalanceBar';
import { Title } from '../../Shared/Styles/title.styles';
import { TableWrapper } from '../../Shared/Styles/MainWrapper';
import { Card } from '../../Shared/Styles/Card';
import { FinanceWrapper } from './finance.styles';

const Finance = props => {
	const user = useSelector(state => state.user);

	return (
		<FinanceWrapper>
			<Title>Финансы и баланс </Title>
			<BalanceBar user={user} />
			<TableWrapper>
				<Card>
					{user.role === 'PARTNER' ? (
						<PartnerLayout {...props} />
					) : (
						<ContractorLayout {...props} />
					)}
				</Card>
			</TableWrapper>
		</FinanceWrapper>
	);
};

export default Finance;
