import React, { useEffect, useState } from 'react';
import styles from './Cabinet.module.css';
import { getAllPockets } from '../../../utils/api/companyActions';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { toogleModal } from '../../../store/actions/modal';
import Payment from '../../Blocks/Payment/Payment';
import { Title } from '../../Shared/Styles/title.styles';
import CustomButton from '../../Shared/Button/Button';

const Pockets = ({ user, toogleModal }) => {
	const [pockets, setPockets] = useState([]);
	let titleColors = idx =>
		idx === 1
			? styles.goldTitle
			: idx === 2
			? styles.platinumTitle
			: styles.silverTitle;
	useEffect(() => {
		getAllPockets()
			.then(({ results }) => console.log(results) || setPockets(results))
			.catch(err => console.log(err));
	}, []);

	return (
		<div className={styles.chooseTariff}>
			<Title mr="0 0 20px 0" >
				Приобретите пакет
			</Title>
			<Row gutter={20} type="flex" justify="space-between">
				{pockets.map((pocket, idx) => (
					<Col lg={8} md={12} sm={12} xs={24} key={idx}>
						<div className={styles.tariffItem} key={pocket.name}>
							<div className={titleColors(idx)}>
								<h4>{pocket.name}</h4>
								<p>Стартовый пакет</p>
							</div>
							<div className={styles.tariffPrice}>
								<div className={styles.priceBlock}>
									<span className={styles.oldPrice}>
										{pocket.oldPrice} {pocket.currency === 'UAH' && 'грн'}
									</span>
									<span className={styles.currentPrice}>
										{pocket.price} {pocket.currency === 'UAH' && 'грн'}
									</span>
								</div>
							</div>
							<div className={styles.tariffItemBody}>
								<ul>
									{pocket.description.split('\n').map((listitem, idx) => (
										<li key={idx}>{listitem}</li>
									))}
								</ul>
							</div>
							<div className={styles.btnWrapper}>
								{user.userPocket === pocket.id ? (
									<CustomButton
										title="Активен"
										type="primary"
										className={styles.activeBtn}
									/>
								) : user.userPocket >= 2 ? (
									<CustomButton
										title="	Приобретён пакет выше"
										disabled
										className={styles.buyBtn}
									/>
								) : (
									<CustomButton
										title="КУПИТЬ"
										type="primary"
										className={styles.buyBtn}
										onClick={() =>
											toogleModal({
												isOpen: true,
												type: '',
												component: Payment,
												data: { id: pocket.id, title: 'Покупка пакета' },
											})
										}
									>
										КУПИТЬ
									</CustomButton>
								)}
							</div>
						</div>
					</Col>
				))}
			</Row>
		</div>
	);
};

const mapDispatchToProps = dispatch => ({
	toogleModal: data => dispatch(toogleModal(data)),
});

export default connect(
	null,
	mapDispatchToProps,
)(Pockets);
