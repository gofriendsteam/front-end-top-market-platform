
import styled from 'styled-components';
import { borderRadius } from '@iso/lib/helpers/style_utils';

const StickerWidgetWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: stretch;
  overflow: hidden;
  ${borderRadius('5px')};

  .isoIconWrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 80px;
    flex-shrink: 0;
    background-color: rgba(0, 0, 0, 0.1);

    i {
      font-size: 30px;
    }
  }

  .isoContentWrapper {
    width: 100%;
    padding: 20px 15px 20px 20px;
    display: flex;
    flex-direction: column;

    .isoStatNumber {
      font-size: 20px;
      font-weight: 500;
      line-height: 1.1;
      margin: 0 0 5px;
    }

    .isoLabel {
      font-size: 16px;
      font-weight: 400;
      margin: 0;
      line-height: 1.2;
    }
  }
`;



export default function({ fontColor, bgColor, width, icon, number, text }) {
	const textColor = {
		color: fontColor,
	};
	const widgetStyle = {
		backgroundColor: bgColor,
		width: width,
	};
	const iconStyle = {
		color: fontColor,
	};

	return (
		<StickerWidgetWrapper className="isoStickerWidget" style={widgetStyle}>
			<div className="isoIconWrapper">
				<i className={icon} style={iconStyle} />
			</div>

			<div className="isoContentWrapper">
				<h3 className="isoStatNumber" style={textColor}>
					{number}
				</h3>
				<span className="isoLabel" style={textColor}>
          {text}
        </span>
			</div>
		</StickerWidgetWrapper>
	);
}
