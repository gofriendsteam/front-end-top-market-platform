import React, { Component } from 'react';
import styles from './Cabinet.module.css';
import 'antd/dist/antd.css';
import '../../../App.css';
import avatar from '../../../img/avatar.png';
import { connect } from 'react-redux';
import { Col, Row } from 'antd';
import { Title } from '../../Shared/Styles/title.styles';
import CustomButton from '../../Shared/Button/Button';
import { toogleModal } from '../../../store/actions/modal';
import { getProfile as getCompanyProfile } from '../../../utils/api/companyActions';
import Pockets from './Pockets';
import comingsoonimg from '../../../img/comingsoon.jpg';

class Cabinet extends Component {
	state = {
		// user: {},
		sellerRating: 3.5,
		company: {},
		packId: 0, // № пакета, меняется при нажатии кнопки "Купить"
	};

	async componentDidMount() {
		const company = await getCompanyProfile();

		this.setState({
			company,
		});
	}

	render() {
		const { company } = this.state;
		const { user } = this.props;
		return (
			<div className={styles.containerWr}>
				<Title>
					Кабинет {user.role === 'PARTNER' ? 'Продавца' : 'Поставщика'}
				</Title>
				<Row gutter={20} type="flex" justify="center">
					<Col span={24} lg={12}>
						<div className={styles.userBlock}>
							<h3>Профиль </h3>
							<div className={styles.userContacts}>
								<div className={styles.userImg}>
									<img src={user.avatarImage || avatar} alt="" />
								</div>
								<div className={styles.userInfo}>
									<div className={styles.stars}>
										<h5 className={styles.userName}>
											{' '}
											{`${user.firstName || 'User'} ${user.lastName || ''}`}
										</h5>
									</div>
									<div className={styles.table}>
										<div className={styles.td}>
											<div>Статус:</div>

											<div className={styles.notActivated}> Активирован</div>
										</div>
										<div className={styles.td}>
											<div>Статус Компании:</div>

											<div className={styles.notActivated}> Активирован</div>
										</div>
										<div className={styles.td}>
											<div>E-Mail:</div>

											<div> {user.email || 'Не указано'}</div>
										</div>
										<div className={styles.td}>
											<div>Компания:</div>

											<div> {company.name || 'Не `указано'}</div>
										</div>
										<div className={styles.td}>
											<div>Телефон:</div>

											<div> {user.phone || 'Не указано'}</div>
										</div>
										<div className={styles.td}>
											<div>Веб-сайт:</div>

											<div> {user.webSite || 'Не указано'}</div>
										</div>
									</div>
								</div>
							</div>

							<CustomButton
								title="Редактировать профиль"
								type="primary"
								href={'/admin/profile_settings'}
							/>
						</div>
					</Col>
					<Col span={24} lg={12}>
						<div className={styles.videoTariff}>
							<h4>В разработке...</h4>
							<div className={styles.videoBlock}>
								<img src={comingsoonimg} alt="comingsoon" />
								{/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
								{/*<iframe*/}
								{/*	src="https://www.youtube.com/embed/skoeojp5_cc"*/}
								{/*	frameBorder="0"*/}
								{/*	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"*/}
								{/*	allowFullScreen*/}
								{/*/>*/}
							</div>
							{this.props.user.role !== 'CONTRACTOR' && (
								<div className={styles.description}>
									<p>
										Для того чтобы пользоваться услугами Маркетплейса, Вы должны
										приобрести один из вариантов пакетов доступа, цена на
										которые 27 000 грн, 45 000 и 59 000 грн. Все доступы,
										которые дает каждый пакет, указаны в описании.
									</p>
								</div>
							)}
						</div>
					</Col>
					<Col span={24}>
						{user.role !== 'CONTRACTOR' && <Pockets user={user} />}
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	toogleModal: data => dispatch(toogleModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cabinet);
