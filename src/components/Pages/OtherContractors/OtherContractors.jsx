import React from 'react';
import { Table } from 'antd';
import styles from './OtherContractors.module.css';

const OtherContractors = () => {
	// const TabPane = Tabs.TabPane;

	const columns = [
		{
			title: 'ID поставщика',
			dataIndex: 'contractorId',
			render: id => id || 'Мой товар',
		},
		{
			title: 'Артикул',
			dataIndex: 'vendorCode',
		},
		{
			title: 'Количество',
			dataIndex: 'count',
		},
		{
			title: 'Цена поставщика',
			dataIndex: 'contractorPriceForPartner',
			render: (price, item) =>
				item.contractorId ? price.toFixed(2) : item.price.toFixed(2),
		},
		{
			title: 'Рекомендуемая розничная цена',
			dataIndex: 'recommendedPrice',
			render: (price, item) => (item.contractorId ? price : '-'),
		},
		{
			title: 'Цена',
			dataIndex: 'price',
			render: (price, item) =>
				price
					? (+price * ((100 + +item.partnerPercent) / 100)).toFixed(2)
					: (
							+item.contractorPriceForPartner *
							((100 + +item.partnerPercent) / 100)
					  ).toFixed(2),
		},
		{
			title: 'Наценка',
			dataIndex: 'partnerPercent',
			render: (partnerPercent, item) =>
				item.contractorPriceForPartner
					? `${(
							(+item.contractorPriceForPartner * +partnerPercent) /
							100
					  ).toFixed(2)} \n (${partnerPercent}%)`
					: `${((+item.price * +partnerPercent) / 100).toFixed(
							2,
					  )} \n (${partnerPercent}%)`,
		},
		{
			title: 'YML',
			dataIndex: 'isAddedToYmlRozetka',
			render: isYml => (isYml ? '+' : '-'),
		},
	];

	return (
		<div>
			<h3 className="page-title">Поставщики товара</h3>
			<div className={`${styles.contractor} page`}>
				<Table
					columns={columns}
					pagination={{ pageSize: 50 }}
					scroll={{ y: 240 }}
				/>
			</div>
		</div>
	);
};
export default OtherContractors;
