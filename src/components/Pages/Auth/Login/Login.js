import React, { Component, useEffect } from 'react';
import { Form, Input } from 'antd';
import CustomButton from '../../../Shared/Button/Button';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import styles from './Login.module.css';

import { login } from '../../../../utils/api/userActions';
// import logo from '../../../img/logo2.png';

const FormItem = Form.Item;

const Login = props => {
	const { getFieldDecorator } = props.form;
	const handleSubmit = async e => {
		e.preventDefault();

		props.form.validateFields((err, user) => {
			if (!err) {
				props.login(user);

			}
		});
	};

	useEffect(() => {
		// console.log(props.user)
		if (props.user.role) {
			props.history.replace('/admin/cabinet')
		}
	}, [props.user.role]);

	return (
		<Form onSubmit={handleSubmit} className='form'>
			<h3 className='title'>TopMarket</h3>
			<FormItem>
				{/*<label>Ваш email</label>*/}
				{getFieldDecorator('email', {
					rules: [
						{
							type: 'email',
							message: 'Неправильный формат e-mail!',
						},
						{ required: true, message: 'Введите ваш email!' },
					],
				})(<Input placeholder="Ваш Email" />)}
			</FormItem>

			<FormItem>
				{/*<label>Пароль</label>*/}
				{getFieldDecorator('password', {
					rules: [{ required: true, message: 'Введите ваш пароль!' }],
				})(
					<Input
						// prefix={<Icon type="lock" style={{fontSize: 13}}/>}
						type="password"
						placeholder='Ваш пароль'
					/>,
				)}
			</FormItem>

			<Link to="/reset_password" className={styles.reset}>
				Забыл пароль
			</Link>

			<div className={styles.actions}>
				<Link to="/registration" className={styles.registration}>
					Регистрация
				</Link>

				<CustomButton
					type="primary"
					htmlType="submit"
					className={styles.loginFormButton}
					title="Войти"
				/>
			</div>
		</Form>
	);
};

const WrappedNormalLoginForm = Form.create()(Login);

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	login: user => dispatch(login(user)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(WrappedNormalLoginForm);
