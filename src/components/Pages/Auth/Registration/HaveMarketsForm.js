import React, { useState } from 'react';
import { Checkbox } from 'antd';
import { useSelector } from 'react-redux';
import CustomButton from '../../../Shared/Button/Button';

const HaveMarketsForm = () => {
	const marketplaces = useSelector(state => state.marketplaces);
	const [cValues, setCheckboxs] = useState([]);
	const onChange = checkedValues => {
		setCheckboxs(checkedValues);
	};

	return (
		<div>
			<div className="description">
				<p>Какие каналы продаж вы сейчас используете для продажи товаров?</p>
				<p>Отметьте галочками один или несколько вариантов.</p>
			</div>
			<Checkbox.Group
				className="checkbox-group"
				options={marketplaces}
				onChange={onChange}
				value={cValues}
			/>
			<CustomButton  className='step-btn' title="Далее" type="primary" />
		</div>
	);
};

export default HaveMarketsForm;
