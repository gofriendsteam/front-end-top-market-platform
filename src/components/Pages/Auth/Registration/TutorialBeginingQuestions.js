import React, { useState } from 'react';
import CustomButton from '../../../Shared/Button/Button';
import { Radio } from 'antd';

const TutorialBeginingQuestions = props => {
	const [choose, setChoose] = useState('already_selling');
	const onChangeTutorialRadioBtn = e => {
		const tutorial_choose = e.target.value;
		if (tutorial_choose === 'already_selling') {
			setChoose(tutorial_choose);
		} else {
			setChoose(tutorial_choose);
		}
	};
	const moveToNextStep = () => {
		const condition = choose === 'already_selling' ? 2 : 3;
		props.setStep(condition);
	};
	return (
		<div className='tutorial-begin-wrapper'>
			<div className='description'>
				<p>Вы успешно создали аккаунт на плтформе <span>TopMarket(SmartLead 2.0)</span>.</p>
				<p>
					Что бы завершить создание своего профиля ответьте на несколько вопросов:
				</p>
			</div>
			<div className='radio-wrapper'>
				<Radio.Group
					onChange={onChangeTutorialRadioBtn}
					defaultValue="already_selling"
					value={choose}
				>
					<Radio className='radio-item' value="already_selling">я уже продаю товары на Розетке</Radio>
					<Radio className='radio-item' value="want_to_sell">
						Хочу начать зарабатывать торгуя на Розетке
					</Radio>
				</Radio.Group>
			</div>
			<CustomButton className='step-btn' title="Далее" type="primary" onClick={moveToNextStep} />
		</div>
	);
};

export default TutorialBeginingQuestions;
