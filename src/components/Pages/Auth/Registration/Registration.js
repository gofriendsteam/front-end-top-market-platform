import React, { Component, Fragment } from 'react';
import { Checkbox, Form, Input, Radio } from 'antd';
// import { Link } from 'react-router-dom';
import { Modal } from 'antd';
import CustomButton from '../../../Shared/Button/Button';
// import styles from './Registration.module.css';
import { registration } from '../../../../utils/api/userActions';
import RegistrationForm from './RegistrationForm';
import HaveMarketsForm from './HaveMarketsForm';
import WantToForm from './WantForm';
import TutorialBeginingQuestions from './TutorialBeginingQuestions';

class Registration extends Component {
	state = {
		role: 'CONTRACTOR',
		tutorialStep: 0,
		// tutorial_choose: 'already_selling',
	};

	onChange = e => {
		this.setState({
			role: e.target.value,
		});
	};

	success = () => {
		const history = this.props.history;
		Modal.success({
			title: 'Регистрация успешна',
			content:
				'Для подтверждения вашего адреса электронной почты мы отправили вам письмо',
			onOk() {
				history.push('/login');
			},
		});
	};

	// compareToFirstPassword = (rule, value, callback) => {
	// 	const { form } = this.props;
	// 	if (value && value !== form.getFieldValue('password')) {
	// 		callback('Пароли не совпадают!');
	// 	} else {
	// 		callback();
	// 	}
	// };
	// handleSubmit = e => {
	// 	e.preventDefault();
	//
	// 	this.props.form.validateFields((err, user) => {
	// 		if (!err) {
	// 			registration({
	// 				...user,
	// 				role: this.state.role,
	// 			}).then(res => {
	// 				console.log(res);
	// 				if (this.state.role === 'PARTNER') {
	// 					this.setState({
	// 						tutorialStep: 1,
	// 					});
	// 				} else {
	// 					console.log('IS CONTRACTOR');
	// 					// this.success();
	// 				}
	// 			});
	// 		}
	// 	});
	// };
	// checkCheckBox = (rule, value, callback) => {
	// 	console.log(value);
	// 	if (!value) {
	// 		callback('Вы не ознакомились с правилами оферты!');
	// 	} else {
	// 		callback();
	// 	}
	// };
	nextStep = val => {
		this.setState({ tutorialStep: val });
	};
	// validateToNextPassword = (rule, value, callback) => {
	// 	const { form } = this.props;
	// 	if (value && this.state.confirmDirty) {
	// 		form.validateFields(['confirm'], { force: true });
	// 	}
	// 	callback();
	// };
	// onChangeTutorialRadioBtn = e => {
	// 	const tutorial_choose = e.target.value;
	// 	if (tutorial_choose === 'already_selling') {
	// 		this.setState({ tutorialStep: 2 });
	// 	} else {
	// 		this.setState({ tutorialStep: 3 });
	// 	}
	// };
	render() {
		const { tutorialStep } = this.state;

		const RegistrationFlow = () => {
			switch (tutorialStep) {
				case 1:
					return <TutorialBeginingQuestions setStep={this.nextStep} />;
				case 2:
					return <HaveMarketsForm />;
				case 3:
					return <WantToForm />;
				case 0:
					return <RegistrationForm setStep={this.nextStep} />;
				default:
					return <RegistrationForm setStep={this.nextStep} />;
			}
		};
		return (
			<div className="form">
				<h3 className="title">TopMarket</h3>
				{/*<button onClick={() => this.nextStep(1)}>next</button>*/}

				<RegistrationFlow />

			</div>
		);
	}
}

export default Registration;
