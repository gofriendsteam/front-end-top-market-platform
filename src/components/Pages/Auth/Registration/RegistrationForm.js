import React, { Fragment, useState } from 'react';
import styles from './Registration.module.css';
import { Checkbox, Form, Input, Radio, notification } from 'antd';
import { Link } from 'react-router-dom';
import CustomButton from '../../../Shared/Button/Button';
import { registration } from '../../../../utils/api/userActions';
import { useHistory } from 'react-router-dom';

const RadioButton = Radio.Button,
	RadioGroup = Radio.Group;

const RegistrationForm = props => {
	const [role, setRole] = useState('CONTRACTOR');
	const [confirmDirty] = useState(true);
	const history = useHistory();
	const { getFieldDecorator } = props.form;
	const onChange = ({ target: { value } }) => {
		setRole(value);
	};
	const validateToNextPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && confirmDirty) {
			form.validateFields(['confirm'], { force: true });
		}
		callback();
	};
	const compareToFirstPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && value !== form.getFieldValue('password')) {
			callback('Пароли не совпадают!');
		} else {
			callback();
		}
	};
	const checkCheckBox = (rule, value, callback) => {
		if (!value) {
			callback('Вы не ознакомились с правилами оферты!');
		} else {
			callback();
		}
	};
	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFields((err, user) => {
			if (!err) {
				registration({
					...user,
					role,
				}).then(res => {

					notification.success({
						title: 'Регистрация прошла успешно.',
						message:
							'Ссылка на подтверждение аккаунта была отправлена вам на емейл.',
					});
					history.push('/login');
					// if (role === 'PARTNER') {
					//
					// 	props.setStep(1);
					// 	this.setState({
					// 		tutorialStep: 1,
					// 	});
					// } else {
					// 	console.log('IS CONTRACTOR');
					// 	this.success();
					// }
				});
			}
		});
	};

	return (
		<Form onSubmit={handleSubmit}>
			<div className={styles.selectedRole}>
				<RadioGroup onChange={onChange} defaultValue="CONTRACTOR">
					<RadioButton value="CONTRACTOR">Поставщик</RadioButton>
					<RadioButton value="PARTNER">Продавец</RadioButton>
				</RadioGroup>
			</div>

			<Form.Item>
				{getFieldDecorator('email', {
					rules: [
						{
							type: 'email',
							message: 'Неправильный формат e-mail!',
						},
						{ required: true, message: 'Введите Ваш email!' },
					],
				})(<Input placeholder="Ваш Email" />)}
			</Form.Item>

			<Form.Item>
				{getFieldDecorator('phone', {
					rules: [
						{
							required: true,
							type: 'string',
							pattern: new RegExp(
								/^(\+?\d{2})?-?(\d{3,4})-?(\d{3,4})-?(\d{2,3})-?(\d{2,3})$/,
							),
							message: 'Неверный номер телефона!',
						},
					],
				})(<Input type="tel" placeholder="Ваш номер телефона" />)}
			</Form.Item>

			<Form.Item>
				{getFieldDecorator('password', {
					rules: [
						{ required: true, message: 'Введите Ваш Пароль!' },
						{
							validator: validateToNextPassword,
						},
					],
				})(<Input.Password placeholder="Ваш пароль" />)}
			</Form.Item>

			<Form.Item>
				{getFieldDecorator('confirmPassword', {
					rules: [
						{ required: true, message: 'Введите Ваш Пароль!' },
						{ validator: compareToFirstPassword },
					],
				})(<Input.Password placeholder="Подтверждение пароля" />)}
			</Form.Item>
			<Form.Item>
				{getFieldDecorator('agreement', {
					valuePropName: 'checked',
					initialValue: false,

					rules: [
						{
							validator: checkCheckBox,
						},
					],
				})(
					<Checkbox>
						Я соглашаюсь с{' '}
						<a
							className="credential-link"
							href="http://google.com"
							target="_blank"
						>
							правилами{' '}
						</a>{' '}
						и{' '}
						<a
							className="credential-link"
							href="http://google.com"
							target="_blank"
						>
							договором оферты
						</a>
					</Checkbox>,
				)}
			</Form.Item>
			<Form.Item>
				<div className={styles.actions}>
					<Link to="/">У меня уже есть аккаунт</Link>
					<CustomButton
						type="primary"
						title="Зарегистрироваться"
						htmlType="submit"
						className={styles.loginFormButton}
					/>
				</div>
			</Form.Item>
		</Form>
	);
};
const WrappedNormalRegistrationForm = Form.create({ name: 'register' })(
	RegistrationForm,
);
export default WrappedNormalRegistrationForm;
