import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { Card } from '../../../Shared/Styles/Card';
import styles from './Registration.module.css';
import { useHistory } from 'react-router-dom';
import Loader from '../../../Shared/Loader/Loader';

import { confirmEmail } from '../../../../utils/api/userActions';

const ConfirmRegistration = () => {
	const [error, setError] = useState(null);
	const [loading, setLoading] = useState(true);
	const history = useHistory();
	useEffect(() => {
		let urlParams = queryString.parseUrl(document.location.search).query;
		const isValidToken = Object.keys(urlParams).length > 0;
		if (isValidToken) {
			confirmEmail(urlParams.token)
				.then(({ verified }) => {
					setLoading(false);
					if (verified) {
						setError(null);
					} else {
						setError('Ссылка не валидная');
					}
				})
				.catch(err => setLoading(false));
		} else {
			history.push('/login');
		}

		// ;
	}, []);

	return (
		<div className={`container ${styles.confirmBlock}`}>
			{loading ? (
				<Loader />
			) : (
				<Card>
					<h1>{error ? 'Ошибка!' : 'Спасибо за регистрацию'}</h1>
					{error && <p>{error}</p>}
					<Link to="/login">Перейти на страницу авторизации</Link>
				</Card>
			)}
		</div>
	);
};

export default ConfirmRegistration;
