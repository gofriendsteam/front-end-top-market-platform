import React, { useState } from 'react';
import { Checkbox } from 'antd';
import CustomButton from '../../../Shared/Button/Button';
import { useSelector } from 'react-redux';

const WantToForm = () => {
	const [cValues, setCheckboxs] = useState([]);
	const marketplaces = useSelector(state => state.marketplaces);

	const onChange = checkedValues => {
		setCheckboxs(checkedValues);
	};
	return (
		<div className="step-wrapper">
			<div className="description">
			<p>На каких площадках вы планируете продавать товары? </p>
			<p>Отметьте галочками один или несколько вариантов. </p>
		</div>
			<Checkbox.Group
				className="checkbox-group"
				options={marketplaces}
				onChange={onChange}
			/>
			<CustomButton className="step-btn" title="Далее" type="primary" />
		</div>
	);
};

export default WantToForm;
