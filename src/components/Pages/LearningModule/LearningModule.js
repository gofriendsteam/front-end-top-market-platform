import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { abcList } from '../../../utils/api/userActions';

// import 'antd/dist/antd.css';
// import {Link} from 'react-router-dom';
import LearningLayout from './LearningLayout';
// import {Modal} from "antd";
// import axios from 'axios';
// import { log } from 'util';

const BASE_URL = 'https://cad17746.ngrok.io/api/v1/abc/';

// STYLED via styled-Blocks
const NavList = styled.ul`
	list-style: none;
	display: flex;
	justify-content: center;
	color: #4a4a4a;
	
	font-style: normal;
	font-weight: 500;
	font-size: 18px;
	line-height: 21px;
	background: #ffffff;
	border: 1px solid #ececec;
	border-radius: 5px;
	margin: 10px 20px;
	overflow: hidden;
`;
const ListItem = styled.li`
	padding: 16px;
	width: max-content;
	cursor: pointer;
	transition: all 0.1s ease;

	&.checked {
		background: #4a90e2;
		color: #ffffff;
	}
`;

class LearningModule extends Component {
	state = {
		activeTab: 0,
		sections: [],
	};

	setActiveTab = id => {
		const activeTab = id;
		this.setState({ activeTab });
	};

	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++
	async componentDidMount() {
		const data = await abcList();
		console.log(data);
		this.setState({ sections: data });
		// const { data } = await axios({
		//   method: 'get',
		//   url: `${BASE_URL}content/list/`,
		//   headers: { 'Content-Type': 'application/json' }
		// });
		// this.setState({ sections: data.results }, () => console.log(this.state));
	}

	render() {
		const { activeTab, sections } = this.state;

		return (
			<Fragment>
				<nav>
					<NavList>
						{sections &&
							sections.map((el, i) => (
								<ListItem
									onClick={() => this.setActiveTab(i)}
									className={activeTab === i && 'checked'}
								>
									{el.name}
								</ListItem>
							))}
					</NavList>
				</nav>
				{sections && sections[activeTab] && (
					<LearningLayout
						key={activeTab}
						sections={sections[activeTab].child}
						NavList={NavList}
						ListItem={ListItem}
						api={BASE_URL}
					/>
				)}
			</Fragment>
		);
	}
}

export default LearningModule;
