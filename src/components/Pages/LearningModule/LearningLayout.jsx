import React, { Component, Fragment } from 'react';

import Lesson from './Lesson';

import styled from 'styled-components';
// import axios from 'axios';
import { abcItem } from '../../../utils/api/userActions';

// STYLED via styled-Blocks
const Section = styled.section`
	background: #ffffff;
	border: 1px solid #ececec;
	border-radius: 5px;
	margin: 10px 20px;
	
	overflow: hidden;
	padding-bottom: 100px;
`;
// const Homework = styled.h4`
//   color: #4a4a4a;
//   font-size: 18px;
//   line-height: 21px;
//   text-align: center;
//   margin-top: 24px;
// `;
// const HurryUp = styled.p`
//   color: #4a4a4a;
//   font-size: 14px;
//   line-height: 16px;
//   text-align: center;
//   margin-top: 14px;
// `;
const List = styled.ul`
	list-style: none;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	margin-top: 8px;
`;
const Item = styled.li`
	width: 250px;
	height: 250px;
	display: flex;
	flex-direction: column;
	font-style: normal;
	color: #000000;
	background: #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
	margin: 20px;
	transition: all 0.3s ease;
	overflow: hidden;

	span {
		position: absolute;
		font-weight: 500;
		font-size: 36px;
		line-height: 41px;
		top: 60px;
		left: 50%;
		transform: translateX(-50%);
		color: transparent;
		transition: color 0.2s ease-in;
	}
	img {
		filter: none;
		transition: filter 0.2s ease-in;
	}

	:hover {
		box-shadow: 0px 0px 35px rgba(0, 0, 0, 0.39);
		cursor: pointer;
		transform: scale(1.02);
	}
	:hover span {
		color: #ffffff;
	}
	:hover img {
		filter: brightness(0.5);
	}
`;
const Title = styled.h5`
	font-weight: 500;
	font-size: 18px;
	line-height: 20px;
	margin: 10px 20px 0;
`;
const Description = styled.p`
	font-weight: normal;
	font-size: 14px;
	line-height: 18px;
	text-align: justify;
	margin: 10px 20px 0;
`;

const ModuleMain = styled.article`
	padding: 24px 10%;
	p {
		margin: 0;
		text-align: justify;
	}
`;
const ModuleTitle = styled.h3`
	text-align: center;
	background: #71aaed;
	font-weight: 500;
	font-size: 18px;
	line-height: 55px;
	text-transform: uppercase;
	color: #ffffff;
	margin: 32px 0 0;
`;

class LearningLayout extends Component {
	state = {
		activeTab: 0,
		sections: this.props.sections,
		isModuleOpened: false,
	};

	setActiveTab = id => {
		const activeTab = id;
		this.setState({ activeTab, isModuleOpened: false });
	};

	openModule = async id => {
		const data = await abcItem(id);

		console.log(data);
		this.setState({ data, isModuleOpened: true });
	};

	// LIFECYCLE METHODS +++++++++++++++++++++++++++++++++++++++++++++++++++
	async componentDidMount() {}

	render() {
		const { activeTab, sections, isModuleOpened, data } = this.state;
		const { NavList, ListItem } = this.props;
		console.log(this.state);
		sections[activeTab] && sections[activeTab].item.reverse();

		return (
		<Fragment>
			<nav>
				<NavList>
					{sections &&
					sections.map((el, i) => (
						<ListItem
							onClick={() => this.setActiveTab(i)}
							className={activeTab === i && 'checked'}
						>
							{el.name}
						</ListItem>
					))}
				</NavList>
			</nav>
			{isModuleOpened ? (
				<Lesson
					data={data}
					Section={Section}
					ModuleTitle={ModuleTitle}
					ModuleMain={ModuleMain}
				/>
			) : (
				<Section>
					{/* <Homework>
              Вы все еще не выполнили <a href="#">Домашние Задания</a>??
            </Homework>
            <HurryUp>
              Поспешите! Помните, без них процесс выхода на Розетку не
              начинается!
            </HurryUp> */}
					{sections.length > 0 &&
					sections[activeTab] &&
					sections[activeTab].item && (
						<List>
							{sections[activeTab].item.map(el => (
								<Item onClick={() => this.openModule(el.id)}>
									<img src={el.image} alt="" height="150" />
									<span>СМОТРЕТЬ</span>
									<Title>{el.name}</Title>
									<Description>{el.description}</Description>
								</Item>
							))}
						</List>
					)}
				</Section>
			)}
		</Fragment>
		);
	}
}

export default LearningLayout;
