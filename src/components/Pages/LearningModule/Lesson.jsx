import React, {Fragment} from 'react';
// import PropTypes from 'prop-types';

const filename = str => {
	const arr = str ? str.split('/') : [];
	return decodeURI(arr[arr.length - 1]);
};

const Lesson = props => {
	const { data, Section, ModuleTitle, ModuleMain } = props;
	return (
		data && (
			<Section>
				<ModuleTitle>{data.name}</ModuleTitle>
				<ModuleMain>
					{data.video.length ? (
						<iframe
							id="ytplayer"
							title="ytb"
							type="text/html"
							width="100%"
							height="360"
							src={
								data.video[0].link +
								'?modestbranding=1;rel=0;controls=2;showinfo=0;iv_load_policy=3;'
							}
							frameBorder="0"
						/>
					) : null}
				</ModuleMain>
				<ModuleMain
					dangerouslySetInnerHTML={{ __html: data.description_HTML }}
				></ModuleMain>
				<ModuleMain>
					<h2>Прикреплённые файлы</h2>
					<p>
						<strong>PDF: </strong>
						{data.pdf.map((el, i) =>
							i ? (
								<Fragment>
									{', '}
									<a href={el.file} target="_blank" rel="noopener noreferrer">
										<b>{el.file && filename(el.file)}</b>
									</a>
								</Fragment>
							) : (
								<a href={el.file} target="_blank" rel="noopener noreferrer">
									<b>{el.file && filename(el.file)}</b>
								</a>
							),
						)}
					</p>
					<p>
						<strong>PINNED: </strong>
						{data.pinned.map((el, i) =>
							i ? (
								<Fragment>
									{', '}
									<a href={el.file} target="_blank" rel="noopener noreferrer">
										<b>{el.file && filename(el.file)}</b>
									</a>
								</Fragment>
							) : (
								<a href={el.file} target="_blank" rel="noopener noreferrer">
									<b>{el.file && filename(el.file)}</b>
								</a>
							),
						)}
					</p>
				</ModuleMain>
			</Section>
		)
	);
};

// Lesson.propTypes = {};

export default Lesson;
