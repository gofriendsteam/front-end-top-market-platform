import React, { useEffect } from 'react';
import { Title } from '../../Shared/Styles/title.styles';
import { Table, notification, Cascader } from 'antd';
import { sendContractorColumn } from '../../../constants/TableColumns/categoriesColumns';
import { sendToNewContractor } from '../../../utils/api/ordersAction';
import { MainLayout, TableWrapper } from '../../Shared/Styles/MainWrapper';
import Filters from '../../Blocks/Filters/Filters';
import { getAllProducts } from '../../../utils/api/productsActions';
import { Card } from '../../Shared/Styles/Card';
import { useFilters } from '../../Containers/useFilters';
import filters from '../../../constants/filtersOptions';
import { useTable } from '../../Containers/useTable';
import { useSelector } from 'react-redux';
import Total from '../../Shared/Total';
import OrderNotification from './OrderNotification';
import { useHistory } from 'react-router-dom';
import {toogleModal} from '../../../store/actions/modal';

const SendSupplier = () => {
	const order = useSelector(state => state.orders);

	// const categories = useSelector(state => state.modifiedCategories);
	const history = useHistory();
	const supplierFilters = {
		...filters.categories,
		category_id: order && order.items[0].categories,
		brand: order && order.items[0].brand
	};

	const [values, handleChangeInputs] = useFilters(supplierFilters);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		// resetRowKeys,
	] = useTable(getAllProducts, values);
	console.log(values);
	const handleSendToContractor = async productId => {
		const orderid = order.id;

		await sendToNewContractor(orderid, { productId });
		notification.success({
			message: 'Товар успешно отправлен!',
		});
	};
	useEffect(() => {
		if (!order) {
			history.replace('orders');
		}
	}, []);

	// const renamedAllCategoriessList = allCategoriesList.map(renaming);
	return (
		<MainLayout>
			<div className="top-nav">
				<Title>Отправить поставщику</Title>
			</div>

			<Card>
				<div className="actions">
					{order && <OrderNotification order={order} />}
					<Total count={count} />
				</div>
				<Filters
					values={values}
					onChange={handleChangeInputs}
					handleChangeFilters={handleChangeInputs}
				/>

				<TableWrapper>
					<Table
						{...config}
						// rowSelection={rowSelection}
						columns={sendContractorColumn(handleSendToContractor)}
						dataSource={products}
						scroll={{ x: 1100 }}
						loading={loading}
						onChange={handleChangeTable}
						size="small"
					/>
				</TableWrapper>
			</Card>
		</MainLayout>
	);
};

export default SendSupplier;
