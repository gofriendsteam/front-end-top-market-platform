import React from 'react';
import { Popover } from 'antd';
import TopbarDropdownWrapper from '../../Blocks/TopBar/TopbarNotification.styles';
import CustomButton from '../../Shared/Button/Button';

const OrderNotification = ({ order }) => {
	const [visible, setVisiblity] = React.useState(false);
	// const customizedTheme = useSelector(state => state.ThemeSwitcher.topbarTheme);

	function handleVisibleChange() {
		setVisiblity(visible => !visible);
	}

	const content = (
		<TopbarDropdownWrapper className="orderNotificaton">
			{/*<div>asdadasdas</div>*/}
			<div className="isoDropdownHeader">
				<h3>Мой Заказ</h3>
			</div>
			<div className="isoDropdownBody">
				{order.items.map(product => (
					<div className="body">
						<div className="name">{product.name}</div>
						<div className="description-block">
							<div>Количество:</div>
							<div>{product.quantity} шт.</div>
						</div>
						<div className="description-block">
							<div>Цена товара:</div>
							<div>{product.price}</div>
						</div>
					</div>
				))}
				{/*{demoNotifications.map(notification => (*/}
				{/*	<a className="isoDropdownListItem" key={notification.id} href="# ">*/}
				{/*		<h5>{notification.name}</h5>*/}
				{/*		<p>{notification.notification}</p>*/}
				{/*	</a>*/}
				{/*))}*/}
			</div>
			<div className="order-footer">
				<div>Сумма заказа:</div>
				<div>{order.amount}</div>
			</div>
		</TopbarDropdownWrapper>
	);
	return (
		<Popover
			content={content}
			trigger="click"
			visible={visible}
			onVisibleChange={handleVisibleChange}
			placement="bottomRight"
		>
			{/*<div className="isoIconWrapper">*/}
			<CustomButton title="Посмотреть заказ" type="primary" />
			{/*</div>*/}
		</Popover>
	);
};
export default OrderNotification;
