import React, { Component } from 'react';
import { Tabs } from 'antd';
import styles from "./CompanySettings.module.css"
import GeneralInformation from './components/GeneralInformation';
import Documents from './components/Documents';
import AboutCompany from './components/AboutCompany';
import CompanyPitch from './components/CompanyPitch';
import NovaPoshta from './NovaPoshta/NovaPoshta';

const TabPane = Tabs.TabPane;
const np = (
  <span>
    <img
      src="https://apimgmtstorelinmtekiynqw.blob.core.windows.net/content/MediaLibrary/Widget/img/np-logo.svg"
      alt=""
      width="30"
    />{' '}
    Новая Почта
  </span>
);
const callback = key => {
  console.log(key);
};

class CompanySettings extends Component {
  render() {
    return (
      <div className={styles.mainWrapper}>
        <Tabs onChange={callback} type="line" animated={false}>
        <TabPane tab="Основные данные" key="1">
          <GeneralInformation />
        </TabPane>

        <TabPane tab="Документы" key="2">
          <Documents />
        </TabPane>

        {/*<TabPane tab="Страница компании" key="3">*/}
        {/*  <AboutCompany />*/}
        {/*</TabPane>*/}

        <TabPane tab="Питч о компании" key="3">
          <CompanyPitch />
        </TabPane>

        <TabPane tab={np} key="4">
          <NovaPoshta />
        </TabPane>
      </Tabs>
      </div>
    );
  }
}

export default CompanySettings;
