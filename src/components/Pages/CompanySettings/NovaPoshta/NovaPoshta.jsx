import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import NovaPoshtaApiKey from './NovaPoshtaApiKey';
import NovaPoshtaAddressList from './NovaPoshtaAddressList';
import NovaPoshtaSendersList from './NovaPoshtaSendersList';

const NovaPoshta = ({ apiKey, CounterpartyRef }) => {
	console.log(apiKey);

	return (
		<div style={{ background: '#fff', padding: '20px' }}>
			<h2>Ключ NovaPoshta API:</h2>
			<NovaPoshtaApiKey />
			{apiKey && (
				<Fragment>
					<NovaPoshtaAddressList
						apiKey={apiKey}
						CounterpartyRef={CounterpartyRef}
					/>
					<NovaPoshtaSendersList
						apiKey={apiKey}
						CounterpartyRef={CounterpartyRef}
					/>
				</Fragment>
			)}
		</div>
	);
};

function mapStateToProps(state) {
	return {
		apiKey: state.user.novaPoshtaApiKey,
		CounterpartyRef: state.user.senderRef,
	};
}

export default connect(mapStateToProps)(NovaPoshta);
