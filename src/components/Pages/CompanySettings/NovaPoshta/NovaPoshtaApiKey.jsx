import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import {  Icon, Input, notification, Popconfirm, Tooltip } from 'antd';
import CustomButton from "../../../Shared/Button/Button"
import { updateProfile } from '../../../../utils/api/userActions';
import { getCounterparties } from '../../../../utils/api/requestNovaPoshta';

const NovaPoshtaApiKey = ({ user, updateProfile }) => {
  let novaPoshtaApiKey = null;

  function addApiKey() {
    getCounterparties(novaPoshtaApiKey)
      .then(({ data }) =>
        data.success
          ? data.data[0].Ref
          : data.errors.forEach((el) => notification.error({ message: el })),
      )
      .then(
        (senderRef) =>
          senderRef &&
          updateProfile({
            novaPoshtaApiKey,
            senderRef,
          }).then(() =>
            notification.success({
              message: 'Сохранено',
            }),
          ),
      );
  }

  function deleteApiKey() {
    updateProfile({
      novaPoshtaApiKey: null,
    }).then(() =>
      notification.success({
        message: 'Ключ успешно удалён',
      }),
    );
  }

  return (
    <p>
      {user.novaPoshtaApiKey ? (
        <Fragment>
          <i style={{ background: '#eeeeee', borderRadius: 4 }}>
            {user.novaPoshtaApiKey}
          </i>
          <Popconfirm
            title={
              <b>
                {/* Вместе с ключём также удалится все остальные данные относящиеся
                к Новой Почте!
                <br /> */}
                Уверены что хотите удалить?
              </b>
            }
            onConfirm={deleteApiKey}
            onCancel={null}
            okText='Да, удалить'
            cancelText='Нет, я передумал'>
            <Tooltip placement='right' title='Удалить ключ'>
              <Icon
                type='close-circle'
                style={{ color: 'red', marginLeft: 15 }}
              />
            </Tooltip>
          </Popconfirm>
        </Fragment>
      ) : (
        <Fragment>
          <Input
            placeholder='Введите Ваш ключ API Новой почты'
            onChange={(e) => {
              novaPoshtaApiKey = e.target.value;
            }}
            style={{ width: 300 }}
          />
          <CustomButton className='apikyBtn' type='primary' title=' Сохранить ключ' onClick={addApiKey} style={{ marginLeft: 15 }}/>

        </Fragment>
      )}
    </p>
  );
};

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateProfile: (user) => dispatch(updateProfile(user)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NovaPoshtaApiKey);
