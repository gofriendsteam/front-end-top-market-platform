import React from 'react';
import styles from './SubcategoryPages.module.css';
import CustomButton from '../../Shared/Button/Button';

const ControlsMenu = ({
	count = 0,
	selectedRowKeys = [],
	handleAddProduct,
}) => {
	return (
		<div className={styles.actions}>
			<CustomButton
				disabled={selectedRowKeys.length === 0}
				title="Добавить в мои товары"
				type="primary"
				// className={`btn ${styles.btnRestyleLeft}`}
				onClick={handleAddProduct}
			/>
		</div>
	);
};
export default ControlsMenu;
