import React, { useEffect, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
	getAllCategories,
	getFilteredProducts,
	copyProducts,
	addProductToMyStore,
	generateYml,
	addProductsToYml,
	deleteProductsFromYml,
} from '../../../utils/api/productsActions';
import { Breadcrumb, notification } from 'antd';
import styles from './SubcategoryPages.module.css';
import { Table  } from 'antd';
import NewProduct from '../../Blocks/EditCreateProduct/NewProduct';
import ControlsMenu from './ControlsMenu';
import Total from '../../Shared/Total';
// import ImportExportProducts from '../../Blocks/ImportExport/ImportExportProducts';
import {
	TableWrapper,
	MainLayout,
} from '../../Shared/Styles/MainWrapper';
import {
	columns_my,
	columns_all,
	columns_contractors,
} from '../../../constants/TableColumns/SubCategoriesTables';
import { Card } from '../../Shared/Styles/Card';
import CustomButton from '../../Shared/Button/Button';
import ImortYmlProducts from '../../Blocks/ImportExport/ImortYmlProducts';
import { toogleModal } from '../../../store/actions/modal';
import { useDispatch } from 'react-redux';

const SubcategoryPages = props => {
	const [filteredProducts, setFilteredProducts] = useState([]);
	const [categories, setCategories] = useState(null);
	const [pageSize, setPageSize] = useState(10);
	const [currentPage, setCurrentPage] = useState(1);
	const [count, setCount] = useState(null);
	const [selectedRowKeys, setSelectedRowKeys] = useState([]);
	const [loading, setLoading] = useState(true);
	const [product, setOpenedProduct] = useState({});
	const [magazineUrls, setUrls] = useState({ rozetkaUrl: '', promUrl: '' });
	const currentIds = props.location.pathname.match(/[^/](\d){1,}[^/]/gi);
	const currentId = props.history.location.pathname.substring(
		props.history.location.pathname.lastIndexOf('/') + 1,
	);
	const dispatch = useDispatch();
	const currentTab = props.location.pathname.replace(/\/\d+/gim, '');

	const type = () => {
		switch (currentTab) {
			case '/admin/categories':
				return 'all';
			case '/admin/my_products':
				return 'my';
			default:
				return 'contractor';
		}
	};

	const queryURL =
		type() === 'all' || type() === 'my'
			? `${currentId}/partners/${type()}?page=1&page_size=${pageSize}`
			: `${currentId}/contractors?page=${currentPage}&page_size=${pageSize}`;

	const getProducts = () => {
		currentPage !== 1 && setCurrentPage(1);
		setLoading(true);
		currentIds &&
			getFilteredProducts(queryURL).then(data => {
				setFilteredProducts(data.results);
				setCount(data.count);
			});
		filteredProducts && setLoading(false);
	};
	useEffect(() => {
		getProducts();
	}, [currentId]);

	const handleUpdateProduct = () => {
		setSelectedRowKeys([]);
		setOpenedProduct({});
		getProducts();
	};

	const handleGenerateYml = async () => {
		const { results } = await generateYml();
		const rozetkaUrl = results.find(el => el.ymlType === 'rozetka')
			? results.find(el => el.ymlType === 'rozetka').template
			: null;
		const promUrl = results.find(el => el.ymlType === 'prom')
			? results.find(el => el.ymlType === 'prom').template
			: null;
		setUrls({ rozetkaUrl, promUrl });

		getProducts();
	};

	const handleAddToYml = async () => {
		const productIds = selectedRowKeys.map(item => filteredProducts[item].id);

		setSelectedRowKeys([]);

		await addProductsToYml(
			{
				ymlType: 'rozetka',
				productIds,
			},
			'rozetka',
		);

		await addProductsToYml(
			{
				ymlType: 'prom',
				productIds,
			},
			'prom',
		);

		getProducts();
	};
	const handleDeleteFromYml = async () => {
		const productIds = selectedRowKeys.map(item => filteredProducts[item].id);

		setSelectedRowKeys([]);

		await deleteProductsFromYml(
			{
				ymlType: 'rozetka',
				productIds,
			},
			'rozetka',
		);

		await deleteProductsFromYml(
			{
				ymlType: 'prom',
				productIds,
			},
			'prom',
		);

		getProducts();
	};
	const sendToMyStore = async () => {
		let sendingIds = [];
		this.state.products.forEach((product, idx) => {
			if (selectedRowKeys.includes(idx)) {
				sendingIds.push(filteredProducts.id);
			}
		});
		const res = await addProductToMyStore({ productListIds: sendingIds });
		notification.success({
			message: 'Товар добавлен в магазин',
		});
	};
	const handleChangeTable = async pagination => {
		const res = await getFilteredProducts(
			type() === 'all' || type() === 'my'
				? `${currentId}/partners/${type()}?page=${
						pagination.current
				  }&page_size=${pagination.pageSize}`
				: `${currentId}/contractors?page=${pagination.current}&page_size=${pagination.pageSize}`,
		);
		setFilteredProducts(res.results);
		setCurrentPage(pagination.current);
		setPageSize(pagination.pageSize);
	};
	const handleAddProduct = async () => {
		let arr = [];
		selectedRowKeys.forEach(item => {
			arr.push(filteredProducts[item].id);
		});
		await copyProducts({
			productListIds: arr,
		});
		getProducts();
		setSelectedRowKeys([]);
	};

	const openProduct = product => {
		setOpenedProduct(product);
	};
	const currentCatalog = props.location.pathname
		.match(/(\D+)(?![^/])/gi)
		.join('');

	useEffect(() => {
		getAllCategories().then(data => setCategories(data));
	}, []);

	const breadCategories = [];
	const goToCategoryMap = (arr, categories) => {
		if (arr.length === 0) {
			return;
		}
		if (arr.length) {
			const bread = categories.find(
				category => category.id.toString() === arr[0] && category,
			);
			categories && breadCategories.push(bread);
		}
		const subcategory =
			categories &&
			categories.filter(
				category => category.id.toString() === arr[0] && category,
			);
		arr.shift();
		return categories && goToCategoryMap(arr, subcategory[0].subcategories);
	};

	currentIds && categories && goToCategoryMap(currentIds, categories);

	let breadLink = currentCatalog;

	// ТАБЛИЦА

	const config = {
		pagination: {
			pageSize: pageSize,
			pageSizeOptions: ['10', '20', '50', '500'],
			showSizeChanger: true,
			total: count,
			current: currentPage,
		},
	};

	const onSelectChange = selectedRowKeys => {
		console.log(selectedRowKeys);
		setSelectedRowKeys(selectedRowKeys);
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		// onSelection: this.onSelection,
		// onSelection
	};

	// КОНЕЦ ТАБЛИЦЫ
	return (
		currentIds && (
			<MainLayout>

					<Breadcrumb separator=">" className='breadcrumb'>
						<Breadcrumb.Item>
							<Link
								to={currentCatalog}
								className='breadcrumb-link'
							>
								{currentCatalog === '/admin/categories'
									? 'Все товары'
									: 'Мои товары'}
							</Link>
						</Breadcrumb.Item>
						{breadCategories.map(bread => {
							breadLink += `/${bread.id}`;
							return (
								<Breadcrumb.Item>
									<Link
										to={breadLink}
										className='breadcrumb-link'
									>
										{bread.name}
									</Link>
								</Breadcrumb.Item>
							);
						})}
					</Breadcrumb>


				<div className="subs-subcategories-list">
					{!!breadCategories.length &&
						breadCategories[breadCategories.length - 1].subcategories.map(
							subcategory => (
								<Link
									to={`${props.location.pathname}/${subcategory.id}`}
									className="subs-links"
								>
									{subcategory.name}
								</Link>
							),
						)}
				</div>

				{/* ТАБЛИЦА */}
				{filteredProducts.length ? (
					<Card>
						{!!count && (
							<div className="sub-categories-btn-nav">
								{type() === 'all' ? (
									<ControlsMenu
										selectedRowKeys={selectedRowKeys}
										handleAddProduct={handleAddProduct}
									/>
								) : type() === 'my' ? (
									<Fragment>
										<CustomButton
											title="Добавить в Экспорт"
											type="primary"
											onClick={handleAddToYml}
											disabled={!selectedRowKeys.length}
										/>
										<CustomButton
											title="Удалить из Экспорта"
											type="danger"
											onClick={handleDeleteFromYml}
											disabled={!selectedRowKeys.length}
										/>
										<CustomButton
											type="primary"
											// disabled={!selectedRowKeys.length}
											title="Импортировать Yml"
											onClick={() => {
												dispatch(
													toogleModal({
														isOpen: true,
														type: 'import',
														component: ImortYmlProducts,
														data: {
															title: 'Импорт Yml файлов',
															width: 800,
															updateProducts: handleUpdateProduct,
														},
													}),
												);
											}}
										/>
									</Fragment>
								) : null}
								<NewProduct
									onUpdate={handleUpdateProduct}
									onUpdateProduct={handleUpdateProduct}
									product={product}
									update={product.id ? true : false}
								/>
								<Total count={count}/>
							</div>
						)}
						{/* : type() === 'all' ? <ImportExportProducts /> : null */}

						{/* rozeetkUrl={rozetkaUrl} promUrl={promUrl} products={products} selectedRowKeys={selectedRowKeys} sendToMyStore={this.sendToMyStore} history={this.props.history}handleGenerateYml={this.handleGenerateYml} handleUpdate={this.handleUpdate} */}
						<TableWrapper>
							<Table
								{...config}
								loading={loading}
								rowSelection={rowSelection}
								columns={
									type() === 'my'
										? columns_my(openProduct)
										: type() === 'all'
										? columns_all
										: type() === 'contractor'
										? columns_contractors(openProduct)
										: null
								}
								scroll={{ x: 900 }}
								className={styles.subcategoryTable}
								dataSource={filteredProducts}
								onChange={handleChangeTable}
								size="small"
							/>
						</TableWrapper>

					</Card>
				) : (
					<p className={styles.emptyTable}>Нет товаров</p>
				)}
			</MainLayout>
		)
	);
};

export default SubcategoryPages;
