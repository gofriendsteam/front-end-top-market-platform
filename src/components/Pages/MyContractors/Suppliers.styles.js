import styled from 'styled-components';

export const SuppliersWrapper = styled.div`
	padding: 24px 0;
	.table-error-value {
		color: #ffabab;
	}
	.ant-table-small > .ant-table-content > .ant-table-body {
	margin: 0;
	}
`;
