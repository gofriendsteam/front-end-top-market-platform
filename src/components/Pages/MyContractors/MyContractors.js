import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Title } from '../../Shared/Styles/title.styles';
import { SuppliersWrapper } from './Suppliers.styles';
import { MyContractorsColumn } from '../../../constants/TableColumns/TabColumns';
import { Card } from '../../Shared/Styles/Card';
import { TableWrapper } from '../../Shared/Styles/MainWrapper';
import { Table } from 'antd';
import { toogleChat } from '../../../store/actions/chat';
import { getMyContractors } from '../../../utils/api/MyContractors';
import { createChat } from '../../../utils/api/userActions';

const MyContractors = () => {
	const [contractors, setContractos] = useState([]);
	const [pageSize, setPageSize] = useState(10);
	const [count, setCount] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	const dispatch = useDispatch();
	const fetchContractors = async () => {
		try {
			const res = await getMyContractors();
			setContractos(res);
		} catch (e) {
			console.log('error', e);
		}
	};
	useEffect(() => {
		fetchContractors();
	}, []);
	const openChat = async (chatId, contractorId) => {
		let mychatId = chatId;
		if (!chatId) {
			const {id} = await createChat({ contractor: contractorId });
			mychatId = id;
		}
		dispatch(
			toogleChat({
				chatId: mychatId,
			}),
		);
	};
	const config = {
		pagination: {
			pageSize,
			pageSizeOptions: [10, 20, 50, 200],
			showSizeChanger: true,
			total: count,
			current: currentPage,
		},
	};
	console.log('', contractors);
	return (
		<SuppliersWrapper>
			<Title>Мои поставщики</Title>
			<Card>
				<TableWrapper>
					<Table
						{...config}
						// rowSelection={rowSelection}
						columns={MyContractorsColumn(openChat)}
						bordered
						dataSource={contractors}
						// scroll={{ x: 1370 }}
						// loading={loading}
						// onChange={handleChangeTable}
						size="small"
					/>
				</TableWrapper>
			</Card>
		</SuppliersWrapper>
	);
};

export default MyContractors;
