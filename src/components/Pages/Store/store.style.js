import styled from 'styled-components';
// import { palette } from 'styled-theme';

export const StoreWrapper = styled.div`
	padding: 24px 0;
	.shop-link {
		color: rgb(120, 129, 149);
		margin-bottom: 15px;
		padding-left: 20px;
		span {
			cursor: pointer;
			font-weight: 500;
			font-size: 16px;
			text-decoration: underline;
		}
	}
	.settings ~ button {
		margin: 0 auto;
	}
	.store-settings-blocks,
	.about,
	.create-store {
		margin-bottom: 15px;
		label {
			font-weight: 500;
			color: #323232;
			display: inline-block;
			margin-bottom: 5px;
		}
	}
	.policy {
		h2 {
			font-size: 18px;
			font-weight: 500;
			color: #323232;
			text-decoration: underline;
			margin-bottom: 15px;
		}
	}
	.about-image label {
		font-size: 18px;
		font-weight: 500;
		color: #323232;
		margin-bottom: 10px;
	}
	.about-block {
		margin-bottom: 15px;
		.about-image {
		}
	}
	.btn-container {
		margin-top: 20px;
		//text-align: center;
	}
	.add-number-btn {
		margin-top: 15px;
		text-align: right;
		button {
		}
	}
	.store-edit-btn {
		width: 300px;
	}
	.create-store {
		width: 50%;
		.inputs-wrapper {
			display: flex;
			align-items: center;
			margin-bottom: 20px;
		}
		.domain-name {
			margin-left: 10px;
			font-style: italic;
			font-weight: 500;
			font-size: 16px;
		}
		button {
			width: 200px;
		}
		@media only screen and (max-width: 768px) {
			width: 100%;
		}
	}
	.logo-controls {
		.logo-description {
			color: #979797;
			font-size: 13px;
			margin-bottom: 10px;
		}
		.bot-wr {
			display: flex;
			align-items: center;
			justify-content: space-between;
		}
	}
	.logo-image {
		height: 300px;
		display: flex;
		overflow: hidden;
		.img {
			height: 300px;
			object-fit: contain;
		}

		align-items: center;
		justify-content: center;
		background: #f1f0f0;
		margin-bottom: 10px;
		i {
			font-size: 100px;
			color: #adacac;
		}
	}
	h4 {
		font-size: 18px;
		margin-bottom: 20px;
		text-decoration: underline;
	}
`;
