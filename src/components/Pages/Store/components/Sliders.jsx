import React from 'react';
import styles from '../Store.module.css';
import Dropzone from 'react-dropzone';
import { Carousel, Input, Row, Col, Icon } from 'antd';
import CustomButton from '../../../Shared/Button/Button';

const Sliders = ({
	slidervalues,
	urlslidervalues,
	handleSave,
	urlState,
	handleChangeInput,
	uploadImagebyUrl,
	handleUploadSliderImages,
	handleChangeUrl,
	deleteImage,

}) => {
	const slides = (values, type) =>
		values.length > 0 ? (
			values.map((img, idx) => (
				<div className={styles.imageWr} key={idx}>
					<Icon type="close-circle" onClick={() => deleteImage(idx, type)} className={styles.deleteImage}/>

					<img src={img.imageDecoded || img.url} alt="photo" />
				</div>
			))
		) : (
			<div className={styles.imageWr}>
				<img
					width={'200'}
					height="200"
					src="https://www.samsung.com/etc/designs/smg/global/imgs/support/cont/NO_IMG_600x600.png"
				/>
			</div>
		);
	return (
		<Row gutter={60}>
			<Col span={24} xl={12}>
				<h3 className={styles.slidertitle}>Загрузить картинку с компьютера</h3>
				<div className={styles.imageList}>
					<Carousel>{slides(slidervalues, 'sliderImages')}</Carousel>
				</div>

				<Dropzone
					onDrop={handleUploadSliderImages}
					accept=".png, .svg, .jpg, .jpeg"
				>
					{({ getRootProps, getInputProps }) => (
						<div {...getRootProps({ className: styles.dropzone })}>
							<input {...getInputProps()} />
							<CustomButton
								type="primary"
								title="Загрузить"
								className={styles.download}
							/>
						</div>
					)}
				</Dropzone>
			</Col>
			<Col span={24} xl={12}>
				<h3 className={styles.slidertitle}>Загрузить картинку через URL:</h3>
				<div className={styles.imageList}>
					<div className={styles.imageList}>
						<Carousel>{slides(urlslidervalues, 'sliderImageUrls')}</Carousel>
					</div>
				</div>
				<div className={styles.byurlsliderupload}>
					<Input
						type="text"
						value={urlState}
						onChange={handleChangeUrl('urlValue')}
						placeholder="Введите URL картиники"
					/>
					<CustomButton
						type="primary"
						onClick={uploadImagebyUrl}
						title="Загрузить через URL"
						className={styles.download}
					/>
				</div>
			</Col>

			<Col span={24}>
				<CustomButton
					type="primary"
					onClick={handleSave}
					title="Редактировать"
				/>
			</Col>
		</Row>
	);
};
export default Sliders;
