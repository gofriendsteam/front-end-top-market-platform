import React from 'react';
import styles from '../Store.module.css';
import Dropzone from 'react-dropzone';
import { Icon, Input, Row, Col } from 'antd';
import CustomButton from '../../../Shared/Button/Button';

const { TextArea } = Input;

const About = ({ handleSave, values, onDrop, handleChangeInput }) => {
	return (
		<Row gutter={60}>
			<Col span={24} md={12} className="about">
				<div className="about-block">
					<label>Заголовок</label>
					<Input
						type="text"
						value={values ? values.title : ''}
						onChange={handleChangeInput('title', 'aboutUs')}
					/>
				</div>

				<div className="about-block">
					<label>Текст</label>
					<TextArea
						autoSize={{ minRows: 3, maxRows: 10 }}
						value={values ? values.text : ''}
						onChange={handleChangeInput('text', 'aboutUs')}
					/>
				</div>

				<div className="about-block">
					<label>Описание</label>
					<TextArea
						autoSize={{ minRows: 3, maxRows: 10 }}
						value={values ? values.description : ''}
						onChange={handleChangeInput('description', 'aboutUs')}
					/>
				</div>
			</Col>
			<Col span={24} md={12}>
				<div className="about-image">
					<label>Картинка</label>
					<div className="logo-image">
						{!values || !values.image ? (
							<Icon type="camera" />
						) : (
							<img className='img' src={values ? values.image : ''} alt="" />
						)}
					</div>
				</div>

				<Dropzone onDrop={onDrop} accept=".png, .svg, .jpg, .jpeg">
					{({ getRootProps, getInputProps }) => (
						<div {...getRootProps({ className: styles.dropzone })}>
							<input {...getInputProps()} />
							<CustomButton
								type="primary"
								title="Загрузить"
								className={`${styles.download} ${styles.magImg}`}
							/>
						</div>
					)}
				</Dropzone>
			</Col>
			<Col span={24} className="btn-container">
				<CustomButton
					type="primary"
					className="store-edit-btn"
					onClick={handleSave}
					title="Редактировать"
				/>
			</Col>
		</Row>
	);
};

export default About;
