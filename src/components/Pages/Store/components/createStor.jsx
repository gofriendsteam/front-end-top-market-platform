import React from 'react';
// import styles from '../Store.module.css';
import { Popconfirm, Input } from 'antd';
import slugify from 'slugify';
import CustomButton from '../../../Shared/Button/Button';

const domain = '.topmarket.ua';

const CreateStore = ({ domainName, handleChangeDomainName, createStore }) => {
	return (
		<div className="create-store">
			<div >
				<label>Адрес поддомена</label>
				<div className='inputs-wrapper'>
					<Input
						type="text"
						value={domainName}
						onChange={handleChangeDomainName}
					/>

					<div className='domain-name'>{domain}</div>
				</div>
			</div>
			<Popconfirm
				title={`Вы уверены, что хотите создать поддомен ${slugify(
					domainName,
					'-',
				)}${domain}?`}
				onConfirm={createStore}
				onCancel={() => {
					console.log('No');
				}}
				disabled={domainName.trim().length === 0}
			>
				<CustomButton
					type="primary"
					title="Создать"
					disabled={domainName.trim().length === 0}
				/>
			</Popconfirm>
		</div>
	);
};

export default CreateStore;
