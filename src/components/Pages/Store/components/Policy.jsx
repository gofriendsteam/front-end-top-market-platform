import React from 'react';
import styles from '../Store.module.css';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Collapse } from 'antd';
import CustomButton from '../../../Shared/Button/Button';

const { Panel } = Collapse;

const Policy = ({ values, handleSave, handleChangeInput }) => {
	return (
		<div className="policy">
			<h2>Информация для покупателя</h2>
			<Collapse accordion>
				<Panel header="Оферта" key="1">
					<CKEditor
						editor={ClassicEditor}
						data={values.siteOffer ? values.siteOffer.text : ''}
						config={{
							removePlugins: [
								'Image',
								'ImageCaption',
								'ImageStyle',
								'ImageToolbar',
								'ImageUpload',
							],
						}}
						onChange={(event, editor) =>
							handleChangeInput(editor.getData(), 'siteOffer')
						}
					/>
				</Panel>
				<Panel header="Обмен и возврат" key="2">
					<CKEditor
						editor={ClassicEditor}
						data={
							values.exchangeAndReturn && values.exchangeAndReturn
								? values.exchangeAndReturn.text
								: ''
						}
						config={{
							removePlugins: [
								'Image',
								'ImageCaption',
								'ImageStyle',
								'ImageToolbar',
								'ImageUpload',
							],
						}}
						onChange={(event, editor) =>
							handleChangeInput(editor.getData(), 'exchangeAndReturn')
						}
					/>
				</Panel>
				<Panel header="Оплата и доставка" key="3">
					<CKEditor
						editor={ClassicEditor}
						data={
							values.deliveryAndPayments && values.deliveryAndPayments.text
								? values.deliveryAndPayments.text
								: ''
						}
						config={{
							removePlugins: [
								'Image',
								'ImageCaption',
								'ImageStyle',
								'ImageToolbar',
								'ImageUpload',
							],
						}}
						onChange={(event, editor) =>
							handleChangeInput(editor.getData(), 'deliveryAndPayments')
						}
					/>
				</Panel>
				<Panel header="Для пользователей" key="4">
					<CKEditor
						editor={ClassicEditor}
						data={
							values.howToUses && values.howToUses.text
								? values.howToUses.text
								: ''
						}
						config={{
							removePlugins: [
								'Image',
								'ImageCaption',
								'ImageStyle',
								'ImageToolbar',
								'ImageUpload',
							],
						}}
						onChange={(event, editor) =>
							handleChangeInput(editor.getData(), 'howToUses')
						}
					/>
				</Panel>
			</Collapse>
			<CustomButton
				type="primary"
				onClick={handleSave}
				title="Редактировать"
				className={`${styles.create}`}
			/>
		</div>
	);
};

export default Policy;
