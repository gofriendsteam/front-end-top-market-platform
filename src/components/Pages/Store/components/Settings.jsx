import React from 'react';
// import styles from '../Store.module.css';
import PhoneNumbers from './phoneNumbers';
// import nike from '../../../../img/nike.jpg';
import Dropzone from 'react-dropzone';
import CustomButton from '../../../Shared/Button/Button';
import { Icon, Row, Col, Input } from 'antd';

const Settings = ({
	handleSave,
	values: {
		google,
		logoDecoded,
		facebook,
		instagram,
		linkedin,
		footerPhonesNumber,
		shopName,
		headerPhonesNumber,
	},
	onDeletePhone,
	add,
	handleChangeInput,
	onDrop,
}) => {
	return (
		<Row gutter={60} className="settings">
			<Col span={24} md={12}>
				<div className="store-settings-blocks">
					<label>Имя магазина</label>
					<Input
						type="text"
						value={shopName}
						// disabled={shopName}
						onChange={handleChangeInput('shopName')}
					/>{' '}
				</div>

				<h4>Информация в «Хэдере»</h4>
				<div className="store-settings-blocks">
					<PhoneNumbers
						data={headerPhonesNumber}
						handleChange={handleChangeInput}
						onDelete={onDeletePhone}
						type="headerPhonesNumber"
						add={add}
					/>
				</div>
				<h4>Информация в «Футере»</h4>

				<div className="store-settings-blocks">
					<PhoneNumbers
						data={footerPhonesNumber}
						handleChange={handleChangeInput}
						onDelete={onDeletePhone}
						type="footerPhonesNumber"
						add={add}
					/>
				</div>
				<h4>Социальные сети</h4>
				<div className="store-settings-blocks">
					<label>Google</label>
					<Input
						type="text"
						value={google}
						onChange={handleChangeInput('google')}
					/>
				</div>
				<div className="store-settings-blocks">
					<label>Facebook</label>
					<Input
						type="text"
						value={facebook}
						onChange={handleChangeInput('facebook')}
					/>
				</div>
				<div className="store-settings-blocks">
					<label>Instagram</label>
					<Input
						type="text"
						value={instagram}
						onChange={handleChangeInput('instagram')}
					/>
				</div>
				<div className="store-settings-blocks">
					<label>Linkedin</label>
					<Input
						type="text"
						value={linkedin}
						onChange={handleChangeInput('linkedin')}
					/>
				</div>
			</Col>
			<Col span={24} md={12}>
				<div className="logo-image">
					{!logoDecoded ? (
						<Icon type="camera" />
					) : (
						<img src={logoDecoded} className='img' alt="defaultImage" />
					)}
				</div>
				<div className="logo-controls">
					<p className="logo-description">
						Логотип должен быть в формате: JPEG,SVG,PNG
					</p>
					<div className="bot-wr">
						<h4>Логотип</h4>
						<Dropzone onDrop={onDrop} accept=".png, .svg, .jpg, .jpeg">
							{({ getRootProps, getInputProps }) => (
								<div {...getRootProps({ className: 'dropzone' })}>
									<input {...getInputProps()} />
									<CustomButton type="primary" title="Загрузить логотип" />
								</div>
							)}
						</Dropzone>
					</div>
				</div>
			</Col>
			<Col span={24} className='btn-container'>
				<CustomButton
					className='store-edit-btn'
					type="primary"
					onClick={handleSave}
					title="Редактировать"
				/>
			</Col>
		</Row>
	);
};

export default Settings;
