import React from 'react';
import styles from '../Store.module.css';
import { Icon, Input } from 'antd';
import CustomButton from '../../../Shared/Button/Button';

const PhoneNumbers = ({ data, handleChange, onDelete, type, add }) => {
	return (
		<div className={styles.todoContainer}>
			{data.map((numbers, idx) => (
				<div>
					<label>Номер телефона {idx + 1} (до 6 включительно)</label>
					<Input
						type="tel"
						placeholder="+38 (096) 933 - 45 - 43"
						value={data[idx]}
						onChange={handleChange(type, null, idx)}
					/>
					{data[idx].trim().length !== 0 && (
						<Icon
							className={styles.cancelBtn}
							type="close-circle"
							onClick={() => onDelete(idx, type)}
						/>
					)}
				</div>
			))}
			{data.length !== 0 && data[0] && (
				<div className="add-number-btn">
					<CustomButton
						title="Добавить номер"
						type="outlined"
						onClick={() => add(type)}
					/>
				</div>
			)}
		</div>
	);
};
export default PhoneNumbers;
