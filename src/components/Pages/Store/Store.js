import React, { Component } from 'react';
// import 'antd/dist/antd.css';
import { getBase64 } from '../../../helpers/functions';
import {
	updateStore,
	createMyStore,
	getMyStore,
} from '../../../utils/api/storeActions';
import About from './components/about';
import CreateStore from './components/createStor';
import { Tabs, notification } from 'antd';
import slugify from 'slugify';
import Settings from './components/Settings';
import Policy from './components/Policy';
import Sliders from './components/Sliders';
import { Title } from '../../Shared/Styles/title.styles';
import { Card } from '../../Shared/Styles/Card';
import { StoreWrapper } from './store.style';
const { TabPane } = Tabs;

class Store extends Component {
	state = {
		currentTab: '1',
		exist: false,
		headerPhonesNumber: [],
		footerPhonesNumber: [],
		domainName: '',
		shopName: '',
		instagram: '',
		facebook: '',
		linkedin: '',
		google: '',
		urlValue: '',
		domainSubdomain: 'SDM',
		updateImage: false,
		exchangeAndReturn: [],
		contactsText: null,
		sliderImages: [],
		sliderImageUrls: [],
		deliveryAndPayments: {},
		howToUses: {},
		aboutUs: null,
	};

	async componentDidMount() {
		const res = await getMyStore();
		if (!res.exist) {
			this.setState({
				...this.state,
				exist: false,
			});
		} else {
			this.setState({
				...this.state,
				...res,
				headerPhonesNumber:
					res.headerPhonesNumber.length === 0 ? [''] : res.headerPhonesNumber,
				footerPhonesNumber:
					res.footerPhonesNumber.length === 0 ? [''] : res.footerPhonesNumber,
				currentTab: '2',
			});
		}
	}
	handleChangeInput = (name, outerObj, idx) => ({ target: { value } }) => {
		if (name === 'headerPhonesNumber' || name === 'footerPhonesNumber') {
			const newArray = [...this.state[name]];

			newArray.splice(idx, 1, value);

			this.setState({
				[name]: newArray,
			});
		} else if (outerObj) {
			this.setState({
				[outerObj]: {
					...this.state[outerObj],
					[name]: value,
				},
			});
		} else {
			this.setState({
				[name]: value,
			});
		}
	};
	CkEditorInputChange = (data, type) => {
		this.setState({ [type]: { text: data } });
	};
	onDeletePhone = (idx, type) => {
		if (this.state[type].length === 1) {
			this.setState({ [type]: [''] });
		} else {
			this.state[type].splice(idx, 1);
			this.setState({ [type]: this.state[type] });
		}
	};
	createStore = async () => {
		const { domainName } = this.state;
		if (!domainName.trim()) return;
		const slugedName = slugify(domainName, '-');

		const res1 = await createMyStore({
			domainName: slugedName,
			domainSubdomain: 'SDM',
		});
		if (res1.exists) {
			notification.error({
				message: 'Извините, этот домен уже занят',
			});
			return;
		} else {
			const res2 = await getMyStore();

			this.setState({
				...this.state,
				currentTab: '2',
				...res2,
				exist: true,
			});
		}
	};

	onDrop = file => {
		getBase64(file[0], result => {
			this.setState({
				logoDecoded: result,
				updateImage: true,
			});
		});
	};
	onDrop2 = file => {
		getBase64(file[0], result => {
			this.setState(
				{
					aboutUs: this.state.aboutUs
						? {
								...this.state.aboutUs,
								image: result,
						  }
						: { image: result },
				},
				() => console.log(this.state),
			);
		});
	};

	handleSave = async e => {
		e.preventDefault();

		let newStore = { ...this.state, domainSubdomain: 'SDM' };
		let result = {};

		Object.keys(newStore).forEach(el => {
			if (newStore[el] !== null) {
				result[el] = newStore[el];
			}
		});

		await updateStore(result);

		notification.success({
			message: 'Данные обновлены!',
		});
	};
	uploadImgByUrl = () => {
		this.setState(prev => {
			const newArr = prev.sliderImageUrls;
			newArr.push({ url: this.state.urlValue });
			return {
				sliderImageUrls: newArr,
			};
		});
	};
	handleChangeDomainName = ({ target: { value } }) => {
		this.setState({
			domainName: value,
		});
	};
	add = type => {
		const newArr = this.state[type];
		if (newArr.length >= 6) return;
		const canAdd = newArr.find(el => el.trim().length === 0);

		if (canAdd === '') return;
		newArr.push('');
		this.setState({
			[type]: newArr,
		});
	};
	uploadSliderImages = files => {
		files.forEach(file => {
			getBase64(file, result => {
				this.setState(prev => {
					prev.sliderImages.push({ imageDecoded: result });
					return {
						sliderImages: prev.sliderImages,
					};
				});
			});
		});
	};
	onDeleteUploadedSliderImages = (idx, type) => {
		this.setState({
			[type]: this.state[type].filter((el, index) => index !== idx),
		});
	};
	gotoMagazine = () => {
		window.open(`http://${this.state.domainName}.topmarket.ua`, '_blank');
	};

	render() {
		const { exist, domainName, shopName } = this.state;

		return (
			<StoreWrapper>
				<Title>Управление интернет магазином {`- ${shopName || ''}`}</Title>
				{domainName && (
					<div className="shop-link">
						Ссылка на ваш магазин -{' '}
						<span onClick={this.gotoMagazine}>{domainName}.topmarket.ua</span>
					</div>
				)}

				<form>
					<Tabs
						type="line"
						animated={false}
						activeKey={this.state.currentTab}
						onChange={key => this.setState({ currentTab: key })}
					>
						<TabPane tab="Создание магазина" key="1" disabled={exist}>
							<Card>
								<CreateStore
									domainName={domainName}
									createStore={this.createStore}
									handleChangeDomainName={this.handleChangeDomainName}
								/>
							</Card>
						</TabPane>
						<TabPane tab="Настройки магазина" key="2" disabled={!exist}>
							<Card>
								<Settings
									values={{ ...this.state }}
									onDrop={this.onDrop}
									handleChangeInput={this.handleChangeInput}
									add={this.add}
									handleSave={this.handleSave}
									onDeletePhone={this.onDeletePhone}
								/>
							</Card>
						</TabPane>
						<TabPane tab="Информация о магазине" key="3" disabled={!exist}>
							<Card>
								<About
									handleSave={this.handleSave}
									handleChangeInput={this.handleChangeInput}
									values={this.state.aboutUs}
									onDrop={this.onDrop2}
								/>
							</Card>
						</TabPane>

						<TabPane tab="Дополнительно" key="4" disabled={!exist}>
							<Card>
								<Policy
									handleSave={this.handleSave}
									values={this.state}
									handleChangeInput={this.CkEditorInputChange}
								/>
							</Card>
						</TabPane>
						<TabPane tab="Слайдеры" key="5" disabled={!exist}>
							<Card>
								<Sliders
									handleSave={this.handleSave}
									slidervalues={this.state.sliderImages}
									urlslidervalues={this.state.sliderImageUrls}
									urlState={this.state.urlValue}
									handleUploadSliderImages={this.uploadSliderImages}
									handleChangeInput={this.CkEditorInputChange}
									handleChangeUrl={this.handleChangeInput}
									uploadImagebyUrl={this.uploadImgByUrl}
									deleteImage={this.onDeleteUploadedSliderImages}
								/>
							</Card>
						</TabPane>
					</Tabs>
				</form>
			</StoreWrapper>
		);
	}
}

export default Store;
