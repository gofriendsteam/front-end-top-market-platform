import React, { Fragment, useState } from 'react';
import styles from '../Orders.module.css';
import { Button, Col, Icon, Input, Popconfirm, Row, Tooltip } from 'antd';
import CreateTTN from './CreateTTN';
import OrderCard from '../../../Blocks/Orders/OrderCard';
import { OrderProductCard } from '../../Orders/components/OrderProductCard.styles';
import CustomButton from '../../../Shared/Button/Button';

const ContractorOrderProduct = ({ record, saveTTN, getOrders1, deleteTTN }) => {
	console.log('record', record);
	const [ttnvalue, setTTNvalue] = useState('');

	const handleInputs = ({ target: { value } }) => {
		setTTNvalue(value);
	};
	return (
		<OrderProductCard>
			<Row gutter={40}>
				<Col span={24} md={16} className="order-card-wrapper">
					<OrderCard record={record.baseOrder} />
				</Col>

				<Col span={24} md={8}>
					<div>
						{record.baseOrder.ttn && (
							<div>ТТН (из заказа на Розетке): {record.baseOrder.ttn}</div>
						)}
						{record.ttn ? (
							<Fragment>
								<span>ТТН (сформированный на платформе): {record.ttn}</span>
								<Popconfirm
									title={<b>Уверены что хотите удалить?</b>}
									onConfirm={() => deleteTTN(record.ttn)}
									onCancel={null}
									okText="Да, удалить"
									cancelText="Нет, я передумал"
								>
									<Tooltip placement="right" title="Удалить ТТН">
										<Icon
											type="close-circle"
											style={{ color: 'red', marginLeft: 15 }}
										/>
									</Tooltip>
								</Popconfirm>
							</Fragment>
						) : (
							<Fragment>
								<label htmlFor="">Введите ТТН: </label>
								<Input
									style={{ marginBottom: 20 }}
									type="text"
									name="inputTTN"
									value={ttnvalue}
									onChange={handleInputs}
								/>
								<CustomButton
									title="Сохранить"
									type="primary"
									onClick={() => saveTTN(record.id)}
								/>
								<div style={{ marginBottom: 20, textAlign: 'center' }}>
									{' '}
									или{' '}
								</div>
								<CreateTTN
									// buttonName="Отправить самому"
									order={record}
									refreshData={getOrders1}
									// disabled={!this.props.user.apiKey}
								/>
							</Fragment>
						)}
					</div>
				</Col>
			</Row>
		</OrderProductCard>
	);
};

export default ContractorOrderProduct;
