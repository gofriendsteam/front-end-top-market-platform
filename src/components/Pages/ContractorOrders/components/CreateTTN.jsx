import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/ru';
import { Button, Input, Modal, Select, Tabs, notification } from 'antd';
import '../CreateTTN.css';

import { editContractorOrder } from '../../../../utils/api/ordersAction';
import { getNovaPoshtaSavedData } from '../../../../utils/api/novaPoshtaActions';
import {
	getCounterpartyContactPersons,
	createNovaPoshtaTTN,
} from '../../../../utils/api/requestNovaPoshta';
import { CreateTTNWrapper } from '../../Orders/components/CreateTTNWrapper.styles';
import CustomButton from '../../../Shared/Button/Button';

const { Option } = Select;
const Tab = Tabs.TabPane;
function callback(key) {
	console.log(key);
}

const GeterateDate = moment().format('l');

const deliveryMethod = {
	1: 'на отделение',
	2: 'на адрес',
};

class CreateTTN extends Component {
	state = {
		order: this.props.order,
		modalVisible: false,
		ttnRequestObject: {
			apiKey: this.props.apiKey,
			modelName: 'InternetDocument',
			calledMethod: 'save',
			methodProperties: {
				NewAddress: '1',
				PayerType: 'Recipient',
				PaymentMethod: 'Cash',
				CargoType: 'Parcel',
				VolumeGeneral: null,
				Weight: null,
				ServiceType:
					this.props.order.baseOrder.delivery.deliveryMethodId === 1
						? 'WarehouseWarehouse'
						: this.props.order.baseOrder.delivery.deliveryMethodId === 2
						? 'WarehouseDoors'
						: null,
				SeatsAmount: '1',
				Description: this.props.order.baseOrder.items.reduce(
					(acc, el, i) => (i ? acc + ', ' + el.name : acc + el.name),
					'',
				),
				Cost: this.props.order.baseOrder.cost,
				CitySender: null,
				Sender: this.props.senderRef,
				SenderAddress: null,
				ContactSender: null,
				SendersPhone: null,
				RecipientCityName: this.props.order.baseOrder.delivery.city,
				RecipientArea: null,
				RecipientAreaRegions: null,
				RecipientAddressName:
					this.props.order.baseOrder.delivery.deliveryMethodId === 1
						? this.props.order.baseOrder.delivery.placeNumber
						: this.props.order.baseOrder.delivery.deliveryMethodId === 2
						? this.props.order.baseOrder.delivery.placeStreet
						: null,
				RecipientHouse:
					this.props.order.baseOrder.delivery.deliveryMethodId === 2
						? this.props.order.baseOrder.delivery.placeHouse
						: null,
				RecipientFlat:
					this.props.order.baseOrder.delivery.deliveryMethodId === 2
						? this.props.order.baseOrder.delivery.placeFlat
						: null,
				RecipientName: this.props.order.baseOrder.delivery.recipientTitle,
				RecipientType: 'PrivatePerson',
				RecipientsPhone: this.props.order.baseOrder.userPhone,
				DateTime: GeterateDate,
				BackwardDeliveryData: [
					{
						PayerType: 'Recipient',
						CargoType: 'Money',
						RedeliveryString: this.props.order.baseOrder.cost,
					},
				],
			},
		},
		packing: {
			length: '',
			width: '',
			height: '',
		},
	};

	toogleModal = () => {
		this.state.modalVisible && this.props.refreshData('');
		this.setState({ modalVisible: !this.state.modalVisible });
	};

	handleChangeSender = value => {
		const [ContactSender, SendersPhone] = value.split(' ');
		this.setState((state, props) => {
			return {
				ttnRequestObject: {
					...state.ttnRequestObject,
					methodProperties: {
						...state.ttnRequestObject.methodProperties,
						ContactSender,
						SendersPhone,
					},
				},
			};
		});
	};

	handleChangeSenderAdress = value => {
		const [CitySender, SenderAddress] = value.split(' ');
		this.setState((state, props) => {
			return {
				ttnRequestObject: {
					...state.ttnRequestObject,
					methodProperties: {
						...state.ttnRequestObject.methodProperties,
						CitySender,
						SenderAddress,
					},
				},
			};
		});
	};

	calculateVolume = () => {
		console.log('Enter to calculateVolume', this.state.packing);

		const VolumeGeneral =
			Object.values(this.state.packing).reduce((acc, el) => acc * el, 1) / 1000;
		VolumeGeneral &&
			this.setState((state, props) => {
				return {
					ttnRequestObject: {
						...state.ttnRequestObject,
						methodProperties: {
							...state.ttnRequestObject.methodProperties,
							VolumeGeneral,
						},
					},
				};
			});
	};

	handleInputs = ({ target: { value, name } }) => {
		name === 'length' || name === 'width' || name === 'height'
			? this.setState(
					(state, props) => {
						return { packing: { ...state.packing, [name]: value } };
					},
					() => this.calculateVolume(),
			  )
			: this.setState((state, props) => {
					return {
						ttnRequestObject: {
							...state.ttnRequestObject,
							methodProperties: {
								...state.ttnRequestObject.methodProperties,
								[name]: value,
							},
						},
					};
			  });
	};

	createTTN = () => {
		// console.log('We are in createTTN method');
		// console.table(this.state.ttnRequestObject);
		const { id } = this.props.order;

		if (this.checkFieldsBeforeCreateTTN()) return;

		createNovaPoshtaTTN(this.state.ttnRequestObject)
			.then(({ data }) =>
				data.success
					? data.data[0]
					: data.errors.forEach(el => notification.error({ message: el })),
			)
			.then(
				data =>
					data &&
					editContractorOrder(id, { ttn: data.IntDocNumber }).then(res =>
						notification.success({ message: `Создана ТТН: ${res.ttn}` }),
					),
			)
			.then(() => this.toogleModal());
	};

	checkFieldsBeforeCreateTTN = () => {
		const {
			ttnRequestObject: {
				methodProperties: {
					ContactSender,
					CitySender,
					ServiceType,
					Description,
					Cost,
					VolumeGeneral,
					Weight,
				},
			},
		} = this.state;

		let errors = 0;

		!ContactSender &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не выбран отправитель',
			});
		!CitySender &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не выбран адрес отправителя',
			});
		!ServiceType &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не выбран тип доставки',
			});
		!Description &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не заполнено описание посылки',
			});
		!Cost &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не указана оценочная стоимость посылки',
			});
		!Weight &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Не указан вес посылки',
			});
		!VolumeGeneral &&
			++errors &&
			notification.warning({
				duration: 5 + errors,
				message: 'Заполните все поля габаритов упаковки',
			});
		return errors;
	};

	componentDidMount = async () => {
		const { apiKey, senderRef } = this.props;
		const novaPoshtaUserData = await getNovaPoshtaSavedData();
		let contactPersons = await getCounterpartyContactPersons(apiKey, senderRef);

		console.log(novaPoshtaUserData);
		console.log(contactPersons);
		contactPersons = contactPersons.data.data;
		console.log(contactPersons);
		this.setState({
			novaPoshtaUserData,
			contactPersons,
		});
	};

	render() {
		const {
			packing: { length, width, height },
			modalVisible,
			contactPersons,
			novaPoshtaUserData,
			order,
			order: {
				baseOrder: { delivery },
			},
			ttnRequestObject: {
				methodProperties: { ServiceType, Description, Cost, Weight },
			},
		} = this.state;
		const {
			toogleModal,
			handleInputs,
			handleChangeSender,
			handleChangeSenderAdress,
			createTTN,
		} = this;

		return (
			<Fragment>
				<CustomButton
					title="Создать ТТН"
					type="primary"
					onClick={toogleModal}
					disabled={!this.props.apiKey}
				/>

				<Modal
					centered
					visible={modalVisible}
					onCancel={toogleModal}
					footer={[
						<Button form="createTTN" onClick={createTTN}>
							Создать ТТН
						</Button>,
					]}
					okButtonProps={{ disabled: true }}
					width={750}
				>
					<CreateTTNWrapper>
						<form id="createTTN" onSubmit={createTTN}>
							<Tabs
								defaultActiveKey="3"
								onChange={callback}
								className="createTTN_tabs"
							>
								<Tab tab="Данные об отправителе" key="1">
									<Select
										style={{ width: '100%', marginBottom: 16 }}
										placeholder="Выберите отправителя"
										onChange={handleChangeSender}
									>
										{contactPersons &&
											contactPersons.map(el => (
												<Option value={`${el.Ref} ${el.Phones}`}>
													{el.Description}, {el.Phones}
												</Option>
											))}
										{/* <Option value="create">Создать отправителя</Option> */}
									</Select>
									<Select
										style={{ width: '100%' }}
										placeholder="Выберите отделение для отправки"
										onChange={handleChangeSenderAdress}
									>
										{novaPoshtaUserData &&
											novaPoshtaUserData.addresses.map(el => (
												<Option
													value={`${el.settlementRef} ${el.warehouseRef}`}
												>
													{el.settlementValue}, {el.warehouseValue}
												</Option>
											))}
										{/* <Option value="create">Создать новый адрес отправки</Option> */}
									</Select>
								</Tab>
								<Tab tab="Данные о получателе" key="2">
									<p>
										<b>Тип доставки: </b>
										{deliveryMethod[delivery.deliveryMethodId] ||
											delivery.deliveryMethodId}
									</p>
									<p>
										<b>Адрес доставки: </b>
										{ServiceType === 'WarehouseWarehouse'
											? `${delivery.city}, отделение №${delivery.placeNumber} (${delivery.placeStreet}, ${delivery.placeHouse})`
											: `${delivery.city}, ${delivery.placeStreet}, ${delivery.placeHouse}, ${delivery.placeFlat}`}
									</p>
									<p>
										<b>Получатель: </b> {delivery.recipientTitle}
									</p>
									<p>
										<b>Телефон получателя: </b> {order.baseOrder.userPhone}
									</p>
								</Tab>

								<Tab tab="Данные о посылке" key="3">
									<div className="about-package-wr">
										<div className="create-input-item">
											<p>Описание: </p>

											<Input
												type="text"
												name="Description"
												value={Description}
												onChange={handleInputs}
												placeholder="Наименование"
											/>
										</div>
										<div className="create-input-item">
											<p>Оценочная стоимость, ₴:</p>{' '}
											<Input
												type="number"
												step="0.01"
												name="Cost"
												value={Cost}
												onChange={handleInputs}
												placeholder="Оценочная стоимость"
											/>{' '}
										</div>
										<div className="create-input-item">
											<p>Вес, кг:</p>{' '}
											<Input
												type="number"
												step="0.1"
												name="Weight"
												value={Weight}
												onChange={handleInputs}
												placeholder="Вес"
											/>{' '}
										</div>
										<div className="create-input-item">
											<p> Габариты упаковки:</p>
											<Input
												type="number"
												step="1"
												placeholder="Длина"
												name="length"
												value={length}
												onChange={handleInputs}
											/>
											<Input
												type="number"
												step="1"
												placeholder="Ширина"
												name="width"
												value={width}
												onChange={handleInputs}
											/>
											<Input
												type="number"
												step="1"
												placeholder="Высота"
												name="height"
												value={height}
												onChange={handleInputs}
											/>{' '}
										</div>
									</div>
								</Tab>
							</Tabs>
						</form>
					</CreateTTNWrapper>
				</Modal>
			</Fragment>
		);
	}
}

function mapStateToProps(state) {
	return {
		apiKey: state.user.novaPoshtaApiKey,
		senderRef: state.user.senderRef,
	};
}

export default connect(mapStateToProps)(CreateTTN);
