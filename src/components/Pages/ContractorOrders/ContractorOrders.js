import React, { Component, useState } from 'react';
import { Tabs, Table, notification } from 'antd';
import { contractorOrdersColumns } from '../../../constants/TableColumns/OrdersColumns';
// import { statusList } from '../../../constants/orderStatus';
import { connect } from 'react-redux';
import {
	editContractorOrder,
	getContractorOrders,
	getOrders,
} from '../../../utils/api/ordersAction';
import ContractorOrderProduct from './components/ContractorOrderProduct';
// import CreateTTN from './components/CreateTTN';
import { Title } from '../../Shared/Styles/title.styles';
import { OrdersWrapper } from '../Orders/OrdersWrapper.styles';
import { TableWrapper } from '../../Shared/Styles/MainWrapper';
import { Card } from '../../Shared/Styles/Card';
import Filters from '../../Blocks/Filters/Filters';
import { useFilters } from '../../Containers/useFilters';
import filtersOptions from '../../../constants/filtersOptions';
import { useTable } from '../../Containers/useTable';

const TabPane = Tabs.TabPane;

function callback(key) {
	console.log(key);
}

const ContractorOrders = ({ user }) => {
	const [values, handleChangeInputs] = useFilters(filtersOptions.orders);
	const [inputTTN, setTTn] = useState('')
	const [tabKey, setKey] = useState(1);
	const changeTabs = value => setKey(value);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		// resetRowKeys,
	] = useTable(getContractorOrders, values, '', `&status_group=${tabKey}`);

	const saveTTN = id => {

		editContractorOrder(id, { ttn: inputTTN })
			.then(res =>
				notification.success({ message: `Заказу присвоен ТТН: ${res.ttn}` }),
			)
			.then(() => {
				setTTn('' );
				fetchData()
			});
	};

	const deleteTTN = ttn => {
		notification.success({message: "В разработке"})
		// const orders1 = this.state.orders1.map(el =>
		// 	el.ttn === ttn ? { ...el, ttn: '' } : el,
		// );
		// this.setState({ orders1 });
	};

	// const { inputTTN, orders1, orders2, orders3 } = this.state;
	// console.log(this.state);

	return (
		<OrdersWrapper>
			<Title>Мои заказы</Title>
			<Card>
				<Filters
					type="orders"
					values={values}
					onChange={handleChangeInputs}
					handleChangeFilters={handleChangeInputs}
				/>

				{/*<SearchOrders onSearch={this.getAllOrders} />*/}
				<TableWrapper>
					<Tabs onChange={changeTabs} type="line" animated={false}>
						<TabPane tab="В обработке" key="1">
							<Table
								{...config}
								loading={loading}
								columns={contractorOrdersColumns}
								expandedRowRender={record => (
									<ContractorOrderProduct
										record={record}
										deleteTTN={deleteTTN}
										getOrders1={fetchData}
										saveTTN={saveTTN}
									/>
								)}
								dataSource={products}
								onChange={handleChangeTable}
							/>
						</TabPane>

						<TabPane tab="Успешно завершены" key="2">
							<Card>
								<Table
									{...config}
									loading={loading}
									columns={contractorOrdersColumns}
									expandedRowRender={record => (
										<span>{record.description}</span>
									)}
									dataSource={products}
									onChange={handleChangeTable}
								/>
							</Card>
						</TabPane>

						<TabPane tab="Неуспешно завершены" key="3">
							<Card>
								<Table
									{...config}
									loading={loading}
									columns={contractorOrdersColumns}
									expandedRowRender={record => (
										<span>{record.description}</span>
									)}
									dataSource={products}
									onChange={handleChangeTable}
								/>
							</Card>
						</TabPane>
					</Tabs>
				</TableWrapper>
			</Card>
		</OrdersWrapper>
	);
};

const mapStateToProps = state => ({
	user: state.user,
});

export default connect(mapStateToProps)(ContractorOrders);
