import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Link } from 'react-router-dom';
import { Table, Modal, Input, Radio, Upload, Icon, notification } from 'antd';
import styles from './ContractorProducts.module.css';
import { putHistory } from '../../../utils/api/history';
import {
	getContractorProducts,
	uploadXls,
	getContractorCategories,
	removeContractorProduct,
	getDownloadsStatus,
} from '../../../utils/api/productsActions';
import { contractorProductsColumn } from '../../../constants/TableColumns/ContractorColumns';
import { getProfile } from '../../../utils/api/userActions';
import NewProduct from '../../Blocks/EditCreateProduct/NewProduct';
import { connect } from 'react-redux';
import { Title } from '../../Shared/Styles/title.styles';
import { toogleModal } from '../../../store/actions/modal';
import {
	MainLayout,
	TableWrapper,
} from '../../Shared/Styles/MainWrapper';
import Preloading from '../../Shared/Loader/TablePreloader';
import CustomButton from '../../Shared/Button/Button';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import ImortYmlProducts from '../../Blocks/ImportExport/ImortYmlProducts';
import { Card } from '../../Shared/Styles/Card';
import Filters from '../../Blocks/Filters/Filters';
import _ from 'lodash';
import EditSelectedProducts from '../../Blocks/EditCreateProduct/EditSelectedProducts';

class ContractorProducts extends Component {
	state = {
		haveRozetkaAkaunt: false,
		loading: false,
		// categories: [],
		selectedRowKeys: [],
		products: [],
		product: {},
		filters: {
			category_id: '',
			name: '',
			vendor_code: '',
			min_price: '',
			max_price: '',
			brand: '',
			in_stock: '',
			contractorId: '',
		},

		count: 0,
		currentPage: 1,
		pageSize: 10,

		openExcelModal: false,
		uploadExel: false,
		uploadRozetka: false,

		inStock: 0,
		notInStock: 0,
		allProductsmodifyOpen: false,
		importValue: 2,
	};

	getMyProducts = async () => {
		this.setState({ loading: true });
		const {
			currentPage,
			pageSize,
			filters: {
				category_id,
				name,
				brand,
				in_stock,
				vendor_code,
				min_price,
				max_price,
			},
		} = this.state;
		const urlParams = [
			category_id ? `&category_id=${category_id}` : '',
			name ? `&name=${name}` : '',
			brand ? `&brand=${brand}` : '',
			in_stock ? `&in_stock=${in_stock}` : '',
			vendor_code ? `&vendor_code=${vendor_code}` : '',
			min_price ? `&min_price=${min_price}` : '',
			max_price ? `&max_price=${max_price}` : '',
		];

		const url = `?page_size=${pageSize}&page=${currentPage +
			urlParams.join('')}`;

		const [all, inStock, notInStock] = await Promise.all([
			getContractorProducts(url),
			getContractorProducts(`?in_stock=${true}`),
			getContractorProducts(`?in_stock=${false}`),
		]);

		this.setState({
			loading: false,
			products: all.results,
			count: all.count,
			inStock: inStock.count,
			notInStock: notInStock.count,
		});
	};

	onSelectChange = selectedRowKeys => {
		this.setState({ selectedRowKeys });
	};

	// handleUploadFile = async (file, type) => {
	// 	const formData = new FormData();
	//
	// 	formData.append('xls_file', file);
	// 	formData.append('file_type', type);
	// 	console.log(formData);
	// 	await uploadXls(formData);
	// 	this.props.history.push('/admin/products/download_history');
	//
	// 	this.handleUpdate();
	// };

	// getCategories = async () => {
	// 	const res = await getContractorCategories();
	//
	// 	this.setState({
	// 		categories: res,
	// 	});
	// };

	handleChangeFilters = ({ target: { name, value } }) => {
		this.setState(
			{
				filters: {
					...this.state.filters,
					[name]: value,
				},
			},
			() => this.getMyProducts(),
		);
	};

	handleRemoveProducts = async () => {
		let idArr = [];
		await this.state.selectedRowKeys.forEach(item => {
			idArr.push(this.state.products[item].id);
		});

		console.log(idArr);
		await removeContractorProduct({ productListIds: idArr });

		this.handleUpdate();
	};

	handleChangeTable = pagination => {
		this.setState(
			{
				currentPage: pagination.current,
				pageSize: pagination.pageSize,
			},
			() => this.getMyProducts(),
		);
	};

	openProduct = product => {
		this.setState({
			product: product,
		});
	};
	handleUpdate = () => {
		this.getMyProducts();
		// this.getCategories();
		this.handleUpdateProduct();
	};

	handleUpdateProduct = () => {
		this.setState({
			selectedRowKeys: [],
			product: {},
		});
	};

	componentWillReceiveProps(nextProps) {
		// console.log('ContractorProducts\ncomponentWillReceiveProps');
		if (nextProps.user.selectedCategory !== this.state.category_id) {
			this.setState(
				{
					filters: {
						...this.state.filters,
						category_id: nextProps.user.selectedCategory,
					},
					currentPage: 1,
				},
				() => this.getMyProducts(),
			);
		}
	}
	editAll = () => {
		this.handleOpenEditAllProducts(true);
	};
	handleOpenEditAllProducts = val => {
		this.setState({
			allProductsmodifyOpen: val,
		});
	};
	async componentDidMount() {
		// console.log('ContractorProducts\ncomponentDidMount');
		this.props.toHistory(this.props.history);
		this.getMyProducts();
		// this.getCategories();
		// this.checkUploader();

		// const res = await getProfile();
		// if (res.rozetkaPassword && res.rozetkaUsername) {
		// 	this.setState({
		// 		haveRozetkaAkaunt: true,
		// 	});
		// }
	}

	trottledChangeFilters = _.throttle(this.handleChangeFilters, 200);

	render() {
		const {
			selectedRowKeys,
			products,
			loading,
			// categories,
			product,
			count,
			currentPage,
			filters,

			inStock,
			notInStock,
			pageSize,
		} = this.state;
		// console.log('-= ContractorProducts =-\nSTATE after render()', this.state);

		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange,
			hideDefaultSelections: true,
			onSelection: this.onSelection,
		};

		const config = {
			pagination: {
				pageSize: pageSize,
				pageSizeOptions: ['10', '20', '50', '500'],
				showSizeChanger: true,
				total: count,
				current: currentPage,
			},
		};

		// const radioStyle = {
		// 	display: 'block',
		// 	height: '28px',
		// 	lineHeight: '28px',
		// 	fontWeight: 'normal',
		// };
		const { toogleModal } = this.props;
		// console.log(products);

		// const { Dragger } = Upload;
		//
		// const draggerProps = {
		// 	name: 'file',
		// 	accept: '.xlsx, .xls, .csv, .xml, .yml',
		// 	multiple: false,
		// 	// action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
		// 	onChange: this.uploadFile,
		// };
		return (
			<MainLayout>
				<div className="top-nav">
					<Title>Мои товары</Title>
					<CategoryList />
					<Link to="/admin/instruction" className="how-to-link">
						Как добавить товар?
					</Link>
				</div>

				<Card>
					<div className="actions partner">
						<NewProduct
							onUpdate={this.handleUpdate}
							onUpdateProduct={this.handleUpdateProduct}
							product={product}
							update={product.id ? true : false}
						/>
						<EditSelectedProducts
							selectedRowKeys={selectedRowKeys.map(item => products[item].id)}
							update={this.state.allProductsmodifyOpen}
							closeModal={this.handleOpenEditAllProducts}
							onUpdate={this.handleUpdate}
						/>
						<CustomButton
							title="Редактировать все"
							type="primary"
							disabled={selectedRowKeys.length < 2}
							onClick={this.editAll}
						/>
						<CustomButton
							title="Удалить товары"
							type="danger"
							disabled={selectedRowKeys.length === 0}
							onClick={this.handleRemoveProducts}
						/>
						<CustomButton
							type="primary"
							// disabled={!selectedRowKeys.length}
							title="Импорт"
							onClick={() => {
								toogleModal({
									isOpen: true,
									type: 'import',
									component: ImortYmlProducts,
									data: {
										title: 'Импорт Yml файлов',
										width: 800,
										updateProducts: this.handleUpdate,
									},
								});
							}}
						/>

						<div className={styles.totalProducts}>
							Товаров: {count} шт.
							<span>-в наличии: {inStock}</span>
							<span>-нет в наличии: {notInStock}</span>
						</div>
					</div>
					<Filters
						values={{ ...filters }}
						onChange={this.trottledChangeFilters}
						handleChangeFilters={this.handleChangeFilters}
					/>

					<TableWrapper>
						<Table
							{...config}
							loading={{ spinning: loading, indicator: <Preloading /> }}
							rowSelection={rowSelection}
							columns={contractorProductsColumn(this.openProduct)}
							dataSource={products}
							onChange={this.handleChangeTable}
							scroll={{ x: 1300 }}
							size="small"
						/>
					</TableWrapper>
					<div className="total-price">Товаров: {count}</div>
				</Card>
			</MainLayout>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	toHistory: history => dispatch(putHistory(history)),
	toogleModal: data => dispatch(toogleModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContractorProducts);
