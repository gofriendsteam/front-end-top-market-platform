import React, { useState } from 'react';
import { notification, Table, Tooltip } from 'antd';
import { TableWrapper, MainLayout } from '../../Shared/Styles/MainWrapper';
import { Card } from '../../Shared/Styles/Card';
import { useFilters } from '../../Containers/useFilters';
import filters from '../../../constants/filtersOptions';
import { useTable } from '../../Containers/useTable';
import { getContractorProducts } from '../../../utils/api/productsActions';
import { contractorProductsColumn } from '../../../constants/TableColumns/ContractorColumns';
import { Title } from '../../Shared/Styles/title.styles';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import { Link } from 'react-router-dom';
import Filters from '../../Blocks/Filters/Filters';
import Total from '../../Shared/Total';
import NewProduct from '../../Blocks/EditCreateProduct/NewProduct';
import EditSelectedProducts from '../../Blocks/EditCreateProduct/EditSelectedProducts';
import CustomButton from '../../Shared/Button/Button';
import ImortYmlProducts from '../../Blocks/ImportExport/ImortYmlProducts';
import styles from './ContractorProducts.module.css';
import { toogleModal } from '../../../store/actions/modal';
import { removeContractorProduct } from '../../../utils/api/productsActions';
import { useDispatch } from 'react-redux';
import ContractorTotal from './Total';

const NewContractorProducts = () => {
	const [editingProduct, setProduct] = useState({});
	const [values, handleChangeInputs] = useFilters(filters.categories);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		resetRowKeys,
	] = useTable(getContractorProducts, values);
	const dispatch = useDispatch();
	const removeProducts = async () => {
		const selectedProducts = products
			.filter((product, idx) => selectedRowKeys.includes(idx))
			.map(({ id }) => id);

		await removeContractorProduct({ productListIds: selectedProducts });

		notification.success({ message: 'Товар был удален.' });
		fetchData();
		resetRowKeys();
	};
	const resetEditedProduct = () => setProduct({});
	return (
		<MainLayout>
			<div className="top-nav">
				<Title>Мои товары</Title>
				<CategoryList />
				<Tooltip title="В разработке" placement="top">
					<Link to="/admin/products" className="how-to-link">
						Как добавить товар?
					</Link>
				</Tooltip>
			</div>
			<Card>
				<div className="actions partner">
					<NewProduct
						onUpdate={fetchData}
						onUpdateProduct={resetEditedProduct}
						product={editingProduct}
						update={editingProduct.id ? true : false}
					/>
					<EditSelectedProducts
						selectedRowKeys={selectedRowKeys.map(item => products[item].id)}
						onUpdate={fetchData}
					/>

					<CustomButton
						title="Удалить товары"
						type="danger"
						disabled={!selectedRowKeys.length}
						onClick={removeProducts}
					/>
					<CustomButton
						type="primary"
						title="Импорт"
						onClick={() => {
							dispatch(
								toogleModal({
									isOpen: true,
									type: 'import',
									component: ImortYmlProducts,
									data: {
										title: 'Импорт Yml файлов',
										width: 800,
										updateProducts: fetchData,
									},
								}),
							);
						}}
					/>

					<ContractorTotal count={count} />
				</div>
				<Filters
					values={values}
					onChange={handleChangeInputs}
					handleChangeFilters={handleChangeInputs}
				/>
				<TableWrapper>
					<Table
						{...config}
						rowSelection={rowSelection}
						columns={contractorProductsColumn(setProduct)}
						dataSource={products}
						scroll={{ x: 1300 }}
						loading={loading}
						onChange={handleChangeTable}
						size="small"
					/>
				</TableWrapper>
			</Card>
		</MainLayout>
	);
};

export default NewContractorProducts;
