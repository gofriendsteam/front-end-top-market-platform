import React, { useState } from 'react';
import { Select, Divider, Icon, Checkbox } from 'antd';

const { Option } = Select;
const allItems = ['3', '4', '5'];
const items = ['1', '2'];

const SelectableContractors = ({ id }) => {
	const concat = [...allItems, ...items];
	return (
		<Select
			mode="multiple"
			style={{ width: '100%' }}
			placeholder="Поставщики"
			defaultValue={items}
			// onChange={handleChange}
		>
			{concat.map(item => (
				<Option key={item}>#{item}</Option>
			))}
		</Select>
	);
};

export default SelectableContractors;
