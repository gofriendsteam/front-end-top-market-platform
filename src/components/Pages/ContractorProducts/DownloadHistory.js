import React, { Fragment } from 'react';
import styles from './ContractorProducts.module.css';
import { Link } from 'react-router-dom';
import TableTemplate from '../../Blocks/Tables/TableTemplate';
import { Title } from '../../Shared/Styles/title.styles';
import { TableWrapper } from '../../Shared/Styles/MainWrapper';
import { Card } from '../../Shared/Styles/Card';

const DownloadHistory = () => {
	return (
		<Fragment>
			<div className={styles.top}>
				<Title>История загрузок</Title>
			</div>
			<TableWrapper>
				<Card>
					<TableTemplate
						willHaveFullData={true}
						actions="productsActions"
						action="getDownloadsStatus"
						moduleName="contractorProducts"
						tableName="uploadsHistory"
						units="загрузок"
						user={{ role: 'CONTRACTOR' }}
					/>
				</Card>
			</TableWrapper>
		</Fragment>
	);
};

export default DownloadHistory;
