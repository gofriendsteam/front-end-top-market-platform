import React, { useEffect, useState } from 'react';
import Total from '../../Shared/Total';
import styled from 'styled-components';
import { getContractorProducts } from '../../../utils/api/productsActions';

const Wrapper = styled.div`
	margin-left: auto;
	min-width: 170px;
	@media only screen and (max-width: 1150px) {
	margin: 10px 0 ;
	}
	.in_stock,
	.out_stock {
		color: #595959;

		font-weight: 500;
		display: flex;
		align-items: center;

		justify-content: space-between;
		& > span {
			font-size: 15px;
		}
	}
	.in_stock {
		& > span {
			color: green;
		}
	}
	.out_stock {
		& > span {
			color: red;
		}
	}
`;

const ContractorTotal = ({ count }) => {
	const [in_stock, setInStock] = useState(0);
	const [out_stocked, setOutStock] = useState(0);

	const getCountsProducts = async () => {
		const [inStock, outStock] = await Promise.all([
			getContractorProducts(`?in_stock=${true}`),
			getContractorProducts(`?in_stock=${false}`),
		]);
		setInStock(inStock.count);
		setOutStock(outStock.count);
	};
	useEffect(() => {
		getCountsProducts();
	}, [count]);
	return (
		<Wrapper>
			<Total count={count} />
			<div className=" in_stock">
				<div>В наличие:</div>
				<span>{in_stock} шт.</span>
			</div>
			<div className=" out_stock">
				<div>Нет в наличие:</div>
				<span>{out_stocked} шт.</span>
			</div>
		</Wrapper>
	);
};

export default ContractorTotal;
