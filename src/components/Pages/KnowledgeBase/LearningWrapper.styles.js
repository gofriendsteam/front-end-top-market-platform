import styled, { createGlobalStyle } from 'styled-components';
import { palette } from 'styled-theme';

export const LearningWrapper = styled.div`
	padding: 24px 0 50px;
	.collapse-container {
		background: #fff;
		iframe {
			width: 880px;
			height: 500px;
			@media only screen and (max-width: 992px) {
			      width: 100%;
			      height: 300px;
			}
			@media only screen and (max-width: 500px) {
			      width: 100%;
			      height: 200px;
			}
		}
		.collapse-item {
			.ant-collapse-content {
				text-align: center;
			}

			//border-bottom: none;
		}
	}
`;
