import React, { Component } from 'react';
import { Collapse, Input } from 'antd';
import styles from './KnowledgeBase.module.css';
import 'antd/dist/antd.css';
import { Title } from '../../Shared/Styles/title.styles';
import { LearningWrapper } from './LearningWrapper.styles';
import { Card } from '../../Shared/Styles/Card';
const Panel = Collapse.Panel;
const { Search } = Input;

class KnowledgeBase extends Component {
	render() {
		return (
			<LearningWrapper>
				<Title>База знаний</Title>
				<Card>
					<Search
						placeholder="Поиск"
						onSearch={value => console.log(value)}
						style={{ marginBottom: 40 }}
					/>

					<Collapse accordion className="collapse-container">
						<Panel
							header="Что такое арбитраж ?"
							key="1"
							className="collapse-item"
						>
							<iframe
								// width="560"
								// height="400"
								src="https://www.youtube.com/embed/j3kWj2yXQmw"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							/>
						</Panel>
						<Panel
							header="Топ 5 источников трафика для товарного бизнеса"
							key="2"
							className="collapse-item"
						>
							<iframe
								src="https://www.youtube-nocookie.com/embed/w-_qfofk1C0"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							/>
						</Panel>
						<Panel header="Топ ошибок продаж" key="3" className="collapse-item">
							<iframe
								src="https://www.youtube-nocookie.com/embed/GyDO9DPlUSo"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							/>
						</Panel>
						<Panel
							header="Как выбрать нишу ? Товарный бизнес"
							key="4"
							className="collapse-item"
						>
							<iframe
								src="https://www.youtube-nocookie.com/embed/VGTZ0IJtwic"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							/>
						</Panel>
						<Panel
							header="Топ 5 товаров лидеры продаж"
							key="5"
							className="collapse-item"
						>
							<iframe
								src="https://www.youtube-nocookie.com/embed/y-2feQXXd8U"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen
							/>
						</Panel>
					</Collapse>
				</Card>
			</LearningWrapper>
		);
	}
}

export default KnowledgeBase;
