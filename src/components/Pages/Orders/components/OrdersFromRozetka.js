import React, { Component, useState } from 'react';
import { Tabs, Table, notification } from 'antd';
import {
	getOrders,
	passToContractor,
} from '../../../../utils/api/ordersAction';
import { orderRozetkaColumn } from '../../../../constants/TableColumns/OrdersColumns';
import { connect } from 'react-redux';
import Filters from '../../../Blocks/Filters/Filters';
import Preloading from '../../../Shared/Loader/TablePreloader';
import OrderProduct from './OrderProduct';
import { sendingOrderToSupplier } from '../../../../store/actions/oreders';
import { useFilters } from '../../../Containers/useFilters';
import filtersOptions from '../../../../constants/filtersOptions';
import { useTable } from '../../../Containers/useTable';

const TabPane = Tabs.TabPane;

const OrdersFromRozetka = ({ user, sendingOrderToSupplier }) => {
	const [values, handleChangeInputs] = useFilters(filtersOptions.orders);
	const [tabKey, setKey] = useState(1);
	const changeTabs = value => setKey(value);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		// resetRowKeys,
	] = useTable(getOrders, values, 'rozetka', `&status_group=${tabKey}`);

	const handlePassToContractor = async id => {
		await passToContractor(id);
		notification.success({
			message: 'Отправлено',
		});

		fetchData();
	};

	return (
		<div>
			<Filters
				type="orders"
				values={values}
				onChange={handleChangeInputs}
				handleChangeFilters={handleChangeInputs}
			/>
			{/*<SearchOrders onSearch={this.getAllOrders} />*/}

			<Tabs type="line" animated={false} onChange={changeTabs}>
				<TabPane tab="В обработке" key="1">
					<div>
						<Table
							{...config}
							columns={orderRozetkaColumn(fetchData)}
							loading={loading}
							expandedRowRender={record => (
								<OrderProduct
									record={record}
									handleSendtoNewContractor={sendingOrderToSupplier}
									handlePassToContractor={handlePassToContractor}
									getOrders1={fetchData}
								/>
							)}
							dataSource={products}
							onChange={handleChangeTable}
						/>
					</div>
				</TabPane>

				<TabPane tab="Успешно завершены" key="2">
					<div>
						<Table
							{...config}
							columns={orderRozetkaColumn()}
							loading={loading}
							// expandedRowRender={record => <span>
							//     {record.description}
							// </span>}
							dataSource={products}
							onChange={handleChangeTable}
						/>
					</div>
				</TabPane>

				<TabPane tab="Неуспешно завершены" key="3">
					<div>
						<Table
							{...config}
							scroll={{ x: 1080 }}
							columns={orderRozetkaColumn()}
							dataSource={products}
							loading={loading}
							onChange={handleChangeTable}
						/>
					</div>
				</TabPane>
			</Tabs>
		</div>
	);
};

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	sendingOrderToSupplier: data => dispatch(sendingOrderToSupplier(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrdersFromRozetka);
