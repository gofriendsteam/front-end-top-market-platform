import styled, { createGlobalStyle } from 'styled-components';
// import { palette } from 'styled-theme';

export const CreateTTNWrapper = styled.div`
	.about-inputs-wr {
		display: flex;
		font-size: 14px;
		font-weight: 400;
		color: rgb(89, 89, 89);
		p {
			font-size: 14px;
			font-weight: 500;
			color: rgb(50, 51, 50);
			line-height: 1.3;
			margin-right: 15px;
		}
	}
	.about-package-wr {
		display: flex;
		flex-wrap: wrap;
		justify-content: space-between;
		.create-input-item {
			margin-bottom: 15px;
			@media only screen and (max-width: 550px) {
				width: 100% !important;
			}
			p {
				font-size: 14px;
				font-weight: 500;
				color: rgb(50, 51, 50);
				line-height: 1.3;
				margin-bottom: 15px;
			}
		}
		.create-input-item:not(:last-child) {
			width: 200px;
		}
		
		.create-input-item:last-child {
			display: flex;
			flex-wrap: wrap;
			justify-content: space-between;
			p {
				flex-basis: 100%;
			}
			input {
				width: 200px;
				margin-bottom: 15px;
				@media only screen and (max-width: 550px) {
					width: 100% !important;
				}
			}
		}
	}
`;
