import styled from 'styled-components';

export const CreateOrderWrapper = styled.div`
	.title {
		color: rgba(0, 0, 0, 0.85);
		font-size: 14px;
		margin-bottom: 10px;
	}
	label {
		font-weight: 500;
		margin-bottom: 10px;
		line-height: 29.9999px;
	}
	.ant-form-item {
		margin-bottom: 10px;
	}
	.select-block {
		margin-bottom: 10px;
		
	}
`;
