import React from 'react';
import ProductsTable from './ProductsTable.style';
import SingleCart from './SingleCart';


const getRealPrice = 	(product) => product.contractorPriceForPartner
	? +product.contractorPriceForPartner
	: +product.price;

const ProductCard = ({ products, selectedProduct,values, seQuantity, deleteCart }) => {

	const totalCount = values.reduce(
		(sum, num) => sum + num.mycount * getRealPrice(num),
		0,
	);
	return (
		<ProductsTable>
			{values.map((value, index) => (
				<SingleCart
					{...value}
					setQuantity={seQuantity}
					deleteCart={deleteCart}
				/>
			))}
			<div className='main-total'>Общая сумма заказа: {totalCount.toFixed(2)} грн.</div>
		</ProductsTable>
	);
};

export default ProductCard;
