import styled from 'styled-components';
import { palette } from 'styled-theme';
// import {
// 	transition,
// 	borderRadius,
// 	boxShadow,
// } from '../../../../helpers/style_utils';

const ProductsTable = styled.div`
	.product-card-wrapper {
		padding: 10px;
		display: flex;
		border: 1px solid rgb(235, 235, 235);
		margin-bottom: 10px;
		//height: 200px;
	}
	.main-total {
		font-size: 16px;
		font-weight: 500;
		color: rgb(50, 51, 50);
		text-align: right;
	}
	.close-btn {
		padding: 0 10px;
		display: flex;
		align-items: center;
		justify-content: center;
		i {
			cursor: pointer;
		}
	}

	.image {
		width: 120px;
		display: flex;
		align-items: center;
		justify-content: center;
		overflow: hidden;
		& > img {
			width: 100%;
			height: auto;
		}
	}
	.content {
		width: 100%;
		padding: 10px;
		.total {
			margin-left: 10px;
		}
		.name {
			font-weight: 500;
			font-size: 15px;
			margin-bottom: 10px;
		}
		.price-block {
			display: flex;
			align-items: center;
		}
		.total,
		.price {
			font-weight: 500;
			color: rgb(50, 51, 50);
		}
	}
`;

export default ProductsTable;
