import React, { useEffect, useState, Fragment } from 'react';
import { Radio, Input, Form, notification } from 'antd';
import CustomButton from '../../../Shared/Button/Button';
import { StatusWrapper } from '../OrdersWrapper.styles';
import { useLocation } from 'react-router-dom';
import _ from 'lodash';
import { updatePartnerStatus } from '../../../../utils/api/userActions';
import CustomScrollBar from '../../../Blocks/ScrollBar/CustomScrollBar';

const StatusBlock = ({ statusChildren, form, id, update }) => {
	const [checkedStatus, setStatus] = useState(statusChildren[0].id);
	const [canSubmit, setSubmition] = useState(null);
	const [loading, setLoading] = useState(false);
	const { pathname } = useLocation();

	const { getFieldDecorator } = form;

	useEffect(() => {
		const currentObjStatus = statusChildren.find(
			child => child.id === checkedStatus,
		);
		if (_.has(currentObjStatus, 'needComment')) {
			setSubmition({
				name: 'systemComment',
				text: 'Заполните поле для комментарии!',
				type: 'text',
			});
		} else if (_.has(currentObjStatus, 'needTtn')) {
			setSubmition({
				name: 'ttn',
				text: 'Заполните поле для номер ТТН!',
				type: 'number',
			});
		} else {
			setSubmition(null);
		}
	}, [checkedStatus]);

	const onChangeStatus = ({ target: { value } }) => {
		setStatus(value);
	};
	const handleSubmit = e => {
		e.preventDefault();
		form.validateFields((err, values) => {
			if (!err) {
				setLoading(true);
				const sendData = canSubmit
					? {
							...values,
							status: checkedStatus,
					  }
					: { status: checkedStatus };
				// const sendingUrl = pathname.slice()
				const urlRoleDependencie = pathname.slice(7);
				const sendingUrl = urlRoleDependencie + '/' + id + '/';
				updatePartnerStatus(sendData, sendingUrl)
					.then(({ rozetkaSuccess }) => {
						setLoading(false);
						if (rozetkaSuccess) {
							update();
							notification.success({ message: 'Статус был изменен.' });
						} else {
							notification.error({
								message: 'Ошибка при смене статуса в Розетке',
							});
						}
					})
					.catch(err => setLoading(false));
			}
		});
	};

	return (
		<StatusWrapper>
			<Form onSubmit={handleSubmit}>
				<h4 className="title">Изменить статус заказа</h4>
				<CustomScrollBar>
					<Radio.Group
						className="radio-wrapper"
						options={statusChildren.map(child => ({
							label: child.name,
							value: child.id,
						}))}
						onChange={onChangeStatus}
						value={checkedStatus}
					/>
					{canSubmit && (
						<Fragment>
							<label>{canSubmit.text}</label>
							<Form.Item>
								{getFieldDecorator(canSubmit.name, {
									rules: [
										{
											required: true,
											message: canSubmit.text,
										},
									],
								})(<Input type={canSubmit.type} placeholder="Введите текст" />)}
							</Form.Item>{' '}
						</Fragment>
					)}
				</CustomScrollBar>
				<CustomButton
					loading={loading}
					htmlType="submit"
					title="Изменить"
					type="primary"
				/>
			</Form>
		</StatusWrapper>
	);
};

export default Form.create()(StatusBlock);
