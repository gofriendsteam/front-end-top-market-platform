import React, { useState } from 'react';
import InputNumber from '../../../Shared/InputNumber';
import defaultImg from '../../../../img/defaultimage.png';
// import { notification } from '../index';

const getDefaultAvatart = item =>
	item.avatarUrl ||
	(item.coverImages.length > 0
		? item.coverImages[0].imageDecoded
		: item.imageUrls.length > 0
		? item.imageUrls[0].url
		: defaultImg);

export default function({
	contractorPriceForPartner,
	deleteCart,
	name,
	id,
	mycount,
	setQuantity,
	price,
	count,
	imageUrls,
	coverImages,
	avatarUrl,
}) {

	const realPrice = contractorPriceForPartner
		? contractorPriceForPartner
		: price;
	const onChange = value => {
		if (!isNaN(value)) {
			if (value <= count) {
				setQuantity(id, +value);
			}
		} else {
			console.log('ERROR');
			return;
		}
	};
	const deleteFromCart = () => {
		deleteCart(id);
	};
	// console.log('card props', quantity, price);
	const totalPrice = (realPrice * mycount).toFixed(2);
	return (
		<div className="product-card-wrapper">
			<div className="close-btn">
				<i className="icon ion-md-close" onClick={deleteFromCart} />
			</div>
			<div className="image">
				<img
					src={getDefaultAvatart({ avatarUrl, coverImages, imageUrls })}
					alt="product-photo"
				/>
			</div>
			<div className="content">
				<div className="name">{name}</div>
				<div className="price">{price}</div>
				<div className="price-block">
					<InputNumber
						min={1}
						max={1000}
						value={mycount}
						step={1}
						onChange={onChange}
					/>

					<div className="total">Цена: {totalPrice} грн</div>
				</div>
			</div>
		</div>
	);
}
