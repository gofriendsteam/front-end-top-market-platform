import styled from 'styled-components';

export const OrderProductCard = styled.div`
	& button {
		width: 100%;
	}
	.product-image {
		width: 240px;
		height: 160px;
		display: flex;
		flex-shrink: 0;
		-webkit-box-align: center;
		align-items: center;
		-webkit-box-pack: center;
		justify-content: center;
		margin-right: 10px;
		background-color: rgb(255, 255, 255);
		overflow: hidden;
		& > img {
			width: 100%;
			height: auto;
		}
	}
	.order-card-wrapper {
		border-right: 1px solid rgb(233, 233, 233);
	}
	.product-card {
		display: flex;
		justify-content: space-between;
		border-bottom: 1px solid rgb(233, 233, 233);
		padding: 15px 0;
	}
	.product-content {
		flex-grow: 1;
	}
	.product-name {
		font-size: 17px;
		margin-bottom: 15px;
	}
	.product-price,
	.product-quantity {
		margin-bottom: 15px;
		display: flex;
		justify-content: space-between;
		align-items: center;
		& > div:first-child {
			font-size: 15px;
			font-weight: 500;
		}
		& > div:last-child {
			font-size: 13px;
			color: rgb(151, 151, 151);
			font-weight: 400;
		}
	}
	.info {
		& > div {
			border-bottom: 1px solid rgb(233, 233, 233);
		}
	}
	.title {
		font-weight: 500;
		font-size: 15px;
		margin-right: 15px;
	}
	.comments {
		display: flex;
		//align-items: center;
		justify-content: space-between;
		padding: 15px 0;

		.text {
			display: flex;
			justify-content: space-between;
			//align-items: center;
		}
		.date {
			font-size: 13px;
			color: rgb(151, 151, 151);
			font-weight: 400;
			margin-right: 10px;
		}
		.comment-text {
			font-size: 14px;
			font-weight: 400;
		}
	}

	.delivery {
		display: flex;
		justify-content: space-between;
		padding: 15px 0;
		.delivery-block {
			width: 300px;

			.delivery-item {
				display: flex;
				align-items: center;
				justify-content: space-between;
				& > div:first-child {
					font-weight: 500;
				}
				& > div:last-child {
					color: rgb(151, 151, 151);
				}
			}
		}
	}
	.last-block {
		display: flex;
		justify-content: space-between;
		align-items: center;
		padding: 15px 0;
	}
	.ttn,
	.total {
		display: flex;
		align-items: center;
		& > div:first-child {
			margin-right: 15px;
		}
		& > div:last-child {
			color: rgb(151, 151, 151);
		}
		.total-title {
			font-size: 18px;
			font-weight: 500;
		}
	}
	.total > div:last-child {
		font-size: 16px;
	}
`;
