import React, { Component } from 'react';
import styles from '../Orders.module.css';

import { statusList } from '../../../../constants/orderStatus';

class SearchOrders extends Component {
	state = {
		id: '',
		min_date: '',
		max_date: '',
		status: '',
		user_fio: '',
		user_phone: '',
	};

	handleChangeInput = ({ target: { value, name } }) => {
		this.setState(
			{
				[name]: value,
			},
			() => this.props.onSearch(this.state),
		);
	};

	render() {
		const {
			id,
			min_date,
			max_date,
			status,
			user_fio,
			// user_phone
		} = this.state;

		return (
			<form className={styles.searchOrders}>
				<div className={styles.orderNumber}>
					<label>Номер заказа</label>
					<input
						type="text"
						name="id"
						value={id}
						onChange={this.handleChangeInput}
					/>
				</div>

				<div className={styles.orderNumber}>
					<label>Название товара /Код товара</label>
					<input
						type="text"
						name="id"
						value={id}
						onChange={this.handleChangeInput}
					/>
				</div>

				<div className={styles.orderDate}>
					<label>Дата заказа</label>
					<input
						type="date"
						name="min_date"
						value={min_date}
						onChange={this.handleChangeInput}
					/>
					<input
						type="date"
						name="max_date"
						value={max_date}
						onChange={this.handleChangeInput}
					/>
				</div>

				<div className={styles.orderStatus}>
					<label>Статус заказа</label>
					<select
						name="status"
						value={status}
						onChange={this.handleChangeInput}
					>
						{statusList.map(item => (
							<option value={item.id} key={item.id}>
								{item.title}
							</option>
						))}
					</select>
				</div>
				<div className={styles.name}>
					<label>ФИО покупателя</label>
					<input
						type="text"
						name="user_fio"
						value={user_fio}
						onChange={this.handleChangeInput}
					/>
				</div>
			</form>
		);
	}
}

export default SearchOrders;
