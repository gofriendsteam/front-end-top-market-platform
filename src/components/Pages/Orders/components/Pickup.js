import React, { useState } from 'react';
import { orderRozetkaColumn } from '../../../../constants/TableColumns/OrdersColumns';
import { Col, Table, Tabs, Row } from 'antd';
import { TableWrapper } from '../../../Shared/Styles/MainWrapper';
import { Card } from '../../../Shared/Styles/Card';
import { useFilters } from '../../../Containers/useFilters';
import { useTable } from '../../../Containers/useTable';
import Filters from '../../../Blocks/Filters/Filters';
import filtersOptions from '../../../../constants/filtersOptions';
import { getPartnerProducts } from '../../../../utils/api/productsActions';
import { getOrders } from '../../../../utils/api/ordersAction';
import OrderCard from '../../../Blocks/Orders/OrderCard';
import { OrderProductCard } from './OrderProductCard.styles';

const TabPane = Tabs.TabPane;

const Pickup = () => {
	const [values, handleChangeInputs] = useFilters(filtersOptions.orders);
	const [tabKey, setKey] = useState(1);
	const changeTabs = value => setKey(value);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		// fetchData,
		// resetRowKeys,
	] = useTable(getOrders, values, 'own', `&status_group=${tabKey}`);

	return (
		<Card>
			<Filters
				type="orders"
				values={values}
				onChange={handleChangeInputs}
				handleChangeFilters={handleChangeInputs}
			/>
			<TableWrapper>
				<Tabs type="line" animated={false} onChange={changeTabs}>
					<TabPane tab="В обработке" key="1">
						<Table
							{...config}
							scroll={{ x: 1080 }}
							loading={loading}
							columns={orderRozetkaColumn()}
							dataSource={products}
							expandedRowRender={record => (
								<OrderProductCard>
									<Row gutter={50}>
										<Col span={24} md={16} className="order-card-wrapper">
											<OrderCard record={record} />
										</Col>
									</Row>
								</OrderProductCard>
							)}
							onChange={e => handleChangeTable(e, '1')}
						/>
					</TabPane>
					<TabPane tab="Успешно завершены" key="2">
						<Table
							{...config}
							scroll={{ x: 1080 }}
							loading={loading}
							columns={orderRozetkaColumn()}
							dataSource={products}
							// onChange={e => handleChangeTable(e, '3')}
						/>
					</TabPane>
					<TabPane tab="Неуспешно завершены" key="3">
						<Table
							{...config}
							scroll={{ x: 1080 }}
							loading={loading}
							columns={orderRozetkaColumn()}
							dataSource={products}
							// onChange={e => handleChangeTable(e, '3')}
						/>
					</TabPane>
				</Tabs>
			</TableWrapper>
		</Card>
	);
};

export default Pickup;
