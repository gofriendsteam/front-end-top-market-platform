import React, { useEffect, useMemo, useState } from 'react';
import {
	Select,
	AutoComplete,
	Form,
	Input,
	Row,
	Col,
	notification,
} from 'antd';
import { getPartnerProducts } from '../../../../utils/api/productsActions';
import {
	getWarehouseByCity,
	getСities,
	getTypes,
} from '../../../../utils/api/requestNovaPoshta';
import SuperSelect from 'antd-virtual-select';
import ProductCard from './ProductCard';
import { CreateOrderWrapper } from './CreateOrderWrapper.style';
import { Card } from '../../../Shared/Styles/Card';
import CustomButton from '../../../Shared/Button/Button';
import { createOrder } from '../../../../utils/api/ordersAction';
import { useOrderPCard } from '../../../Containers/useOrderPCard';
import { shouldDeleteError } from '../../../../helpers/functions';

// const SelectOption = Select.Option;
const { Option } = AutoComplete;

const CreateForm = ({ form }) => {
	const [products, setProducts] = useState([]);
	const [loading, setLoading] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(null);
	const [cities, setcities] = useState([]);
	const [selectedCity, setSelectedCity] = useState(undefined);
	const [warehouses, setWarehouses] = useState([]);
	const [selectedWarehouse, setSelectedWarehouse] = useState(undefined);
	const [error, setError] = useState(null);
	const [submitLoading, setSubmitLoading] = useState(false);
	const [autoCstate, setAutoCState] = useState('');

	const [
		inputNumber,
		seQuantity,
		deleteCart,
		resetProductsCart,
	] = useOrderPCard(selectedProduct);

	const productChildren = useMemo(
		() => products.map(({ id, name }) => <Option key={id}>{name}</Option>),
		[products],
	);

	const getCities = async () => {
		const {
			data: { data },
		} = await getСities();

		const renamed = data.map(({ DescriptionRu, Ref }) => (
			<Option city={DescriptionRu} key={Ref}>
				{DescriptionRu}
			</Option>
		));

		setcities(renamed);
	};
	const getWareHouse = async ciyRef => {
		const {
			data: { data },
		} = await getWarehouseByCity(ciyRef);

		const renamed = data.map(
			({ Ref, DescriptionRu, Number, ShortAddressRu }) => (
				<Option
					value={Ref}
					number={Number}
					shortName={ShortAddressRu}
					key={Ref}
				>
					{DescriptionRu}
				</Option>
			),
		);
		setWarehouses(renamed);
	};

	const selectProduct = productId => {
		const foundProduct = products.find(({ id }) => id === +productId);
		setAutoCState('');
		setSelectedProduct(foundProduct);
	};

	const handleSearchProducts = async value => {
		shouldDeleteError(error, 'products', setError);
		setAutoCState(value);
		setLoading(true);
		try {
			const { results } = await getPartnerProducts(`?name=${value}&in_stock=${true}`);
			setProducts(results);
			setLoading(false);
			// console.log('', results);
		} catch (e) {
			console.log('Error', e);
			setLoading(false);
		}
	};

	useEffect(() => {
		getCities();
	}, []);

	useEffect(() => {
		selectedCity && getWareHouse(selectedCity);
	}, [selectedCity]);

	const selectCity = (city, { key }) => {
		shouldDeleteError(error, 'city', setError);
		setSelectedCity(key);
		setSelectedWarehouse(undefined);
	};
	const selectWarehouse = (warehouse, { key }) => {
		shouldDeleteError(error, 'warehouse', setError);
		setSelectedWarehouse(key);
	};
	const handleWarehouseSearch = () => {
		shouldDeleteError(error, 'warehouse', setError);
	};

	const handleCitySearch = e => {
		shouldDeleteError(error, 'city', setError);
	};
	const resetForm = () => {
		setError(null);
		form.resetFields();
		setSelectedWarehouse(undefined);
		setWarehouses([]);
		setSelectedCity(undefined);
		setSelectedProduct(null);
		setProducts([]);
		resetProductsCart();
	};
	const myValidate = () => {
		let error = {};
		if (!selectedCity) {
			error.city = 'Выберите город!';
		}
		if (!selectedWarehouse) {
			error.warehouse = 'Выберите отделение!';
		}
		if (inputNumber.length === 0) {
			error.products = 'Выберите хотя бы один товар!';
		}
		return error;
	};
	const handleSubmit = e => {
		e.preventDefault();

		form.validateFieldsAndScroll((err, values, ...rest) => {
			console.log('error', rest);
			const errors = myValidate();
			const isValid = Object.keys(errors).length > 0;

			if (isValid) {
				setError(errors);
				return;
			}

			if (!err) {
				setSubmitLoading(true);
				const {
					props: { city },
				} = cities.find(city => city.key == selectedCity);
				const {
					props: { number, shortName },
				} = warehouses.find(warehouse => warehouse.key == selectedWarehouse);
				const sendItems = inputNumber.map(({ id, mycount }) => ({
					productId: id,
					quantity: mycount,
				}));

				const data = {
					items: sendItems,
					userPhone: values.phone,
					delivery: {
						deliveryServiceName: 'Новая Почта',
						recipientTitle: values.username,
						placeStreet: shortName,
						placeNumber: number,
						city,
					},
				};
				createOrder(data)
					.then(response => {
						resetForm();

						notification.success({
							message: 'Заказ был успешно создан!',
						});

						setSubmitLoading(false);
					})
					.catch(err => {
						notification.error({
							message: err.message,
						});
						setSubmitLoading(false);
					});
			}
		});
	};

	return (
		<CreateOrderWrapper>
			<Card>
				<Form onSubmit={handleSubmit}>
					<Row gutter={50}>
						<Col span={24} lg={12}>
							<Form.Item
								label="Выберите товар:"
								hasFeedback
								help={error && error.products}
								validateStatus={
									loading
										? 'validating'
										: error && error.products
										? 'error'
										: ''
								}
							>
								<AutoComplete
									allowClear
									backfill
									value={autoCstate}
									style={{ width: '100%' }}
									onSearch={handleSearchProducts}
									placeholder="Введите название товара"
									onSelect={selectProduct}
								>
									{productChildren}
								</AutoComplete>
							</Form.Item>
							<Form.Item label="Ф.И.О.">
								{form.getFieldDecorator('username', {
									rules: [
										{
											required: true,
											message: 'Заполните поле с ФИО!',
										},
									],
								})(<Input placeholder="Пример: Зорян Олександ" />)}
							</Form.Item>
							<Form.Item label="Номер телефона">
								{form.getFieldDecorator('phone', {
									rules: [
										{
											required: true,
											message: 'Введите телефонный номер!',
										},
										{
											pattern: new RegExp(
												/^(\+?\d{2})?-?(\d{3,4})-?(\d{3,4})-?(\d{2,3})-?(\d{2,3})$/,
											),
											message: 'Неверный формат E-mail!',
										},
									],
								})(<Input placeholder="Пример: +380982991802" />)}
							</Form.Item>

							<div className="select-block">
								<Form.Item
									label="Выберите город:"
									hasFeedback
									help={error && error.city}
									validateStatus={error && error.city ? 'error' : ''}
								>
									<SuperSelect
										className="select city"
										style={{ width: '100%' }}
										filterOption
										showSearch
										value={selectedCity}
										placeholder="Выберите город"
										onSearch={handleCitySearch}
										onChange={selectCity}
									>
										{cities}
									</SuperSelect>
								</Form.Item>
							</div>
							<div className="select-block">
								<Form.Item
									label="Выберите отделение:"
									hasFeedback
									help={error && error.warehouse}
									validateStatus={error && error.warehouse ? 'error' : ''}
								>
									<Select
										className="select warehouse"
										style={{ width: '100%' }}
										value={selectedWarehouse}
										onSearch={handleWarehouseSearch}
										placeholder="Выберите отделение Новой почты"
										onChange={selectWarehouse}
										disabled={!selectedCity}
									>
										{warehouses}
									</Select>
								</Form.Item>
							</div>
						</Col>
						<Col span={24} lg={12}>
							<ProductCard
								values={inputNumber}
								seQuantity={seQuantity}
								deleteCart={deleteCart}
								products={products}
								selectedProduct={selectedProduct}
							/>
						</Col>
					</Row>
					<CustomButton
						loading={submitLoading}
						title="Создать заказ"
						type="primary"
						htmlType="submit"
					/>
				</Form>
			</Card>
		</CreateOrderWrapper>
	);
};
const CreateOrder = Form.create({ name: 'createOrder' })(CreateForm);

export default CreateOrder;
