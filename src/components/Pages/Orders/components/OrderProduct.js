import React, { useState } from 'react';
import styles from '../Orders.module.css';
import { Button, Popover, Timeline, Tooltip, Row, Col } from 'antd';
import moment from 'moment';
import { OrderProductCard } from './OrderProductCard.styles.js';
import CustomButton from '../../../Shared/Button/Button';
import { Link } from 'react-router-dom';
import CreateTTN from './CreateTTN';
import OrderCard from '../../../Blocks/Orders/OrderCard';

const OrderProduct = ({
	handleSendtoNewContractor,
	record,
	getOrders1,
	handlePassToContractor,
}) => {
	if (!record.rozetkaId)
		return (
			<div style={{ fontSize: 25 }}>Заказы из Моего магазина в Разработке!</div>
		);
	return (
		<OrderProductCard>
			<Row gutter={40}>
				<Col
					span={24}
					md={16}
					className='order-card-wrapper'
				>
					<OrderCard record={record}/>
				</Col>
				<Col span={24} md={8}>
					<div className="order-actions">
						<Tooltip
							title={
								!record.passedToContractor
									? 'Нету закрепленого поставщика за этим товаром'
									: 'Заказу уже присвоен ТТН'
							}
							trigger={
								record.ttn || !record.passedToContractor ? 'hover' : 'none'
							}
						>
							<CustomButton
								title="Отправить поставщику"
								type="primary"
								// className="createTTN_mainButton sendToContractor"
								onClick={() => handlePassToContractor(record.id)}
								disabled={record.ttn || !record.passedToContractor}
							/>
						</Tooltip>
						{!record.ttn && (
							<Tooltip
								title="Заказу уже присвоен ТТН"
								trigger={record.ttn ? 'hover' : 'none'}
							>
								<Button
									type="primary"
									onClick={() => handleSendtoNewContractor(record)}
								>
									<Link to="/admin/sendsupplier">
										Отправить товар другому поставщику
									</Link>
								</Button>
							</Tooltip>
						)}
						<CreateTTN
							// buttonName="Отправить самому"
							order={record}
							refreshData={getOrders1}
							// disabled={!this.props.user.apiKey}
						/>
					</div>
				</Col>
			</Row>
		</OrderProductCard>
	);
};

export default OrderProduct;
