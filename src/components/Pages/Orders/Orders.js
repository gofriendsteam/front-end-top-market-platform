import React  from 'react';
// import styles from './Orders.module.css';
import { Tabs } from 'antd';
import OrdersFromRozetka from './components/OrdersFromRozetka';
import { Title } from '../../Shared/Styles/title.styles';
import { Card } from '../../Shared/Styles/Card';
import { OrdersWrapper } from './OrdersWrapper.styles';
import { TableWrapper } from '../../Shared/Styles/MainWrapper';
import { toogleModal } from '../../../store/actions/modal';
// import CustomButton from '../../Shared/Button/Button';
import Pickup from './components/Pickup';
import { useDispatch } from 'react-redux';
import CreateOrder from './components/CreateOrder';

const TabPane = Tabs.TabPane;

function callback(key) {
	console.log(key);
}

const Orders = () => {
	const dispatch = useDispatch();

	const createNewOrder = () => {
		dispatch(
			toogleModal({
				isOpen: true,
				type: 'Create',
				component: CreateOrder,
				data: {
					title: 'Создание заказа',
				},
			}),
		);
	};

	return (
		<OrdersWrapper>
			<Title>Мои заказы</Title>
			<div>
				{/*<CustomButton*/}
				{/*	title="+ Создать заказ"*/}
				{/*	type="primary"*/}
				{/*	onClick={createNewOrder}*/}
				{/*/>*/}
				<Tabs onChange={callback} type="line" animated={false}>
					<TabPane tab="Заказы из Rozetka" key="1">
						<Card>
							<TableWrapper>
								<OrdersFromRozetka />
							</TableWrapper>
						</Card>
					</TabPane>

					<TabPane tab="Заказы из Top Market" key="2" disabled>
						Content of Tab Pane 2
					</TabPane>
					<TabPane tab="Заказы из Prom.ua" key="3" disabled>
						Content of Tab Pane 3
					</TabPane>
					<TabPane tab="Создать заказ" key="4">
						<CreateOrder/>
					</TabPane>
					<TabPane tab="Заказы на самовыкуп" key="5">
						<Pickup/>
					</TabPane>
					{/*<TabPane tab="+ Создать заказ" key="4">*/}
					{/*	Content of Tab Pane 3*/}
					{/*</TabPane>*/}
				</Tabs>
			</div>
		</OrdersWrapper>
	);
};

export default Orders;
