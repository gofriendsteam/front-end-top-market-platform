import styled from 'styled-components';
// import { palette } from 'styled-theme';

export const OrdersWrapper = styled.div`
	padding: 24px 0 24px 0;
	table tr.ant-table-expanded-row {
		background: #fff !important;
	}
`;
export const StatusWrapper = styled.div`
	
	.radio-wrapper {
		display: flex;
		flex-direction: column;
		label {
			border-top: 1px solid #ececec;

			padding: 10px 0;
			&:last-child {
				margin-bottom: 20px;
			}
		}
	}
	button {
		width: 100%;
	}
	.title {
		font-weight: 500;
		font-size: 17px;
		margin-bottom: 15px;
	}
`;
