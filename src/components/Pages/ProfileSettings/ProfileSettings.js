import React, { Component, Fragment } from 'react';
import { Tabs, Table, Checkbox, Form, notification } from 'antd';
import styles from './ProfileSettings.module.css';
import { toogleModal } from '../../../store/actions/modal';
import MainInfo from './MainInfo';
import {
	getProfile,
	updateProfile,
	changePassword,
} from '../../../utils/api/userActions';
import { connect } from 'react-redux';
import UserShopCabinet from './UserShopCabinet';

const TabPane = Tabs.TabPane,
	CheckboxGroup = Checkbox.Group,
	FormItem = Form.Item;
// Option = Select.Option;

const columns = [
	{
		title: 'Имя',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'ID',
		dataIndex: 'id',
		key: 'id',
	},
	{
		title: 'Прибыль',
		dataIndex: 'profit',
		key: 'profit',
	},
	{
		title: 'Дата регистрации',
		dataIndex: 'registrationDate',
		key: 'registrationDate',
	},
];

class ProfileSettings extends Component {
	state = {
		firstName: '',
		lastName: '',
		patronymic: '',
		email: '',
		webSite: '',
		phone: '',
		organizationalLegalFormOfTheCompany: '',
		organization: '',
		edpnou: '',
		vatPayerCertificate: '',
		bankName: '',
		mfi: '',
		checkingAccount: '',
		avatarImage: '',
		updateImage: false,
		emailNotifications: [],
		phoneNotifications: [],
	};

	handleChangeInput = ({ target: { name, value } }) => {
		this.setState({
			[name]: value,
		});
	};

	handleChangeCheckbox = (e, type) => {
		this.setState({
			[`${type}Notifications`]: e,
		});
		console.log(`${type}: ___ ${e}`);
	};

	onDrop = file => {
		this.getBase64(file[0], result => {
			this.setState({
				avatarImage: result,
				updateImage: true,
			});
		});
	};

	getBase64(file, cb) {
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function() {
			cb(reader.result);
		};
		reader.onerror = function(error) {
			console.log('Error: ', error);
		};
	}

	handleSaveProfile = async e => {
		e.preventDefault();

		let requestData = this.state;

		let emailNot = {},
			smsNot = {};

		this.state.emailNotifications.forEach(item => {
			emailNot[item] = true;
		});
		this.state.phoneNotifications.forEach(item => {
			smsNot[item] = true;
		});

		if (!this.state.updateImage) delete requestData.avatarImage;

		await this.props
			.updateProfile({
				...requestData,
				emailNotifications: emailNot,
				phoneNotifications: smsNot,
			})
			.then(() =>
				notification.success({
					message: 'Сохранено',
				}),
			);
	};

	async componentDidMount() {
		const res = await getProfile();
		let emailNot = [],
			smsNot = [];

		for (let key in res.emailNotifications) {
			if (res.emailNotifications[key] === true) {
				emailNot.push(key);
			}
		}

		for (let key in res.phoneNotifications) {
			if (res.phoneNotifications[key] === true) {
				smsNot.push(key);
			}
		}

		this.setState({
			...res,
			emailNotifications: emailNot,
			phoneNotifications: smsNot,
		});
	}

	render() {
		// const { userRole  } = this.props;
		return (
			<div className={styles.mainWrapper}>
				<Tabs type="line" animated={false}>
					<TabPane tab="Основные данные" key="1" className={styles.mainInfo}>
						<MainInfo
							{...this.state}
							handleChangeCheckbox={this.handleChangeCheckbox}
							toogleModal={this.props.toogleModal}
							onDrop={this.onDrop}
							handleChangeInput={this.handleChangeInput}
							handleSaveProfile={this.handleSaveProfile}
						/>
					</TabPane>

					<TabPane tab="Реферальная программа" key="2">
						<div className={styles.referralProgram}>
							<h5>Реферальная ссылка</h5>
							<div className={styles.copyLink}>
								<input type="text" value={'Находится в разработке'} disabled />
								<span>
									Отправьте ссылку вашим знакомым. После прохождения регистрации
									по этой ссылке новый пользователь станет вашим рефералом
								</span>
							</div>

							<div className={styles.table}>
								<Table columns={columns} />
							</div>
						</div>
					</TabPane>
					<TabPane tab="Кабинет Rozetka/Prom" key="3">
						<UserShopCabinet />
					</TabPane>
				</Tabs>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	userRole: state.user.role,
});

const mapDispatchToProps = dispatch => ({
	updateProfile: user => dispatch(updateProfile(user)),
	toogleModal: data => dispatch(toogleModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettings);
