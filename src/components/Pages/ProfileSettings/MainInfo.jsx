import React from 'react';
import defaultAvatar from '../../../img/avatar.png';
import styles from './ProfileSettings.module.css';
import Dropzone from 'react-dropzone';
import { Icon, Tooltip, Input, Checkbox } from 'antd';
import CustomButton from '../../Shared/Button/Button';
import ChangePasswordForm from '../../Blocks/ChangePasswordForm/ChangePasswordForm';

const CheckboxGroup = Checkbox.Group;
const emailOptions = [
	{ label: 'О новом заказе', value: 'newOrder' },
	{ label: 'Об изменении статуса ТТН', value: 'ttnChange' },
	{ label: 'О получении счета на оплату', value: 'orderPaid' },
	{ label: 'О получении отчета о продажах', value: 'salesReport' },
	{ label: 'О новом сообщении внутренней почты', value: 'newMessage' },
	{ label: 'Об отмене заказа', value: 'cancelOrder' },
];

const MainInfo = ({
	avatarImage,
	bankName,
	checkingAccount,
	mfi,
	vatPayerCertificate,
	edpnou,
	organization,
	organizationalLegalFormOfTheCompany,
	firstName,
	lastName,
	patronymic,
	email,
	phone,
	webSite,
	handleSaveProfile,
	handleChangeInput,
	onDrop,
	toogleModal,
	handleChangeCheckbox,
	emailNotifications,
}) => {
	return (
		<div>
			<form className={styles.userMainInfo} onSubmit={handleSaveProfile}>
				<div className={styles.formInputs}>
					<div>
						<label>Имя</label>
						<Input
							type="text"
							name="firstName"
							value={firstName || ''}
							onChange={handleChangeInput}
							size='large'
						/>
					</div>
					<div>
						<label>Фамилия</label>
						<Input

							type="text"
							name="lastName"
							value={lastName || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>Отчество</label>
						<Input
							type="text"
							name="patronymic"
							value={patronymic || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>E-mail</label>
						<Input
							type="email"
							name="email"
							value={email || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>Телефон</label>
						<Input
							type="tel"
							name="phone"
							value={phone || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>Веб-сайт</label>
						<Input
							type="text"
							name="webSite"
							placeholder="https://example.com.ua"
							value={webSite || ''}
							onChange={handleChangeInput}
						/>
					</div>

					<h2>Юридические данные</h2>

					<div>
						<label>Организационно-правовая форма предприятия</label>
						<Input
							onChange={handleChangeInput}
							name="organizationalLegalFormOfTheCompany"
							value={organizationalLegalFormOfTheCompany}
						/>
					</div>
					<div>
						<label>Организация</label>
						<Input
							type="text"
							name="organization"
							value={organization || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>ЕДРПОУ</label>
						<Input
							type="text"
							name="edpnou"
							value={edpnou || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>Свидетельства плательщика НДС</label>
						<Input
							type="text"
							name="vatPayerCertificate"
							value={vatPayerCertificate || ''}
							onChange={handleChangeInput}
						/>
					</div>

					<h2>Платежная информация</h2>

					<div>
						<label>Название банка</label>
						<Input
							type="text"
							name="bankName"
							value={bankName || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>МФО</label>
						<Input
							type="text"
							name="mfi"
							value={mfi || ''}
							onChange={handleChangeInput}
						/>
					</div>
					<div>
						<label>Рассчетный счет</label>
						<Input
							type="text"
							name="checkingAccount"
							value={checkingAccount || ''}
							onChange={handleChangeInput}
						/>
					</div>
				</div>

				<div className={styles.userInfo}>
					<div className={styles.ChangeAvatar}>
						<div className={styles.userAvatar}>
							<img src={avatarImage ? avatarImage : defaultAvatar} alt="" />
							<Dropzone onDrop={onDrop} accept=".png, .svg, .jpg, .jpeg">
								{({ getRootProps, getInputProps }) => (
									<div {...getRootProps({ className: 'dropzone' })}>
										<input {...getInputProps()} />
										<button className={styles.uploadBtn}>
											<Icon type="camera" />
										</button>
									</div>
								)}
							</Dropzone>
						</div>
						<div className={styles.userAvatarInfo}>
							<h3>Изменить аватар</h3>
							<span>Размер аватара должен быть не меньше 150х150 пикселей</span>
							<CustomButton
								type="primary"
								className={styles.btnPrimary}
								onClick={() =>
									toogleModal({
										isOpen: true,
										data: {
											title: 'Изменить пароль',
										},
										component: ChangePasswordForm,
										type: '',
									})
								}
								title="Изменить пароль"
							/>
						</div>
					</div>

					<div className={styles.EmailNotifications}>
						<h3>Уведомления на E-mail</h3>

						<Tooltip title="Находится в разработке">
							<CheckboxGroup
								options={emailOptions}
								value={emailNotifications}
								onChange={e => handleChangeCheckbox(e, 'email')}
							/>
						</Tooltip>
					</div>

					<CustomButton
						type="primary"
						className={styles.save}
						htmlType="submit"
						title="Сохранить"
					/>
				</div>
			</form>
		</div>
	);
};

export default MainInfo;
