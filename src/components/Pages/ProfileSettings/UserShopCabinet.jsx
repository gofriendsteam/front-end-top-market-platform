import React, { useState, useEffect } from 'react';
import styles from './ProfileSettings.module.css';
import {
	getRozetkaData,
	updateRozetkaData,
} from '../../../utils/api/userActions';
import { notification, Input } from 'antd';
import CustomButton from "../../Shared/Button/Button";

const UserShopCabinet = () => {
	const [rozetka, setRozetkaData] = useState({
		rozetkaUsername: '',
		rozetkaPassword: '',
	});

	const handleChange = e => {
		setRozetkaData({ ...rozetka, [e.target.name]: e.target.value });
	};
	const updateHandler = data => {
		if (!data.valid) {
			notification.error({
				message: 'Неверный учетные данные!',
			});
		} else {
			notification.success({
				message: 'Аккаунт Rozetka был подключен!',
			});
		}
	};
	const handleSubmitRozetka = () => {
		updateRozetkaData(rozetka)
			.then(updateHandler)
			.catch(err => console.log(err));
	};
	useEffect(() => {
		getRozetkaData()
			.then(data => setRozetkaData(data))
			.catch(err => console.log(err));
	}, []);

	return (
		<div className={styles.referralProgram}>
			<h2>Ваши учетнные данные в Rozetka и Prom</h2>
			<div className={styles.shopAccountWr}>
				<div className={styles.shopsaccaunts}>
					<h3>Аккаунт Rozetka</h3>
					<div>
						<label>Логин</label>
						<Input
							type="password"
							name="rozetkaUsername"
							value={rozetka.rozetkaUsername || ''}
							onChange={handleChange}
						/>
					</div>
					<div>
						<label>Пароль</label>
						<Input
							type="password"
							name="rozetkaPassword"
							value={rozetka.rozetkaPassword || ''}
							onChange={handleChange}
						/>
					</div>

					<CustomButton title='Обновить' className={styles.btnPrimary} type='primary' onClick={handleSubmitRozetka}>

					</CustomButton>
				</div>
				{/*<div className={styles.shopsaccaunts}>*/}
				{/*	<h3>Аккаунт Prom (В разработке!)</h3>*/}
				{/*	<div>*/}
				{/*		<label>Логин</label>*/}
				{/*		<Input*/}
				{/*			type="password"*/}
				{/*			name="rozetkaUsername"*/}
				{/*			// value={rozetkaUsername || ''}*/}
				{/*			// onChange={handleChangeInput}*/}
				{/*		/>*/}
				{/*	</div>*/}
				{/*	<div>*/}
				{/*		<label>Пароль</label>*/}
				{/*		<Input*/}
				{/*			type="password"*/}
				{/*			name="rozetkaPassword"*/}
				{/*			// value={rozetkaPassword || ''}*/}
				{/*			// onChange={handleChangeInput}*/}
				{/*		/>*/}
				{/*	</div>*/}
				{/*</div>*/}
			</div>
		</div>
	);
};
export default UserShopCabinet;
