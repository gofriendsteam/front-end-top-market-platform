import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { TableWrapper, MainLayout } from '../../Shared/Styles/MainWrapper';
import { Card } from '../../Shared/Styles/Card';
import { Title } from '../../Shared/Styles/title.styles';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import AllProducts from './components/AllProducts';
import RozetkaProducts from './components/RozetkaProducts';
import {
	getPartnerProducts,
	getYmlProducts2,
} from '../../../utils/api/productsActions';

const TabPane = Tabs.TabPane;

const NewMyProducts = () => {
	const [tabKey, setKey] = useState(1);
	const [count, setCounts] = useState({
		allProductsCount: 0,
		ymlProductsCount: 0,
	});

	const fetch = async () => {
		const [allProducts, ymlProducts] = await Promise.all([
			getPartnerProducts(),
			getYmlProducts2('rozetka'),
		]);

		setCounts({
			allProductsCount: allProducts.count,
			ymlProductsCount:
				ymlProducts.count,
		});
	};
	useEffect(() => {
		fetch();
	}, []);
	const handleChangeTabs = key => setKey(key);
	return (
		<MainLayout>
			<div className="my-product-header">
				<Title>Мои товары</Title>
				<CategoryList />
			</div>
			<Tabs type="line" animated={false} onChange={handleChangeTabs}>
				<TabPane
					tab={`Товары на продажу - ${count.allProductsCount} шт.`}
					key="1"
				>
					<Card>
						<TableWrapper>
							<AllProducts key={tabKey} />
						</TableWrapper>
					</Card>
				</TabPane>
				<TabPane tab={`Товары Rozetka - ${count.ymlProductsCount} шт.`} key="2">
					<Card>
						<TableWrapper>
							<RozetkaProducts key={tabKey} />
						</TableWrapper>
					</Card>
				</TabPane>
			</Tabs>
		</MainLayout>
	);
};

export default NewMyProducts;
