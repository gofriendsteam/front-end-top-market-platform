import React, { Component } from 'react';
import { connect } from 'react-redux';
import { MyProductColumn } from '../../../constants/TableColumns/TabColumns';
import styles from './MyProducts.module.css';
import 'antd/dist/antd.css';
import { Table, Tabs, Modal, notification, Input, Icon } from 'antd';
import { toogleModal } from '../../../store/actions/modal';
import ImortYmlProducts from '../../Blocks/ImportExport/ImortYmlProducts';
import EditSelectedProducts from '../../Blocks/EditCreateProduct/EditSelectedProducts';
// import PriceListTable from '../../Blocks/PriceListTable/PriceListTable';
// import InactiveGoodsTable from '../../Blocks/InactiveGoodsTable/InactiveGoodsTable';
import NewProduct from '../../Blocks/EditCreateProduct/NewProduct';
import {
	uploadXls,
	getAllCategories,
	getPartnerProducts,
	removePartnerProduct,
	generateYml,
	addProductsToYml,
	deleteProductsFromYml,
	addProductToMyStore,
	// getMySiteProducts,
} from '../../../utils/api/productsActions';
import { putHistory } from '../../../utils/api/history';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import ImportExportProducts from '../../Blocks/ImportExport/ImportExportProducts';
import YmlProductsList from './components/YmlProductsList';
import { Title } from '../../Shared/Styles/title.styles';
import { MainLayout, TableWrapper } from '../../Shared/Styles/MainWrapper';
import CustomButton from '../../Shared/Button/Button';
import { Card } from '../../Shared/Styles/Card';
import Filters from '../../Blocks/Filters/Filters';
import _ from 'lodash';
import Preloading from '../../Shared/Loader/TablePreloader';

const TabPane = Tabs.TabPane;

class MyProducts extends Component {
	state = {
		products: [],
		product: {},
		selectedRowKeys: [],
		categories: [],
		allProductsmodifyOpen: false,
		filters: {
			category_id: '',
			name: '',
			vendor_code: '',
			min_price: '',
			max_price: '',
			brand: '',
			in_stock: '',
			user_id: '',
		},
		count: 0,
		pageSize: 10,
		currentPage: 1,
		tabKey: '1',
		loading: false,
	};
	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.user.selectedCategory !== this.state.category_id) {
			this.setState(
				{
					filters: {
						...this.state.filters,
						category_id: nextProps.user.selectedCategory,
					},
					currentPage: 1,
				},
				() => this.getMyProducts(),
			);
		}
	}
	shouldComponentUpdate(props, state) {
		if (state.tabKey === '1' && state.tabKey !== this.state.tabKey) {
			this.getMyProducts();
		}
		return true;
	}
	async componentDidMount() {
		this.props.toHistory(this.props.history);
		this.getMyProducts();

		const res = await getAllCategories();
		this.setState({
			categories: res,
		});
	}
	togglePreloader = loading => this.setState({ loading });
	getMyProducts = async () => {
		this.togglePreloader(true);
		const {
			currentPage,
			pageSize,
			filters: {
				category_id,
				name,
				brand,
				in_stock,
				vendor_code,
				min_price,
				max_price,
				user_id,
			},
		} = this.state;

		const urlParams = [
			category_id ? `&category_id=${category_id}` : '',
			name ? `&name=${name}` : '',
			brand ? `&brand=${brand}` : '',
			in_stock ? `&in_stock=${in_stock}` : '',
			vendor_code ? `&vendor_code=${vendor_code}` : '',
			min_price ? `&min_price=${min_price}` : '',
			max_price ? `&max_price=${max_price}` : '',
			user_id ? `&user_id=${user_id}` : '',
		];

		const url = `?page_size=${pageSize}&page=${currentPage +
			urlParams.join('')}`;
		try {
			const all = await getPartnerProducts(url);

			this.setState({
				products: all.results,
				count: all.count,
				loading: false,
			});
		} catch (e) {
			this.togglePreloader(false);
		}
	};
	//
	// handleSelectCategory = category => {
	// 	this.setState(
	// 		{
	// 			filters: {
	// 				...this.state.filters,
	// 				category_id: category.key,
	// 			},
	// 		},
	// 		() => this.getMyProducts(),
	// 	);
	// };

	handleChangeTable = pagination => {
		this.setState(
			{
				currentPage: pagination.current,
				pageSize: pagination.pageSize,
				selectedRowKeys: [],
			},
			() => this.getMyProducts(),
		);
	};

	handleChangeFilters = ({ target: { name, value } }) => {
		this.setState(
			{
				filters: {
					...this.state.filters,
					[name]: value,
				},
			},
			() => this.getMyProducts(),
		);
	};
	handleTabsChange = key => {
		this.setState({ tabKey: key });
	};
	handleRemoveProducts = async () => {
		let idArr = [];
		await this.state.selectedRowKeys.forEach(item => {
			idArr.push(this.state.products[item].id);
		});

		await removePartnerProduct({ productListIds: idArr });

		this.handleUpdate();
	};
	handleOpenEditAllProducts = val => {
		this.setState({
			allProductsmodifyOpen: val,
		});
	};
	handleOpenWindow = product => {
		this.setState({
			product,
		});
	};

	handleUpdate = () => {
		this.getMyProducts();
		// this.getCategories();
		this.handleUpdateProduct();
	};
	handleUpdateProduct = () => {
		this.setState({
			selectedRowKeys: [],
			product: {},
		});
	};

	onSelectChange = selectedRowKeys => {
		this.setState({ selectedRowKeys });
		// () => this.setState({exportValue: this.state.selectedRowKeys.length ? 7 : 2})
	};

	handleAddToYml = async type => {
		const productIds = this.state.selectedRowKeys.map(
			item => this.state.products[item].id,
		);

		this.setState({ selectedRowKeys: [] });

		await addProductsToYml(
			{
				ymlType: type,
				productIds,
			},
			type,
		);
		this.handleCloseYmlConfirmClose();
		this.getMyProducts();
		notification.success({ message: 'Товар был добавлен в YML Rozetka!' });
	};
	editAll = () => {
		this.handleOpenEditAllProducts(true);
	};
	sendToMyStore = async () => {
		let sendingIds = [];
		this.state.products.forEach((product, idx) => {
			if (this.state.selectedRowKeys.includes(idx)) {
				sendingIds.push(product.id);
			}
		});
		const res = await addProductToMyStore({ productListIds: sendingIds });
		this.setState({
			selectedRowKeys: [],
		});
		notification.success({
			message: 'Товар добавлен в мой магазин!',
		});
	};
	trottledChangeFilters = _.throttle(this.handleChangeFilters, 200);
	handleCloseYmlConfirmOpen = () => {
		this.setState({ ymlConfirmVisible: true });
	};
	handleCloseYmlConfirmClose = () => {
		this.setState({ ymlConfirmVisible: false });
	};
	render() {
		console.log('-= MyProducts =-\nSTATE on render()', this.state);
		const {
			products,
			product,
			selectedRowKeys,
			loading,
			count,
			pageSize,
			currentPage,
			filters,
		} = this.state;
		const { toogleModal } = this.props;
		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange,
			hideDefaultSelections: true,
			onSelection: this.onSelection,
		};

		const config = {
			pagination: {
				pageSize: pageSize,
				pageSizeOptions: ['10', '20', '50', '500'],
				showSizeChanger: true,
				total: count,
				current: currentPage,
			},
		};

		return (
			<MainLayout>
				<div className="my-product-header">
					<Title>Мои товары</Title>
					<CategoryList />
				</div>

				<Tabs onChange={this.handleTabsChange} type="line" animated={false}>
					<TabPane tab={`Товары на продажу - ${products.length}`} key="1">
						<Card>
							<div className="controls">
								<CustomButton
									title="Добавить в YML"
									type="primary"
									disabled={!selectedRowKeys.length}
									onClick={this.handleCloseYmlConfirmOpen}
								/>

								<Modal
									visible={this.state.ymlConfirmVisible}
									title="Выберите в какой XML добавить ваш Товар."
									// onOk={this.handleCloseYmlConfirmOpen}
									onCancel={this.handleCloseYmlConfirmClose}
									footer={
										<div className={styles.xmlComfirmDiv}>
											<CustomButton
												title="Rozetka YML"
												type="primary"
												onClick={() => this.handleAddToYml('rozetka')}
												// disabled={!selectedRowKeys.length}
											/>
											<CustomButton
												title="Prom YML"
												type="primary"
												disabled
												onClick={() => this.handleAddToYml('prom')}
												// disabled={!selectedRowKeys.length}
											/>
										</div>
									}
								/>
								<CustomButton
									title="Добавить в Мой магазин"
									type="primary"
									disabled={!selectedRowKeys.length}
									onClick={this.sendToMyStore}
									// disabled={!selectedRowKeys.length}
								/>
								<CustomButton
									title="Редактировать все"
									type="primary"
									disabled={selectedRowKeys.length < 2}
									onClick={this.editAll}
								/>
								<CustomButton
									type="primary"
									// disabled={!selectedRowKeys.length}
									title="Импортировать Yml"
									onClick={() => {
										toogleModal({
											isOpen: true,
											type: 'import',
											component: ImortYmlProducts,
											data: {
												title: 'Импорт Yml файлов',
												width: 800,
												updateProducts: this.handleUpdate,
											},
										});
									}}
								/>

								<CustomButton
									type="danger"
									disabled={!selectedRowKeys.length}
									title="Удалить товары"
									onClick={this.handleRemoveProducts}
								/>

								<div className="total">Товаров: {count} шт.</div>
							</div>
							<Filters
								values={{ ...filters }}
								onChange={this.trottledChangeFilters}
								handleChangeFilters={this.handleChangeFilters}
							/>
							<TableWrapper>
								<Table
									{...config}
									rowSelection={rowSelection}
									columns={MyProductColumn(this.handleOpenWindow)}
									dataSource={products}
									scroll={{ x: 1370 }}
									onChange={this.handleChangeTable}
									size="small"
									loading={{
										spinning: loading,
										indicator: <Preloading />,
									}}
								/>
							</TableWrapper>
							<div className={styles.bottomBtns}>
								<div className={styles.totalProductsBottom}>
									Товаров: {count} шт.
								</div>
							</div>
						</Card>
					</TabPane>
					<TabPane tab={'Товары в YML'} key="2">
						<YmlProductsList tabKey={this.state.tabKey} />
					</TabPane>
				</Tabs>
				<EditSelectedProducts
					selectedRowKeys={selectedRowKeys.map(
						item => this.state.products[item].id,
					)}
					update={this.state.allProductsmodifyOpen}
					closeModal={this.handleOpenEditAllProducts}
					onUpdate={this.handleUpdate}
				/>
				<NewProduct
					onUpdate={this.handleUpdate}
					onUpdateProduct={this.handleUpdateProduct}
					product={product}
					update={product.id ? true : false}
				/>
			</MainLayout>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	toHistory: history => dispatch(putHistory(history)),
	toogleModal: data => dispatch(toogleModal(data)),
	// selectedCategory: category => dispatch(selectedCategory(category)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyProducts);
