import React, { useState, useEffect } from 'react';
import { MyProductColumn } from '../../../../constants/TableColumns/TabColumns';
import { getYmlProducts } from '../../../../utils/api/productsActions';
import { Table } from 'antd';
import NewProduct from '../../../Blocks/EditCreateProduct/NewProduct';

const PromTab = ({ currentTab }) => {
	const [products, setproductsFromYml] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [pageSize, setpageSize] = useState('10');
	const [count, setCount] = useState(0);
	const [product, setProduct] = useState({});

	const getProducts = () =>
		currentTab === '1' &&
		getYmlProducts('rozetka').then(data => {
			setproductsFromYml(data.results);
			setCount(data.count);
		});

	const handleOpenWindow = product => {
		setProduct(product);
	};
	const handleUpdate = () => {
		getProducts();
	};
	const handleChangeTable = pagination => {
		setCurrentPage(pagination.current);
		setpageSize(pagination.pageSize);
		getProducts();
	};
	const handleUpdateProduct = () => {
		setProduct({});
	};
	useEffect(() => {
		getProducts();
		// if (productsProm.length !== 0 && productsRozetka !== 0) return;
	}, [currentTab]);
	const config = {
		pagination: {
			pageSize: pageSize,
			pageSizeOptions: ['10', '20', '50', '500'],
			showSizeChanger: true,
			total: count,
			current: currentPage,
		},
	};
	return (
		<div>
			<h2>Товары Prom</h2>
			<Table
				{...config}
				// rowSelection={rowSelection}
				columns={MyProductColumn(handleOpenWindow)}
				dataSource={products}
				onChange={handleChangeTable}
			/>
			<NewProduct
				onUpdate={handleUpdate}
				onUpdateProduct={handleUpdateProduct}
				product={product}
				update={product.id ? true : false}
			/>
		</div>
	);
};
export default PromTab;
