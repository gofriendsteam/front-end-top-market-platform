import React, { useEffect, useState } from 'react';
import { getCategoriesById } from '../../../actions/productsActions';

const MyProductsSub = (props) => {
  const currentId = props.history.location.pathname.substring(
    props.history.location.pathname.lastIndexOf('/') + 1,
  );
  const categories = getCategoriesById(currentId).then((res) =>
    console.log(res),
  );
  const [selectedCategory, setCategory] = useState(categories);

  console.log(selectedCategory, currentId);
  return <div></div>;
};

export default MyProductsSub;
