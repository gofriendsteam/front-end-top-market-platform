import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { notification, Table } from 'antd';
import { useFilters } from '../../../Containers/useFilters';
import filters from '../../../../constants/filtersOptions';
import { useTable } from '../../../Containers/useTable';
import { getPartnerProducts } from '../../../../utils/api/productsActions';
import { MyProductColumn } from '../../../../constants/TableColumns/TabColumns';
import Filters from '../../../Blocks/Filters/Filters';
import CustomButton from '../../../Shared/Button/Button';
import { toogleModal } from '../../../../store/actions/modal';
import { useDispatch } from 'react-redux';
import Addtoyml from '../../../Blocks/AddToYml/Addtoyml';
import {
	addProductToMyStore,
	removePartnerProduct,
} from '../../../../utils/api/productsActions';
import EditSelectedProducts from '../../../Blocks/EditCreateProduct/EditSelectedProducts';
import ImortYmlProducts from '../../../Blocks/ImportExport/ImortYmlProducts';
import Total from '../../../Shared/Total';
import NewProduct from '../../../Blocks/EditCreateProduct/NewProduct';
import { useHistory } from 'react-router-dom';

const AllProducts = ({ key }) => {
	const {
		location: { state },
	} = useHistory();
	// console.log('', state);
	const [values, handleChangeInputs] = useFilters(
		state && state.contractorId
			? { ...filters.categories, user_id: state.contractorId }
			: filters.categories,
	);
	const [editingProduct, setProduct] = useState({});
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		resetRowKeys,
	] = useTable(getPartnerProducts, values);
	// [{
	// {
	// 	message: "",
	// 		date: "",
	// 	senderName: "",
	// 	senderEmail: "",
	// }
	// 	sender: {
	// 		id: 1,
	// 		name: 'asd',
	//
	// 	}
	// }]
	const dispatch = useDispatch();
	const selectedProducts = useMemo(
		() => selectedRowKeys.map(item => products[item].id),
		[selectedRowKeys],
	);
	const resetEditedProduct = () => setProduct({});
	const openImport = () => {
		dispatch(
			toogleModal({
				isOpen: true,
				type: 'import',
				component: ImortYmlProducts,
				data: {
					title: 'Импорт Yml файлов',
					width: 800,
					updateProducts: fetchData,
				},
			}),
		);
	};

	const sendToMyStore = async () => {
		await addProductToMyStore({ productListIds: selectedProducts });
		resetRowKeys();
		notification.success({
			message: 'Товар добавлен в мой магазин!',
		});
	};
	const removeProducts = async () => {
		await removePartnerProduct({ productListIds: selectedProducts });
		notification.success({ message: 'Товар был удален.' });
		fetchData();
		resetRowKeys();
	};
	const handleAddToYml = () =>
		dispatch(
			toogleModal({
				isOpen: true,
				type: 'yml',
				component: Addtoyml,
				data: {
					// width: 500,
					title: 'Выберите в какой XML добавить ваш Товар.',
					selectedIds: selectedProducts,
					update: fetchData,
				},
			}),
		);
	useEffect(() => {
		if (key == '1') {
			fetchData();
		}
	}, [key]);
	return (
		<Fragment>
			<div className="controls">
				<CustomButton
					title="Добавить в YML"
					type="primary"
					disabled={!selectedRowKeys.length}
					onClick={handleAddToYml}
				/>
				<CustomButton
					title="Добавить в Мой магазин"
					type="primary"
					disabled={!selectedRowKeys.length}
					onClick={sendToMyStore}
				/>
				<EditSelectedProducts
					selectedRowKeys={selectedProducts}
					onUpdate={fetchData}
				/>
				<CustomButton
					type="primary"
					title="Импортировать Yml"
					onClick={openImport}
				/>
				<CustomButton
					type="danger"
					disabled={!selectedRowKeys.length}
					title="Удалить товары"
					onClick={removeProducts}
				/>
				<Total count={count} />
			</div>
			<Filters
				values={values}
				onChange={handleChangeInputs}
				handleChangeFilters={handleChangeInputs}
			/>
			<NewProduct
				onUpdate={fetchData}
				onUpdateProduct={resetEditedProduct}
				product={editingProduct}
				update={editingProduct.id ? true : false}
			/>
			<Table
				{...config}
				rowSelection={rowSelection}
				columns={MyProductColumn(setProduct)}
				dataSource={products}
				scroll={{ x: 1370 }}
				loading={loading}
				onChange={handleChangeTable}
				size="small"
			/>
		</Fragment>
	);
};

export default AllProducts;
