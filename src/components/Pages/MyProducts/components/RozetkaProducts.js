import React, { useEffect, useState } from 'react';
import ExportYmlProducts from '../../../Blocks/ImportExport/ExportYmlProducts';
import { notification, Table } from 'antd';
import { MyProductColumn } from '../../../../constants/TableColumns/TabColumns';
import { useFilters } from '../../../Containers/useFilters';
import filters from '../../../../constants/filtersOptions';
import { useTable } from '../../../Containers/useTable';
import {
	getYmlProducts2,
	deleteProductsFromYml,
	getPartnerProducts,
} from '../../../../utils/api/productsActions';
import CustomButton from '../../../Shared/Button/Button';
import NewProduct from '../../../Blocks/EditCreateProduct/NewProduct';

const RozetkaProducts = ({ key }) => {
	const [url, values, handleChangeInputs] = useFilters(filters.categories);
	const [editingProduct, setProduct] = useState({});
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		resetRowKeys,
	] = useTable(getYmlProducts2, url, 'rozetka');
	useEffect(() => {
		if (key == "2") {
			fetchData();
		}
	}, [key]);
	console.log('tab2');
	const resetEditedProduct = () => setProduct({});
	const handleDeleteFromYml = async type => {
		const productIds = selectedRowKeys.map(item => products[item].id);

		await deleteProductsFromYml(
			{
				ymlType: type,
				productIds,
			},
			type,
		);
		notification.success({ message: 'Товар был удален.' });
		fetchData();
		resetRowKeys();
	};
	return (
		<div>
			<div className="ymlrozetka-head">
				<ExportYmlProducts productslength={products.length} />
				<CustomButton
					title="Удалить из YML"
					type="danger"
					onClick={() => handleDeleteFromYml('rozetka')}
					disabled={selectedRowKeys.length === 0}
				/>
			</div>
			<NewProduct
				onUpdate={fetchData}
				onUpdateProduct={resetEditedProduct}
				product={editingProduct}
				update={editingProduct.id ? true : false}
			/>
			<Table
				{...config}
				rowSelection={rowSelection}
				columns={MyProductColumn(setProduct)}
				dataSource={products}
				loading={loading}
				onChange={handleChangeTable}
				scroll={{ x: 1320 }}
			/>
		</div>
	);
};

export default RozetkaProducts;
