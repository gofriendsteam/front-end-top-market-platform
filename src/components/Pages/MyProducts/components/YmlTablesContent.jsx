import React, { useEffect, useState } from 'react';
import { Tabs, Table } from 'antd';
import { MyProductColumn } from '../../../helpers/TabColumns';
import { getYmlProducts } from '../../../actions/productsActions';

const YmlTablesContent = ({ type, currentTab }) => {
	const typeToFetch = type.toLowerCase();
	const [products, setproductsFromYml] = useState([]);
	const [count, setCount] = useState(0);
	console.log(type);
	useEffect(() => {
		// if (productsProm.length !== 0 && productsRozetka !== 0) return;
		currentTab === '1' &&
			typeToFetch === 'rozetka' &&
			getYmlProducts(typeToFetch).then(data => {
				setproductsFromYml(data.results);
				setCount(data.results);
			});
	}, [currentTab]);

	return (
		<div>
			<h2>Товары {type}</h2>
			<Table
				// rowSelection={rowSelection}
				columns={MyProductColumn()}
				dataSource={products}
			/>
		</div>
	);
};
export default YmlTablesContent;
