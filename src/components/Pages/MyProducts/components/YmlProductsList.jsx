import React, { useState } from 'react';
import { Tabs } from 'antd';
import PromTab from './PromTab';
import RozetkaTab from './RozetkaTab';
// import ImportExportProducts from '../../../Blocks/ImportExport/ImportExportProducts';
import { generateYml } from '../../../../utils/api/productsActions';
// import styles from '../MyProducts.module.css';
import ExportYmlProducts from '../../../Blocks/ImportExport/ExportYmlProducts';
import {Card} from "../../../Shared/Styles/Card"

const TabPane = Tabs.TabPane;

const YmlProductsList = ({ tabKey, closeModal}) => {
	const [currentTab, setTab] = useState('1');


	return (
		<Card>
			<ExportYmlProducts/>
			<Tabs
				type="line"
				animated={false}
				activeKey={currentTab}
				onChange={key => setTab(key)}
			>
				<TabPane tab={'Товары YML Rozetka'} key="1">
					<RozetkaTab   closeModal={closeModal} currentTab={currentTab} tabKey={tabKey} />
				</TabPane>
				<TabPane tab={'Товары YML Prom'} key="2" disabled>
					<PromTab type="Prom" currentTab={currentTab} />
				</TabPane>
			</Tabs>
		</Card>
	);
};
export default YmlProductsList;
