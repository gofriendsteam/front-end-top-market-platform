import React, { useState, useEffect } from 'react';
import { MyProductColumn } from '../../../../constants/TableColumns/TabColumns';
import {
	getYmlProducts,
	deleteProductsFromYml,
} from '../../../../utils/api/productsActions';
import { Table, notification } from 'antd';
import NewProduct from '../../../Blocks/EditCreateProduct/NewProduct';
import { TableWrapper } from '../../../Shared/Styles/MainWrapper';
import CustomButton from '../../../Shared/Button/Button';
import EditSelectedProducts from '../../../Blocks/EditCreateProduct/EditSelectedProducts';

const RozetkaTab = ({ currentTab, tabKey, closeModal }) => {
	const [products, setproductsFromYml] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [allProductsmodifyOpen, toogleModal] = useState(false);
	const [pageSize, setpageSize] = useState('10');
	const [count, setCount] = useState(0);
	const [product, setProduct] = useState({});
	const [selectedRowKeys, setRowKeys] = useState([]);

	const getProducts = () => {
		const url = `?page_size=${pageSize}&page=${currentPage}`;

		currentTab === '1' &&
			getYmlProducts('rozetka', url).then(data => {
				setproductsFromYml(data.results);
				setCount(data.count);
			});
	};

	const handleOpenWindow = product => {
		if (selectedRowKeys.length > 0) {
			toogleModal(true);
		} else {
			setProduct(product);
		}
	};
	const handleUpdate = () => {
		getProducts();
	};
	const handleChangeTable = pagination => {
		console.log(pagination);
		setCurrentPage(pagination.current);
		setpageSize(pagination.pageSize);
	};
	const handleUpdateProduct = () => {
		setProduct({});
	};
	const onSelectChange = selectedRowKeys => {
		setRowKeys(selectedRowKeys);
	};
	const handleDeleteFromYml = async type => {
		const productIds = selectedRowKeys.map(item => products[item].id);

		setRowKeys([]);

		await deleteProductsFromYml(
			{
				ymlType: type,
				productIds,
			},
			type,
		);
		getProducts();
		notification.success({ message: 'Товар был удален!' });
	};
	useEffect(() => {
		tabKey === '2' && getProducts();
	}, [currentPage, pageSize, currentTab, tabKey]);

	const config = {
		pagination: {
			pageSize: pageSize,
			pageSizeOptions: ['10', '20', '50', '500'],
			showSizeChanger: true,
			total: count,
			current: currentPage,
		},
	};
	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		// onSelection: this.onSelection,
	};
	return (
		<div>
			<div className="ymlrozetka-head">
				<h2>Товары Rozetka</h2>

				<CustomButton
					title="Удалить из YML"
					type="danger"
					onClick={() => handleDeleteFromYml('rozetka')}
					disabled={selectedRowKeys.length === 0}
				/>
			</div>
			<TableWrapper>
				<Table
					{...config}
					rowSelection={rowSelection}
					columns={MyProductColumn(handleOpenWindow)}
					dataSource={products}
					onChange={handleChangeTable}
					scroll={{ x: 1320 }}
				/>
			</TableWrapper>

			<NewProduct
				onUpdate={handleUpdate}
				onUpdateProduct={handleUpdateProduct}
				product={product}
				update={product.id ? true : false}
			/>
			<EditSelectedProducts
				selectedRowKeys={selectedRowKeys.map(item => products[item].id)}
				update={allProductsmodifyOpen}
				closeModal={toogleModal}
				onUpdate={handleUpdate}
			/>
		</div>
	);
};

export default RozetkaTab;
