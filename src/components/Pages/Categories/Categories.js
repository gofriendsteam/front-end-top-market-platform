import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Table, Spin, Icon, Input } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// import styles from './Categories.module.css';
import CustomButton from '../../Shared/Button/Button';
import {
	copyProducts,
	getAllCategories,
	getAllProducts,
} from '../../../utils/api/productsActions';
import { toogleModal } from '../../../store/actions/modal';
import { putHistory } from '../../../utils/api/history';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import { categoriesColumn } from '../../../constants/TableColumns/categoriesColumns';
import {
	MainLayout,
	TableWrapper,
} from '../../Shared/Styles/MainWrapper';
import { Title } from '../../Shared/Styles/title.styles';
import { Card } from '../../Shared/Styles/Card';
import Filters from '../../Blocks/Filters/Filters';
import _ from 'lodash';
import Preloading from '../../Shared/Loader/TablePreloader';


class Categories extends Component {
	state = {
		selectedRowKeys: [],
		products: [],
		categories: [],
		loading: false,
		filters: {
			category_id: '',
			name: '',
			vendor_code: '',
			min_price: '',
			max_price: '',
			brand: '',
			in_stock: '',
			independent_products: '',
		},

		count: 0,
		pageSize: 10,
		currentPage: 1,
	};

	onSelectChange = selectedRowKeys => {
		this.setState({ selectedRowKeys });
	};
	togglePreloader = loading => this.setState({ loading });
	getProducts = async () => {
		this.togglePreloader(true);
		const {
			currentPage,
			pageSize,
			filters: {
				category_id,
				name,
				brand,
				in_stock,
				vendor_code,
				min_price,
				max_price,
				independent_products,
			},
		} = this.state;
		const urlParams = [
			category_id ? `&category_id=${category_id}` : '',
			name ? `&name=${name}` : '',
			brand ? `&brand=${brand}` : '',
			in_stock ? `&in_stock=${in_stock}` : '',
			vendor_code ? `&vendor_code=${vendor_code}` : '',
			min_price ? `&min_price=${min_price}` : '',
			max_price ? `&max_price=${max_price}` : '',
			independent_products
				? `&independent_products=${independent_products}`
				: '',
		];

		const url = `?page_size=${pageSize}&page=${currentPage +
			urlParams.join('')}`;
		try {
			const res = await getAllProducts(url);

			this.setState({
				products: res.results,
				count: res.count,
				loading: false,
			});
		} catch (e) {
			this.togglePreloader(false);
		}
	};

	handleChangeTable = pagination => {
		this.setState(
			{
				currentPage: pagination.current,
				pageSize: pagination.pageSize,
			},
			() => this.getProducts(),
		);
	};

	handleCopyProducts = async () => {
		let arr = [];

		await this.state.selectedRowKeys.forEach(item => {
			arr.push(this.state.products[item].id);
		});

		await copyProducts({
			productListIds: arr,
		});

		this.getProducts();

		this.setState({
			selectedRowKeys: [],
		});
	};

	handleChangeFilters = ({ target }) => {
		console.log(target)
		if (!target) return;
		this.setState(
			{
				filters: {
					...this.state.filters,
					[target.name]: target.value,
				},
			},
			() => this.getProducts(),
		);
	};
	trottledChangeFilters = _.throttle(this.handleChangeFilters, 200);
	handleSelectCategory = category => {
		this.setState(
			{
				filters: {
					...this.state.filters,
					category_id: category.key,
				},
			},
			() => this.getProducts(),
		);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.user.selectedCategory !== this.state.category_id) {
			this.setState(
				{
					filters: {
						...this.state.filters,
						category_id: nextProps.user.selectedCategory,
					},
					currentPage: 1,
				},
				() => this.getProducts(),
			);
		}
	}

	async componentDidMount() {
		this.props.toHistory(this.props.history);
		this.props.history.location.state
			? this.setState(
					{
						...this.state,
						filters: {
							name: this.props.history.location.state.filters.name,
						},
					},
					() => this.getProducts(),
			  )
			: this.getProducts();

		const res = await getAllCategories();
		this.setState({
			categories: res,
		});
	}

	render() {
		const {
			selectedRowKeys,
			products,
			count,
			pageSize,
			currentPage,
			loading,
		} = this.state;

		const rowSelection = {
			selectedRowKeys,
			onChange: this.onSelectChange,
			hideDefaultSelections: true,
			onSelection: this.onSelection,
		};

		const config = {
			pagination: {
				pageSize: pageSize,
				pageSizeOptions: [10, 20, 50, 500],
				showSizeChanger: true,
				total: count,
				current: currentPage,
			},
		};

		return (
			<MainLayout>
				<div className="top-nav">
					<Title>Все товары</Title>
					<CategoryList />
					<Link to="/admin/instruction_sellers" className="how-to-link">
						Как добавить товар?
					</Link>
				</div>

				<Card>
					<div className="actions">
						<CustomButton
							disabled={selectedRowKeys.length === 0}
							type="primary"
							title="Добавить в мои товары"
							onClick={this.handleCopyProducts}
						/>

						<div className="total-price">Товаров: {count} шт.</div>
					</div>
					<Filters
						values={{ ...this.state.filters }}
						onChange={this.trottledChangeFilters}
						handleChangeFilters={this.handleChangeFilters}
					/>

					<TableWrapper>
						<Table
							{...config}
							rowSelection={rowSelection}
							columns={categoriesColumn}
							dataSource={products}
							scroll={{ x: 1100 }}
							loading={{
								spinning: loading,
								indicator: <Preloading/>,
							}}
							onChange={this.handleChangeTable}
							size="small"
						/>
					</TableWrapper>
					<div className="bottom-total-price">Товаров: {count} шт.</div>
				</Card>
			</MainLayout>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
});

const mapDispatchToProps = dispatch => ({
	toHistory: history => dispatch(putHistory(history)),
	toogleModal: data => dispatch(toogleModal(data)),
	// selectedCategory: category => dispatch(selectedCategory(category)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Categories);
