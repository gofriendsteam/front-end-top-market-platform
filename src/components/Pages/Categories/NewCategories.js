import React from 'react';
import { Table,notification } from 'antd';
import { categoriesColumn } from '../../../constants/TableColumns/categoriesColumns';
import { TableWrapper, MainLayout } from '../../Shared/Styles/MainWrapper';
import {
	copyProducts,
	getAllProducts,
} from '../../../utils/api/productsActions';
import { Card } from '../../Shared/Styles/Card';
import Filters from '../../Blocks/Filters/Filters';
import { useFilters } from '../../Containers/useFilters';
import { useTable } from '../../Containers/useTable';
import filters from '../../../constants/filtersOptions';
import { Title } from '../../Shared/Styles/title.styles';
import CategoryList from '../../Blocks/CategoryMenu/CategoryList';
import { Link } from 'react-router-dom';
import Total from '../../Shared/Total';
import CustomButton from '../../Shared/Button/Button';

const NewCategories = () => {
	const [values, handleChangeInputs] = useFilters(filters.categories);
	const [
		selectedRowKeys,
		rowSelection,
		config,
		count,
		products,
		loading,
		handleChangeTable,
		fetchData,
		resetRowKeys,
	] = useTable(getAllProducts, values);

	const handleAddToMyproducts = async () => {
		const copiedProducts = products
			.filter((product, idx) => selectedRowKeys.includes(idx))
			.map(({ id }) => id);

		await copyProducts({ productListIds: copiedProducts });
		notification.success({
			message: "Товар добавлен в 'Мои товары'",
		});
		fetchData();
		resetRowKeys();
	};
	console.log('PRODUCTS', products);
	return (
		<MainLayout>
			<div className="top-nav">
				<Title>Все товары</Title>
				<CategoryList />
				<Link to="/admin/instruction_sellers" className="how-to-link">
					Как добавить товар?
				</Link>
			</div>
			<Card>
				<div className="actions">
					<CustomButton
						disabled={selectedRowKeys.length === 0}
						type="primary"
						title="Добавить в мои товары"
						onClick={handleAddToMyproducts}
					/>

					<Total count={count} />
				</div>
				<Filters
					values={values}
					onChange={handleChangeInputs}
					handleChangeFilters={handleChangeInputs}
				/>
				<TableWrapper>
					<Table
						// rowKey={record => record.id}
						{...config}
						rowSelection={rowSelection}
						columns={categoriesColumn}
						dataSource={products}
						scroll={{ x: 900 }}
						loading={loading}
						onChange={handleChangeTable}
						size="small"
					/>
				</TableWrapper>
			</Card>
		</MainLayout>
	);
};

export default NewCategories;
