import React, { Component } from 'react';
import styles from './Instruction.module.css';
import { Title } from '../../Shared/Styles/title.styles';
import { Card } from '../../Shared/Styles/Card';
import img1 from '../../../img/allprodouct1.png';
import img2 from '../../../img/allprodouct2.png';
import img3 from '../../../img/allprodouct3.png';

class InstructionsForSellers extends Component {
	state = {
		categories: [],
	};

	render() {
		return (
			<div className={styles.main}>
				<Title pd="0 0 20px 0">Инструкция по добавлению товаров</Title>

				<Card>
					<p>
						Зайдите во вкладку «Все товары». Здесь находится полный каталог
						товаров поставщиков. Для вашего удобства вы можете фильтровать их по
						разным критериям (артикул, наличие, бренд, название, категория
						товара, розничная цена, топ-товары/товары со скидкой), а ещё выбрать
						интересующую вас категорию с помощью панели слева.
					</p>

					<div className="instruction-image">
						<img src={img1} alt="img1" />
					</div>

					<p>
						После того как вы определились с категорией или определенными
						товарами, выделите их с помощью галочки (поштучно или массово).
					</p>

					<p>
						Как только выделите товары, у вас активируется кнопка «Добавить в
						Мои товары». Нажмите её и все выделенные карточки попадут в ваш
						каталог.
					</p>

					<p>
						Здесь также можете воспользоваться фильтрами, а также
						отредактировать карточку товара под свой интернет-магазин.
					</p>

					<div className="instruction-image">
						<img src={img2} alt="img1" />
					</div>

					<p>
						Полную информацию о товаре (фотографии, описание, опции) возможно
						увидеть, нажав на товар.
					</p>

					<div className="instruction-image">
						<img src={img3} alt="img1" />
					</div>
				</Card>
			</div>
		);
	}
}

export default InstructionsForSellers;
