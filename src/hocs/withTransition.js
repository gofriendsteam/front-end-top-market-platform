import React, { useState, useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';

const withTransition = Component => props => {
	const [expand, setTransitonState] = useState(false);
	useEffect(() => setTransitonState(true), [])
	return (
		<CSSTransition


			in={expand}
			timeout={1200}
			classNames="route-transition"
		>
			<Component {...props} />
		</CSSTransition>
	);
};

export default withTransition;
