import styled from 'styled-components';
// import { palette } from 'styled-theme';
// import { transition, borderRadius } from '../helpers/style_utils';
// import WithDirection from '../helpers/rtl';

const UsersideLayout = styled.div`
	height: 100vh;
	input,
	h3,
	h2,
	label,
	a,
	p,
	div,
	button {
		font-family: 'Roboto', sans-serif;
		text-shadow: rgba(0, 0, 0, 0.004) 1px 1px 1px;
	}
	a {
		color: rgb(89, 89, 89);
	}

	.auth-layout {
		height: 100vh;
		position: relative;
		background: url('./loginbgimage.jpg') center center / cover no-repeat;
		&::before {
			content: '';
			width: 100%;
			height: 100%;
			display: flex;
			background-color: rgba(0, 0, 0, 0.6);
			position: absolute;
			z-index: 1;
			top: 0px;
			left: 0px;
			right: inherit;
		}
	}
	button {
		margin: 0;
	}
	.title {
		font-size: 24px;
		font-weight: 300;
		line-height: 1;
		text-align: center;
		margin-bottom: 50px;
		text-transform: uppercase;
		color: rgb(120, 129, 149);
	}
	.form {
		position: absolute;
		top: 50%;
		left: 50%;

		transform: translate(-50%, -55%);
		//margin: 100px auto 0 auto;
		padding: 70px 20px 40px 20px;
		width: 600px;
		z-index: 2;
		@media only screen and (max-width: 640px) {
			width: 97%;
			text-align: center;
			button {
				width: 100%;
				margin: 0 auto !important;
			}
			.description {
				width: 100%;
			}
			.checkbox-group {
				text-align: left;
			}
		}
		background: #fff;
		p {
			font-size: 12px;
			font-weight: 400;
			line-height: 1.2;
			text-align: center;
			color: rgb(193, 193, 193);
		}
	}
	.checkbox-group {
		display: flex;
		flex-direction: column;
		padding-left: 60px;
		margin-bottom: 50px;

		label {
			margin-bottom: 20px;
		}
	}

	.description {
		width: 87%;
		margin: 0 auto 50px auto;
		p {
			margin-bottom: 5px;
			text-align: left;
			font-size: 13px;
		}
		span {
			color: #323232;
			font-weight: 500;
		}
	}
	.tutorial-begin-wrapper {
		.radio-wrapper {
			margin: 0 auto;
			width: 70%;
			.radio-item {
				margin-bottom: 20px;
			}
		}
	}

	.step-btn {
		margin: 50px auto 0 auto;
		width: 261px;
		display: inherit;
	}
	.credential-link {
		color: #86c1f7;
	}
	.credential-link:hover {
		text-decoration: underline;
	}
	.user-side-topbtn {
		margin: 0;
		width: 200px;
	}
`;

export default UsersideLayout;
