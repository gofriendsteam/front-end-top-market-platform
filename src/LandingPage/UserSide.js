import React from 'react';
import { Layout } from 'antd';
import UsersideLayout from '../styles/UserSideStyles';

const UserSide = ({ children }) => {
	return (
		<UsersideLayout>
			<Layout className="auth-layout">{children}</Layout>
		</UsersideLayout>
	);
};

export default UserSide;
